<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class PunchItemFiles extends Model
{
    protected $table = 'punch_item_files';

    protected static function boot()
    {
        parent::boot();
    }
}
