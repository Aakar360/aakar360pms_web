<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTendersCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenders_category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tender_id')->unsigned();
            $table->text('category')->nullable();
            $table->text('level')->nullable();
            $table->text('parent')->nullable();
            $table->text('inc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenders_category');
    }
}
