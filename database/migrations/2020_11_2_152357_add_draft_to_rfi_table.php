<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDraftToRfiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rfi', function (Blueprint $table) {
            $table->string('draft')->nullable()->after('rfi_manager');
            $table->string('schimpact')->nullable()->after('status');
            $table->string('schimpact_days')->nullable()->after('schimpact');
            $table->string('costimpact')->nullable()->after('status');
            $table->string('costimpact_days')->nullable()->after('costimpact');
            $table->string('projectid')->nullable()->after('costimpact_days');
            $table->string('titleid')->nullable()->after('projectid');
            $table->string('costitemid')->nullable()->after('titleid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rfi', function (Blueprint $table) {
            $table->dropIfExists('draft');
            $table->dropIfExists('schimpact');
            $table->dropIfExists('schimpact_days');
            $table->dropIfExists('costimpact');
            $table->dropIfExists('costimpact_days');
            $table->dropIfExists('projectid');
            $table->dropIfExists('titleid');
            $table->dropIfExists('costitemid');
        });
    }
}
