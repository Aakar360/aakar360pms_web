<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class CostItemDescription extends Model
{
    protected $table = 'cost_item_description';

    protected static function boot()
    {
        parent::boot();
    }
}
