<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveAssigned extends Model
{
    protected $fillable = ['user_id','leave_rule_id','effective_date'];
}
