<?php

namespace App\Http\Controllers\Client;

use App\Helper\Files;
use App\Helper\Reply;
use App\Http\Requests\Tasks\StoreTask;
use App\Notifications\NewClientTask;
use App\Notifications\NewTask;
use App\Notifications\TaskCompleted;
use App\Notifications\TaskReminder;
use App\Notifications\TaskUpdated;
use App\Notifications\TaskUpdatedClient;
use App\Project;
use App\Projectclient;
use App\PunchItem;
use App\PunchItemFiles;
use App\PunchItemReply;
use App\Rfi;
use App\RfiFile;
use App\RfiReply;
use App\Task;
use App\TaskboardColumn;
use App\TaskCategory;
use App\TaskFile;
use App\Traits\ProjectProgress;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use App\EmployeeDocs;
use App\Http\Requests\EmployeeDocs\CreateRequest;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ManageRfiController extends ClientBaseController
{
    use ProjectProgress;

    private $mimeType = [
        'txt' => 'fa-file-text',
        'htm' => 'fa-file-code-o',
        'html' => 'fa-file-code-o',
        'php' => 'fa-file-code-o',
        'css' => 'fa-file-code-o',
        'js' => 'fa-file-code-o',
        'json' => 'fa-file-code-o',
        'xml' => 'fa-file-code-o',
        'swf' => 'fa-file-o',
        'flv' => 'fa-file-video-o',

        // images
        'png' => 'fa-file-image-o',
        'jpe' => 'fa-file-image-o',
        'jpeg' => 'fa-file-image-o',
        'jpg' => 'fa-file-image-o',
        'gif' => 'fa-file-image-o',
        'bmp' => 'fa-file-image-o',
        'ico' => 'fa-file-image-o',
        'tiff' => 'fa-file-image-o',
        'tif' => 'fa-file-image-o',
        'svg' => 'fa-file-image-o',
        'svgz' => 'fa-file-image-o',

        // archives
        'zip' => 'fa-file-o',
        'rar' => 'fa-file-o',
        'exe' => 'fa-file-o',
        'msi' => 'fa-file-o',
        'cab' => 'fa-file-o',

        // audio/video
        'mp3' => 'fa-file-audio-o',
        'qt' => 'fa-file-video-o',
        'mov' => 'fa-file-video-o',
        'mp4' => 'fa-file-video-o',
        'mkv' => 'fa-file-video-o',
        'avi' => 'fa-file-video-o',
        'wmv' => 'fa-file-video-o',
        'mpg' => 'fa-file-video-o',
        'mp2' => 'fa-file-video-o',
        'mpeg' => 'fa-file-video-o',
        'mpe' => 'fa-file-video-o',
        'mpv' => 'fa-file-video-o',
        '3gp' => 'fa-file-video-o',
        'm4v' => 'fa-file-video-o',

        // adobe
        'pdf' => 'fa-file-pdf-o',
        'psd' => 'fa-file-image-o',
        'ai' => 'fa-file-o',
        'eps' => 'fa-file-o',
        'ps' => 'fa-file-o',

        // ms office
        'doc' => 'fa-file-text',
        'rtf' => 'fa-file-text',
        'xls' => 'fa-file-excel-o',
        'ppt' => 'fa-file-powerpoint-o',
        'docx' => 'fa-file-text',
        'xlsx' => 'fa-file-excel-o',
        'pptx' => 'fa-file-powerpoint-o',


        // open office
        'odt' => 'fa-file-text',
        'ods' => 'fa-file-text',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'RFI';
        $this->pageIcon = 'ti-layout-list-thumb';
        $this->middleware(function ($request, $next) {
            if (!in_array('tasks', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    public function index()
    {
        $this->projects = Project::all();
        $this->clients = User::allClients();
        $this->employees = User::allEmployees();
        $this->taskBoardStatus = TaskboardColumn::all();
        $this->items = Rfi::get();
        return view('client.rfi.index', $this->data);
    }

    public function data(Request $request, $startDate = null, $endDate = null, $hideCompleted = null, $projectId = null)
    {
//        $taskBoardColumn = TaskboardColumn::where('slug', 'incomplete')->first();

        $tasks = PunchItem::join('users', 'users.id', '=', 'punch_item.user_id')
            ->select('punch_item.*', 'users.name', 'users.image');

//        $tasks->where(function ($q) use ($startDate, $endDate) {
//            $q->whereBetween(DB::raw('DATE(tasks.`due_date`)'), [$startDate, $endDate]);
//
//            $q->orWhereBetween(DB::raw('DATE(tasks.`start_date`)'), [$startDate, $endDate]);
//        });

//        if ($projectId != 0 && $projectId !=  null && $projectId !=  'all') {
//            $tasks->where('tasks.project_id', '=', $projectId);
//        }

//        if ($request->clientID != '' && $request->clientID !=  null && $request->clientID !=  'all') {
//            $tasks->where('projects.client_id', '=', $request->clientID);
//        }

//        if ($request->assignedTo != '' && $request->assignedTo !=  null && $request->assignedTo !=  'all') {
//            $tasks->where('tasks.user_id', '=', $request->assignedTo);
//        }

//        if ($request->assignedBY != '' && $request->assignedBY !=  null && $request->assignedBY !=  'all') {
//            $tasks->where('creator_user.id', '=', $request->assignedBY);
//        }

//        if ($request->status != '' && $request->status !=  null && $request->status !=  'all') {
//            $tasks->where('tasks.board_column_id', '=', $request->status);
//        }

//        if ($hideCompleted == '1') {
//            $tasks->where('tasks.board_column_id', $taskBoardColumn->id);
//        }

        $tasks->get();

        return DataTables::of($tasks)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $action = '<a href="' . route('admin.punch-items.edit', $row->id) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>';

//                $recurringTaskCount = Task::where('recurring_task_id', $row->id)->count();
//                $recurringTask = $recurringTaskCount > 0 ? 'yes' : 'no';

//                $action .= '&nbsp;&nbsp;<a href="javascript:;" class="btn btn-danger btn-circle sa-params"
//                      data-toggle="tooltip" data-task-id="' . $row->id . '" data-recurring="' . $recurringTask . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';

                return $action;
            })
            ->editColumn('due_date', function ($row) {
                if ($row->due_date->isPast()) {
                    return '<span class="text-danger">' . $row->due_date->format($this->global->date_format) . '</span>';
                }
                return '<span class="text-success">' . $row->due_date->format($this->global->date_format) . '</span>';
            })
            ->editColumn('name', function ($row) {
                return ($row->image) ? '<img src="' . asset('user-uploads/avatar/' . $row->image) . '"
                                                            alt="user" class="img-circle" width="30"> ' . ucwords($row->name) : '<img src="' . asset('default-profile-2.png') . '"
                                                            alt="user" class="img-circle" width="30"> ' . ucwords($row->name);
            })
            ->editColumn('clientName', function ($row) {
                return ($row->clientName) ? ucwords($row->clientName) : '-';
            })
            ->editColumn('created_by', function ($row) {
                if (!is_null($row->created_by)) {
                    return ($row->created_image) ? '<img src="' . asset('user-uploads/avatar/' . $row->created_image) . '"
                                                            alt="user" class="img-circle" width="30"> ' . ucwords($row->created_by) : '<img src="' . asset('default-profile-2.png') . '"
                                                            alt="user" class="img-circle" width="30"> ' . ucwords($row->created_by);
                }
                return '-';
            })
            ->editColumn('heading', function ($row) {
                return '<a href="javascript:;" data-task-id="' . $row->id . '" class="show-task-detail">' . ucfirst($row->heading) . '</a>';
            })
            ->editColumn('column_name', function ($row) {
                return '<label class="label" style="background-color: ' . $row->label_color . '">' . $row->column_name . '</label>';
            })
            ->editColumn('project_name', function ($row) {
                if (is_null($row->project_id)) {
                    return "";
                }
                return '<a href="' . route('admin.projects.show', $row->project_id) . '">' . ucfirst($row->project_name) . '</a>';
            })
            ->rawColumns(['column_name', 'action', 'project_name', 'clientName', 'due_date', 'name', 'created_by', 'heading'])
            ->removeColumn('project_id')
            ->removeColumn('image')
            ->removeColumn('created_image')
            ->removeColumn('label_color')
            ->make(true);
    }

    public function editRfi($id)
    {
        $this->punchitem = Rfi::findOrFail($id);
        $this->employees = User::allEmployees();
        $this->files = RfiFile::where('rfi_id',$id)->get();
        return view('client.rfi.edit', $this->data);
    }

    public function updateRfi(Request $request, $id)
    {
        $task = Rfi::findOrFail($id);
        $task->title = $request->title;
        $task->subject = $request->subject;
        $task->rfi_manager = $request->rfi_manager;
        if($request->assign_to != '') {
            $task->assign_to = implode(',', $request->assign_to);
        }
        $task->due_date = Carbon::createFromFormat($this->global->date_format, $request->due_date, $this->global->timezone)->format('Y-m-d');
        $task->status = $request->status;

        $task->rec_contractor = $request->rec_contractor;
        $task->received_from = $request->received_from;
        if($request->distribution != '') {
            $task->distribution = implode(',', $request->distribution);
        }
        $task->location = $request->location;
        $task->drawing_no = $request->drawing_no;
        $task->spec_section = $request->spec_section;
        $task->question = $request->question;
        if ($request->private != '') {
            $task->private = $request->private;
        }
        if ($request->reference != '') {
            $task->reference = $request->reference;
        }
        $task->added_by = $this->user->id;

        $task->save();
        return Reply::dataOnly(['taskID' => $task->id]);
        //        return Reply::redirect(route('admin.all-tasks.index'), __('messages.taskUpdatedSuccessfully'));
    }

    public function destroy(Request $request, $id)
    {
        $task = PunchItem::findOrFail($id);

        // If it is recurring and allowed by user to delete all its recurring tasks
//        if ($request->has('recurring') && $request->recurring == 'yes') {
//            Task::where('recurring_task_id', $id)->delete();
//        }

        $taskFiles = PunchItemFiles::where('task_id', $id)->get();

        foreach ($taskFiles as $file) {
            Files::deleteFile($file->hashname, 'task-files/' . $file->task_id);
            $file->delete();
        }

        PunchItem::destroy($id);
        //calculate project progress if enabled

        return Reply::success(__('Punch item deleted successfully'));
    }

    public function createRfi()
    {
        $this->employees = User::allEmployees();
        return view('client.rfi.create', $this->data);
    }

    public function clientsList($projectId)
    {
        $this->clients = Projectclient::byProject($projectId);
        $list = view('client.punch-items.clients-list', $this->data)->render();
        return Reply::dataOnly(['html' => $list]);
    }

    public function dependentTaskLists($projectId, $taskId = null)
    {
        $completedTaskColumn = TaskboardColumn::where('slug', '!=', 'completed')->first();
        if($completedTaskColumn)
        {
            $this->allTasks = Task::where('board_column_id', $completedTaskColumn->id)
                ->where('project_id', $projectId);
            if($taskId != null)
            {
                $this->allTasks = $this->allTasks->where('id', '!=', $taskId);
            }
            $this->allTasks = $this->allTasks->get();
        }else {
            $this->allTasks = [];
        }
        $list = view('client.punch-items.dependent-task-list', $this->data)->render();
        return Reply::dataOnly(['html' => $list]);
    }

    public function storeRfi(Request $request)
    {
        $task = new Rfi();
        $task->title = $request->title;
        $task->subject = $request->subject;
        $task->rfi_manager = $request->rfi_manager;
        if($request->assign_to != '') {
            $task->assign_to = implode(',', $request->assign_to);
        }
        $task->due_date = Carbon::createFromFormat($this->global->date_format, $request->due_date, $this->global->timezone)->format('Y-m-d');
        $task->status = $request->status;
        $task->rec_contractor = $request->rec_contractor;
        $task->received_from = $request->received_from;
        if($request->distribution != '') {
            $task->distribution = implode(',', $request->distribution);
        }
        $task->location = $request->location;
        $task->drawing_no = $request->drawing_no;
        $task->spec_section = $request->spec_section;
        $task->question = $request->question;
        if ($request->private != '') {
            $task->private = $request->private;
        }
        if ($request->reference != '') {
            $task->reference = $request->reference;
        }
        $task->added_by = $this->user->id;
        $task->save();

        return Reply::dataOnly(['taskID' => $task->id]);
    }

    public function ajaxCreate($columnId)
    {
        $this->projects = Project::all();
        $this->columnId = $columnId;
        $this->employees = User::allEmployees();
        $completedTaskColumn = TaskboardColumn::where('slug', '!=', 'completed')->first();
        if($completedTaskColumn)
        {
            $this->allTasks = Task::where('board_column_id', $completedTaskColumn->id)->get();
        }else {
            $this->allTasks = [];
        }
        return view('client.punch-items.ajax_create', $this->data);
    }

    public function remindForTask($taskID)
    {
        $task = Task::with('user')->findOrFail($taskID);
        $notifyUser = $task->user;
        $notifyUser->notify(new TaskReminder($task));
        return Reply::success('messages.reminderMailSuccess');
    }

    public function show($id)
    {
        $this->task = Task::with('board_column')->findOrFail($id);
        $view = view('client.punch-items.show', $this->data)->render();
        return Reply::dataOnly(['status' => 'success', 'view' => $view]);
    }

    public function showFiles($id)
    {
        $this->taskFiles = TaskFile::where('task_id', $id)->get();
        return view('client.punch-items.ajax-file-list', $this->data);
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param $projectId
     * @param $hideCompleted
     */
    public function export($startDate, $endDate, $projectId, $hideCompleted)
    {
        $tasks = PunchItem::join('users', 'users.id', '=', 'punch_item.assign_to')
            ->select('punch_item.*', 'users.name', 'users.image');

//        $tasks->where(function ($q) use ($startDate, $endDate) {
//            $q->whereBetween(DB::raw('DATE(tasks.`due_date`)'), [$startDate, $endDate]);
//
//            $q->orWhereBetween(DB::raw('DATE(tasks.`start_date`)'), [$startDate, $endDate]);
//        });
//
//        if ($projectId != 0) {
//            $tasks->where('tasks.project_id', '=', $projectId);
//        }
//
//        if ($hideCompleted == '1') {
//            $tasks->where('tasks.status', '=', 'incomplete');
//        }

        $attributes =  ['image', 'due_date'];

        $tasks = $tasks->get()->makeHidden($attributes);

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Project', 'Title', 'Assigned To', 'Status', 'Due Date'];

        // Convert each client of the returned collection into an array,
        // and append it to the payments array.
        foreach ($tasks as $row) {
            $exportArray[] = $row->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('task', function ($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Task');
            $excel->setCreator('Aakar360')->setCompany($this->companyName);
            $excel->setDescription('task file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));
                });
            });
        })->download('xlsx');
    }

    public function storeImage(Request $request)
    {
        if ($request->hasFile('file')) {
            foreach ($request->file as $fileData){
                $storage = config('filesystems.default');
                $rfi = Rfi::where('id',$request->task_id)->first();
                $file = new RfiFile();
                $file->user_id = $this->user->id;
                $file->rfi_id = $request->task_id;
                switch($storage) {
                    case 'local':
                        $fileData->storeAs('user-uploads/task-files/'.$rfi->title, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs('task-files/'.$rfi->title, $fileData, $fileData->getClientOriginalName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'task-files')
                            ->first();

                        if(!$dir) {
                            Storage::cloud()->makeDirectory('task-files');
                        }

                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->task_id)
                            ->first();

                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/'.$rfi->title);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->task_id)
                                ->first();
                        }

                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->getClientOriginalName());

                        $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->getClientOriginalName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('task-files/'.$rfi->title.'/', $fileData, $fileData->getClientOriginalName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/task-files/'.$request->task_id.'/'.$fileData->getClientOriginalName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }

                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
//                $this->logProjectActivity($request->task_id, __('messages.newFileUploadedToTheProject'));
            }

        }
        return Reply::redirect(route('client.rfi.index'), __('modules.projects.projectUpdated'));
    }

    public function destroyImage(Request $request, $id)
    {
        $file = Rfi::findOrFail($id);

        Files::deleteFile($file->hashname,'task-files/'.$file->rfi_id);

        Rfi::destroy($id);

        $this->taskFiles = Rfi::where('rfi_id', $file->task_id)->get();

        $view = view('client.rfi.index', $this->data)->render();

        return Reply::successWithData(__('messages.fileDeleted'), ['html' => $view, 'totalFiles' => sizeof($this->taskFiles)]);
    }

    public function removeFile($id){
        $taskFiles = PunchItemFiles::findOrFail($id);

            Files::deleteFile($taskFiles->hashname, 'task-files/' . $taskFiles->task_id);
        $taskFiles->delete();

        return Reply::success(__('image deleted successfully'));
    }

    public function reply($id)
    {
        $this->punchitem = PunchItem::findOrFail($id);
        $this->employees = User::allEmployees();
        $this->files = PunchItemFiles::where('task_id',$id)->get();
        $this->replies = PunchItemReply::where('punch_item_id',$id)->get();
        return view('client.punch-items.reply', $this->data);
    }

    public function comment($id)
    {
        $this->punchitem = PunchItem::findOrFail($id);
        $this->employees = User::allEmployees();
        $this->files = PunchItemFiles::where('task_id',$id)->get();
        $this->replies = PunchItemReply::where('punch_item_id',$id)->get();
        return view('client.punch-items.comment', $this->data);
    }

    public function detailsRfi($id)
    {
        $this->punchitem = Rfi::findOrFail($id);
        $this->employees = User::allEmployees();
        $this->files = RfiFile::where('rfi_id',$id)->get();
        $this->replies = RfiReply::where('rfi_id',$id)->get();
        return view('client.rfi.details', $this->data);
    }

    public function replyPost(Request $request, $id)
    {
        if($request->status) {
            $task = Rfi::findOrFail($id);
            $task->status = $request->status;
            $task->save();
        }

        $pi = new RfiReply();
        $pi->comment = $request->comment;
        $pi->rfi_id = $id;
        $pi->added_by = $this->user->id;

        $pi->save();

        return Reply::success(__('Comment added successfully'));
    }

    public function commentPost(Request $request, $id)
    {
        $pi = new PunchItemReply();
        $pi->comment = $request->comment;
        $pi->punch_item_id = $id;
        $pi->added_by = $this->user->id;

        $pi->save();

        return Reply::success(__('Comment added successfully'));
    }
}
