<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class MeetingBusinessItem extends Model
{
    protected $table = 'meeting_business_items';

    protected static function boot()
    {
        parent::boot();
    }
}
