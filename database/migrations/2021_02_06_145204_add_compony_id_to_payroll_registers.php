<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddComponyIdToPayrollRegisters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payroll_registers', function (Blueprint $table) {
            $table->integer('company_id')->after('user_id');
            $table->integer('month')->after('company_id');
            $table->integer('year')->after('month');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payroll_registers', function (Blueprint $table) {
            //
        });
    }
}
