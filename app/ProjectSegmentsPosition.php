<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ProjectSegmentsPosition extends Model
{
    protected $table = 'project_segments_position';

    protected static function boot()
    {
        parent::boot();

    }
}
