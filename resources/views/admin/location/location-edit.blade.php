<div class="modal-header">
    <div class="portlet-body">
    <button type="button" class="close" data-dismiss="modal">
        <span aria-hidden="true">&times;</span>
        <span class="sr-only">Close</span>
    </button>
    <h4 class="modal-title" id="myModalLabel">Edit Location</h4>

{!! Form::open(['id'=>'editLocation','class'=>'ajax-form','method'=>'POST']) !!}
<!-- Modal Body -->
<div class="modal-body">
    <p class="statusMsg"></p>

    <div class="form-group">
        <label for="inputName">Project Name</label>
        <input type="text" class="form-control" id="inputName" name="buildingName" value="{{$location_selected->location_name}}" placeholder="Enter  Name"/>
    </div>
    <div class="form-group">
        <label for="inputName">Description	</label>
        <input type="text" class="form-control" id="inputName" name="description" value="{{$location_selected->description}}"  placeholder="Enter Description"/>
    </div>
    <div class="form-group">
        <label for="inputName">Local Address</label>
        <input type="text" class="form-control" id="inputName" name="localAddress" value="{{$location_selected->local_address}}"   placeholder="Enter Local Address"/>
    </div>
    <div class="form-group">
        <label for="inputName">State {{$location_selected->state_id}}</label>
        <select class="form-control"  name="state_id"   onchange="getCity(this.value);"  >

            <?php  ?>
            @foreach($state as $states)
                <?php
                $selected = '';
                if($location_selected->state_id !== NULL){
                    if($location_selected->state_id == $states->id){
                        $selected = 'selected';
                    } }?>
                <option  value="{{$states->id}}" <?=$selected; ?>>{{$states->name}}</option>

            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="inputName">City</label>
        <select class="form-control" name="city_id" id="cityedit"   >

            <?php  ?>
            @foreach($city as $cities)

                <?php

                $selected = '';
                if($cities->id !== NULL){
                if($location_selected->city_id == $cities->id){
                $selected = 'selected';
                } }?>
                <option  value="{{$cities->id}}" <?=$selected; ?>>{{$cities->name}}</option>

            @endforeach

        </select>
    </div>
    <div class="form-actions">
        <button type="button" id="save-location" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
    </div>
    {!! Form::close() !!}
</div>
</div>
</div>

<script>

    $('#save-location').click(function () {

        $.easyAjax({
            url: '{{route('admin.location.location-update', [$location_selected->id])}}',
            container: '#editSubLocation',
            type: "POST",
            data: $('#editLocation').serialize(),
            success: function (response) {
                console.log(response);
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });


    function getCity($state_id) {

        $('#cityedit').html('');
        var url = "{{ route('admin.location.get-city',':id') }}";
        url = url.replace(':id', $state_id);

        var token = "{{ csrf_token() }}";

        $.easyAjax({
            type: 'POST',
            url: url,
            data: {'_token': token,'state_id':$state_id},
            success: function (response) {
                if (response.status == "success") {
                    $.unblockUI();
                    var options = [];
                    var rData = [];
                    rData = response.city;
                    $.each(rData, function( index, value ) {
                        //  alert(value.id);
                        var selectData = '';
                        selectData = '<option value="'+index+'">'+value+'</option>';
                        options.push(selectData);
                    });

                    $('#cityedit').html(options);
                    $('#cityedit').selectpicker('refresh');
                }
            }
        });


    }
</script>

