@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.tenders.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">Bidding</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

@endpush
@section('content')

    <div class="container-fluid">
        <div class="row ">
            <div class="panel panel-inverse form-shadow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-12">
                           Awarded contract information
                        </div>
                    </div>
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table-striped" style="width: 100%;">
                            <tr>
                                <td style="width: 15%;"><b>Tender</b></td>
                                <td  style="width: 35%;">{{ $awardedcontract->title }}</td>
                            </tr>
                            <tr>
                                <td style="width: 15%;"><b>@lang('app.number')</b></td>
                                <td  style="width: 35%;">{{ $awardedcontract->number }}</td>
                                <td style="width: 15%;"><b>@lang('app.status')</b></td>
                                <td  style="width: 35%;">{{ $awardedcontract->status }}</td>
                                <td style="width: 15%;"><b>@lang('app.contractor')</b></td>
                                <td  style="width: 35%;">{{ $awardedcontract->contractor_id ? get_user_name($awardedcontract->contractor_id) : '' }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-sm-4 col-xs-4">
                                <div class="form-group">
                                    <label for="project_id"><b>@lang('app.projects')</b></label>
                                    <select class="select2 form-control projectid" name="project_id" data-style="form-control" required>
                                        <option value="">Select @lang('app.projects')</option>
                                        @foreach($projectlist as $project)
                                            <option value="{{ $project->id }}" <?php if($awardedcontract->project_id==$project->id){ echo 'selected';}?>>{{ ucwords($project->project_name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-4">
                                <div class="form-group">
                                    <label for="title_id">@lang('app.subprojects')</label>
                                    <select class="select2 form-control titlelist" id="titlelist" name="title_id" data-style="form-control" required>
                                        <option value="">Select @lang('app.subprojects')</option>
                                        @if($titlelist)
                                            @foreach($titlelist as $title)
                                                <option value="{{ $title->id }}" <?php if($awardedcontract->subproject_id==$title->id){ echo 'selected';}?>>{{ ucwords($title->title) }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-4 segmentblock" @if(count($segmentlist)>0) style="display: none;" @endif>
                                <div class="form-group">
                                    <label for="title_id">@lang('app.segment')</label>
                                    <select class="select2 form-control segmentslist" id="segmentslist" name="segment_id" data-style="form-control" required>
                                        <option value="">Select @lang('app.segment')</option>
                                        @if(count($segmentlist)>0)
                                            @foreach($segmentlist as $segment)
                                                <option value="{{ $segment->id }}" <?php if($awardedcontract->segment_id==$segment->id){ echo 'selected';}?>>{{ ucwords($segment->name) }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <div class="combo-sheet">
                                    <div class="table-responsive">
                                        <table id="boqtable" class="table table-bordered default footable-loaded footable boqtable" data-resizable-columns-id="users-table">
                                            <thead>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>@lang('app.task')</td>
                                                <td>Quantity</td>
                                                <td>Unit</td>
                                                <td>Price</td>
                                                <td>Total Amount</td>
                                            </tr>
                                            </thead>
                                            <tbody class="colposition">
                                            <?php
                                                    function costitemsboq($boqarray){
                                                        $awardedcontract = $boqarray['awardedcontract'];
                                                        $level = $boqarray['level'];
                                                        $catitem = $boqarray['catitem'];

                                            $level1categoryarray =  \App\AwardContractCategory::where('awarded_contract_id',$awardedcontract->id)->where('level',$level)->orderBy('inc','asc')->get();
                                            if($level1categoryarray){
                                            foreach ($level1categoryarray as $level1category){
                                                if(empty($catitem)){
                                                    $catitem = $level1category->category;
                                                }else{
                                                    $catitem .= ','.$level1category->category;
                                                }
                                            ?>
                                            <tr >
                                                <td><a href="javascript:void(0);"  class="red remove-item-category" data-catitem="{{ $level1category->category }}" data-catrow="{{ $catitem }}" data-toggle="tooltip"  data-original-title="Delete Category" ><i class="fa fa-trash"></i></a></td>
                                                <td><i class="fa fa-plus-circle"></i></td>
                                                <td></td>
                                                <td>{{ get_category($level1category->category) }}</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <?php  $tenderproductsarray  = \App\AwardContractProducts::where('awarded_contract_id',$awardedcontract->id)->where('category',$catitem)->orderBy('inc','asc')->get();
                                            foreach ($tenderproductsarray as $tenderproducts){
                                            ?>
                                            <tr  id="rowitem{{ $tenderproducts->id }}">
                                                <td><a href="javascript:void(0);"  class="red remove-item" data-rowid="{{ $tenderproducts->id }}" data-toggle="tooltip"  data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>
                                                <td><i class="fa fa-plus-circle"></i></td>
                                                <td></td>
                                                <td><input  class="cell-inp" type="text"   readonly value="{{ get_cost_name($tenderproducts->cost_items) }}"> </td>
                                                <td><input type="text" class="cell-inp"  readonly name="qty[{{ $tenderproducts->id }}]" value="{{ $tenderproducts->qty }}"></td>
                                                <td><input type="text" class="cell-inp"   readonly value="{{ get_unit_name($tenderproducts->unit) }}"></td>
                                                <td><input type="text" class="cell-inp" readonly value="{{ $tenderproducts->rate }}"></td>
                                                <td><input type="text" class="cell-inp" readonly  value="{{ $tenderproducts->finalrate }}"></td>

                                            </tr>
                                            <?php  } ?>

                                            <tr >
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><strong>Sub total</strong></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <?php  $tenderproductsarray  = \App\AwardContractProducts::where('awarded_contract_id',$awardedcontract->id)->where('category',$catitem)->sum('finalrate');
                                                    echo $tenderproductsarray;
                                                    ?>
                                                </td>
                                            </tr>
                                           <?php $boqarray = array();
                                            $boqarray['awardedcontract'] = $awardedcontract;
                                            $boqarray['level'] = $level+1;
                                            $boqarray['catitem'] = $catitem;
                                            echo costitemsboq($boqarray); ?>

                                                <?php    }?>

                                            <?php  } }    ?>
                                            <?php
                                            $boqarray = array();
                                            $boqarray['awardedcontract'] = $awardedcontract;
                                            $boqarray['level'] = 1;
                                            $boqarray['catitem'] = '';
                                            echo costitemsboq($boqarray);
                                            ?>

                                            <tr >
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><strong>Grand total</strong></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <?php  $tenderproductsarray  = \App\AwardContractProducts::where('awarded_contract_id',$awardedcontract->id)->sum('finalrate');
                                                    echo $tenderproductsarray;
                                                    ?>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="projectID" id="projectID">
                        </div>
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')

    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script src="{{ asset('js/jquery.dragtable.js') }}"></script>
    <script src="{{ asset('js/jquery.resizableColumns.min.js') }}"></script>
    <script>

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('.datepicker').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.tenders.tenderSellerSubmit')}}',
                container: '#createProjectCategory',
                type: "POST",
                data: $('#createProjectCategory').serialize(),
                success: function (response) {
                     // silence
                }
            })
        });
    </script>
@endpush

