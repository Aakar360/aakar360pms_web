<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ProjectPermission extends Model
{
    protected $table = 'project_member_permissions';

    protected static function boot()
    {
        parent::boot();
    }
}
