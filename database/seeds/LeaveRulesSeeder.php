<?php

use Illuminate\Database\Seeder;

class LeaveRulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $rules = new \App\LeavesRules();
        $rules->leave_rule_name = 'Loss Of Pay';
        $rules->save();
    }
}
