<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRfitypeToRfiFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rfi_files', function (Blueprint $table) {
            $table->string('rfitype')->nullable()->after('rfi_id');
            $table->string('reply_id')->nullable()->after('rfitype');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rfi_files', function (Blueprint $table) {
            $table->dropIfExists('rfitype');
            $table->dropIfExists('reply_id');
        });
    }
}
