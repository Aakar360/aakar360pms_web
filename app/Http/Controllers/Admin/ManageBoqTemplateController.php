<?php

namespace App\Http\Controllers\Admin;

use App\BoqCategory;
use App\BoqTemplatePosition;
use App\Contractors;
use App\CostItems;
use App\Helper\Reply;
use App\AssetCategory;
use App\Asset;
use App\Project;
use App\ProjectCategory;
use App\BoqTemplateProduct;
use App\ProjectMember;
use App\SubAsset;
use App\Boqtemplate;
use App\Task;
use App\Title;
use App\Type;
use App\Units;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class ManageBoqTemplateController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Boq template';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'pms';
    } 
    public function index()
    {
        $this->boqtemplatearray = Boqtemplate::all();
        $this->projectcategoryarray = ProjectCategory::all();
        return view('admin.boqtemplate.index', $this->data);
    }
    public function data(Request $request)
    {
        $user = Auth::user();
        $category = $request->category;
        $inspectionarray = Boqtemplate::query();
        if(!empty($category)){
            $inspectionarray = Boqtemplate::where('category',$category);
        }
        return DataTables::of($inspectionarray)
            ->addIndexColumn()
            ->addColumn('action', function ($row) use ($user) {
                $actionlink = '';
                $actionlink .= '<a type="button" class="btn btn-default editBoqtemplate" data-boqtemplate-id="'.$row->id.'"  href="javascript:;" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                if($row->user_id==$user->id){
                    $actionlink .='<a href="javascript:;" data-boqtemplate-id="'.$row->id.'" class="btn btn-sm btn-danger btn-circle sa-params"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                }
                return $actionlink;
            })->addColumn('boqlist', function ($row) use ($user) {
                $actionlink = '';
                $actionlink .= '<a class="btn btn-default" href="'.route('admin.boq-template.addboq',[$row->id]).'" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-list" aria-hidden="true"></i></a>';
                return $actionlink;
            })
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->editColumn(
                'description',
                function ($row) {
                    return substr(strip_tags($row->description),0,50);
                }
            )
            ->editColumn(
                'category',
                function ($row) {
                    return get_project_category_name($row->category);
                }
            )
            ->editColumn(
                'added_by',
                function ($row) {
                    return  '<div class="row"><div class="col-sm-3 col-xs-4">'.get_users_images($row->user_id).'</div><div class="col-sm-9 col-xs-8"><a href="' . route('admin.employees.show', $row->user_id) . '">'.ucwords(get_user_name($row->user_id)).'</a></div></div>';
                }
            )
            ->rawColumns([ 'boqlist', 'action','type', 'description', 'added_by'])
            ->make(true);
    }

    public function store(Request $request)
    {
        $user = $this->user;
        $boqtemplate = new Boqtemplate();
        $boqtemplate->user_id = $user->id;
        $boqtemplate->company_id = $user->company_id;
        $boqtemplate->name = $request->name;
        $boqtemplate->category = $request->category;
        $boqtemplate->description = $request->description;
        $boqtemplate->save();
        return Reply::success(__('messages.boqtemplateCreated'));
    }

    public function edit($id)
    {
        $boqtemplate =  Boqtemplate::find($id);
        $this->boqtemplate = $boqtemplate;
        $this->projectcategoryarray = ProjectCategory::all();
        return view('admin.boqtemplate.edit', $this->data);
    }

    public function update(Request $request,$id)
    {
        $user = $this->user;
        $boqtemplate = Boqtemplate::find($id);
        $boqtemplate->user_id = $user->id;
        $boqtemplate->company_id = $user->company_id;
        $boqtemplate->name = $request->name;
        $boqtemplate->category = $request->category;
        $boqtemplate->description = $request->description;
        $boqtemplate->save();

        return Reply::success(__('messages.boqtemplateUpdated'));
    }
    public function destroy($id)
    {
        Boqtemplate::destroy($id);
        $categoryData = Boqtemplate::all();
        return Reply::successWithData(__('messages.boqtemplateDeleted'),['data' => $categoryData]);
    }
    public function addboq(Request $request, $id){

        $user = $this->user;
        $boqtemplate = Boqtemplate::where('id',$id)->first();
        $this->boqtemplate = $boqtemplate;
        $this->userarray = User::all();
        $this->id = (int)$id;
        $columsarray = array(
            'task'=>'Task',
            'description'=>'Description',
            'qty'=>'Qty',
            'rate'=>'Rate',
            'unit'=>'Unit',
            'worktype'=>'Work type',
            'marktype'=>'Markup type',
            'markvalue'=>'Markup value',
            'finalrate'=>'Final rate',
            'totalamount'=>'Total amount'
        );
        $col =1; foreach($columsarray as $colkey => $colums){
            $postitionarray = BoqTemplatePosition::where('template_id',$id)->where('itemslug',$colkey)->where('position','col')->first();
            if(empty($postitionarray->id)){
                $columsarray = new BoqTemplatePosition();
                $columsarray->user_id = $user->id;
                $columsarray->company_id = $user->company_id;
                $columsarray->template_id = $id;
                $columsarray->position = 'col';
                $columsarray->itemid = 0;
                $columsarray->itemname = $colums;
                $columsarray->itemslug = $colkey;
                $columsarray->collock = 0;
                $columsarray->level = 0;
                $columsarray->parent = 0;
                $columsarray->catlevel = '';
                $columsarray->inc = $col;
                $columsarray->save();
                $col++;
            }
        }
        $this->categories = BoqCategory::get();
        $this->unitsarray = Units::get();
        $this->typesarray = Type::get();
        $this->tn = \App\Boqtemplate::where('id',$id)->first();
        return view('admin.boqtemplate.addBoq', $this->data);
    }
    public function addBoqCostItemRow(Request $request){

        if(empty($request->templateid)){
            return array('status'=>false,'message'=>'Please Select Boqtemplates');
        }
        $user = $this->user;
        $maxcostitemid = BoqTemplateProduct::where('template_id',$request->templateid)->max('inc');
        $newid = $maxcostitemid+1;
        $costitemproduct = $request->itemid;
        $costitemslist = \App\BoqTemplateProduct::where('template_id',$request->templateid)->orderBy("id",'asc')->get();
        $unitsarray = \App\Units::orderBy("id",'asc')->get();
        $typesarray = \App\Type::orderBy("id",'asc')->get();
        $costitem = CostItems::find($costitemproduct);
        $proprogetdat = new BoqTemplateProduct();
        $proprogetdat->user_id = $user->id;
        $proprogetdat->company_id = $user->company_id;
        $proprogetdat->template_id = $request->templateid;
        $proprogetdat->category = $request->category;
        $proprogetdat->cost_items_id = $costitem->id;
        $proprogetdat->unit = get_cost_item_unit_name($costitem->cost_items_id);
        $proprogetdat->description = $costitem->description;
        $proprogetdat->rate = $costitem->rate;
        $proprogetdat->qty = $costitem->qty;
        $proprogetdat->inc = $newid;
        $proprogetdat->save();

        $catvalue =  explode(',',$request->category);
        $catitem = !empty($catvalue[1]) ? $catvalue[1] : $catvalue[0];
        $contenthtml = '';
        $colpositionarray = \App\BoqTemplatePosition::where('template_id',$request->templateid)->where('position','col')->orderBy('inc','asc')->get();
        $contenthtml .= '<tr data-depth="2" class="collpse level2 catrow'.$proprogetdat->category.'">
                           <td><a href="javascript:void(0);"  class="red sa-params" data-toggle="tooltip" data-costitem-id="' . $proprogetdat->id . '" data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>';
        if($request->cattype=='single'){
            $contenthtml .= '<td colspan="3" class="text-right"><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-minus-circle"></i> </a></td>';
        }else{
            $contenthtml .= '<td colspan="3" class="text-right"><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-minus-circle"></i> </a></td>';
        }
        foreach ($colpositionarray as $colposition) {
            switch ($colposition->itemslug) {
                case  'task':
                    $contenthtml .= '<td><input data-itemid="' . $proprogetdat->id . '"  class="cell-inp updateproject" data-item="costitem" list="costitem' . $proprogetdat->id . '"  value="' . get_cost_name($proprogetdat->cost_items_id) . '">
                                         <datalist id="costitem' . $proprogetdat->id . '">';
                    foreach ($costitemslist as $costitem) {
                        $contenthtml .= '<option data-value="' . $costitem->id . '" >' . get_cost_name($costitem->cost_items_id) . '</option>';
                    }
                    $contenthtml .= '</datalist>
                                     </td>';
                    break;
                case 'description':
                    $contenthtml .= '<td><textarea data-itemid="' . $proprogetdat->id . '" data-item="description" onkeydown="textAreaAdjust(this)" style="height: 25px;" class="cell-inp updateproject" >' .  $proprogetdat->description . '</textarea></td>';
                    break;
                case 'rate':
                    $contenthtml .= '<td><input data-itemid="' . $proprogetdat->id . '" data-item="rate" type="text" class="cell-inp updateproject ratevalue' . $proprogetdat->id . '"  data-cat="' . $catitem . '" name="rate" value="' .  $proprogetdat->rate . '"  ></td>';
                    break;
                case 'unit':
                    $contenthtml .= '<td>
                               <input  data-itemid="' . $proprogetdat->id . '" data-item="unit" class="cell-inp updateproject" list="unitdata' . $proprogetdat->id . '"  value="' . get_unit_name($proprogetdat->unit) . '">
                                   <datalist id="unitdata' . $proprogetdat->id . '">';
                    foreach ($unitsarray as $units) {
                        $contenthtml .= '<option data-value="' . $units->id . '" >' . $units->name . '</option>';
                    }
                    $contenthtml .= '</datalist>
                                     </td>';
                    break;
                case 'qty':
                    $contenthtml .= '<td><input data-itemid="' . $proprogetdat->id . '" data-item="qty" type="text" class="cell-inp updateproject qty' . $proprogetdat->id . '"   value="' . $proprogetdat->qty. '" ></td>';
                    break;
                case 'worktype':
                    $contenthtml .= '<td> <input data-itemid="' . $proprogetdat->id . '" data-item="worktype" class="cell-inp updateproject" list="type' . $proprogetdat->id . '" >
                                  <datalist id="type' . $proprogetdat->id . '">';
                    foreach ($typesarray as $types) {
                        $contenthtml .= '<option value="' . $types->id . '" >' . ucwords($types->title) . '</option>';
                    }
                    $contenthtml .= '</datalist>
                           </td>';
                    break;
                case 'marktype':
                    $contenthtml .= '<td>
                               <input  data-itemid="' . $proprogetdat->id . '" data-item="marktype" onchange="calmarkup(' . $proprogetdat->id . ')"  class="cell-inp updateproject marktype' . $proprogetdat->id . '" list="marktype' . $proprogetdat->id . '"  >
                               <datalist id="marktype' . $proprogetdat->id . '">
                                   <option value="percent">Percent</option>
                                   <option value="amt">Amount</option>
                               </datalist>
                               </td>';
                    break;
                case 'markvalue':
                    $contenthtml .= '<td><input type="text" data-item="markvalue" data-itemid="' . $proprogetdat->id . '" class="cell-inp updateproject markvalue' . $proprogetdat->id . '" data-cat="' . $catitem . '" name="markvalue" onchange="calmarkup(' . $proprogetdat->id . ')" ></td>';
                    break;
                case 'finalrate':
                    $contenthtml .= '<td><input type="text" data-item="finalrate" data-itemid="' . $proprogetdat->id . '" class="cell-inp updateproject totalrate' . $proprogetdat->id . ' finalrate' . $catitem . '"  data-cat="' . $catitem . '" name="finalrate" ></td>';
                    break;
                case 'totalamount':
                    $contenthtml .= '<td><input type="text" data-item="totalamount" data-itemid="' . $proprogetdat->id . '" class="cell-inp updateproject grandvalue totalamount' . $proprogetdat->id . ' finalamount' . $catitem . '"  data-cat="' . $catitem . '" name="finalamount" ></td>';
                    break;
            }
        }
        $contenthtml .= '</tr>';
        return array('status'=>true,'message'=>$contenthtml);
    }
    public function costitemDestroy($id)
    {
        BoqTemplateProduct::where('id',$id)->delete();
        return Reply::success(__('messages.taskDelete'));

    }
    public function costitemCatDestroy($id)
    {
        $position = BoqTemplatePosition::where('id',$id)->first();
        if(!empty($position->id)){
            $category = '';
            if(!empty($position->parent)){
                $category .= $position->parent;
                $category .= ',';
            }
            $category .= $position->itemid;
            BoqTemplateProduct::where('category',$category)->where('template_id',$position->id)->delete();
            $position->delete();
            return Reply::success(__('messages.taskDelete'));
        }
    }
    public function addBoqCostItemCategory(Request $request){

        if(empty($request->templateid)){
            return array('status'=>false,'message'=>'Please Select Boqtemplates');
        }
        $parent = $request->parent ?: 0;
        $maxpositionarray = \App\BoqTemplatePosition::where('template_id',$request->templateid)->where('position','row')->where('level',$request->level)->orderBy('inc','desc')->first();
        $newid = !empty($maxpositionarray->inc) ? $maxpositionarray->inc+1 : 1;
        $columsarray = new BoqTemplatePosition();
        $columsarray->template_id = $request->templateid;
        $columsarray->position = 'row';
        $columsarray->itemid = $request->catid;
        $columsarray->itemname = $request->category;
        $columsarray->level = $request->level;
        $columsarray->catlevel = $request->catlevel;
        $columsarray->itemslug = '';
        $columsarray->collock = 0;
        $columsarray->parent = $parent;
        $columsarray->inc = $newid;
        $columsarray->save();
        $contenthtml = '';
        if($request->level==0){
            $contenthtml .= '<tr  data-depth="0" class="collpse level0">
                        <td><a href="javascript:void(0);"  class="red sa-params-cat" data-toggle="tooltip" data-position-id="'.$columsarray->id.'" data-level="0" data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>
                        <td colspan="4"><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-plus-circle"></i></a>';
        }
        if($request->level==1){
            $contenthtml .= '<tr  data-depth="1" class="collpse level1">
                 <td><a href="javascript:void(0);"  class="red sa-params-cat" data-toggle="tooltip" data-position-id="'.$columsarray->id.'" data-level="0" data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>
                 <td colspan="4"><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-plus-circle"></i> </a>';
        }
        $contenthtml .= $request->category.'</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>  
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>';
        return array('status'=>true,'message'=>$contenthtml);
    }
    public function projectBoqCreate(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }

            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->categories = BoqCategory::where('parent','0')->get();
        $this->id = $id;
        return view('admin.projects.project-boq-create', $this->data);
    }
    public  function boqChangeColPosition(Request $request){
        if(empty($request->boqtemplate)){
            return array('status'=>false,'message'=>'Please Select Boqtemplates');
        }
        $positionarray = $request->position;
        $boqtemplate = $request->templateid;
        if(!empty($positionarray)){
            $x=1; foreach ($positionarray as $position){
                BoqTemplatePosition::where('template_id',$boqtemplate)->where('id',$position)->update(['inc'=>$x]);
                $x++; }
        }
    }
    public  function boqChangeCatPosition(Request $request){
        if(empty($request->boqtemplate)){
            return array('status'=>false,'message'=>'Please Select Boqtemplates');
        }
        $positionarray = $request->position;
        $boqtemplate = $request->boqtemplate;
        $title = $request->title;
        $boqtemplate = $request->boqtemplate;
        if(!empty($positionarray)){
            $x=1; foreach ($positionarray as $position){
                BoqTemplatePosition::where('template_id',$boqtemplate)->where('id',$position)->update(['inc'=>$x]);
                $x++; }
        }
    }
    public  function boqChangeCostitemPosition(Request $request){
        if(empty($request->boqtemplate)){
            return array('status'=>false,'message'=>'Please Select Boqtemplates');
        }
        $positionarray = $request->position;
        $boqtemplate = $request->boqtemplate;
        $title = $request->title;
        $boqtemplate = $request->boqtemplate;
        if(!empty($positionarray)){
            $x=1; foreach ($positionarray as $position){
                BoqTemplateProduct::where('template_id',$boqtemplate)->where('id',$position)->update(['inc'=>$x]);
                $x++; }
        }
    }
    public  function  BoqLock(Request $request){
        if(empty($request->template_id)){
            return array('status'=>false,'message'=>'Please Select Boqtemplates');
        }
        $positionarray = $request->lockcolms;
        $boqtemplate = $request->template_id;
        if(!empty($positionarray)&&count($positionarray)>0){
            BoqTemplatePosition::where('template_id',$boqtemplate)->update(['collock'=>0]);
            foreach ($positionarray as $key => $position){
                BoqTemplatePosition::where('template_id',$boqtemplate)->where('id',$key)->update(['collock'=>1]);
            }
        }else{
            BoqTemplatePosition::where('template_id',$boqtemplate)->update(['collock'=>0]);
        }
        return redirect(route('admin.boq-template.addBoq',[$boqtemplate]));
    }
    public  function updateCostItem(Request $request){
        if(empty($request->templateid)){
            return array('status'=>false,'message'=>'Please Select Boqtemplates');
        }
        $user = $this->user;
        $item = $request->item;
        $itemid = $request->itemid;
        $itemvalue = $request->itemvalue;
        $title = $request->title;
        $boqtemplate = $request->templateid;
        $dataarray = array();
        $dataarray[$item] = $itemvalue;
        switch($item){
            case 'cost_item_id':
                $costitem = CostItems::where('cost_item_name',$itemvalue)->first();
                $dataarray[$item] = $costitem->id;
                break;
            case 'unit':
                $costitem = Units::where('name',$itemvalue)->first();
                $dataarray[$item] = $costitem->id;
                break;
            case 'worktype':
                $worktype = Type::where('title',$itemvalue)->first();
                if(!empty($worktype->id)){
                    $dataarray[$item] = $worktype->id;
                }
                break;
        }
        BoqTemplateProduct::where('template_id',$boqtemplate)->where('id',$itemid)->update($dataarray);
        $projectcostitem = BoqTemplateProduct::where('template_id',$boqtemplate)->where('id',$itemid)->first();
        $marktype = $projectcostitem->marktype;
        $markvalue = $projectcostitem->markvalue;
        $rate = $projectcostitem->rate;
        $qty = $projectcostitem->qty;
        $finalrate = $rate;
        $totalamount = 0;
        if(!empty($qty)&&!empty($rate)){
            $totalamount = $qty*$rate;
            $projectcostitem->finalrate = $rate;
            $projectcostitem->finalamount = $totalamount;
        }
        if(!empty($marktype)&&!empty($markvalue)){
            if($marktype=='percent'){
                $percent = ($markvalue/100)*$rate;
                $amount = $rate+$percent;
                $finalrate = $amount*$qty;
            }
            if($marktype=='amount'){
                $percent =$markvalue;
                $amount = $rate+$percent;
                $finalrate = $amount*$qty;
            }
            $totalamount = $finalrate;
            $projectcostitem->finalrate = $finalrate;
            $projectcostitem->finalamount = $totalamount;
        }
        $projectcostitem->save();
    }
}
