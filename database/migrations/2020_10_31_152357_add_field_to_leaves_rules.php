<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToLeavesRules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leaves_rules', function (Blueprint $table) {
            $table->string('leave_allowed_in_year');
            $table->string('leave_allowed_in_month');
            $table->string('continue_leave_allow');
            $table->string('max_leave_to_carry_forword');
            $table->string('all_remaining_leaves');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leaves_rules', function (Blueprint $table) {
            //
        });
    }
}
