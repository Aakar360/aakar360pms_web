@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang("app.menu.home")</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="https://cdn.rawgit.com/mervick/emojionearea/master/dist/emojionearea.min.css">
   <style>

        .image_upload{
            position: absolute;
            top: 3px;
            right:3px;
        }
        .image_upload >form >input {
            display: none;
        }
        .image_upload img{
            width: 24px;
            cursor: pointer;
        }
    </style>
@endpush

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="jumbotron m-0 p-0 bg-transparent">
                    <div class="row m-0 p-0 position-relative">
                        <div class="col-12 p-0 m-0 position-absolute" style="right: 0px;">
                            <div class="card border-0 rounded" style="box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.10), 0 6px 10px 0 rgba(0, 0, 0, 0.01); overflow: hidden; height: 100vh;">

                                <div class="card-header p-1 bg-light border border-top-0 border-left-0 border-right-0" style="color: rgba(96, 125, 139,1.0);">

                                    <img class="img-responsive img-circle float-left" style="width: 50px; height: 50px;" src="{{ get_users_image_link($user->id) }}" />

                                    <h6 class="float-left" style="margin: 0px; margin-left: 10px; margin-top: 10px;"> {{ ucwords($user->name) }}</h6>

                                    <div class="dropdown show">

                                        <a id="dropdownMenuLink" data-toggle="dropdown" class="btn btn-sm float-right text-secondary" role="button"><h5><i class="fa fa-ellipsis-h" title="Ayarlar!" aria-hidden="true"></i>&nbsp;</h5></a>

                                        <div class="dropdown-menu dropdown-menu-right border p-0" aria-labelledby="dropdownMenuLink">

                                            <a class="dropdown-item p-2 text-secondary" href="#"> <i class="fa fa-user m-1" aria-hidden="true"></i> Profile </a>
                                            <hr class="my-1"></hr>
                                            <a class="dropdown-item p-2 text-secondary" href="#"> <i class="fa fa-trash m-1" aria-hidden="true"></i> Delete </a>

                                        </div>
                                    </div>

                                </div>

                                <div class="card bg-sohbet border-0 m-0 p-0" style="height: 100vh;">
                                    <div id="sohleft" class="card border-0 m-0 p-0 position-relative bg-transparent" style="overflow-y: auto; height: 100vh;">

                                        <div class="panel-group" id="accordion">

                                            @include('layouts.pulsemenu');



                                        </div>

                                </div>



                            </div>
                        </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="jumbotron m-0 p-0 bg-transparent">
                    <div class="row m-0 p-0 position-relative">
                        <div class="col-12 p-0 m-0 position-absolute" style="right: 0px;">
                            <div class="card border-0 rounded" style="box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.10), 0 6px 10px 0 rgba(0, 0, 0, 0.01); overflow: hidden; height: 100vh;">

                                <div class="card bg-sohbet border-0 m-0 p-0" style="height: 100vh;">
                                    <div id="sohbet" class="card border-0 m-0 p-0 position-relative bg-transparent" style="background:url({{ asset_url('back-study-photo.png') }});overflow-y: auto; height: 100vh;">


                                        <div class="chat-box ">

                                            <ul class="chat-list slimscroll p-t-30 chats"></ul>


                                        </div>


                                    </div>

                                <div class="w-100 card-footer p-0 bg-light border border-bottom-0 border-left-0 border-right-0">

                                    <div class="row send-chat-box">
                                        <div class="col-sm-12">

                                            <input type="text" name="message" id="submitTexts" autocomplete="off" placeholder="@lang("modules.messages.typeMessage")"
                                                   class="form-control ">
                                            <div class="form-group">

                                                <div class="image_upload">
                                                    <form id="uploadImage" method="POST" >
                                                        <label for="uploadDoc"><img src="https://banner2.cleanpng.com/20180714/etx/kisspng-computer-icons-hyperlink-agriculture-silhouette-5b49e2945e6606.7405367315315687883867.jpg" ></label>
                                                        <input type="file" name="uploadDoc" id="uploadDoc"  accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
text/plain, application/pdf">
                                                        <label for="uploadFile"><img src="https://www.pngitem.com/pimgs/m/365-3654427_cloud-upload-icon-svg-hd-png-download.png" ></label>
                                                        <input type="file" name="uploadFile" id="uploadFile"  accept=".jpg,.png,video/mp4,video/x-m4v,video/*">

                                                    </form>
                                                </div>

                                            </div>
                                            <input id="dpID" value="{{$dpData}}" type="hidden"/>
                                            <input id="dpName" value="{{$dpName}}" type="hidden"/>

                                            <div class="custom-send">
                                                <button id="submitBtn" class="btn btn-danger btn-rounded" type="button">@lang("modules.messages.send")
                                                </button>
                                            </div>
                                            <div id="errorMessage"></div>
                                        </div>
                                    </div>

                                </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- .row -->

    {{--Ajax Modal
    <div class="modal fade bs-modal-md in" id="newChatModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
<script src="{{ asset('js/cbpFWTabs.js') }}"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="https://cdn.rawgit.com/mervick/emojionearea/master/dist/emojionearea.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">

    $('.chat-left-inner > .chatonline').slimScroll({
        height: '100%',
        position: 'right',
        size: "0px",
        color: '#dcdcdc',

    });
    $(function () {
        $(window).load(function () { // On load
            $('.chat-list').css({'height': (($(window).height()) - 370) + 'px'});
        });
        $(window).resize(function () { // On resize
            $('.chat-list').css({'height': (($(window).height()) - 370) + 'px'});
        });
    });

    // this is for the left-aside-fix in content area with scroll

    $(function () {
        $(window).load(function () { // On load
            $('.chat-left-inner').css({
                'height': (($(window).height()) - 240) + 'px'
            });
        });
        $(window).resize(function () { // On resize
            $('.chat-left-inner').css({
                'height': (($(window).height()) - 240) + 'px'
            });
        });
    });


    $(".open-panel").click(function () {
        $(".chat-left-aside").toggleClass("open-pnl");
        $(".open-panel i").toggleClass("ti-angle-left");
    });
</script>
<script>

    $(function () {
        $('#userList').slimScroll({
            height: '350px'
        });
    });

    var dpButtonID = "";
    var dpName = "";
    var scroll = true;

    var dpClassID = '{{$dpData}}';

    if (dpClassID) {
        $('#dp_' + dpClassID).addClass('active');
    }

   /* //getting data
    window.setInterval(function(){
        getChatData(dpButtonID, dpName);
        /// call your function here
    }, 5000);*/

    $('#submitTexts').keypress(function (e) {

        var key = e.which;
        if (key == 13)  // the enter key code
        {
            e.preventDefault();
            $('#submitBtn').click();
            return false;
        }
    });



     /*$('#uploadFile').on('change',function () {
            $('#uploadImage').ajaxSubmit({
              target:"#group_chat_message",
                resetForm:true,
            });
    });*/


    //submitting message
    $('#submitBtn').on('click', function (e) {
        e.preventDefault();
        //getting values by input fields
        var submitText = $('#submitTexts').val();
        var dpID = $('#dpID').val();
        var uploadDoc = $('#uploadDoc').val();
        uploadDoc = uploadDoc.split("\\");
        uploadDoc = uploadDoc[uploadDoc.length - 1];

        var uploadFile = $('#uploadFile').val();
        uploadFile = uploadFile.split("\\");
        uploadFile = uploadFile[uploadFile.length - 1];
        //checking fields blank
         if (dpID == '' || submitText == undefined) {
            $('#errorMessage').html('<div class="alert alert-danger"><p>@lang('messages.noUser ')</p></div>');
            return;
        } else {

            var url = "{{ route('admin.user-chat.message-submit') }}";
            var token = "{{ csrf_token() }}";
            $.easyAjax({
                type: 'POST',
                url: url,
                messagePosition: '',
                data: {'message': submitText, 'doc':uploadDoc,'image':uploadFile, 'user_id': dpID, '_token': token},
                container: ".chat-form",
                blockUI: true,
                redirect: false,
                success: function (response) {
                    var blank = "";
                    $('#submitTexts').val('');

                    //getting values by input fields
                    var dpID = $('#dpID').val();
                    var dpName = $('#dpName').val();


                    //set chat data
                    getChatData(dpID, dpName);

                    //set user list
                    $('.userList').html(response.userList);

                    //set active user
                    if (dpID) {
                        $('#dp_' + dpID + 'a').addClass('active');
                    }
                }
            });
        }

        return false;
    });


    //getting all chat data according to user
    //submitting message
    $("#userSearch").keyup(function (e) {
        var url = "{{ route('admin.user-chat.user-search') }}";

        $.easyAjax({
            type: 'GET',
            url: url,
            messagePosition: '',
            data: {'term': this.value},
            container: ".userList",
            success: function (response) {
                //set messages in box
                $('.userList').html(response.userList);
            }
        });
    });



    //getting all chat data according to user
    function getChatData(id, dpName, scroll) {
        var getID = '';
        $('#errorMessage').html('');
        if (id != "" && id != undefined && id != null) {
            $('.userList li a.active ').removeClass('active');
            $('#dpa_' + id).addClass('active');
            $('#dpID').val(id);
            getID = id;
            $('#badge_' + id).val('');
        } else {
            $('.userList li:first-child a').addClass('active');
            getID = $('#dpID').val();
        }

        var url = "{{ route('admin.user-chat.index') }}";

        $.easyAjax({
            type: 'GET',
            url: url,
            messagePosition: '',
            data: {'userID': getID},
            container: ".chats",
            success: function (response) {
                //set messages in box
                $('.chats').html(response.chatData);
                scrollChat();
            }
        });
    }

    function scrollChat() {
        if(scroll == true) {
            $('.chat-list').stop().animate({
                scrollTop: $(".chat-list")[0].scrollHeight
            }, 800);
        }
        scroll = false;
    }

    $('#new-chat').click(function () {
        var url = '{{ route('admin.user-chat.create')}}';
        $('#modelHeading').html('Start Conversation');
        $.ajaxModal('#newChatModal',url);
    })

</script>
@endpush