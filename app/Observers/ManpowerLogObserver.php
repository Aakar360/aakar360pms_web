<?php

namespace App\Observers;

use App\ManpowerLog;

class ManpowerLogObserver
{

    public function saving(ManpowerLog $log)
    {
        // Cannot put in creating, because saving is fired before creating. And we need company id for check bellow
        if (company()) {
            $log->company_id = company()->id;
        }
    }

}
