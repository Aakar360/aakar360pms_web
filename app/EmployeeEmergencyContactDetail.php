<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeEmergencyContactDetail extends Model
{
    protected $fillable = ['company_id','employee_id','user_id','family_memeber_name','family_memeber_relation','family_memeber_number'];
}
