@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.indent.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @lang('modules.indent.createTitle')</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'createIndent','class'=>'ajax-form','method'=>'POST', 'autocomplete'=>'off']) !!}

                            <div class="form-body">
                                <h3 class="box-title">@lang('modules.indent.indentDetails')</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.store') (*)</label>
                                            <select class="form-control select2" name="store_id" id="store" data-style="form-control" required>
                                                <option value="">Select Store</option>
                                                @foreach($stores as $supplier)
                                                    <option value="{{$supplier->id}}">{{ $supplier->company_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">@lang('app.menu.projects') (*)</label>
                                            <select class="form-control select2" name="project_id" id="project" data-style="form-control" required>
                                                <option value="">Select Projects</option>
                                                @foreach($projects as $supplier)
                                                    <option value="{{$supplier->id}}">{{ $supplier->project_name }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">@lang('modules.indent.remark')</label>
                                            <input type="text" id="remark" name="remark" value="" class="form-control" >
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <h3 class="box-title">@lang('modules.indent.productDetail')</h3>
                                <hr>
                                <div class="row" style="background-color: #efefef; padding-top: 5px;">
                                    <div class="proentry">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">@lang('modules.indent.category')</label>
                                                <select class="form-control select2" name="product" data-style="form-control product">
                                                    <option value="">Select Category</option>
                                                    @foreach($products as $product)
                                                        <option value="{{$product->id}}">{{ get_local_product_name($product->id) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label class="control-label">@lang('modules.indent.unit')</label>
                                                <div style="display: none;" id="unitData"></div>
                                                <input type="text" readonly value="" class="form-control quantity" name="unit_id_data" id="unit">
                                            </div>
                                        </div>

                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label class="control-label">Est. Qty</label>
                                                <input type="text" readonly id="estQty" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label class="control-label">Issued. Qty</label>
                                                <input type="text" readonly id="reqQty" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label class="control-label">Req. Qty</label>
                                                <input type="text" name="quantity" value="" class="form-control quantity" placeholder="Enter Quantity">
                                            </div>
                                        </div>

                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label class="control-label">@lang('modules.indent.dateRequired')</label>
                                                <input type="text" name="date" value="" class="form-control date-picker" placeholder="Select Date">
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label class="control-label">@lang('modules.indent.brand')</label>
                                                <select class="form-control select2" name="brand" id="brandData" data-style="form-control brand">
                                                    <option value="">Select Brand</option>
                                                    @forelse($brands as $brand)
                                                        <option value="{{$brand->id}}">{{ $brand->name }}</option>
                                                    @empty
                                                    @endforelse
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">@lang('modules.indent.remark')</label>
                                                <input type="text" name="remarkx" value="" class="form-control" placeholder="Enter Remark">
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label class="control-label" style="width: 100%;">@lang('modules.indent.action')</label>
                                                <a href="javascript:void(0)" class="add-button btn btn-primary" style="color: #ffffff;">Add</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="table-responsive" id="pdata">

                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" id="save-form" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
                                <a href="{{ route('admin.indent.index') }}" class="btn btn-default">@lang('app.cancel')</a>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

@endsection

@push('footer-script')
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>

<script>
    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });
    $(".date-picker").datepicker({
        todayHighlight: true,
        autoclose: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });

    $('#save-form').click(function () {
        $.easyAjax({
            url: '{{route('admin.indent.store')}}',
            container: '#createIndent',
            type: "POST",
            redirect: true,
            data: $('#createIndent').serialize()
        })
    });
    $(document).on('change', 'select[name=product]', function(){
        var pid = $(this).val();
        var token = '{{ csrf_token() }}';
        var store_id = $('select[name=store_id]').val();
        var project_id = $('select[name=project_id]').val();
        $.ajax({
            url: '{{route('admin.stores.projects.indent.getBrands')}}',
            type: 'POST',
            data: {_token: token, pid: pid, store_id: store_id, project_id: project_id},
            success: function (data) {
                $('#brandData').html(data.brands);
                $('#estQty').val(data.estQty);
                $('#reqQty').val(data.reqQty);
                $('#unit').val(data.unit);
                $('#unitData').html(data.unitData);
                $("#brandData").select2("destroy");
                $("#brandData").select2();
            }
        });

    });
    $(document).on('click', '.add-button', function(){
        var btn = $(this);
        var cid = $('select[name=product]').val();
        var bid = $('select[name=brand]').val();
        var qty = $('input[name=quantity]').val();
        var unit = $('select[name=unit]').val();
        var dated = $('input[name=date]').val();
        var remark = $('input[name=remarkx]').val();
        var store_id = $('select[name=store_id]').val();
        var project_id = $('select[name=project_id]').val();
        if(cid == '' || qty == '' || qty == 0 || unit == '' || dated == '' || store_id == '' || project_id == ''){
            alert('Invalid Data. All fields are mandatory.');
        }else{
            $.ajax({
                url: '{{route('admin.indent.storeTmp')}}',
                type: 'POST',
                data: {_token : '{{ csrf_token()  }} ', cid: cid, bid:bid, qty: qty, unit: unit, dated: dated, remark: remark, store_id: store_id, project_id: project_id},
                redirect: false,
                beforeSend: function () {
                    btn.html('Adding...');
                },
                success: function (data) {
                    $('#pdata').html(data);
                },
                complete: function () {
                    btn.html('Add');
                }

            });
        }
    });
    $(document).on('click', '.deleteRecord', function(){
        var btn = $(this);
        var did = btn.data('key');
        $.ajax({
            url: '{{route('admin.indent.deleteTmp')}}',
            type: 'POST',
            data: {_token : '{{ csrf_token()  }} ', did: did},
            redirect: false,
            beforeSend: function () {
                btn.html('Deleting...');
            },
            success: function (data) {
                $('#pdata').html(data);
            },
            complete: function () {
                btn.html('Delete');
            }

        });
    });
</script>
@endpush

