<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePfandEsiSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pfand_esi_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('effective_date');
            $table->string('min_pf_calc_amount');
            $table->string('emp_pf_contribution');
            $table->string('fixed_amount');
            $table->string('custum_formula_select');
            $table->string('custum_formula_percent');
            $table->string('employer_pf_contibution');
            $table->string('enable_min_ceilig');
            $table->string('lop_dependent');
            $table->string('emploer_ctc_contribution');
            $table->string('allow_override');
            $table->string('admin_ctc_charges');
            $table->string('employer_contribution');
            $table->string('include_empl_ctc');
            $table->string('allow_esi_override');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pfand_esi_settings');
    }
}
