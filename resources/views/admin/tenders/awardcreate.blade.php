@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.awardedcontracts.awardedContracts') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
    <style>
        .disable {
            pointer-events: none;
        }
        .rc-handle-container {
            position: relative;
        }
        .rc-handle {
            position: absolute;
            width: 7px;
            cursor: ew-resize;
            margin-left: -3px;
            z-index: 2;
        }
        table.rc-table-resizing {
            cursor: ew-resize;
        }
        table.rc-table-resizing thead,
        table.rc-table-resizing thead > th,
        table.rc-table-resizing thead > th > a {
            cursor: ew-resize;
        }
        .combo-sheet{
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;
        }
        .combo-sheet .table-wrapper{
            min-width: 1350px;
        }
        .combo-sheet thead th{
            font-size: 12px;
            background-color: #C2DCE8;
            color: #3366CC;
            padding: 3px 5px !important;
            text-align: center;
            position: sticky;
            top: -1px;
            z-index: 1;
        &:first-of-type {
             left: 0;
             z-index: 3;
         }
        }
        .combo-sheet td{
            padding: 0 !important;
        }
        .combo-sheet td input.cell-inp, .combo-sheet td textarea.cell-inp{
            min-width: 100%;
            max-width: 100%;
            width: 100%;
            border: none !important;
            padding: 3px 5px;
            cursor: default;
            color: #000000;
            font-size: 1.2rem;
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,sans-serif;
        }
        .combo-sheet .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
            border: 1px solid #bdbdbd;
        }
        .combo-sheet tr:hover td, .combo-sheet tr:hover input{
            background-color: #ffdd99;
        }
        .combo-sheet tr.inFocus, .combo-sheet tr.inFocus input{
            background-color: #eaf204;
        }
    </style>

@endpush
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">@lang('app.add') @lang('app.menu.awardedcontracts')</h3>

                <p class="text-muted m-b-30 font-13"></p>
                <div class="row">
                    {!! Form::open(['id'=>'createTenderPayment','class'=>'ajax-form','method'=>'POST']) !!}

                    <ul class="nav nav-pills">
                        <li @if(empty($awardedcontract->id)) class="active" @endif><a data-toggle="pill" href="#home">Details</a></li>
                        <li @if(!empty($awardedcontract->id)) class="active" @endif ><a data-toggle="pill" class="@if(empty($awardedcontract->id)) disable @endif" id="taskinfoli" href="#menu1">Task info</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="home" class="tab-pane fade @if(empty($awardedcontract->id)) in active @endif">

                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="title">@lang('app.title')</label>
                                    <input type="text" class="form-control" id="title" name="title">
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="number">@lang('app.number')</label>
                                    <input type="text" class="form-control" id="number" name="number">
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="title">Status</label>
                                    <select class="select2 form-control" name="status" data-style="form-control" required>
                                        <option value="">Please Select Status</option>
                                        <option value="Open">Open</option>
                                        <option value="Initiated">Initiated</option>
                                        <option value="Not accepted">Not accepted</option>
                                        <option value="Closed">Closed</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="duedate">Contractors</label>
                                    <select class="select2 form-control" name="contractor" data-style="form-control" required>
                                        <option value="">Please Select Contractor</option>
                                        @foreach($contractorsarray as $contractor)
                                            <option value="{{ $contractor->id }}">{{ ucwords($contractor->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-6 form-group">
                                <div class="form-group">
                                    <label for="duedate">Distribution</label>
                                    <select class="select2" name="distribution[]" data-style="form-control" multiple required>
                                        <option value="">Please Select Distribution</option>
                                        @foreach($employees as $employee)
                                            <option value="{{ $employee->id }}">{{ ucwords($employee->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xs-12 form-group" >
                                <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                <div id="file-upload-box" >
                                    <div class="row" id="file-dropzone">
                                        <div class="col-md-12">
                                            <div class="dropzone dropheight"  id="file-upload-dropzone">
                                                <div class="fallback">
                                                    <input name="file[]" type="file" multiple/>
                                                </div>
                                                <input name="image_url" id="image_url" type="hidden" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="menu1" class="tab-pane fade @if(!empty($awardedcontract->id)) in active @endif">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <label for="project_id"><b>@lang('app.projects')</b></label>
                                            <select class="select2 form-control projectid" id="projectid" name="project_id" data-style="form-control" required>
                                                <option value="">Select @lang('app.projects')</option>
                                                @foreach($projectlist as $project)
                                                    <option value="{{ $project->id }}" @if(!empty($awardedcontract)&&($awardedcontract->project_id==$project->id)) selected @endif >{{ ucwords($project->project_name) }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <label for="title_id">@lang('app.subprojects')</label>
                                            <select class="select2 form-control titlelist" id="titlelist" name="title_id" data-style="form-control" required>
                                                <option value="">Select @lang('app.subprojects')</option>
                                                @if($titlelist)
                                                    @foreach($titlelist as $title)
                                                        <option value="{{ $title->id }}" @if(!empty($awardedcontract)&&($awardedcontract->subproject_id==$title->id)) selected @endif >{{ ucwords($title->title) }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-4 segmentblock" @if(count($segmentlist)>0) style="display: none;" @endif>
                                        <div class="form-group">
                                            <label for="title_id">@lang('app.segment')</label>
                                            <select class="select2 form-control segmentslist" id="segmentslist" name="segment_id" data-style="form-control" required>
                                                <option value="">Select @lang('app.segment')</option>
                                                @if(count($segmentlist)>0)
                                                    @foreach($segmentlist as $segment)
                                                        <option value="{{ $segment->id }}" @if(!empty($awardedcontract)&&($awardedcontract->segment_id==$segment->id)) selected @endif >{{ ucwords($segment->name) }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="combo-sheet">
                                            <div class="table-responsive">
                                                <table id="mytable" class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>@lang('app.task')</td>
                                                        <td>Unit</td>
                                                        <td>Quantity</td>
                                                        <td>Rate</td>
                                                        <td>Final Rate</td>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="colposition" id="contractproductsloop">

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <input type="hidden" name="projectID" id="projectID">
                                    <input type="hidden" name="awardedID" id="awardedID" value="@if(!empty($awardedcontract)){{ $awardedcontract->id }}@endif">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-xs-12">
                        <button type="submit" id="save-form" class="btn btn-success waves-effect waves-light m-r-10">
                            @lang('app.save')
                        </button>
                        {{ csrf_field() }}
                        <button type="reset" class="btn btn-inverse waves-effect waves-light">@lang('app.reset')</button>
                        <input name="awarded_contract_id" id="tenderID" type="hidden" />
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')

    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script>
        Dropzone.autoDiscover = false;
        //Dropzone class
        myDropzone = new Dropzone("div#file-upload-dropzone", {
            url: "{{ route('admin.awardedcontracts.storeImage') }}",
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks:true,
            parallelUploads:10,
            init: function () {
                myDropzone = this;
            }
        });
        myDropzone.on('sending', function(file, xhr, formData) {
            var ids = $('#awardedID').val();
            formData.append('awarded_id', ids);
        });
        myDropzone.on('completemultiple', function () {
            $("#taskinfoli").removeClass('disable');
            $('a[href="#menu1"]').trigger('click');
            $('.nav-tabs a[href="#menu1').tab('show');
        });
        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('.datepicker').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        $(document).on('click', '.cell-inp', function(){
            var inp = $(this);
            $('tr').removeClass('inFocus');
            inp.parent().parent().addClass('inFocus');
        });
        function calmarkup(costitem){
            var calvalue = 0;
            var  percentvalue = 0;
            var qty = parseInt($(".qty"+costitem).val());
            var rate = parseInt($(".ratevalue"+costitem).val());
            var unit = parseInt($(".unitvalue"+costitem).val());
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: '{{ route('admin.awardedcontracts.updateboqcostitem') }}',
                method: 'POST',
                data: {
                    '_token':token,
                    qty:qty,
                    rate:rate,
                    unit:unit,
                    itemid:costitem
                },
                success: function(response){
                    contractproductsloop();
                }
            });
        }
        $('#save-form').click(function () {
            $.easyAjax({
                url: '{{route('admin.awardedcontracts.awardedstore')}}',
                container: '#createTenderPayment',
                type: "POST",
                redirect: true,
                data: $('#createTenderPayment').serialize(),
                success: function(response){
                    if(myDropzone.getQueuedFiles().length > 0){
                        awardedID = response.awardedID;
                        $('#awardedID').val(response.awardedID);
                        myDropzone.processQueue();
                    }
                    else{
                        $("#taskinfoli").removeClass('disable');
                        $('a[href="#menu1"]').trigger('click');
                        $('.nav-tabs a[href="#menu1').tab('show');
                    }
                }
            })
        });
        $(document).on('click','.remove-item', function () {
            $(this).closest('.item-row').fadeOut(300, function() {
                $(this).remove();
            });
        });
        $('.summernote').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ["view", ["fullscreen"]]
            ]
        });
        $(".projectid").change(function () {
            var awardedcontract = $("#awardedID").val();
            var project = $(this).val();
            if(project){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.awardedcontracts.projecttitles') }}",
                    data: {'_token': token,'projectid': project,'awardedcontract': awardedcontract},
                    success: function(data){
                        $("select.titlelist").html("");
                        $("select.titlelist").html(data);
                        $('select.titlelist').select2();
                    }
                });
            }
        });
        $("#titlelist").change(function () {
            var awardedcontract = $("#awardedID").val();
            var project = $(".projectid").find(':selected').val();
            var titleid = $(this).val();
            if(project&&titleid){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.awardedcontracts.boqcostitems') }}",
                    data: {'_token': token,'projectid': project,'titleid': titleid,'awardedcontract': awardedcontract},
                    success: function(data){
                        $("select.segmentslist").select2();
                        $("select.segmentslist").html("");
                        $(".costitemslist").html("");
                        $(".costitemscatlist").html("");
                        if(data.segments){
                            $(".segmentblock").show();
                            $("select.segmentslist").html(data.segments);
                        }else{
                            $(".segmentblock").hide();
                        }
                        $(".costitemslist").html(data.costitem);
                        $(".costitemscatlist").html(data.costcat);

                    }
                });
            }
        });
        $("#segmentslist").change(function () {
            var awardedcontract = $("#awardedID").val();
            var project = $(".projectid").find(':selected').val();
            var titleid = $("#titlelist").find(':selected').val();
            var segmentid = $(this).val();
            if(project&&titleid){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.awardedcontracts.boqcostitems') }}",
                    data: {'_token': token,'projectid': project,'titleid': titleid,'segmentid': segmentid,'awardedcontract': awardedcontract},
                    success: function(data){
                        contractproductsloop();
                    }
                });
            }
        });
        var r = 0;
        $(document).on("change",".costitemrow",function () {
            r++;
            var awardedcontract = $("#awardedID").val();
            var trindex = $(this).closest('tr').index();
            var project = $("#projectid").find(':selected').val();
            var titleid = $("#titlelist").find(':selected').val();
            var segmentid = $("#segmentslist").find(':selected').val();
            var rowid = $(this).val();
            var catid = $(this).data('cat');
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.awardedcontracts.boqrow') }}",
                data: {
                    '_token': token,
                    'awardedcontract': awardedcontract,
                    'projectid': project,
                    'titleid': titleid,
                    'segmentid': segmentid,
                    'rowid': rowid,
                    'catid': catid
                },
                success: function(data){
                    contractproductsloop();
                    /*if(trindex>0){
                        $("#mytable > tbody > tr").eq(trindex-1).after(data);
                    }else{
                        $(".row_position").prepend(data);
                    }
                    $(".costitemrow").val("");*/
                }
            });
        });
        var r = 0;
        $(document).on("change",".costitemcatrow",function () {
            r++;
            var trindex = $(this).closest('tr').index();
            var awardedcontract = $("#awardedID").val();
            var project = $(".projectid").find(':selected').val();
            var titleid = $("#titlelist").find(':selected').val();
            var segmentsid = $("#segmentslist").find(':selected').val();
            var level = $(this).data('level');
            var parent = $(this).data('parent');
            var catid = $(this).val();
            var awardedcontract = $("#awardedID").val();
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.awardedcontracts.boqcatrow') }}",
                data: {
                    '_token': token,
                    'awardedcontract': awardedcontract,
                    'projectid': project,
                    'titleid': titleid,
                    'segmentsid': segmentsid,
                    'catid': catid,
                    'level': level,
                    'parent': parent
                },
                success: function(data){
                    if(data=='failed') {
                        alert('Contract not found. Please try again');
                    }else{
                        contractproductsloop();
                    }
                    /* if(trindex>0){
                         $("#mytable > tbody > tr").eq(trindex-1).after(data);
                     }else{
                         $(".row_position").prepend(data);
                     }*/
                }
            });
        });
        contractproductsloop();
            function contractproductsloop(){
                var contractid = '{{ !empty($awardedcontract) ? $awardedcontract->id :0 }}';
                var projectid = $("#projectid").val();
                var subprojectid = $("#titlelist").val();
                var token = "{{ csrf_token() }}";
                $.ajax({
                    url : "{{ route('admin.awardedcontracts.contractproductsloop') }}",
                    method: 'POST',
                    data: {'_token': token,'contractid': contractid,'projectid': projectid,'subprojectid': subprojectid },
                    success: function (response) {
                        $("#contractproductsloop").html("");
                        $("#contractproductsloop").html(response);
                    }
                });
            }
    </script>
@endpush

