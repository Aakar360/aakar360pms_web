<?php

namespace App\Observers;

use App\WeatherLog;

class WeatherLogObserver
{

    public function saving(WeatherLog $log)
    {
        // Cannot put in creating, because saving is fired before creating. And we need company id for check bellow
        if (company()) {
            $log->company_id = company()->id;
        }
    }

}
