<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class DirectReportTeam extends Model
{
    protected $table = 'direct_report_team';

}
