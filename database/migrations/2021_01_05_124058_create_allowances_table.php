<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllowancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('allowances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('item_name');
            $table->date('purchase_date');
            $table->string('purchase_from')->nullable();
            $table->double('price');
            $table->integer('currency_id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->string('expence_type')->nullable();
            $table->string('bill')->nullable();
            $table->integer('user_id')->unsigned();
            $table->enum('status', ['pending', 'approved', 'rejected'])->default('pending');
            $table->timestamps();
        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('allowances');
    }
}
