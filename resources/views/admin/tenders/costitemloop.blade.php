                            <?php
                                    function boqhtml($boqarray){

                            $user = $boqarray['user'];
                                        $tenders = $boqarray['tenders'];
                                        $level = $boqarray['level'];
                                        $unitsarray = $boqarray['unitsarray'];
                                        $categoryarray = $boqarray['categoryarray'];
                                        $catitem = $boqarray['catitem'];
                            $newlevel = $level+1;
                            $level1categoryarray =  \App\TendersCategory::where('tender_id',$tenders->id)->where('level',$level)->orderBy('inc','asc')->get();
                            if($level1categoryarray){
                            foreach ($level1categoryarray as $level1category){
                            $costitemsarray = \App\ProjectCostItemsProduct::where('project_id',$tenders->project_id)->where('title',$tenders->title_id)->where('category',$catitem)->pluck('cost_items_id','id');

                            if(!empty($catitem)){
                                    $catitem .= ','.$level1category->category;
                                }else{
                                    $catitem = $level1category->category;
                                }

                            ?>
                            <tr class="collpse">
                                <td><a href="javascript:void(0);"  class="red sa-params-cat" data-position-id="{{ $level1category->id }}" data-catitem="{{ $level1category->category }}" data-catrow="{{ $catitem }}" data-toggle="tooltip"  data-original-title="Delete Category" ><i class="fa fa-trash"></i></a></td>
                                <td><i class="fa fa-plus-circle"></i></td>
                                <td></td>
                                <td>{{ get_category($level1category->category) }}</td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php  $tenderproductsarray  = \App\TendersProduct::where('tender_id',$tenders->id)->where('category',$catitem)->orderBy('inc','asc')->get();
                            foreach ($tenderproductsarray as $tenderproducts){
                            ?>
                            <tr   id="rowitem{{ $tenderproducts->id }}">
                                <td><a href="javascript:void(0);"  class="red remove-item" data-rowid="{{ $tenderproducts->id }}" data-toggle="tooltip"  data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>
                                <td><i class="fa fa-plus-circle"></i></td>
                                <td></td>
                                <td>
                                    <input  class="cell-inp" list="costitemlist{{ $tenderproducts->id }}"  value="{{ get_cost_name($tenderproducts->cost_items) }}">
                                    <datalist  id="costitemlist{{ $tenderproducts->id }}">
                                        <?php    foreach($costitemsarray as $costitem =>$value){
                                        $costitemname = get_cost_name($value); ?>
                                        <option value="{{ $costitemname }}" data-rowid="{{ $costitem }}"  >{{ $costitemname }}</option>
                                        <?php   } ?>
                                    </datalist>
                                </td>
                                <td><input type="text" class="cell-inp" name="qty[{{ $tenderproducts->id }}]" value="{{ $tenderproducts->qty }}"></td>
                                <td>
                                    <input  class="cell-inp" list="unitdatacat{{ $tenderproducts->id }}" value="{{ get_unit_name($tenderproducts->unit) }}">
                                    <datalist id="unitdatacat{{ $tenderproducts->id }}">
                                        <?php foreach($unitsarray as $units){ ?>
                                        <option  value="{{ $units->id }}" >{{ $units->name }}</option>
                                        <?php   }  ?>
                                    </datalist>
                                </td>
                            </tr>
                            <?php }?>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <input  class="cell-inp costitemrow" data-cat="{{ $catitem }}" list="costitemlist" placeholder="@lang('app.task')">
                                    <datalist class="costitemslist"  id="costitemlist">
                                        <?php  if(!empty($costitemsarray)){
                                        foreach ($costitemsarray as $costitem => $value) {
                                        $costitemname = get_cost_name($value); ?>
                                        <option value="{{ $costitemname }}" data-rowid="{{ $costitem }}"  >{{ $costitemname }}</option>
                                        <?php     } }?>
                                    </datalist>
                                </td>
                                <td><input type="text" class="cell-inp"></td>
                                <td>
                                    <input  class="cell-inp" list="unitdatacat" >
                                    <datalist id="unitdatacat">
                                        <?php foreach($unitsarray as $units){ ?>
                                            <option data-value="{{ $units->id }}" >{{ $units->name }}</option>
                                        <?php } ?>
                            </datalist>
                                </td>
                            </tr>
                            <?php
                            $boqarray = array();
                            $boqarray['user'] = $user;
                            $boqarray['tenders'] = $tenders;
                            $boqarray['unitsarray'] = $unitsarray;
                            $boqarray['categoryarray'] = $categoryarray;
                            $boqarray['catitem'] = $catitem;
                            $boqarray['level'] = $newlevel;
                            echo  boqhtml($boqarray);
                            ?>
                            <tr>
                                <td></td>
                                <td></td>
                                <td><i class="fa fa-plus-circle"></i></td>
                                <td>
                                    <input  class="cell-inp costitemcatrow" data-parent="{{ $catitem }}" data-level="{{ $newlevel }}"  list="costitemcatlist" placeholder="@lang('app.task') Category">
                                    <datalist class="costitemscatlist"  id="costitemcatlist">
                                        <?php  if(!empty($categoryarray)){
                                        foreach ($categoryarray as $colums) { ?>
                                        <option value="{{ $colums->id }} " data-name="{{ $colums->title }}" >{{ $colums->title }}</option>
                                        <?php     } }?>

                                    </datalist>
                                </td>
                                <td><input type="text" class="cell-inp"></td>
                                <td><input type="text" class="cell-inp"></td>
                            </tr>
                            <?php } }     }  ?>
                            <?php
                            $boqarray = array();
                            $boqarray['user'] = $user;
                            $boqarray['level'] = 1;
                            $boqarray['tenders'] = $tenders;
                            $boqarray['unitsarray'] = $unitsarray;
                            $boqarray['categoryarray'] = $categoryarray;
                            $boqarray['catitem'] = '';
                            echo  boqhtml($boqarray);
                            ?>


                            <tr>
                                <td></td>
                                <td></td>
                                <td><i class="fa fa-plus-circle"></i></td>
                                <td>
                                    <input  class="cell-inp costitemcatrow" data-level="1" data-parent="0"  list="costitemcatlist" placeholder="@lang('app.task') Category">
                                    <datalist class="costitemscatlist"  id="costitemcatlist">
                                        <?php  if(!empty($categoryarray)){
                                        foreach ($categoryarray as $colums) { ?>
                                        <option value="{{ $colums->id }} " data-name="{{ $colums->title }}" >{{ $colums->title }}</option>
                                        <?php     } }?>

                                    </datalist>
                                </td>
                                <td><input type="text" class="cell-inp"></td>
                                <td><input type="text" class="cell-inp"></td>
                            </tr>