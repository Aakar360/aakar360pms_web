<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AwardedContracts extends Model
{
    protected $table = 'awarded_contracts';
}