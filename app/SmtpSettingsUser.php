<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmtpSettingsUser extends Model
{
    protected $table = 'smtp_settings_user';
    protected $guarded = ['id'];
    protected $appends = ['set_smtp_message'];

    public function verifySmtp()
    {
        $status = $this->status;
        if($status=='1'){
            $settings = $this;
        }else{
            $settings = $this;
        }
        $mailarray = array();
        if($settings->mail_driver == 'mail'){
            switch ($settings->mail_sender){
                case 'google':
                    $mailarray['host'] = 'smtp.gmail.com';
                    $mailarray['port'] = '465';
                    $mailarray['encryption'] = 'ssl';
                    break;
                case 'zoho':
                    $mailarray['host'] = 'smtp.zoho.in';
                    $mailarray['port'] = '465';
                    $mailarray['encryption'] = 'ssl';
                    break;
                case 'yahoo':
                    $mailarray['host'] = 'smtp.mail.yahoo.com';
                    $mailarray['port'] = '465';
                    $mailarray['encryption'] = 'ssl';
                    break;
            }
        }else if ($settings->mail_driver == 'smtp') {
            $mailarray['host'] = $settings->mail_host;
            $mailarray['port'] = $settings->mail_port;
            $mailarray['encryption'] = $settings->mail_encryption;
        }

        if($mailarray){
            try {
                $transport = new \Swift_SmtpTransport($mailarray['host'], $mailarray['port'], $mailarray['encryption']);
                $transport->setUsername($settings->mail_username);
                $transport->setPassword($settings->mail_password);
                $mailer = new \Swift_Mailer($transport);
                $mailer->getTransport()->start();
                if ($settings->verified == 0) {
                    $settings->verified = 1;
                    $settings->save();
                }
                return [
                    'success' => true,
                    'message' => __('messages.smtpSuccess')
                ];
            } catch (\Swift_TransportException $e) {
                $settings->verified = 0;
                $settings->save();
                return [
                    'success' => false,
                    'message' => $e->getMessage()
                ];
            } catch (\Exception $e) {
                $settings->verified = 0;
                $settings->save();
                return [
                    'success' => false,
                    'message' => $e->getMessage()
                ];
            }
        }
    }

    public function getSetSmtpMessageAttribute()
    {
        if ($this->mail_driver == 'smtp') {
            return ' <div class="alert alert-danger">
                    ' . __('messages.smtpNotSet') . '
                    <a href="' . route('admin.email-settings.index') . '" class="btn btn-info btn-small">Visit SMTP Settings <i
                                class="fa fa-arrow-right"></i></a>
                </div>';
        }
        return null;
    }
}
