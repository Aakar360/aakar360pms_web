@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.submittals.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.addNew')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

    <style>
        .panel-black .panel-heading a, .panel-inverse .panel-heading a {
            color: unset!important;
        }
    </style>
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> @if($submittaltype=='update') Update @elseif($submittaltype=='revision') Revision @endif Submittals</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'storeTask','class'=>'ajax-form','method'=>'POST']) !!}

                        <div class="form-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Title</label>
                                        <input type="text" name="title" class="form-control" value="{{ $submittals->title }}" placeholder="Title *" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Number & Revision</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" name="number" class="form-control" readonly value="{{ $submittals->number }}"  placeholder="Number *" required>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="revision" class="form-control" readonly  placeholder="Revision *"  value="{{ $submittals->revision }}"  required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <br>
                                    <div class="form-group row">
                                        <label for="private">Private or Public</label><br>
                                        <input id="private" name="private" value="1" @if($submittals->private=='1') checked @endif type="checkbox">
                                        <label for="private">Private</label>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <br>
                                    <div class="form-group row">
                                        <label for="private">Draft</label><br>
                                        <input id="draft" name="draft" value="1" @if($submittals->draft=='1') checked @endif type="checkbox">
                                        <label for="draft">Draft</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <br>
                                    <div class="form-group">
                                        <label class="control-label">Status
                                        </label>
                                        <select class="selectpicker form-control" name="status" data-style="form-control" required>
                                            <option value="">Please Select Status</option>
                                            <option value="Open" @if($submittals->status=='Open') selected @endif>Open</option>
                                            <option value="Close" @if($submittals->status=='Close') selected @endif>Close</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Responsible Contractor</label>
                                        <select class="selectpicker form-control" name="rec_contractor" id="user_id">
                                            <option value="">Choose Responsible Contractor</option>
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee->id }}"  @if($submittals->rec_contractor==$employee->id) selected @endif>{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Received From</label>
                                        <select class="selectpicker form-control" name="received_from" id="user_id">
                                            <option value="">Choose Received From</option>
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee->id }}"   @if($submittals->received_from==$employee->id) selected @endif >{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Submit By</label>
                                        <input type="text" name="submitdate" class="form-control datepicker"  value="{{ get_input_date($submittals->submitdate) }}"  autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Issue Date</label>
                                        <input type="text" name="issuedate" class="form-control datepicker"  value="{{ get_input_date($submittals->issuedate) }}"  autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Received Date</label>
                                        <input type="text" name="receiveddate" class="form-control datepicker"  value="{{ get_input_date($submittals->receiveddate) }}"  autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Final Due Date</label>
                                        <input type="text" name="finalduedate" class="form-control datepicker"  value="{{ get_input_date($submittals->finalduedate) }}"  autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Submittals Manager</label>
                                        <select class="selectpicker form-control" name="submittal_manager" id="user_id">
                                            <option value="">Choose Submittals Manager</option>
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee->id }}" @if($submittals->submittal_manager==$employee->id) selected @endif >{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Type
                                        </label>
                                        <select class="selectpicker form-control" name="type" data-style="form-control" required>
                                            <option value="">Please Select Type</option>
                                            <option value="Document" @if($submittals->type=='Document') selected @endif >Document</option>
                                            <option value="Other" @if($submittals->type=='Other') selected @endif >Other</option>
                                            <option value="Plans" @if($submittals->type=='Plans') selected @endif >Plans</option>
                                            <option value="Prints" @if($submittals->type=='Prints') selected @endif >Prints</option>
                                            <option value="Product Information" @if($submittals->type=='Product Information') selected @endif >Product Information</option>
                                            <option value="Product Manual" @if($submittals->type=='Product Manual') selected @endif >Product Manual</option>
                                            <option value="Sample" @if($submittals->type=='Sample') selected @endif >Sample</option>
                                            <option value="Shop Drawing" @if($submittals->type=='Shop Drawing') selected @endif >Shop Drawing</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Location</label>
                                        <input type="text" name="location" class="form-control" value="{{ $submittals->location }}"  placeholder="Location *" required>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="control-label">Distribution List
                                        </label>
                                        <?php $distributionlist = explode(',',$submittals->distribution);?>
                                        <select class="selectpicker form-control" name="distribution[]" data-style="form-control" multiple required>
                                            <option value="">Please Select Distribution</option>
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee->id }}"  @if(in_array($employee->id,$distributionlist)) selected @endif>{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-4">
                                    <div class="form-group">
                                        <label for="project_id"><b>Projects</b></label>
                                        <select class="select2 form-control projectid" id="projectid" name="project_id" data-style="form-control" required>
                                            <option value="">Please Select Project</option>
                                            @foreach($projectlist as $project)
                                                <option value="{{ $project->id }}" @if($submittals->projectid==$project->id) selected @endif >{{ ucwords($project->project_name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-4">
                                    <div class="form-group">
                                        <label for="title_id">Project Title</label>
                                        <select class="select2 form-control titlelist" id="titlelist" name="title_id" data-style="form-control" required>
                                            <option value="">Please Select Title</option>
                                            @if($titlelist)
                                                @foreach($titlelist as $title)
                                                    <option value="{{ $title->id }}" <?php if($submittals->titleid==$title->id){ echo 'selected';}?>>{{ ucwords($title->title) }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-4">
                                    <div class="form-group">
                                        <label for="title_id">@lang('app.task')</label>
                                        <select class="select2 form-control costitemlist" id="costitemlist" name="costitem" data-style="form-control" required>
                                            <option value="">Please Select @lang('app.task')</option>
                                            @if($costitemlist)
                                                @foreach($costitemlist as $costitem)
                                                    <option value="{{ $costitem }}" <?php if($submittals->costitemid==$costitem){ echo 'selected';}?>>{{ ucwords(get_cost_name($costitem)) }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Description</label>
                                        <textarea id="description" name="description" class="form-control summernote">{{ $submittals->description }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <b>Files</b>
                                    <br>
                                    <div class="row" id="list">
                                        <ul class="list-group" id="files-list">
                                            @foreach($files as $file)
                                                <?php
                                                $fx = explode('.', $file->hashname);
                                                $ext = $fx[(count($fx)-1)];
                                                $html5class = '';
                                                if($ext=='jpg'||$ext=='png'||$ext=='jpeg'){
                                                    $html5class = 'html5lightbox';
                                                }
                                                ?>
                                                <li class="list-group-item">
                                                    <div class="row">
                                                        <div class="col-md-9">
                                                            {{ $file->filename }}
                                                        </div>
                                                        <div class="col-md-3 edit-submittals">
                                                            @if($file->external_link != '')
                                                                <a target="_blank" href="{{ $file->external_link }}"
                                                                   data-toggle="tooltip" data-original-title="View"
                                                                   class="btn1 btn-info btn-circle"  {{ $html5class }}><i
                                                                            class="fa fa-search"></i></a>

                                                            @elseif(config('filesystems.default') == 'local')
                                                                <a target="_blank" href="{{ asset_url('submittals-files/'.$submittals->id.'/'.$file->hashname) }}"
                                                                   data-toggle="tooltip" data-original-title="View"
                                                                   class="btn1 btn-info btn-circle"  {{ $html5class }}><i
                                                                            class="fa fa-search"></i></a>

                                                            @elseif(config('filesystems.default') == 's3')
                                                                <a target="_blank" href="{{ $url.'submittals-files/'.$submittals->id.'/'.$file->filename }}"
                                                                   data-toggle="tooltip" data-original-title="View"
                                                                   class="btn1 btn-info btn-circle"  {{ $html5class }}><i
                                                                            class="fa fa-search"></i></a>
                                                            @elseif(config('filesystems.default') == 'google')
                                                                <a target="_blank" href="{{ $file->google_url }}"
                                                                   data-toggle="tooltip" data-original-title="View"
                                                                   class="btn1 btn-info btn-circle"  {{ $html5class }}><i
                                                                            class="fa fa-search"></i></a>
                                                            @elseif(config('filesystems.default') == 'dropbox')
                                                                <a target="_blank" href="{{ $file->dropbox_link }}"
                                                                   data-toggle="tooltip" data-original-title="View"
                                                                   class="btn1 btn-info btn-circle"  {{ $html5class }}><i
                                                                            class="fa fa-search"></i></a>
                                                            @endif

                                                            <a href="javascript:;" data-toggle="tooltip" data-original-title="Delete" onclick="removeFile({{ $file->id }})"
                                                               class="btn1 btn-danger btn-circle" ><i class="fa fa-times"></i></a>
                                                            <span class="cleasubmittalsx m-l-10 detail-month">{{ $file->created_at->diffForHumans() }}</span>
                                                        </div>
                                                    </div>
                                                </li>

                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                                <div class="row m-b-20">
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                        <div id="file-upload-box" >
                                            <div class="row" id="file-dropzone">
                                                <div class="col-md-12">
                                                    <div class="dropzone"
                                                         id="file-upload-dropzone">
                                                        {{ csrf_field() }}
                                                        <div class="fallback">
                                                            <input name="file" type="file" multiple/>
                                                        </div>
                                                        <input name="image_url" id="image_url" type="hidden" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="submittalsID" id="submittalsID">
                                        <input type="hidden" name="submittaltype" value="{{ $submittaltype }}">
                                    </div>
                                </div>
                                <hr>
                                <h2>Submital Schedule Information</h2>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">On Site Date</label>
                                        <input type="text" name="onsitedate" value="{{ get_input_date($submittals->onsitedate) }}" class="form-control datepicker onsitedate" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Lead time</label>
                                        <input type="text" name="leadtime" value="{{ $submittals->leadtime }}" class="form-control leadtime" placeholder="Days " autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Planned Return Date</label>
                                        <input type="text" name="planreturndate" value="{{ get_input_date($submittals->planreturndate) }}" class="form-control datepicker planreturndate" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Design Team review time</label>
                                        <input type="text" name="designreviewtime" value="{{ $submittals->designreviewtime }}" class="form-control designreviewtime" placeholder="Days " autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Planned Internal review completed Date</label>
                                        <input type="text" name="planinternaldate"  value="{{ get_input_date($submittals->planinternaldate) }}" class="form-control datepicker planinternaldate" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Internal review time</label>
                                        <input type="text" name="internalreviewtime"  value="{{  $submittals->internalreviewtime }}" class="form-control internalreviewtime" placeholder="Days " autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Planned Submit Date</label>
                                        <input type="text" name="plansubmitdate"  value="{{ get_input_date($submittals->plansubmitdate) }}" class="form-control datepicker plansubmitdate" autocomplete="off">
                                    </div>
                                </div>
                                <h2>Delivery Information</h2>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Anticipated Delivery Date</label>
                                        <input type="text" name="anticipateddeliverydate"  value="{{ get_input_date($submittals->anticipateddeliverydate) }}" class="form-control datepicker" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Schedule task</label>
                                        <input type="text" name="scheduletask"  value="{{ $submittals->scheduletask }}" class="form-control" placeholder="Schedule task " autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Confirm Delivery Date</label>
                                        <input type="text" name="confirmdeliverydate"  value="{{ get_input_date($submittals->confirmdeliverydate) }}" class="form-control datepicker" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Actual Delivery Date</label>
                                        <input type="text" name="actualdeliverydate"  value="{{ get_input_date($submittals->actualdeliverydate) }}" class="form-control datepicker" autocomplete="off">
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h2>Submittal Workflow</h2>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="javascript:void(0);" class="add-row btn1 btn-primary btn-circle"><i class="fa fa-plus-circle"></i> Add Row</a>
                                    </div>
                                </div>
                                <div id="submittalworkflow">
                                    @if($submittedworkflowarray)
                                        @foreach($submittedworkflowarray as $submittedworkflow)
                                            <div class="form-group" id="workflowrowid{{ $submittedworkflow->id }}">
                                               <div class="row">
                                                   <div class="col-md-3">
                                                       <select class="form-control" name="workflowusers[{{ $submittedworkflow->id }}]" >
                                                           <option value="">Search People</option>
                                                            @foreach($employees as $employee)
                                                               <option value="{{ $employee->id }}" @if($submittedworkflow->user_id==$employee->id) selected @endif>{{ $employee->name }}</option>
                                                            @endforeach
                                                         </select>
                                                       </div>
                                                   <div class="col-md-3">
                                                       <select class="form-control" name="role[{{ $submittedworkflow->id }}]" >
                                                           <option value="">Select Role</option>
                                                           <option value="Approver"  @if($submittedworkflow->role=='Approver') selected @endif >Approver</option>
                                                           <option value="Submitter"  @if($submittedworkflow->role=='Submitter') selected @endif>Submitter</option>
                                                           </select>
                                                       </div>
                                                   <div class="col-md-3">
                                                       <input type="text" class="form-control datepicker" value="{{ get_input_date($submittedworkflow->duedate) }}" name="duedate[{{ $submittedworkflow->id }}]" />
                                                       </div>
                                                   <div class="col-md-3">
                                                       <a href="javascript:void(0);" data-workflow-id="{{ $submittedworkflow->id }}"  class="btn btn-outline btn-success btn-sm removeBlock" ><i class="fa fa-trash-o"></i></a>
                                                       </div>
                                                   </div>
                                               </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <!--/row-->

                        </div>

                        <div class="form-actions">
                            {{ csrf_field() }}
                            <button type="button" id="store-task" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.save')</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>


    <script>

        Dropzone.autoDiscover = false;
        //Dropzone class
        myDropzone = new Dropzone("div#file-upload-dropzone", {
            url: "{{ route('admin.submittals.storeImage') }}",
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks:true,
            parallelUploads:10,
            init: function () {
                myDropzone = this;
            }
        });

        myDropzone.on('sending', function(file, xhr, formData) {
            console.log(myDropzone.getAddedFiles().length,'sending');
            var ids = $('#submittalsID').val();
            formData.append('submittals_id', ids);
        });

        myDropzone.on('completemultiple', function () {
            var msgs = "Submittals Created";
            $.showToastr(msgs, 'success');
            window.location.href = '{{ route('admin.submittals.index') }}'

        });
        $('.summernote').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ["view", ["fullscreen"]]
            ]
        });
        $(".projectid").change(function () {
            var project = $(this).val();
            if(project){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.submittals.projecttitles') }}",
                    data: {'_token': token,'projectid': project},
                    success: function(data){
                        $("select.titlelist").html("");
                        $("select.titlelist").html(data);
                        $('select.titlelist').select2();
                    }
                });
            }
        });
        $("#titlelist").change(function () {
            var project = $("#projectid").select2().val();
            var titlelist = $(this).val();
            if(project){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.submittals.costitembytitle') }}",
                    data: {'_token': token,'projectid': project,'title': titlelist},
                    success: function(data){
                        $("select.costitemlist").html("");
                        $("select.costitemlist").html(data);
                        $('select.costitemlist').select2();
                    }
                });
            }
        });
        $(".leadtime").change(function () {
            var leadtime = $(this).val();
            var onsitedate = $(".onsitedate").datepicker('getDate');
            if(leadtime&&onsitedate){
                onsitedate.setDate(onsitedate.getDate() - leadtime);
                var day = onsitedate.getDate();
                var month = onsitedate.getMonth() + 1;
                var year = onsitedate.getFullYear();
                if (day < 10) {
                    day = "0" + day;
                }
                if (month < 10) {
                    month = "0" + month;
                }
                var date = day + "-" + month + "-" + year;
                $(".planreturndate").datepicker('setDate', date);
            }
        });
        $(".designreviewtime").change(function () {
            var designreviewtime = $(this).val();
            var planreturndate = $(".planreturndate").datepicker('getDate');
            if(designreviewtime&&planreturndate){
                planreturndate.setDate(planreturndate.getDate() - designreviewtime);
                var day = planreturndate.getDate();
                var month = planreturndate.getMonth() + 1;
                var year = planreturndate.getFullYear();
                if (day < 10) {
                    day = "0" + day;
                }
                if (month < 10) {
                    month = "0" + month;
                }
                var date = day + "-" + month + "-" + year;
                $(".planinternaldate").datepicker('setDate', date);
            }
        });
        $(".internalreviewtime").change(function () {
            var internalreviewtime = $(this).val();
            var planinternaldate = $(".planinternaldate").datepicker('getDate');
            if(internalreviewtime&&planinternaldate){
                planinternaldate.setDate(planinternaldate.getDate() - internalreviewtime);
                var day = planinternaldate.getDate();
                var month = planinternaldate.getMonth() + 1;
                var year = planinternaldate.getFullYear();
                if (day < 10) {
                    day = "0" + day;
                }
                if (month < 10) {
                    month = "0" + month;
                }
                var date = day + "-" + month + "-" + year;
                $(".plansubmitdate").datepicker('setDate', date);
            }
        });
        $('#store-task').click(function () {

            $.easyAjax({
                url: '{{route('admin.submittals.updateSubmittals', [$submittals->id])}}',
                container: '#storeTask',
                type: "POST",
                data: $('#storeTask').serialize(),
                success: function(response){
                    if(myDropzone.getQueuedFiles().length > 0){
                        submittalsID = response.submittalsID;
                        $('#submittalsID').val(response.submittalsID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "@lang('Submittals Updated successfully')";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('admin.submittals.index') }}'
                    }
                }
            })

        });

        jQuery('.datepicker').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        var ques = 1;
        $('.add-row').on("click",function () {
            var catid = $(this).data('cat');
            var item = '<div class="form-group">'
                +'<div class="row">'
                +'<div class="col-md-3">'
                +'<select class="form-control" name="workflowusers[]" data-quesid="'+ques+'">'
                +'<option value="">Search People</option>';
            @foreach($employees as $employee)
                item += '<option value="{{ $employee->id }}">{{ $employee->name }}</option>';
            @endforeach
                item += '</select>'
                +'</div>'
                +'<div class="col-md-3">'
                +'<select class="form-control" name="role[]" >'
                +'<option value="">Select Role</option>'
                +'<option value="Approver">Approver</option>'
                +'<option value="Submitter">Submitter</option>'
                +'</select>'
                +'</div>'
                +'<div class="col-md-3">'
                +'<input type="text" class="form-control datepicker" name="duedate[]" />'
                +'</div>'
                +'<div class="col-md-3">'
                +'<a href="javascript:void(0);"  class="btn btn-outline btn-success btn-sm removeBlock" ><i class="fa fa-trash-o"></i></a>'
                +'</div>'
                +'</div>'
                +'</div>';
            $("#submittalworkflow").append(item);
            ques++;
        });
        $(document).on('click','.removeBlock', function () {
            var workflow = $(this).data("workflow-id");
            if(workflow){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.submittals.removeworkflow') }}",
                    data: {'_token': token,'workflowid': workflow},
                    success: function(data){
                        $("#workflowrowid"+workflow).remove();
                    }
                });
            }
        });
    </script>
@endpush

