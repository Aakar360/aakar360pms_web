<?php

use Illuminate\Database\Seeder;

class PaySlipFormatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = \Faker\Factory::create();
        $companies = \App\Company::all();

        $companies->each(function ($company) use ( $faker) {
            for ($i=1;$i<=7;$i++) {
                $format = new \App\PaySlipFormat();
                $format->compony_id = $company->id;
                $format->name = 'format'.$i;
                $format->save();
            }
        });
    }
}
