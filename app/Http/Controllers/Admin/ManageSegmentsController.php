<?php

namespace App\Http\Controllers\Admin;

use App\BoqCategory;
use App\Contractors;
use App\CostItems;
use App\Helper\Reply;
use App\AssetCategory;
use App\Asset;
use App\Project;
use App\ProjectCostItemsProduct;
use App\ProjectMember;
use App\ProjectSegmentsPosition;
use App\ProjectSegmentsProduct;
use App\SubAsset;
use App\Segment;
use App\Task;
use App\Title;
use App\Type;
use App\Units;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ManageSegmentsController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Segments';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'pms';
    } 
    public function index()
    {
        $this->segments = Segment::all();
        $this->projectarray = Project::all();
        return view('admin.segments.index', $this->data);
    }


    public function store(Request $request)
    {
        $segment = new Segment();
        $segment->name = $request->name;
        $segment->projectid = $request->projectid;
        $segment->titleid = $request->titleid;
        $segment->save();
        return Reply::success(__('messages.segmentAdded'));
    }

    public function edit($id)
    {
        $segment =  Segment::find($id);
        $this->segment = $segment;
        $this->projectarray = Project::all();
        $this->titlesarray = !empty($segment->projectid) ? Title::where('project_id',$segment->projectid)->get() : array();
        return view('admin.segments.edit', $this->data);
    }

    public function update(Request $request,$id)
    {
        $segment = Segment::find($id);
        $segment->name = $request->name;
        $segment->projectid = $request->projectid;
        $segment->titleid = $request->titleid;
        $segment->save();

        return Reply::success(__('messages.segmentUpdate'));
    }
    public function destroy($id)
    {
        Segment::destroy($id);
        $categoryData = Segment::all();
        return Reply::successWithData(__('messages.segmentDelete'),['data' => $categoryData]);
    }
    public function Segments(Request $request, $id){

        $user = $this->user;
        $project = Project::where('company_id',$user->company_id)->get()->pluck('id')->toArray();
        $pm = ProjectMember::where('user_id',$user->id)->get()->pluck('project_id')->toArray();
        $prarray = array_filter(array_merge($project,$pm));
        $this->projectlist = Project::whereIn('id', $prarray)->get();
        $this->user = $user;
        if(is_numeric($id)){
            $this->project = Project::with(['tasks' => function($query) use($request){
                if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                    $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
                }
                if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                    $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
                }
            }])->find($id);
            $this->titles = Title::where('project_id',$id)->get();
            $this->id = $id;
        }else{
            $this->titles = array();
            $this->id = 'all';
        }
        return view('admin.projects.segments', $this->data);
    }
    public function segmentsTitle(Request $request, $id,$title=false,$segment=false){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }
            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $title = $title ?: 0;
        $user = $this->user;
        $segmentsarray = Segment::where('projectid',$id)->where('titleid',$title)->get();
        $this->segmentsarray = $segmentsarray;
        $this->userarray = User::all();
        $this->title = (int)$title;
        if(empty($segment)){
            $segment = !empty($segmentsarray[0]) ?  $segmentsarray[0]->id : '';
        }
        $this->segment = (int)$segment;
        $this->id = (int)$id;
        $columsarray = array(
            'costitem'=>'Cost item',
            'description'=>'Description',
            'assign_to'=>'Assign to',
            'contractor'=>'Contractor',
            'startdate'=>'Start date',
            'enddate'=>'End date',
            'totalqty'=>'BOQ Qty',
            'qty'=>'Segment Qty',
            'rate'=>'Rate',
            'unit'=>'Unit',
            'finalrate'=>'Final rate',
            'totalamount'=>'Total amount'
        );
        $col =1; foreach($columsarray as $colkey => $colums){
            $postitionarray = ProjectSegmentsPosition::where('project_id',$id)->where('title',$title)->where('segment',$segment)->where('itemslug',$colkey)->where('position','col')->first();
            if(empty($postitionarray->id)){
                $columsarray = new ProjectSegmentsPosition();
                $columsarray->project_id = $id;
                $columsarray->title = $title;
                $columsarray->segment = $segment;
                $columsarray->position = 'col';
                $columsarray->itemid = 0;
                $columsarray->itemname = $colums;
                $columsarray->itemslug = $colkey;
                $columsarray->collock = 0;
                $columsarray->level = 0;
                $columsarray->catlevel = '';
                $columsarray->inc = $col;
                $columsarray->save();
                $col++;
            }
        }
        $categoryarray = DB::table('project_segments_product')
            ->select( DB::raw("(GROUP_CONCAT(category SEPARATOR ',')) as `category`"))
            ->where('project_id',$id)
            ->where('title',$title)
            ->where('segment',$segment)
            ->first();
        $categoryarray = array_unique(array_filter(explode(',',$categoryarray->category)));
        $boqlevel1categories = BoqCategory::whereIn('id',$categoryarray)->orderby('id','asc')->get();
        $col =1; foreach($boqlevel1categories as  $colums){
            $prevcol = ProjectSegmentsPosition::where('project_id',$id)->where('title',$title)->where('segment',$segment)->where('itemid',$colums->id)->first();
            if(empty($prevcol->id)){
                $columsarray = new ProjectSegmentsPosition();
                $columsarray->project_id = $id;
                $columsarray->title = $title;
                $columsarray->segment = $segment;
                $columsarray->position = 'row';
                $columsarray->itemid = $colums->id;
                $columsarray->itemname = $colums->title;
                $columsarray->itemslug = '';
                $columsarray->collock = 0;
                $columsarray->parent = 0;
                $columsarray->catlevel = '';
                if($colums->parent==0){
                    $columsarray->level = 0;
                    $columsarray->parent = 0;
                }else{
                    $columsarray->level = 1;
                    $columsarray->parent = $colums->parent;
                }
                $columsarray->inc = $col;
                $columsarray->save();
                $col++;
            }
        }
        $this->categories = BoqCategory::get();
        $this->unitsarray = Units::get();
        $this->typesarray = Type::get();
        $this->contractorarray =  User::getAllContractors($user);
        return view('admin.projects.segmentstitlenew', $this->data);
    }
    public function addBoqCostItemRow(Request $request){

        if(empty($request->segment)){
            return array('status'=>false,'message'=>'Please Select Segments');
        }
        $user = $this->user;
        $maxcostitemid = ProjectSegmentsProduct::where('title',$request->titleid)->where('project_id',$request->projectid)->where('segment',$request->segment)->max('inc');
        $newid = $maxcostitemid+1;
        $costitemproduct = $request->itemid;
        $costitemslist = \App\ProjectCostItemsProduct::where('title',$request->titleid)->where('project_id',$request->projectid)->orderBy("id",'asc')->get();
        $contractorarray =  User::getAllContractors($user);
        $userarray = \App\User::orderBy("id",'asc')->get();
        $unitsarray = \App\Units::orderBy("id",'asc')->get();
        $costitem = ProjectCostItemsProduct::find($costitemproduct);
        $proprogetdat = new ProjectSegmentsProduct();
        $proprogetdat->title = $request->titleid;
        $proprogetdat->project_id = $request->projectid;
        $proprogetdat->segment = $request->segment;
        $proprogetdat->category = $request->category;
        $proprogetdat->cost_items_product_id = $costitem->id;
        $proprogetdat->cost_items_id = $costitem->cost_items_id;
        $proprogetdat->unit = get_cost_item_unit_name($costitem->cost_items_id);
        $proprogetdat->description = $costitem->description;
        $proprogetdat->assign_to = $costitem->assign_to;
        $proprogetdat->rate = $costitem->rate;
        $proprogetdat->start_date = date('Y-m-d');
        $proprogetdat->deadline = date('Y-m-d');
        $proprogetdat->totalqty = $costitem->qty;
        $proprogetdat->inc = $newid;
        $proprogetdat->save();

        $catvalue =  explode(',',$request->category);
        $catitem = !empty($catvalue[1]) ? $catvalue[1] : $catvalue[0];
        $contenthtml = '';
        $colpositionarray = \App\ProjectSegmentsPosition::where('project_id',$request->projectid)->where('title',$request->titleid)->where('segment',$request->segment)->where('position','col')->orderBy('inc','asc')->get();
        $contenthtml .= '<tr data-depth="2" class="collpse level2 catrow'.$proprogetdat->category.'">
                           <td><a href="javascript:void(0);"  class="red sa-params" data-toggle="tooltip" data-costitem-id="' . $proprogetdat->id . '" data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>';
        if($request->cattype=='single'){
            $contenthtml .= '<td colspan="3" class="text-right"><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-minus-circle"></i> </a></td>';
        }else{
            $contenthtml .= '<td colspan="3" class="text-right"><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-minus-circle"></i> </a></td>';
        }
        foreach ($colpositionarray as $colposition) {
            switch ($colposition->itemslug) {
                case  'costitem':
                    $contenthtml .= '<td><input data-itemid="' . $proprogetdat->id . '"  class="cell-inp updateproject" data-item="costitem" list="costitem' . $proprogetdat->id . '"  value="' . get_cost_name($proprogetdat->cost_items_id) . '">
                                         <datalist id="costitem' . $proprogetdat->id . '">';
                    foreach ($costitemslist as $costitem) {
                        $contenthtml .= '<option data-value="' . $costitem->id . '" >' . get_cost_name($costitem->cost_items_id) . '</option>';
                    }
                    $contenthtml .= '</datalist>
                                     </td>';
                    break;
                case 'description':
                    $contenthtml .= '<td><textarea data-itemid="' . $proprogetdat->id . '" data-item="description" onkeydown="textAreaAdjust(this)" style="height: 25px;" class="cell-inp updateproject" >' .  $proprogetdat->description . '</textarea></td>';
                    break;
                case 'assign_to':
                    $contenthtml .= '<td><input  class="cell-inp updateproject" data-item="assign_to" data-itemid="' . $proprogetdat->id . '" data-cat="' . $catitem . '"  list="assign'.$proprogetdat->id.'"  value="'.get_user_name($proprogetdat->assign_to).'">
                                               <datalist class="assignlist"  id="assign'.$proprogetdat->id.'">';
                    foreach($userarray as $useras){
                        $contenthtml .= '<option  value="'.$useras->id.'"  data-value="'.$useras->name.'"  >'.$useras->name.'</option>';
                    }
                    $contenthtml .= '</datalist></td>';
                    break;
                case 'contractor':
                    $contenthtml .= '<td><input  class="cell-inp updateproject" data-item="contractor" data-itemid="' . $proprogetdat->id . '" data-cat="' . $catitem . '"  list="contractorid'.$proprogetdat->id.'"  value="'.get_user_name($proprogetdat->contractor).'">
                                               <datalist class="contractorlist"  id="contractorid'.$proprogetdat->id.'">';
                    foreach($contractorarray as $contractor){
                        $contenthtml .= '<option  value="'.$contractor->id.'"  data-value="'.$contractor->name.'" >'.$contractor->name.'</option>';
                    }
                    $contenthtml .= '</datalist></td>';
                    break;
                case 'startdate':
                    $contenthtml .= '<td><input data-itemid="' . $proprogetdat->id . '" data-item="start_date" type="text" class="cell-inp datepicker updateproject" name="startdate" value="'.date('d-m-Y').'"  ></td>';
                    break;
                case 'enddate':
                    $contenthtml .= '<td><input data-itemid="' . $proprogetdat->id . '" data-item="deadline" type="text" class="cell-inp datepicker updateproject" name="deadline" value="'.date('d-m-Y').'"  ></td>';
                    break;
                case 'rate':
                    $contenthtml .= '<td><input data-itemid="' . $proprogetdat->id . '" data-item="rate" type="text" class="cell-inp updateproject ratevalue' . $proprogetdat->id . '"  data-cat="' . $catitem . '" name="rate" value="' .  $proprogetdat->rate . '"  ></td>';
                    break;
                case 'unit':
                    $contenthtml .= '<td>
                               <input  data-itemid="' . $proprogetdat->id . '" data-item="unit" class="cell-inp updateproject" list="unitdata' . $proprogetdat->id . '"  value="' . get_unit_name($proprogetdat->unit) . '">
                                   <datalist id="unitdata' . $proprogetdat->id . '">';
                    foreach ($unitsarray as $units) {
                        $contenthtml .= '<option data-value="' . $units->id . '" >' . $units->name . '</option>';
                    }
                    $contenthtml .= '</datalist>
                                     </td>';
                    break;
                case 'qty':
                    $contenthtml .= '<td><input data-itemid="' . $proprogetdat->id . '" data-item="qty" type="text" class="cell-inp updateproject qty' . $proprogetdat->id . '"   value="' . $proprogetdat->qty. '" ></td>';
                    break;
                case 'totalqty':
                    $contenthtml .= '<td><input data-itemid="' . $proprogetdat->id . '" data-item="totalqty" type="text" class="cell-inp updateproject totalqty' . $proprogetdat->id . '"   value="' . $proprogetdat->totalqty. '"  ></td>';
                    break;
                case 'finalrate':
                    $contenthtml .= '<td><input type="text" data-item="finalrate" data-itemid="' . $proprogetdat->id . '" class="cell-inp updateproject totalrate' . $proprogetdat->id . ' finalrate' . $catitem . '"  data-cat="' . $catitem . '" name="finalrate" ></td>';
                    break;
                case 'totalamount':
                    $contenthtml .= '<td><input type="text" data-item="totalamount" data-itemid="' . $proprogetdat->id . '" class="cell-inp updateproject grandvalue totalamount' . $proprogetdat->id . ' finalamount' . $catitem . '"  data-cat="' . $catitem . '" name="finalamount" ></td>';
                    break;
            }
        }
        $contenthtml .= '</tr>';
        return array('status'=>true,'message'=>$contenthtml);
    }
    public function costitemDestroy($id)
    {
        ProjectSegmentsProduct::where('id',$id)->delete();
        return Reply::success(__('messages.taskDelete'));

    }
    public function costitemCatDestroy($id)
    {
        $position = ProjectSegmentsPosition::where('id',$id)->first();
        if(!empty($position->id)){
            $category = '';
            if(!empty($position->parent)){
                $category .= $position->parent;
                $category .= ',';
            }
            $category .= $position->itemid;
            ProjectSegmentsProduct::where('category',$category)->where('project_id',$position->project_id)->where('title',$position->title)->where('segment',$position->segment)->delete();
            $position->delete();
            return Reply::success(__('messages.taskDelete'));
        }
    }
    public function addBoqCostItemCategory(Request $request){

        if(empty($request->segment)){
            return array('status'=>false,'message'=>'Please Select Segments');
        }
        $parent = $request->parent ?: 0;
        $maxpositionarray = \App\ProjectSegmentsPosition::where('project_id',$request->projectid)->where('title',$request->titleid)->where('segment',$request->segment)->where('position','row')->where('level',$request->level)->orderBy('inc','desc')->first();
        $newid = !empty($maxpositionarray->inc) ? $maxpositionarray->inc+1 : 1;
        $columsarray = new ProjectSegmentsPosition();
        $columsarray->project_id = $request->projectid;
        $columsarray->title = $request->titleid;
        $columsarray->segment = $request->segment;
        $columsarray->position = 'row';
        $columsarray->itemid = $request->catid;
        $columsarray->itemname = $request->category;
        $columsarray->level = $request->level;
        $columsarray->catlevel = $request->catlevel;
        $columsarray->itemslug = '';
        $columsarray->collock = 0;
        $columsarray->parent = $parent;
        $columsarray->inc = $newid;
        $columsarray->save();
        $contenthtml = '';
        if($request->level==0){
            $contenthtml .= '<tr  data-depth="0" class="collpse level0">
                        <td><a href="javascript:void(0);"  class="red sa-params-cat" data-toggle="tooltip" data-position-id="'.$columsarray->id.'" data-level="0" data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>
                        <td colspan="4"><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-plus-circle"></i></a>';
        }
        if($request->level==1){
            $contenthtml .= '<tr  data-depth="1" class="collpse level1">
                 <td><a href="javascript:void(0);"  class="red sa-params-cat" data-toggle="tooltip" data-position-id="'.$columsarray->id.'" data-level="0" data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>
                 <td colspan="4"><a href="javascript:void(0);" class="opntoggle iconcode"><i class="fa fa-plus-circle"></i> </a>';
        }
        $contenthtml .= $request->category.'</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td> 
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>';
        return array('status'=>true,'message'=>$contenthtml);
    }
    public function projectBoqCreate(Request $request, $id){
        $this->project = Project::with(['tasks' => function($query) use($request){
            if ($request->startDate !== null && $request->startDate != 'null' && $request->startDate != '') {
                $query->where(DB::raw('DATE(`start_date`)'), '>=', Carbon::createFromFormat('Y-m-d', $request->startDate));
            }

            if ($request->endDate !== null && $request->endDate != 'null' && $request->endDate != '') {
                $query->where(DB::raw('DATE(`due_date`)'), '<=', Carbon::createFromFormat('Y-m-d', $request->endDate));
            }
        }])->find($id);
        $this->categories = BoqCategory::where('parent','0')->get();
        $this->id = $id;
        return view('admin.projects.project-boq-create', $this->data);
    }
    public  function boqChangeColPosition(Request $request){
        if(empty($request->segment)){
            return array('status'=>false,'message'=>'Please Select Segments');
        }
        $positionarray = $request->position;
        $projectid = $request->projectid;
        $title = $request->title;
        $segment = $request->segment;
        if(!empty($positionarray)){
            $x=1; foreach ($positionarray as $position){
                ProjectSegmentsPosition::where('project_id',$projectid)->where('title',$title)->where('segment',$segment)->where('id',$position)->update(['inc'=>$x]);
                $x++; }
        }
    }
    public  function boqChangeCatPosition(Request $request){
        if(empty($request->segment)){
            return array('status'=>false,'message'=>'Please Select Segments');
        }
        $positionarray = $request->position;
        $projectid = $request->projectid;
        $title = $request->title;
        $segment = $request->segment;
        if(!empty($positionarray)){
            $x=1; foreach ($positionarray as $position){
                ProjectSegmentsPosition::where('project_id',$projectid)->where('title',$title)->where('segment',$segment)->where('id',$position)->update(['inc'=>$x]);
                $x++; }
        }
    }
    public  function boqChangeCostitemPosition(Request $request){
        if(empty($request->segment)){
            return array('status'=>false,'message'=>'Please Select Segments');
        }
        $positionarray = $request->position;
        $projectid = $request->projectid;
        $title = $request->title;
        $segment = $request->segment;
        if(!empty($positionarray)){
            $x=1; foreach ($positionarray as $position){
                ProjectSegmentsProduct::where('project_id',$projectid)->where('title',$title)->where('segment',$segment)->where('id',$position)->update(['inc'=>$x]);
                $x++; }
        }
    }
    public  function projectBoqLock(Request $request){
        if(empty($request->segment)){
            return array('status'=>false,'message'=>'Please Select Segments');
        }
        $positionarray = $request->lockcolms;
        $projectid = $request->projectid;
        $title = $request->title;
        $segment = $request->segment;
        if(!empty($positionarray)&&count($positionarray)>0){
            ProjectSegmentsPosition::where('project_id',$projectid)->where('title',$title)->where('segment',$segment)->update(['collock'=>0]);
            foreach ($positionarray as $key => $position){
                ProjectSegmentsPosition::where('project_id',$projectid)->where('title',$title)->where('segment',$segment)->where('id',$key)->update(['collock'=>1]);
            }
        }else{
            ProjectSegmentsPosition::where('project_id',$projectid)->where('title',$title)->where('segment',$segment)->update(['collock'=>0]);
        }
        return redirect(url('admin/projects/segments/'.$projectid.'/'.$title.'/'.$segment));
    }
    public  function updateCostItem(Request $request){
        if(empty($request->segment)){
            return array('status'=>false,'message'=>'Please Select Segments');
        }
        $user = $this->user;
        $item = $request->item;
        $itemid = $request->itemid;
        $itemvalue = $request->itemvalue;
        $projectid = $request->projectid;
        $title = $request->title;
        $segment = $request->segment;
        $dataarray = array();
        $dataarray[$item] = $itemvalue;
        switch($item){
            case 'cost_item_id':
                $costitem = CostItems::where('cost_item_name',$itemvalue)->first();
                $dataarray[$item] = $costitem->id;
                break;
            case 'unit':
                $costitem = Units::where('name',$itemvalue)->first();
                $dataarray[$item] = $costitem->id;
                break;
            case 'start_date':
                $dataarray[$item] = date('Y-m-d',strtotime($itemvalue));
                break;
            case 'deadline':
                $dataarray[$item] = date('Y-m-d',strtotime($itemvalue));
                break;
            case 'worktype':
                $worktype = Type::where('title',$itemvalue)->first();
                if(!empty($worktype->id)){
                    $dataarray[$item] = $worktype->id;
                }
                break;
        }
        ProjectSegmentsProduct::where('project_id',$projectid)->where('title',$title)->where('segment',$segment)->where('id',$itemid)->update($dataarray);
        $projectcostitem = ProjectSegmentsProduct::where('project_id',$projectid)->where('title',$title)->where('segment',$segment)->where('id',$itemid)->first();
        $marktype = $projectcostitem->markuptype;
        $markupvalue = $projectcostitem->markupvalue;
        $rate = $projectcostitem->rate;
        $qty = $projectcostitem->qty;
        $adjustment = $projectcostitem->adjustment ?: 0;
        $finalrate = $rate;
        $totalamount = 0;
        if(!empty($qty)&&!empty($rate)){
            $totalamount = $qty*$rate;
            $projectcostitem->finalrate = $rate;
            $projectcostitem->finalamount = $totalamount;
        }
        if(!empty($marktype)&&!empty($markupvalue)){
            if($marktype=='percent'){
                $percent = ($markupvalue/100)*$rate;
                $amount = $rate+$percent;
                $finalrate = $amount*$qty;
            }
            if($marktype=='amount'){
                $percent =$markupvalue;
                $amount = $rate+$percent;
                $finalrate = $amount*$qty;
            }
            $totalamount = $finalrate-$adjustment;
            $projectcostitem->finalrate = $finalrate;
            $projectcostitem->finalamount = $totalamount;
        }
        $projectcostitem->save();
    }
}
