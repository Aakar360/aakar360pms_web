<?php

namespace App\Http\Controllers\Api;

use App\AppProject;
use App\BoqCategory;
use App\Company;
use App\Contractors;
use App\CostItems;
use App\Designation;
use App\Employee;
use App\EmployeeDetails;
use App\EmployeeDocs;
use App\EmployeeSkill;
use App\Helper\Files;
use App\Helper\Reply;
use App\Http\Controllers\Controller;
use App\ModuleSetting;
use App\Notifications\NewClientTask;
use App\Notifications\NewTask;
use App\Package;
use App\PermissionRole;
use App\ProjectActivity;
use App\ProjectAttachmentDesigns;
use App\ProjectAttachmentFiles;
use App\ProjectCostItemsFinalQty;
use App\ProjectCostItemsPosition;
use App\ProjectCostItemsProduct;
use App\ProjectMember;
use App\ProjectMilestone;
use App\ProjectPermission;
use App\ProjectTemplate;
use App\ProjectTimeLog;
use App\Rfi;
use App\Role;
use App\Skill;
use App\Task;
use App\TaskboardColumn;
use App\TaskFile;
use App\Team;
use App\Title;
use App\Traits\ProjectProgress;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\FileManager;
use App\Project;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use DB;

class AppEmployeeController extends Controller
{
    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }

    protected function validatetToken($apikey,$token)
    {
        if (isset($apikey)) {
            if (!$this->validateAPI($apikey)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            $response = array();
            if(isset($token)) {
                if (!empty($token)) {
                    $user = User::where('appid', $token)->first();
                    if($user === null){
                        $response['message'] = 'Invalid token';
                        $response['status'] = 300;
                        return $response;
                    }
                    $response['user'] = $user;
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    return $response;
                }
                $response['message'] = 'Api Token cannot be empty';
                $response['status'] = 300;
                return $response;
            }
            $response['message'] = 'Api Token not sent';
            $response['status'] = 300;
            return $response;

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function __construct(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];

            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', $user->company_id)->first();

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function employeeRegister(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $projectid = $request->project_id;
                $sharetoproject = $request->sharetoproject;
                $name = $request->name;
                $email = $request->email;
                $mobile = $request->mobile;
                $user_type = $request->user_type;
                $password = trim($request->password);
                $gender = $request->gender;
                $address = $request->address;
                $fcm = $request->fcm;
                if(!checkMobile($mobile)){
                    $mobile = str_replace(" ", "", $mobile);
                    $mobile = str_replace("-", "", $mobile);
                    $len = strlen($mobile);
                    $c = $len - 10;
                    if($c){
                        $mobile = substr($mobile, $c, 10);
                    }
                }
                if(!empty($email)&&!checkEmail($email)){
                    $response['message'] = 'Invalid email. try again';
                    $response['status'] = 300;
                    return $response;
                }
                if(!empty($projectid)){
                    $projectdetails = AppProject::where('id',$projectid)->first();
                    $usercompany = $projectdetails->company_id;
                }else{
                    $usercompany = $user->company_id;
                }
                $users = Employee::where('user_type',$user_type)->where('mobile',$mobile)->where('company_id',$usercompany)->first();
                if(empty($users->id)){
                    $alruser = User::withoutGlobalScope('company')->withoutGlobalScope('active')->where('mobile',$mobile)->orWhere('email',$email)->first();
                    if(empty($alruser->id)){
                        $alruser = new User();
                        $alruser->name = $name;
                        $alruser->email = $email ? : '';
                        $alruser->mobile = $mobile;
                        $alruser->login = 'disable';
                        $alruser->save();
                    }
                    $users = new Employee();
                    $users->company_id = $usercompany;
                    $users->user_id = $alruser->id;
                    $prevmem = Employee::where('mobile',$mobile)->where('company_id',$usercompany)->first();
                    if(!empty($prevmem->id)){
                        $users->name = $prevmem->name;
                    }else{
                        $users->name = $name;
                    }
                    $users->email = $email ? : '';
                    $users->mobile = $mobile;
                    if($password){
                        $users->password = bcrypt($password);
                    }
                    $users->gender = $gender ?: '';
                    $users->status = 'active';
                    $users->user_type = $user_type;
                    $users->added_by = $user->id;
                    $users->save();
                }

                if(!empty($projectid)){
                    $prevproject = ProjectMember::where('user_id',$users->user_id)->where('employee_id',$users->id)->where('project_id',$projectid)->first();
                    if(empty($prevproject->id)){
                        $alreuser = User::withoutGlobalScope('company')->withoutGlobalScope('active')->find($users->user_id);
                        $projectdetails = AppProject::where('id',$projectid)->first();
                        $level = 0;
                        if($projectdetails->added_by==$user->id){
                            $level = 1;
                        }else{
                            $prevproject = ProjectMember::where('user_id',$user->id)->where('project_id',$projectdetails->id)->first();
                            $prolevel = !empty($prevproject) ? $prevproject->level : 0;
                            $level = $prolevel+1;
                        }
                        $pm = new ProjectMember();
                        $pm->company_id = $projectdetails->company_id;
                        $pm->project_id = $projectdetails->id;
                        $pm->user_id = $users->user_id;
                        $pm->employee_id = $users->id;
                        $pm->user_type = $user_type;
                        $pm->assigned_by = $user->id;
                        $pm->share_project = '0';
                        $pm->level = $level;
                        if($sharetoproject=='1'){
                            $pm->share_project = '1';
                            if(!empty($alreuser)&&!empty($alreuser->fcm)){
                                $notifmessage['title'] = 'Project Shared';
                                $notifmessage['body'] = 'You have been added to ' . ucwords(get_project_name($projectid)) . ' project by ' . $user->name;
                                $notifmessage['activity'] = 'projects';
                                sendFcmNotification($alreuser->fcm, $notifmessage);
                            }
                        }
                        $pm->save();
                        /* $urole = Role::where('name', $user_type)->where('company_id', $users->company_id)->first();
                         if($urole === null){
                             $response['message'] = 'The company invited you do not have user role type '.$user_type;
                             $response['status'] = '401';
                             return $response;
                         }*/
                        /* comment by santosh
                         * to disable auto permission applicable for employee
                         *  $perms = ProjectPermission::where('user_id', $user->id)->where('project_id', $projectid)->get()->pluck('permission_id');
                         foreach ($perms as $perm) {
                             $pprm = new ProjectPermission();
                             $pprm->project_id = $projectid;
                             $pprm->user_id = $users->user_id;
                             $pprm->permission_id = $perm;
                             $pprm->save();
                         }*/
                        $response['status'] = 200;
                        $response['message'] = 'User Assigned To Project';
                        return $response;
                    }else{
                        $response['status'] = 300;
                        $response['message'] = 'User already assigned to project';
                        return $response;
                    }
                }
                $payload['message'] = 'success';
                $payload['status'] = 200;
                $payload['response'] = $users;
            } catch (\Exception $e) {
                $userid = 0;
                app_log($e,'employee-register',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }else {
            $payload['message'] = 'Unauthorised';
            $payload['status'] = '401';
        }
        return $payload;
    }
    public function employeeList(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $proData = $projectusers= $employeeusers = array();
                $projectid = $request->project_id ? $request->project_id : '';
                $user_type = $request->user_type ?: 'employee';
                $searchkey = $request->search;
                $projectdetails = AppProject::find($projectid);
                if(!empty($projectdetails->id)){
                    $companyid = $projectdetails->company_id;
                }else{
                    $companyid = $user->company_id;
                }
              /*  $projectusers = Employee::join('project_members','project_members.employee_id','=','employee.id')
                    ->where('employee.user_type', $user_type)
                    ->whereIn('project_members.project_id', $projectid)->groupBy('employee.user_id');
                if($searchkey){
                    $projectusers = $projectusers->where(function($q) use ($searchkey) {
                        $q->where('employee.mobile','like','%'.$searchkey.'%')
                            ->orWhere('employee.name','like','%'.$searchkey.'%')
                            ->orWhere('employee.email','like','%'.$searchkey.'%');
                    });
                }
                $projectusers = $projectusers->pluck('project_members.employee_id')->toArray();
                $users = array_unique(array_merge($projectusers,$employeeusers));
                */
                $employeeusers = Employee::where('company_id', $companyid);
                if(!empty($searchkey)){
                    $employeeusers = $employeeusers->where(function($q) use ($searchkey) {
                        $q->where('mobile','like','%'.$searchkey.'%')
                            ->orWhere('name','like','%'.$searchkey.'%')
                            ->orWhere('email','like','%'.$searchkey.'%');
                    });
                }else{
                    $employeeusers = $employeeusers->where('user_type', $user_type);
                }
                $empusers = $employeeusers->groupBy('mobile')->orderBy('user_type')->pluck('id')->toArray();
                $page = $request->page;
                $response = array();
                if($page=='all'){
                    $users = Employee::whereIn('id',$empusers)->get();
                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    $users = Employee::whereIn('id',$empusers)->orderBy('name','asc')->offset($skip)->take($count)->get();
                }
                foreach ($users as $emuser) {
                    $addedtoproject = '0';
                    $sharedtoproject = '0';

                    if(!empty($projectid)){
                        $projectmembers  = ProjectMember::where('employee_id',$emuser->id)->where('user_id',$emuser->user_id)->where('project_id', $projectid)->where('user_type', $user_type)->first();
                        if(!empty($projectmembers)){
                            $addedtoproject = '1';
                            if($projectmembers->share_project=='1'){
                                $sharedtoproject = '1';
                            }
                        }
                    }
                    $proData[] = array(
                        'id' => $emuser->id,
                        'company_id' => $emuser->company_id,
                        'user_id' => $emuser->user_id,
                        'name' => $emuser->name,
                        'email' => $emuser->email,
                        'mobile' => $emuser->mobile,
                        'image' => $emuser->image,
                        'user_type' => $emuser->user_type,
                        'image_path' => get_employee_image_link($emuser->id),
                        'addedtoproject' => $addedtoproject,
                        'sharedtoproject' => $sharedtoproject
                    );
                }
                $response['status'] = 200;
                $response['message'] = 'Users List Fetched';
                $response['response'] = $proData;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'employee-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function projectEmployeeList(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $proData = array();
                $projectid = $request->project_id ? explode(',',$request->project_id) : array();
                $user_type = $request->user_type ?: 'employee';
                $searchkey = $request->search;
                $page = $request->page;
                if($page=='all'){

                    $users = Employee::join('project_members','project_members.employee_id','=','employee.id')
                        ->select('employee.*')->where('employee.user_type', $user_type)
                        ->whereIn('project_members.project_id', $projectid)->groupBy('employee.user_id')->orderBy('employee.name','asc')->get();

                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }

                    $users = Employee::join('project_members','project_members.employee_id','=','employee.id')
                        ->select('employee.*')->where('employee.user_type', $user_type)
                        ->whereIn('project_members.project_id', $projectid)->groupBy('employee.user_id')->orderBy('employee.name','asc')->offset($skip)->take($count)->get();

                }

                foreach ($users as $emuser) {
                    $addedtoproject = '0';
                    $sharedtoproject = '0';

                    if(!empty($projectid)){
                        $projectmembers  = ProjectMember::where('employee_id',$emuser->id)->where('user_id',$emuser->user_id)->whereIn('project_id', $projectid)->first();
                        if(!empty($projectmembers)){
                            $addedtoproject = '1';
                            if($projectmembers->share_project=='1'){
                                $sharedtoproject = '1';
                            }
                        }
                    }
                    $proData[] = array(
                        'id' => $emuser->id,
                        'company_id' => $emuser->company_id,
                        'user_id' => $emuser->user_id,
                        'name' => $emuser->name,
                        'email' => $emuser->email,
                        'mobile' => $emuser->mobile,
                        'image' => $emuser->image,
                        'user_type' => $emuser->user_type,
                        'image_path' => get_employee_image_link($emuser->id),
                        'addedtoproject' => $addedtoproject,
                        'sharedtoproject' => $sharedtoproject
                    );
                }
                $response['status'] = 200;
                $response['message'] = 'Users List Fetched';
                $response['response'] = $proData;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'project-employee-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function addEmployeetoProject(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $proData = array();
                $employeeid = $request->employee_id;
                $projectid = $request->project_id;
                $user_type = $request->user_type;
                $sharetoproject = $request->sharetoproject;
                $employee = Employee::where('id', $employeeid)->first();
                if(empty($employee->user_id)){
                    $response['status'] = 300;
                    $response['message'] = 'User Not found';
                    return $response;
                }
                $prevproject = ProjectMember::where('user_id',$employee->user_id)->where('company_id',$user->company_id)->where('project_id',$projectid)->first();
                if(empty($prevproject->id)){
                    $pm = new ProjectMember();
                    $pm->company_id = $user->company_id;
                    $pm->project_id = $projectid;
                    $pm->user_id = $employee->user_id;
                    $pm->employee_id = $employee->id;
                    $pm->user_type = $user_type;
                    $pm->assigned_by = $user->id;
                    $pm->share_project = '0';
                    if($sharetoproject=='1'){
                        $pm->share_project = '1';
                    }
                    $pm->save();
                    $urole = Role::where('name', $user_type)->where('company_id', $user->company_id)->first();
                    if($urole === null){
                        $response['message'] = 'The company invited you do not have user role type '.$user_type;
                        $response['status'] = '401';
                        return $response;
                    }
                    $perms = PermissionRole::where('role_id', $urole->id)->get()->pluck('permission_id');
                    foreach ($perms as $perm) {
                        $pprm = new ProjectPermission();
                        $pprm->project_id = $projectid;
                        $pprm->user_id = $employee->user_id;
                        $pprm->permission_id = $perm;
                        $pprm->save();
                    }
                    $response['status'] = 200;
                    $response['message'] = 'User Assigned To Project';
                    return $response;
                }else{
                    $response['status'] = 300;
                    $response['message'] = 'User already assigned to project';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'add-employee-to-project',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function shareProject(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $employeeid = $request->employee_id;
                $employee = Employee::find($employeeid);
                if(empty($employee->id)){
                    $response['status'] = 300;
                    $response['message'] = 'Employee Not Found';
                    return $response;
                }
                $sharetoproject = $request->sharetoproject;
                $projectmember = ProjectMember::where('project_id',$request->project_id)->where('user_id',$employee->user_id)->where('company_id',$employee->company_id)->first();
                if(!empty($projectmember)){
                    $projectmember->share_project = 0;
                    if($sharetoproject=='1'){
                        $projectmember->share_project = '1';
                    }
                    $projectmember->save();

                    $response['status'] = 200;
                    $response['message'] = 'Project Shared Successfully';
                    return $response;
                }
                $response['status'] = 300;
                $response['message'] = 'Member not found';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'share-project',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function employeeDetails(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = $employeinfo = array();
                $employeeid = $request->employee_id;
                $user_type = $request->user_type;
                $employee = Employee::find($employeeid);
                $employeedetails = EmployeeDetails::where('employee_id',$employeeid)->first();
                $firmname = '';
                $landline = '';
                $gst_no = '';
                if($user_type=='contractor'){
                    $contractordetails = Contractors::where('company_id',$employee->company_id)->where('user_id',$employee->user_id)->first();
                    if(!empty($contractordetails->id)){
                        $firmname = $contractordetails->firmname;
                        $landline = $contractordetails->landline;
                        $gst_no = $contractordetails->gst_no;
                    }
                }

                $empdetails = array(
                    'id' => $employee->id,
                    'company_id' => $employee->company_id,
                    'user_id' => $employee->user_id,
                    'name' => $employee->name,
                    'email' => $employee->email,
                    'mobile' => $employee->mobile,
                    'image' => $employee->image,
                    'user_type' => $employee->user_type,
                    'image_path' => get_employee_image_link($employee->id),
                    'gender' => $employee->gender,
                    'address' => !empty($employeedetails) ? $employeedetails->address : '',
                    'unique_id' => !empty($employeedetails) ? $employeedetails->unique_id : '',
                    'firmname' => $firmname,
                    'landline' => $landline,
                    'gst_no' => $gst_no,
                );
                $employeinfo['employee'] = $empdetails;
                $employeinfo['employeeskills'] = EmployeeSkill::where('employee_id',$employeeid)->get();
                $employeinfo['employeedocuments'] = EmployeeDocs::where('employee_id',$employeeid)->get();
                $employeinfo['skills'] = Skill::all();
                $employeinfo['teams'] = Team::all();
                $employeinfo['designations'] = Designation::all();
                $response['status'] = 200;
                $response['message'] = 'Employee details';
                $response['response'] = $employeinfo;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'employee-details',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function employeeUpdate(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $user_type = $request->user_type;
                $employeeid = $request->employee_id;
                $employee = Employee::findOrFail($employeeid);
                $employee->name = $request->name;
                $employee->email = $request->email;
                if ($request->password != '') {
                    $employee->password = Hash::make($request->password);
                }
                $employee->mobile = $request->mobile;
                $employee->gender = $request->gender;
                $employee->status = $request->status;
                if ($request->hasFile('image')) {
                    $storage = storage();
                    $image = $request->image;
                    switch($storage) {
                        case 'local':
                            $destinationPath = 'uploads/avatar/';
                            if (!file_exists('public/'.$destinationPath)) {
                                mkdir('public/'.$destinationPath, 0777, true);
                            }
                            $image->storeAs($destinationPath, $image->hashName());
                            break;
                        case 's3':
                            Storage::disk('s3')->putFileAs('avatar/', $request->image, $request->image->hashName(), 'public');
                            break;
                        case 'google':
                            $dir = '/';
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                            $dir = $contents->where('type', '=', 'dir')
                                ->where('image', '=', 'avatar')
                                ->first();

                            if(!$dir) {
                                Storage::cloud()->makeDirectory('avatar');
                            }

                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('image', '=', $request->image)
                                ->first();

                            if ( ! $directory) {
                                Storage::cloud()->makeDirectory($dir['path'].'/');
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('image', '=', $request->image)
                                    ->first();
                            }

                            Storage::cloud()->putFileAs($directory['basename'], $request->image, $request->image->getClientOriginalName());

                            $user->google_url = Storage::cloud()->url($directory['path'].'/'.$request->image->getClientOriginalName());

                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('task-files/'.'/', $request->image, $request->image->getClientOriginalName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/avtar/'.'/'.$request->image->getClientOriginalName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $user->dropbox_link = $dropboxResult['url'];
                            break;
                    }

                    $employee->image= $request->image->hashName();
                }
                $employee->save();
                $employeedetails = EmployeeDetails::where('employee_id', '=', $user->id)->first();
                if (empty($employeedetails)) {
                    $employeedetails = new EmployeeDetails();
                    $employeedetails->employee_id = $user->id;
                }
                $employeedetails->address = $request->address;
                $employeedetails->save();

                if($user_type=='contractor'){
                    $contractordetails = Contractors::where('company_id',$employee->company_id)->where('user_id',$employee->user_id)->first();
                    if(!empty($contractordetails->id)){
                        $contractordetails->firmname = $request->firmname;
                        $contractordetails->landline = $request->landline;
                        $contractordetails->gst_no = $request->gst_no;
                        $contractordetails->save();
                    }
                }
                $response['status'] = 200;
                $response['message'] = 'Profile updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'employee-update',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function deleteEmployee(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $employeeid = $request->employee_id;
                $employee = Employee::where('id',$employeeid)->first();
                if(empty($employee->id)){
                    $response['status'] = 300;
                    $response['message'] = 'User Not found';
                    return $response;
                }
                ProjectMember::where('employee_id', $employee->id)->delete();
                $employee->delete();
                $response['status'] = 200;
                $response['message'] = 'Employee Removed Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-employee',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function removeProjectEmployee(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $employeeid = $request->employee_id;
                $projectid =  $request->project_id;
                $employee = Employee::where('id',$employeeid)->first();
                if(empty($employee->id)){
                    $response['status'] = 300;
                    $response['message'] = 'User Not found';
                    return $response;
                }
                $projectdetails = AppProject::find($projectid);
                $projectmember = ProjectMember::where('project_id', $projectid)->where('employee_id', $employee->id)->where('user_id', $employee->user_id)->first();
                ProjectPermission::where('project_id', $projectid)->where('user_id', $employee->user_id)->delete();
                $level = $projectmember->level;
                $user_id = $projectmember->user_id;
                $assigned_by = $projectdetails->added_by;
                if($level>1){
                    $assigned_by = $projectmember->assigned_by;
                }
                $newlevel = $level+1;
                $projectmember->delete();
                $assingedprojects = ProjectMember::where('project_id', $projectid)->where('level',$newlevel)->where('assigned_by',$user_id)->get();
                if(count($assingedprojects)>0){
                    foreach ($assingedprojects as $assingedproj){
                        $assingedproj->assigned_by = $assigned_by;
                        $assingedproj->save();
                    }
                }
                $x=0;
                $newlvel = 1;
                $minlevel = ProjectMember::where('project_id', $projectid)->orderBy('level','asc')->first();
                if(!empty($minlevel->level)){
                    $minlevl = $minlevel->level;
                    /* deleted level */
                    if($minlevl<=1){
                        $newlvel = $level;
                    }
                    while ($x==0){
                        $levelprojectsarray = ProjectMember::where('project_id', $projectid)->where('level',$newlevel)->where('assigned_by',$assigned_by)->get();
                        if(count($levelprojectsarray)>0){
                            foreach ($levelprojectsarray as $levelproject){
                                $levelproject->level = $newlvel;
                                $levelproject->save();
                            }
                            $newlevel = $newlevel+1;
                            $newlvel = $newlvel+1;
                            $assigned_by = !empty($levelprojectsarray[0]->user_id) ? $levelprojectsarray[0]->user_id : 0;
                        }else{
                            $x=1;
                        }
                    }
                }

                $response['status'] = 200;
                $response['message'] = 'Employee Removed Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'remove-project-employee',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function removeProjectsByEmployee(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $employeeid = $request->employee_id;
                $selected = $request->selected;
                $projectarray = array_filter(array_unique(explode(',',$request->project_id)));
                $employee = Employee::where('id',$employeeid)->first();
                if(empty($employee->id)){
                    $response['status'] = 300;
                    $response['message'] = 'Employee Not found';
                    return $response;
                }
                if($selected=='1'){
                    $projectarray =  ProjectMember::where('employee_id', $employee->id)->where('user_id', $employee->user_id)->groupBy('project_id')->get()->pluck('project_id')->toArray();
                }

                foreach($projectarray as $projectid){
                    $projectdetails = AppProject::find($projectid);
                    $projectmember = ProjectMember::where('project_id', $projectid)->where('employee_id', $employee->id)->where('user_id', $employee->user_id)->first();
                    ProjectPermission::where('project_id', $projectid)->where('user_id', $employee->user_id)->delete();
                    $level = $projectmember->level;
                    $user_id = $projectmember->user_id;
                    $assigned_by = $projectdetails->added_by;
                    if($level>1){
                        $assigned_by = $projectmember->assigned_by;
                    }
                    $newlevel = $level+1;
                    $projectmember->delete();
                    $assingedprojects = ProjectMember::where('project_id', $projectid)->where('level',$newlevel)->where('assigned_by',$user_id)->get();
                    if(count($assingedprojects)>0){
                        foreach ($assingedprojects as $assingedproj){
                            $assingedproj->assigned_by = $assigned_by;
                            $assingedproj->save();
                        }
                    }
                    $x=0;
                    $newlvel = 1;
                    $minlevel = ProjectMember::where('project_id', $projectid)->orderBy('level','asc')->first();
                    if(!empty($minlevel->level)){
                        $minlevl = $minlevel->level;
                        /* deleted level */
                        if($minlevl<=1){
                            $newlvel = $level;
                        }
                        while ($x==0){
                            $levelprojectsarray = ProjectMember::where('project_id', $projectid)->where('level',$newlevel)->where('assigned_by',$assigned_by)->get();
                            if(count($levelprojectsarray)>0){
                                foreach ($levelprojectsarray as $levelproject){
                                    $levelproject->level = $newlvel;
                                    $levelproject->save();
                                }
                                $newlevel = $newlevel+1;
                                $newlvel = $newlvel+1;
                                $assigned_by = !empty($levelprojectsarray[0]->user_id) ? $levelprojectsarray[0]->user_id : 0;
                            }else{
                                $x=1;
                            }
                        }
                    }
                }
                if($selected=='1'){
                    $employee->delete();
                }
                $response['status'] = 200;
                $response['message'] = 'Employee Removed Successfully';
                return $response;

            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'remove-projects-by-employee',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function employeeJobUpdate(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $employeeid = $request->employee_id;
                $user = $this->user;
                $tags = explode(',',$request->skills);
                if (!empty($tags)) {
                    EmployeeSkill::where('employee_id', $employeeid)->where('employee_id', $user->company_id)->delete();
                    foreach ($tags as $tag) {
                        $skillData = Skill::firstOrCreate(['id' => strtolower($tag)]);
                        $skill = new EmployeeSkill();
                        $skill->company_id = $user->company_id;
                        $skill->employee_id = $request->id;
                        $skill->skill_id = $skillData->id;
                        $skill->save();
                    }
                }
                $employee = EmployeeDetails::where('employee_id', '=', $employeeid)->first();

                if (empty($employee)) {
                    $employee = new EmployeeDetails();
                    $employee->user_id = $request->id;
                    $employee->unique_id = $request->unique_id;
                }else{
                    $employee->unique_id = $request->unique_id;
                }
                if($request->workingrate == "hourly"){
                    $employee->hourly_rate = $request->rate ? trim($request->rate) :'';
                }elseif ($request->workingrate == "monthly"){
                    $employee->monthly_rate = $request->rate ? trim($request->rate) :'';
                }else{
                    $employee->yearly_rate = $request->rate ? trim($request->rate) :'';
                }
                $employee->joining_date = Carbon::parse($request->start_date)->format('Y-m-d');
                $employee->last_date = null;
                if ($request->last_date != '') {
                    $employee->last_date = Carbon::parse($request->start_date)->format('Y-m-d');
                }
                $employee->department_id = $request->department;
                $employee->designation_id = $request->designation;
                $employee->save();
                $response['status'] = 200;
                $response['message'] = 'Details updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'employee-job-update',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
}