<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="eventDetailModalClose">×</button>
    <h4 class="modal-title"><i class="ti-plus"></i> @lang('modules.tasks.newTask')</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'storeTask','class'=>'ajax-form','method'=>'POST']) !!}

        <div class="form-body">
            <div class="row">

                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">@lang('app.startDate')</label>
                        <input type="text" name="start_date" id="start_date2" class="form-control" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">@lang('app.dueDate')</label>
                        <input type="text" name="due_date" autocomplete="off" id="due_date2" class="form-control">
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">@lang('modules.tasks.assignTo')</label>
                        <select class="select2 form-control" data-placeholder="@lang('modules.tasks.chooseAssignee')" name="assign_to" id="task_user_id" >
                            <option value=""></option>
                            @foreach($employees as $employee)
                                <option value="{{ $employee->id }}">{{ ucwords($employee->name) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>





            </div>
            <!--/row-->

        </div>
        <div class="form-actions">
            <button type="button" id="store-task" onclick="storeTask()" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.save')</button>
        </div>

        @if(isset($columnId))
            {!! Form::hidden('board_column_id', $columnId) !!}
        @endif

        @if(isset($projectId))
            {!! Form::hidden('project_id', $projectId) !!}
        @endif

        @if(isset($pageName))
            {!! Form::hidden('page_name', $pageName) !!}
        @endif

        @if(isset($parentGanttId))
            <?php $ex = explode('?', $parentGanttId);
            ?>
            {!! Form::hidden('parent_gantt_id', $ex[0]) !!}
        @endif

        @if(isset($ex[1]))
            <?php $ex1 = explode('=', $ex[1]);
            ?>
            <input type="hidden" name="cost_item_id" value="{{ $ex1[1] }}">
        @endif

        @if(isset($ex[2]))
            <?php $ex2 = explode('=', $ex[2]);
            ?>
            <input type="hidden" name="title" value="{{ $ex2[1] }}">
        @endif

        {!! Form::close() !!}
    </div>
</div>

<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

<script>

    jQuery('#start_date2').datepicker({
        autoclose: true,
        todayHighlight: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    }).on('changeDate', function (selected) {
        $('#due_date2').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });
        var minDate = new Date(selected.date.valueOf());
        $('#due_date2').datepicker("update", minDate);
        $('#due_date2').datepicker('setStartDate', minDate);
    });

    $("#task_project_id").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    $("#task_user_id").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    $("#dependent_task_id").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    $('#task_project_id').change(function () {
        var id = $(this).val();

        var url = '{{route('admin.all-tasks.members', ':id')}}';
        url = url.replace(':id', id);

        $.easyAjax({
            url: url,
            type: "GET",
            redirect: true,
            success: function (data) {
                $('#task_user_id').html(data.html);
            }
        })

        // For getting dependent task
        var dependentTaskUrl = '{{route('admin.all-tasks.dependent-tasks', ':id')}}';
        dependentTaskUrl = dependentTaskUrl.replace(':id', id);
        $.easyAjax({
            url: dependentTaskUrl,
            type: "GET",
            success: function (data) {
                $('#dependent_task_id').html(data.html);
            }
        })
    });

    $('#repeat-task').change(function () {
        if($(this).is(':checked')){
            $('#repeat-fields').show();
        }
        else{
            $('#repeat-fields').hide();
        }
    })

    $('#dependent-task').change(function () {
        if($(this).is(':checked')){
            $('#dependent-fields').show();
        }
        else{
            $('#dependent-fields').hide();
        }
    })

    $('#createTaskCategory').click(function(){
        var url = '{{ route('admin.taskCategory.create-cat')}}';
        $('#modelHeading').html("@lang('modules.taskCategory.manageTaskCategory')");
        $.ajaxModal('#taskCategoryModal', url);
        $('#eventDetailModalClose').click();
    })
</script>
