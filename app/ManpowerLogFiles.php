<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ManpowerLogFiles extends Model
{
    protected $table = 'manpower_log_files';

    protected static function boot()
    {
        parent::boot();
    }
}
