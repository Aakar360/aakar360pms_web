@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">
                <i class="{{ $pageIcon }}"></i>
                {{ __($pageTitle) }}
            </h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-6 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.manage-drawings.index') }}">{{ __($pageTitle) }}</a></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
    <style>
        .file-bg {
            height: 150px;
            overflow: hidden;
            position: relative;
        }
        .file-bg .overlay-file-box {
            opacity: .9;
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            height: 100%;
            text-align: center;
        }
    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">

            <section>
                <div class="sttabs tabs-style-line">
                    {{--@include('admin.projects.show_project_menu')--}}
                    <div class="content-wrap">
                        <section id="section-line-3" class="show">
                            <div class="row">
                                <div class="col-md-12" id="files-list-panel">
                                    <div class="white-box">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <h2>{{ __($pageTitle) }}</h2>
                                            </div>
                                            <div class="col-md-3">
                                                <a href="{{ route('admin.manage-drawings.index') }}" class="btn btn-outline btn-success btn-sm">Go back</a>
                                            </div>
                                        </div>

                                        @if(session()->has('message'))
                                            <div class="alert alert-info">
                                                {{ session('message') }}
                                            </div>
                                        @endif
                                        <div class="row m-b-10">
                                            <?php if(!empty($_GET['path'])){ ?>
                                           <div class="col-md-2">
                                                <a href="javascript:;" id="show-folder-create-modal"
                                                   class="btn btn-success btn-outline createTitle"><i class="ti-folder"></i> @lang('modules.projects.createFolder')</a>
                                            </div>
                                            <div class="col-md-2">
                                                <a href="javascript:;" id="show-dropzone"
                                                   class="btn btn-success btn-outline"><i class="ti-upload"></i> @lang('modules.projects.uploadFile')</a>
                                            </div>
                                            {{--<div class="col-md-2">
                                                <a href="javascript:;" id="show-link-form"
                                                   class="btn btn-success btn-outline"><i class="ti-link"></i> @lang('modules.projects.addFileLink')</a>
                                            </div>--}}
                                            <?php } ?>
                                        </div>

                                        <div class="row m-b-20 hide" id="file-dropzone-box">
                                            <div class="col-md-12">
                                                <form method="post" id="updateTask" action="{{ route('admin.manage-drawings.store') }}">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Revision file</label>
                                                            <select class="form-control" name="revisionid">
                                                                <option value="">Select Drawing</option>
                                                                <?php if(!empty($files)){
                                                                    foreach ($files as $filedata){
                                                                            if($filedata->type=='file'){
                                                                        ?>
                                                                            <option value="{{ $filedata->id }}">{{ $filedata->filename }}</option>
                                                                        <?php } }     }?>

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Name</label>
                                                            <input type="text" class="form-control" name="name" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Description</label>
                                                            <textarea  class="form-control" rows="7" name="description" ></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Files</label>
                                                        <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                                        <div id="file-upload-box" >
                                                            <div class="row" id="file-dropzone">
                                                                <div class="col-md-12">
                                                                    <div class="dropzone dropheight"
                                                                         id="file-upload-dropzone">
                                                                        {{ csrf_field() }}
                                                                        <div class="fallback">
                                                                            <input name="file" type="file" multiple/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="fileID" id="fileID">
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <button type="submit" id="update-file" class="btn btn-success" >@lang('app.upload')</button>
                                                            <input type="hidden" class="form-control" id="fileparent" name="parent" value="<?=isset($_GET['path'])? $_GET['path'] :'0';?>" placeholder="Folder Name *">
                                                            <input type="hidden" class="form-control" name="project_id" id="projectID" value="{{ !empty($filemanager->project_id) ? $filemanager->project_id : '' }}" required>
                                                            <input type="hidden" class="form-control" name="pathtype" value="outline">
                                                            <input name="view" type="hidden" id="view" value="list">
                                                            {{ csrf_field() }}
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                        <div class="row m-b-20 hide" id="file-link">
                                            {!! Form::open(['id'=>'file-external-link','class'=>'ajax-form','method'=>'POST']) !!}
                                            <input type="hidden" class="form-control" name="parent" value="<?=!empty($_GET['path']) ? $_GET['path']:'0';?>" placeholder="Folder Name *">
                                             <input name="view" type="hidden" id="view" value="list">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">@lang('app.name')</label>
                                                    <input type="text" name="filename" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <label for="">@lang('modules.projects.addFileLink')</label>
                                                    <input type="text" name="external_link" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input name="pathtype" type="hidden"  value="outline">
                                                    <button class="btn btn-success" id="save-link">@lang('app.submit')</button>
                                                </div>
                                            </div>

                                            {!! Form::close() !!}
                                        </div>

                                    {{--<ul class="nav nav-tabs" role="tablist" id="list-tabs">--}}
                                    {{--<li role="presentation" class="active nav-item" data-pk="list"><a href="#list" class="nav-link" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs">  @lang('app.list')</span></a></li>--}}
                                    {{--<li role="presentation" class="nav-item" data-pk="thumbnail"><a href="#thumbnail" class="nav-link thumbnail" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">@lang('app.thumbnail')</span></a></li>--}}
                                    {{--</ul>--}}
                                    <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="list">
                                                <ul class="list-group" id="files-list">
                                                    <li class="list-group-item">
                                                        <div class="table-responsive">
                                                            <table class="table table-stripped">
                                                                <thead>
                                                                <th>Name</th>
                                                                <th>Size</th>
                                                                <?php if(!empty($_GET['path'])){?>
                                                                <th>Name</th>
                                                                <th>Description</th>
                                                                <th>Revision</th>
                                                                <?php }?>
                                                                <th>Last Modified</th>
                                                                <th>Action</th>
                                                                </thead>
                                                                <tbody>
                                                                @forelse($files as $file)
                                                                 <?php
                                                                         $showlist= true;
                                                                 if($file->foldertype=='private'&&$user->id!=$file->user_id){
                                                                     $showlist= false;
                                                                     }
                                                                     if($showlist){
                                                                        $path = $file->id;
                                                                        $revisionfiles = \App\FileManager::selectRaw("*,count(*) as count")->where('revisionfileid',$file->id)->orderBy('id','desc')->first();
                                                                 ?>
                                                                    <tr id="file{{ $file->id }}">
                                                                        <td>
                                                                            @if($file->type == 'file')
                                                                                <i class="ti-file"></i> {{ $file->filename }}
                                                                            @else
                                                                                <a href="{{url('admin/manage-drawings/manage-drawings')}}?path={{$path}}">
                                                                                    <i class="ti-folder"></i> {{ $file->filename }}
                                                                                </a>
                                                                            @endif
                                                                        </td>
                                                                        <td>{{ $file->size }} Bytes</td>
                                                                        <?php if(!empty($_GET['path'])){?>
                                                                        <td>{{ $file->name }}</td>
                                                                        <td>{{ $file->description }}</td>
                                                                        <td>@if(!empty($revisionfiles))<a href="{{ route('admin.manage-drawings.revisionFiles',[$file->project_id,$file->id]) }}?path={{ $_GET['path'] }}">{{  $revisionfiles->count }}</a>@else 0 @endif</td>

                                                                            <?php }?>
                                                                        <td><span class="m-l-10">{{ date("d-m-Y", strtotime($file->created_at)) }}</span></td>
                                                                        <?php if(!empty($_GET['path'])){?>
                                                                        <td>
                                                                            <a href="javascript:;" onclick="showName({{ $file->id }});"
                                                                               data-toggle="tooltip" data-original-title="Rename"
                                                                               class="btn btn-info btn-circle"><i
                                                                                        class="fa fa-edit"></i></a>
                                                                            <a href="javascript:void(0);" class="btn btn-warning btn-circle movefile" data-file="{{ $file->id }}"  data-toggle="tooltip" data-original-title="Move to Outline" ><i class="fa fa-reply-all"></i></a>
                                                                            @if($file->type == 'file')
                                                                                @if($file->external_link != '')
                                                                                    <a target="_blank" href="{{ $file->external_link }}"
                                                                                       data-toggle="tooltip" data-original-title="View"
                                                                                       class="btn btn-info btn-circle"><i
                                                                                                class="fa fa-search"></i></a>
                                                                                @elseif($storage == 'local')
                                                                                    <?php
                                                                                    $fx = explode('.', $file->hashname);
                                                                                    $ext = $fx[(count($fx)-1)];
                                                                                    ?>
                                                                                    @if($ext != 'pdf')
                                                                                        <?php
                                                                                        $folder = '';
                                                                                        $x=1;
                                                                                        $p = $file->parent;
                                                                                        while($x==1){
                                                                                            $fn = \App\FileManager::where('id', $p)->where('type', 'folder')->first();
                                                                                            if($fn !== null){
                                                                                                $folder = $fn->filename.'/'.$folder;
                                                                                                $p = $fn->parent;
                                                                                            }else{
                                                                                                $x=0;
                                                                                            }
                                                                                        }
                                                                                        $html5class = '';
                                                                                        if($ext=='jpg'||$ext=='png'||$ext=='jpeg'){
                                                                                            $html5class = 'html5lightbox';
                                                                                        }
                                                                                        ?>
                                                                                        <a target="_blank" href="{{ uploads_url().'project-files/'.$file->project_id.'/'.$folder.$file->hashname }}"
                                                                                           data-toggle="tooltip" data-original-title="View"
                                                                                           class="btn btn-info btn-circle {{ $html5class }}"><i
                                                                                                    class="fa fa-search"></i></a>
                                                                                    @else
                                                                                        <a href="{{ route('admin.projects.projectEditFile', [$file->project_id, $file->id]) }}"
                                                                                           data-toggle="tooltip" data-original-title="View"
                                                                                           class="btn btn-info btn-circle"><i
                                                                                                    class="fa fa-eye"></i></a>
                                                                                    @endif
                                                                                @elseif($storage == 's3')
                                                                                    <?php
                                                                                    $fx = explode('.', $file->hashname);
                                                                                    $ext = $fx[(count($fx)-1)];
                                                                                    ?>
                                                                                    @if($ext != 'pdf')
                                                                                        <?php
                                                                                        $folder = '';
                                                                                        $x=1;
                                                                                        $p = $file->parent;
                                                                                        while($x==1){
                                                                                            $fn = \App\FileManager::where('id', $p)->where('type', 'folder')->first();
                                                                                            if($fn !== null){
                                                                                                $folder = $fn->filename.'/'.$folder;
                                                                                                $p = $fn->parent;
                                                                                            }else{
                                                                                                $x=0;
                                                                                            }
                                                                                        }
                                                                                        $html5class = '';
                                                                                        if($ext=='jpg'||$ext=='png'||$ext=='jpeg'){
                                                                                            $html5class = 'html5lightbox';
                                                                                        }
                                                                                        ?>
                                                                                        <a target="_blank" href="{{ awsurl().'project-files/'.$file->project_id.'/'.$folder.$file->hashname }}"
                                                                                           data-toggle="tooltip" data-original-title="View"
                                                                                           class="btn btn-info btn-circle {{ $html5class }}"><i
                                                                                                    class="fa fa-search"></i></a>
                                                                                    @else
                                                                                        <a href="{{ route('admin.projects.projectEditFile', [$file->project_id, $file->id]) }}"
                                                                                           data-toggle="tooltip" data-original-title="View"
                                                                                           class="btn btn-info btn-circle"><i
                                                                                                    class="fa fa-eye"></i></a>
                                                                                    @endif
                                                                                @elseif($storage == 'google')
                                                                                    <a target="_blank" href="{{ $file->google_url }}"
                                                                                       data-toggle="tooltip" data-original-title="View"
                                                                                       class="btn btn-info btn-circle"><i
                                                                                                class="fa fa-search"></i></a>
                                                                                @elseif($storage == 'dropbox')
                                                                                    <a target="_blank" href="{{ $file->dropbox_link }}"
                                                                                       data-toggle="tooltip" data-original-title="View"
                                                                                       class="btn btn-info btn-circle"><i
                                                                                                class="fa fa-search"></i></a>
                                                                                @endif
                                                                                {{--@if(is_null($file->external_link))--}}
                                                                                {{--&nbsp;&nbsp;--}}
                                                                                {{--<a href="{{ route('admin.files.download', $file->id) }}"--}}
                                                                                {{--data-toggle="tooltip" data-original-title="Download"--}}
                                                                                {{--class="btn btn-inverse btn-circle"><i--}}
                                                                                {{--class="fa fa-download"></i></a>--}}
                                                                                {{--@endif--}}
                                                                            @endif
                                                                            @if($file->type == 'folder')
                                                                                @if($file->locked == '0')
                                                                                    <a href="javascript:;" onclick="editModal({{ $file->id }}, '{{ $file->filename }}');"
                                                                                       data-toggle="tooltip" data-original-title="Rename"
                                                                                       class="btn btn-info btn-circle"><i
                                                                                                class="fa fa-pencil"></i></a>
                                                                                @endif
                                                                            @endif
                                                                            @if($file->locked == '0')
                                                                                <a href="javascript:;" data-toggle="tooltip"
                                                                                   data-original-title="Delete"
                                                                                   data-file-id="{{ $file->id }}"
                                                                                   class="btn btn-danger btn-circle sa-params" data-pk="list"><i
                                                                                            class="fa fa-times"></i></a>
                                                                        </td>
                                                                        @endif
                                                                        <?php }else{ ?>
                                                                        <td><a href="javascript:;" data-toggle="tooltip"
                                                                               data-original-title="Delete"
                                                                               data-file-id="{{ $file->id }}"
                                                                               class="btn btn-danger btn-circle sa-params" data-pk="list"><i
                                                                                        class="fa fa-times"></i></a></td>
                                                                        <?php }?>
                                                                    </tr>
                                                                 <?php }?>
                                                                @empty
                                                                    <tr>
                                                                        <td colspan="5">@lang('messages.noFileUploaded')</td>
                                                                    </tr>
                                                                @endforelse
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="thumbnail">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div><!-- /content -->
                </div><!-- /tabs -->
            </section>
        </div>


    </div>
    <!-- .row -->
    <?php if(!empty($_GET['path'])){  ?>
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form method="post" action="{{ route('admin.manage-drawings.createFolder') }}">
                    <div class="modal-header">
                        <button type="button" class="close closeModel" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading">Create Folder</span>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12 form-group">
                                <input type="text" class="form-control" name="foldername" placeholder="Folder Name *" required>
                            </div>
                            <div class="col-lg-12 form-group">
                                <input type="text" class="form-control" name="name" placeholder="Name" required>
                            </div>
                            <div class="col-lg-12 form-group">
                                <textarea name="description" class="form-control" placeholder="Description" ></textarea>
                            </div>
                            <div class="col-lg-12 form-group">
                                <label><input type="radio" name="foldertype" value="public" checked > Public</label>
                                <label><input type="radio" name="foldertype" value="private" > Private</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" class="form-control" name="parent" value="{{ $filemanager->id }}" required>
                        <input type="hidden" class="form-control" name="project_id" value="{{ $filemanager->project_id }}" required>
                        <input type="hidden" class="form-control" name="type" value="folder" required>
                        <input type="hidden" class="form-control" name="pathtype" value="outline" required>
                        @csrf
                        <button type="submit" name="submit" class="btn btn-primary">Create</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="editModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form method="post" id="editFileIDSubmit" action="{{ route('admin.manage-drawings.editFolder') }}">
                    <div class="modal-header">
                        <button type="button" class="close closeEditButton" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading">Edit Folder Name</span>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12 form-group">
                                <input type="text" class="form-control" name="foldername" id="editData" placeholder="Folder Name *" required>
                                <input type="hidden" name="fileid"  id="editFileIDData" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{ csrf_field() }}
                        <button type="submit" name="submit" id="save-file-update" class="btn blue">Update</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}
    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="showName" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form method="post" id="createNameEdit" action="{{ route('admin.manage-drawings.editName') }}">
                    <div class="modal-header">
                        <button type="button" class="close closeEditButton" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading">Edit Name</span>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12 form-group">
                                <input type="text" class="form-control" name="name" placeholder="Name" id="editNameData" required>
                                <input type="hidden" name="fileid"  id="editNameIDData" required>
                            </div>
                            <div class="col-lg-12 form-group">
                                <textarea name="description" class="form-control" placeholder="Description"  id="editDescData" ></textarea>
                            </div>
                            <div class="col-lg-12 form-group">
                                <label><input type="radio" name="foldertype" value="public" class="editFolType" > Public</label>
                                <label><input type="radio"  name="foldertype"  value="private" class="editFolType" > Private</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{ csrf_field() }}
                        <button type="submit" name="submit" id="save-form-name" class="btn btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Ajax Modal Ends--}}
    <?php }?>


@endsection

@push('footer-script')
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/html5lightbox/html5lightbox.js') }}"></script>
    <script>
     /*   $('#project_id').change(function(){
            var projectId = $(this).val();
            window.location.href = '{{ route('admin.manage-drawings.index') }}'+'/'+projectId;

        });*/
        $('.createTitle').click(function(){
            $('#modelHeading').html("Create New");
//            $.ajaxModal('#taskCategoryModal');
            $('#taskCategoryModal').show();
        })

        $('.closeModel').click(function(){
            $('#taskCategoryModal').hide();
        })

        $('.closeEditButton').click(function(){
            $('#showName').hide();
            $('#editModal').hide();
        })

     function editModal(val, name) {
         $('#editModal').show();
         $('#modelHeading').html("Edit Name");
         $('#editData').val(name);
         $('#editFileIDData').val(val);

     }
     function showName(val) {
         $('#showName').show();
         $('#namemodelHeading').html("Edit Name");
         var fileid = $(this).data('file');
         var token = '{{ csrf_token() }}';
         $.easyAjax({
             url: '{{ route('admin.manage-drawings.showName') }}',
             type: "POST",
             data: {
                 '_token':token,
                 'fileid':val
             },
             success: function (response) {
                 $('#editNameIDData').val(response.id);
                 $('#editNameData').val(response.name);
                 $('#editDescData').text(response.description);
                 if(response.foldertype=='private'){
                     $('.editFolType[value="private"]').prop('checked', true);
                 }else{
                     $('.editFolType[value="public"]').prop('checked', true);
                 }
             }
         });
     }
        $('#show-dropzone').click(function () {
            $('#file-dropzone-box').toggleClass('hide show');
        });

        $('#show-link-form').click(function () {
            $('#file-link').toggleClass('hide show');
        });

        $("body").tooltip({
            selector: '[data-toggle="tooltip"]'
        });

     Dropzone.autoDiscover = false;
     //Dropzone class
     myDropzone = new Dropzone("div#file-upload-dropzone", {
         url: "{{ route('admin.manage-drawings.storeImage') }}",
         headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
         paramName: "file",
         maxFilesize: 50,
         maxFiles: 10,
         autoProcessQueue: false,
         uploadMultiple: false,
         addRemoveLinks:true,
         parallelUploads:10,
         init: function () {
             myDropzone = this;
         }
     });

     myDropzone.on('sending', function(file, xhr, formData) {
         console.log(myDropzone.getAddedFiles().length,'sending');
         var ids = $("#fileID").val();
         formData.append('file_id', ids);
         var pids = $("#projectID").val();
         formData.append('project_id', pids);
         var filepa = $("#fileparent").val();
         formData.append('parent', filepa);
         location.reload();
     });
     myDropzone.on('completemultiple', function () {
         var msgs = "File updated successfully";
         $.showToastr(msgs, 'success');
         var filepa = $("#fileparent").val();
         location.reload();

     });
     //    update task
     $('#update-file').click(function (e) {
         var filepa = $("#fileparent").val();
         e.preventDefault();
         $.easyAjax({
             url: '{{route('admin.manage-drawings.store')}}',
             container: '#updateTask',
             type: "POST",
             data: $('#updateTask').serialize(),
             success: function(response){
                 if(myDropzone.getQueuedFiles().length > 0){
                     fileID = response.fileID;
                     $('#fileID').val(response.fileID);
                     myDropzone.processQueue();
                 }
                 else{
                     var msgs = "File updated successfully";
                     $.showToastr(msgs, 'success');
                     location.reload();
                 }
             }
         })
     });
      /*  // "myAwesomeDropzone" is the camelized version of the HTML element's ID
        Dropzone.options.fileUploadDropzone = {
            paramName: "file", // The name that will be used to transfer the file
//        maxFilesize: 2, // MB,
            dictDefaultMessage: "@lang('modules.projects.dropFile')",
            accept: function (file, done) {
                done();
            },
            init: function () {
                this.on("success", function (file, response) {
                   /!* var viewName = $('#view').val();
                    if(viewName == 'list') {
                        $('#files-list-panel ul.list-group').html(response.html);
                    } else {
                        $('#thumbnail').empty();
                        $(response.html).hide().appendTo("#thumbnail").fadeIn(500);
                    }*!/
                })
            }
        };*/

        $('body').on('click', '.sa-params', function () {
            var id = $(this).data('file-id');
            var deleteView = $(this).data('pk');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {

                    var url = "{{ route('admin.manage-files.destroy',':id') }}";
                    url = url.replace(':id', id);
                    var token = "{{ csrf_token() }}";
                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE', 'view': deleteView},
                        success: function (response) {
                            $("#file"+id).remove();
                        }
                    });
                }
            });
        });

        /*$('.thumbnail').on('click', function(event) {
            event.preventDefault();
            $('#thumbnail').empty();
            $.easyAjax({
                type: 'GET',
                data: {
                  id: projectID
                },
                success: function (response) {
                    $(response.view).hide().appendTo("#thumbnail").fadeIn(500);
                }
            });
        });*/

        $('#save-link').click(function () {
            $.easyAjax({
                url: '{{route('admin.manage-files.storeLink')}}',
                container: '#file-external-link',
                type: "POST",
                redirect: true,
                data: $('#file-external-link').serialize(),
                success: function () {
                    window.location.reload();
                }
            })
        });

        $('#list-tabs').on("shown.bs.tab",function(event){
            var tabSwitch = $('#list').hasClass('active');
            if(tabSwitch == true) {
                $('#view').val('list');
            } else {
                $('#view').val('thumbnail');
            }
        });
     $('#save-form-name').click(function (e) {
         e.preventDefault();
         $.easyAjax({
             url: '{{route('admin.manage-drawings.editName')}}',
             container: '#createNameEdit',
             type: "POST",
             redirect: true,
             data: $('#createNameEdit').serialize(),
             success: function(response){
                 $('#showName').hide();
                 $('#createNameEdit')[0].reset();
                 location.reload();
             }
         })
     });
     $('#save-file-update').click(function (e) {
         e.preventDefault();
         $.easyAjax({
             url: '{{route('admin.manage-drawings.editFolder')}}',
             container: '#editFileIDSubmit',
             type: "POST",
             redirect: true,
             data: $('#editFileIDSubmit').serialize(),
             success: function(response){
                 $('#editModal').hide();
                 $('#editFileIDSubmit')[0].reset();
                 location.reload();
             }
         })
     });
        $('ul.showProjectTabs .projectFiles').addClass('tab-current');
    </script>
@endpush
