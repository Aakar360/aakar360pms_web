@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.tenders.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.updateCostitem')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

@endpush
@section('content')

    <div class="container-fluid">
        <div class="row ">
            <div class="panel panel-inverse form-shadow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            @lang('app.updateCostitem')
                        </div>
                        <div class="col-md-6 text-right"  >
                            <a href="{{ route('admin.tenders.edit', [$tenders->id]) }}" style="color:white" class="btn1 btn-info btn-circle"><i class="fa fa-edit"></i> Edit</a>
                            <a href="{{ route('admin.tenders.editcostitem', [$tenders->id]) }}" style="color:white" class="btn1 edit-btn btn-circle"><i class="icon-settings"></i> @lang('app.task')</a>
                        </div>
                    </div>
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <div class="row">
                    {!! Form::open(['id'=>'createTenderPayment','class'=>'ajax-form','method'=>'POST']) !!}
                    <div class="col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="title"><b>@lang('app.title')</b></label>
                           <p>{{ $tenders->name }}</p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-4">
                        <div class="form-group">
                            <label for="number"><b>@lang('app.number')</b></label>
                            <p>{{ $tenders->number }}</p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-4">
                        <div class="form-group">
                            <label for="title"><b>@lang('app.status')</b></label>
                            <p>{{ $tenders->status }}</p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-4">
                        <div class="form-group">
                            <label for="duedate"><b>@lang('app.duedate')</b></label>
                            <p>{{ $tenders->deadline ? date('d-m-Y',strtotime($tenders->deadline)) : 'N/A' }}</p>
                        </div>
                    </div>

                    <div class="col-sm-4 col-xs-4">
                        <div class="form-group">
                            <label for="project_id"><b>@lang('app.projects')</b></label>
                            <select class="select2 form-control projectid" name="project_id" data-style="form-control" required>
                                <option value="">Please Select Project</option>
                                @foreach($projectlist as $project)
                                    <option value="{{ $project->id }}" <?php if($tenders->project_id==$project->id){ echo 'selected';}?>>{{ ucwords($project->project_name) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-4">
                        <div class="form-group">
                            <label for="title_id">Project Title</label>
                            <select class="select2 form-control titlelist" id="titlelist" name="title_id" data-style="form-control" required>
                                <option value="">Please Select Title</option>
                                @if($titlelist)
                                    @foreach($titlelist as $title)
                                        <option value="{{ $title->id }}" <?php if($tenders->title_id==$title->id){ echo 'selected';}?>>{{ ucwords($title->title) }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-4 segmentblock" @if(count($segmentlist)>0) style="display: none;" @endif>
                        <div class="form-group">
                            <label for="title_id">@lang('app.segment')</label>
                            <select class="select2 form-control segmentslist" id="segmentslist" name="segment_id" data-style="form-control" required>
                                <option value="">Select @lang('app.segment')</option>
                                @if(count($segmentlist)>0)
                                    @foreach($segmentlist as $segment)
                                        <option value="{{ $segment->id }}" <?php if($tenders->segment_id==$segment->id){ echo 'selected';}?>>{{ ucwords($segment->name) }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-xs-12">
                        <div class="combo-sheet">
                            <div class="table-responsive">
                                <table id="boqtable" class="table table-bordered default footable-loaded footable boqtable" data-resizable-columns-id="users-table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>@lang('app.task')</th>
                                    <th>Quantity</th>
                                    <th>Unit</th>
                                </tr>
                            </thead>
                                    <tbody class="colposition" id="costitemloop">

                                    </tbody>
                        </table>
                    </div>
                    </div>
                    </div>

                    <input type="hidden" name="projectID" id="projectID">
                    <div class="col-sm-12 col-xs-12">
                        <button type="submit" id="save-form" class="btn btn-success waves-effect waves-light m-r-10">
                            @lang('app.save')
                        </button>
                        {{ csrf_field() }}
                        <button type="reset" class="btn btn-inverse waves-effect waves-light">@lang('app.reset')</button>
                    </div>
                    {!! Form::close() !!}
                </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

@endsection

@push('footer-script')

    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
    <script src="{{ asset('js/jquery.dragtable.js') }}"></script>
    <script src="{{ asset('js/jquery.resizableColumns.min.js') }}"></script>
    <script>

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });
        jQuery('.datepicker').datepicker({
            toggleActive: true,
            format: '{{ $global->date_picker_format }}',
            language: '{{ $global->locale }}',
            autoclose: true,
            weekStart:'{{ $global->week_start }}',
        });
        $(".projectid").change(function () {
            var project = $(this).val();
            var tender = '{{ $tenders->id }}';
            if(project){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.tenders.projecttitles') }}",
                    data: {'_token': token,'projectid': project,'tender': tender},
                    success: function(data){
                        $("select.titlelist").html("");
                        $("select.titlelist").html(data);
                        $('select.titlelist').select2();
                    }
                });
            }
        });
        $("#titlelist").change(function () {
            var project = $(".projectid").find(':selected').val();
            var titleid = $(this).val();
            var tender = '{{ $tenders->id }}';
            if(project&&titleid){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.tenders.boqcostitems') }}",
                    data: {'_token': token,'projectid': project,'titleid': titleid,'tender': tender},
                    success: function(data){
                        $("select.segmentslist").select2();
                        $("select.segmentslist").html("");
                        $(".costitemslist").html("");
                        $(".costitemscatlist").html("");
                        if(data.segments){
                            $(".segmentblock").show();
                            $("select.segmentslist").html(data.segments);
                        }else{
                            $(".segmentblock").hide();
                        }
                        $(".costitemslist").html(data.costitem);
                        $(".costitemscatlist").html(data.costcat);

                    }
                });
            }
        });
        $("#segmentslist").change(function () {
            var project = $(".projectid").find(':selected').val();
            var titleid = $("#titlelist").find(':selected').val();
            var segmentid = $(this).val();
            var tender = '{{ $tenders->id }}';
            if(project&&titleid){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.tenders.boqcostitems') }}",
                    data: {'_token': token,'projectid': project,'titleid': titleid,'segmentid': segmentid,'tender': tender},
                    success: function(data){
                        $(".costitemslist").html("");
                        $(".costitemslist").html(data.costitem);
                        $(".costitemscatlist").html("");
                        $(".costitemscatlist").html(data.costcat);
                    }
                });
            }
        });
        var r = 0;
        $(document).on("change",".costitemrow",function () {
            r++;
            var trindex = $(this).closest('tr').index();
            var project = $(".projectid").find(':selected').val();
            var titleid = $("#titlelist").find(':selected').val();
            var listid = $(this).attr("list");
            var listval = $(this).val();
           /* var rowid = $("#"+listid).find('option:selected').attr("data-rowid");*/
            var rowid = $("#"+listid+" option[value='"+ listval +"']").attr("data-rowid");
            var costitemname = $(this).val();
            var catid = $(this).data('cat');
            var tender = '{{ $tenders->id }}';
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.tenders.boqrow') }}",
                data: {'_token': token,'projectid': project,'titleid': titleid,'tender': tender,'rowid': rowid,'costitemname': costitemname,'catid': catid},
                success: function(data){
                    costitemloop();
                }
            });
        });
        var r = 0;
        $(document).on("change",".costitemcatrow",function () {
            r++;
            var trindex = $(this).closest('tr').index();
            var project = $(".projectid").find(':selected').val();
            var titleid = $("#titlelist").find(':selected').val();
            var level = $(this).data('level');
            var parent = $(this).data('parent');
            var catid = $(this).val();
            var tender = '{{ $tenders->id }}';
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('admin.tenders.boqcatrow') }}",
                data: {'_token': token,'projectid': project,'titleid': titleid,'catid': catid,'tender': tender,'level': level,'parent': parent},
                success: function(data){
                    costitemloop();
                }
            });
        });
        $('body').on('click', '.remove-item', function(){
            var id = $(this).data('rowid');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted team!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    var url = "{{ route('admin.tenders.boqrowremove',':id') }}";
                    url = url.replace(':id', id);
                    var token = "{{ csrf_token() }}";
                    $.easyAjax({
                        type: 'DELETE',
                        url: url,
                        data: {'_token': token},
                        success: function (response) {
                            if (response.status == "success") {
                                $("#rowitem"+id).remove();
                            }
                        }
                    });
                }
            });
        });
        $('body').on('click', '.sa-params-cat', function(){
            var id = $(this).data('position-id');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted Category!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {

                    var url = "{{ route('admin.tenders.costitem-cat-destroy',':id') }}";
                    url = url.replace(':id', id);
                    var token = "{{ csrf_token() }}";
                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE'},
                        success: function (response) {
                            if (response.status == "success") {
                                costitemloop();
                            }
                        }
                    });
                }
            });
        });
        $('.boqtable').dragtable({persistState: function(table) {
            var token = '{{ csrf_token() }}';
            var selectarray = [];
            var project = $(".projectid").find(':selected').val();
            var titleid = $("#titlelist").find(':selected').val();
            table.sortOrder['_token']=token;
            table.el.find('th').each(function(i) {
                var col = $(this).attr("col");
                if(col) {
                    selectarray.push(col);
                }
            });
            $.ajax({
                 url : "{{ route('admin.tenders.boqcolchangeposition') }}",
                method: 'POST',
                data: {
                    '_token':token,
                    'projectid': project,
                    'title': titleid,
                    position:selectarray
                },
            });
        }
        });
        costitemloop();
        function  costitemloop() {
            var project = $(".projectid").find(':selected').val();
            var titleid = $("#titlelist").find(':selected').val();
            var tender = '{{ $tenders->id }}';
            var token = "{{ csrf_token() }}";
            $.ajax({
                url : "{{ route('admin.tenders.costitemloop') }}",
                method: 'POST',
                data: {'_token': token,'projectid': project,'titleid': titleid,'tender': tender},
                success: function (response) {
                        $("#costitemloop").html("");
                        $("#costitemloop").html(response);
                }
            });
        }
    </script>
@endpush

