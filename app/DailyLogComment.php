<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class DailyLogComment extends Model
{
    protected $table = 'daily_log_comments';

    protected static function boot()
    {
        parent::boot();
    }
}
