<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeatherLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->nullable();
            $table->integer('project_id')->nullable();
            $table->integer('title_id')->default(0);
            $table->integer('segment_id')->default(0);
            $table->integer('task_id')->default(0);
            $table->integer('added_id')->nullable();
            $table->string('mintemp')->nullable();
            $table->string('maxtemp')->nullable();
            $table->string('sitecondition')->nullable();
            $table->string('weathercondition')->nullable();
            $table->string('raintimings')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather_logs');
    }
}
