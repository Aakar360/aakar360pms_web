<?php

namespace App\Http\Controllers\Api;

use App\AppLogs;
use App\AppProject;
use App\BoqCategory;
use App\Company;
use App\CostItems;
use App\Helper\Files;
use App\Helper\Reply;
use App\Http\Controllers\Controller;
use App\ManpowerCategory;
use App\ManpowerLog;
use App\ManpowerLogFiles;
use App\ManpowerLogReply;
use App\Notifications\NewClientTask;
use App\Notifications\NewTask;
use App\ProjectActivity;
use App\ProjectAttachmentDesigns;
use App\ProjectAttachmentFiles;
use App\ProjectCostItemsFinalQty;
use App\ProjectCostItemsPosition;
use App\ProjectCostItemsProduct;
use App\ProjectMember;
use App\ProjectMilestone;
use App\ProjectsLogs;
use App\ProjectTemplate;
use App\ProjectTimeLog;
use App\PunchItem;
use App\PunchItemFiles;
use App\Rfi;
use App\Task;
use App\TaskboardColumn;
use App\TaskFile;
use App\Title;
use App\Traits\ProjectProgress;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\FileManager;
use App\Project;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use DB;
use Intervention\Image\Facades\Image;

class AppManpowerlogController extends Controller
{
    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }

    protected function validatetToken($apikey,$token)
    {
        $response = array();
        if (isset($apikey)) {
            if (!$this->validateAPI($apikey)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            if(isset($token)) {
                if (!empty($token)) {
                    $user = User::where('appid', $token)->first();
                    if($user === null){
                        $response['message'] = 'Invalid token';
                        $response['status'] = 401;
                        return $response;
                    }
                    $response['user'] = $user;
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    return $response;
                }
                $response['message'] = 'Api Token cannot be empty';
                $response['status'] = 401;
                return $response;
            }
            $response['message'] = 'Api Token not sent';
            $response['status'] = 401;
            return $response;

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function __construct(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];

            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', $user->company_id)->first();

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function createManpowerLog(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $manpowerid = $request->manpowerid;
                $memberExistsInTemplate = false;
                if(!empty($manpowerid)){
                    $timeLog =  ManpowerLog::find($manpowerid);
                }else{
                    $timeLog = new ManpowerLog();
                }
                $timeLog->company_id = $user->company_id;
                $timeLog->contractor = $request->contractor;
                $timeLog->manpower_category = $request->category;
                $timeLog->manpower = $request->manpower;
                $timeLog->workinghours = $request->workinghours;
                $timeLog->description = $request->description;
                if($request->task_id){
                    $task = Task::find($request->task_id);
                    $timeLog->project_id = $task->project_id;
                    $timeLog->title_id = $task->title;
                    $timeLog->segment_id = $task->segment;
                    $timeLog->costitem_id = $task->cost_item_id;
                }else{
                    $timeLog->project_id = $request->project_id;
                    $timeLog->title_id = $request->title_id;
                    $timeLog->segment_id = $request->segment_id;
                    $timeLog->costitem_id = $request->costitem;
                }
                $timeLog->task_id = $request->task_id ?: 0;
                $timeLog->added_by = $user->id;
                $timeLog->save();


                $imagesarray = array_filter(explode(',',$request->imagesids));
                if(!empty($imagesarray)){
                    foreach ($imagesarray as $images){
                        $punchfile = ManpowerLogFiles::find($images);
                        if(!empty($punchfile->id)){
                            $punchfile->manpower_id = $timeLog->id;
                            $punchfile->save();
                        }
                    }
                }

                $medium = $request->medium ?: 'android';
                $createlog = new ProjectsLogs();
                $createlog->company_id = $user->company_id;
                $createlog->added_id = $user->id;
                $createlog->project_id = $timeLog->project_id;
                $createlog->subproject_id = $timeLog->title_id ?: 0;
                $createlog->segment_id = $timeLog->segment_id ?: 0;
                $createlog->medium = $medium;
                $createlog->module_id = $timeLog->id;
                $createlog->module = 'manpower_logs';
                $createlog->heading =  $timeLog->manpower.' '.get_manpower_category($timeLog->manpower_category).' worked for '.$timeLog->workinghours.' hours';
                if(empty($manpowerid)) {
                    $createlog->modulename = 'manpower_created';
                    $createlog->description = 'Manpower log created by ' . $user->name . ' for the project ' . get_project_name($timeLog->project_id);
                 }else{
                    $createlog->modulename = 'manpower_updated';
                    $createlog->description = 'Manpower log updated by ' . $user->name . ' for the project ' . get_project_name($timeLog->project_id);
                 }
                 $createlog->save();

                $response['status'] = 200;
                $response['manpowerid'] = $timeLog->id;
                if(empty($manpowerid)) {
                    $response['message'] = 'Manpower Added Successfully';
                }else{
                    $response['message'] = 'Manpower Updated Successfully';
                }
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-man-power',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function appManpowerLog(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $taskid = $request->task_id;
                $projectselectarray = explode(',',$request->project_id);
                $subprojectid = $request->subproject_id;
                $segmentid = $request->segment_id;
                if(!empty($projectselectarray)){
                    $prarray = $projectselectarray;
                }else{
                    $project = Project::where('company_id',$user->company_id)->get()->pluck('id')->toArray();
                    $pm = ProjectMember::where('user_id',$user->id)->get()->pluck('project_id')->toArray();
                    $prarray = array_filter(array_merge($project,$pm));
                }

                $projects = ManpowerLog::whereIn('project_id', $prarray);
                if(!empty($subprojectid)){
                    $projects = $projects->where('title_id',$subprojectid);
                }
                if(!empty($segmentid)){
                    $projects = $projects->where('segment_id',$segmentid);
                }
                if($taskid){
                    $projects = $projects->where('task_id',$taskid);
                }
                $page = $request->page;
                if($page=='all'){
                    $projects = $projects->orderBy('id','desc')->get();
                }else{
                    $count = 3;
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    $projects = $projects->offset($skip)->take($count)->orderBy('id','DESC')->get()->toArray();
                }
                $response['status'] = 200;
                $response['message'] = 'Man power logs List Fetched';
                $response['response'] = $projects;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'man-power-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function manpowerLog(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $manpowerid = $request->manpowerid;
                if(!empty($request->manpowerid)){
                    $response = array();
                    $projects = ManpowerLog::where('id',$manpowerid)->first();
                    if($projects !== null){
                        $response['status'] = 300;
                        $response['message'] = 'No record found';
                    }
                    $response['status'] = 200;
                    $response['message'] = 'Man power logs List Fetched';
                    $response['response'] = $projects;
                }else{
                    $response['status'] = 301;
                    $response['message'] = 'Invalid Labour log';
                }
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'man-power',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function deleteManpowerLog(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $manpowerarray = array_filter(explode(',',$request->manpowerid));
                foreach ($manpowerarray as $manpower) {
                    $task = ManpowerLog::findOrFail($manpower);

                    ProjectsLogs::where('module_id',$task->id)->where('module','manpower_logs')->delete();
                    ManpowerLogFiles::where('manpower_id',$task->id)->delete();
                    $manpowerreply = ManpowerLogReply::where('manpower_id',$task->id)->get();
                    foreach ($manpowerreply as $manpowerre){
                        ProjectsLogs::where('module_id',$manpowerre->id)->where('module','manpower_reply')->delete();
                        $manpowerre->delete();
                    }
                    $task->delete();

                }
                $response['status'] = 200;
                $response['message'] = 'Manpower logs deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-man-power',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function createManpowerCat(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $punchitemid = $request->categoryid;
                if(!empty($punchitemid)){
                    $timeLog =  ManpowerCategory::find($punchitemid);
                }else{
                    $timeLog = new ManpowerCategory();
                }
                $timeLog->company_id = $user->company_id;
                $timeLog->title = $request->title;
                $timeLog->save();
                $response['status'] = 200;
                $response['message'] = 'Man power Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-man-power-category',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function appManpowerCat(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projects = ManpowerCategory::get()->toArray();
                $response['status'] = 200;
                $response['message'] = 'Man power logs List Fetched';
                $response['response'] = $projects;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'man-power-category-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function deleteManpowerCat(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $manpowerarray = array_filter(explode(',',$request->categoryid));
                foreach ($manpowerarray as $manpower) {
                    $task = ManpowerCategory::findOrFail($manpower);
                    $task->delete();
                }
                $response['status'] = 200;
                $response['message'] = 'Man power logs deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-man-power-category',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function storeImage(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $taskid = $request->taskid;
                if ($request->hasFile('file')) {
                    $storage = storage();
                    $companyid = $user->company_id;
                    $fileData =  $request->file('file');
                    $file = new ManpowerLogFiles();
                    $file->added_by = $user->id;
                    $file->company_id = $companyid;
                    $file->task_id = $request->taskid ?: 0;
                    $file->reply_id = $request->replyid ?: 0;
                    $file->manpower_id = $request->manpowerid ?: 0;
                    switch($storage) {
                        case 'local':
                            $destinationPath = 'uploads/manpower-log-files/'.$request->manpowerid;
                            if (!file_exists('public/'.$destinationPath)) {
                                mkdir('public/'.$destinationPath, 0777, true);
                            }
                            $fileData->storeAs($destinationPath, $fileData->hashName());
                            break;
                        case 's3':
                            Storage::disk('s3')->putFileAs('/manpower-log-files/'.$request->manpowerid, $fileData, $fileData->hashName(), 'public');
                            break;
                        case 'google':
                            $dir = '/';
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                            $dir = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', 'manpower-log-files')
                                ->first();
                            if(!$dir) {
                                Storage::cloud()->makeDirectory('manpower-log-files');
                            }
                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->manpowerid)
                                ->first();

                            if ( ! $directory) {
                                Storage::cloud()->makeDirectory($dir['path'].'/'.$request->manpowerid);
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('filename', '=', $request->manpowerid)
                                    ->first();
                            }
                            Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());
                            $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());
                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('manpower-log-files/'.$request->manpowerid.'/', $fileData, $fileData->getClientOriginalName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/task-files/'.$request->manpowerid.'/'.$fileData->getClientOriginalName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $file->dropbox_link = $dropboxResult['url'];
                            break;
                    }

                    $file->filename = $fileData->getClientOriginalName();
                    $file->hashname = $fileData->hashName();
                    $file->size = $fileData->getSize();
                    $file->save();
                    $response['status'] = 200;
                    $response['imageid'] = $file->id;
                    $response['message'] = 'Man power Image Uploaded';
                    return $response;

                }
                $response['status'] = 300;
                $response['message'] = 'Uploading failed. Please try again';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'store-man-power-image',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function replyPost(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                if(!empty($request->manpowerid)){
                    $id = $request->manpowerid;
                    $mentionusers = !empty($request->mentionusers) ? $request->mentionusers : '';
                    $punchitem = ManpowerLog::find($id);
                    if($punchitem){
                        $pi = new ManpowerLogReply();
                        $pi->company_id = $user->company_id;
                        $pi->comment = $request->comment;
                        $pi->manpower_id = $punchitem->id;
                        $pi->added_by = $user->id;
                        $pi->mentionusers = $mentionusers;
                        $pi->save();

                        $imagesarray = array_filter(explode(',',$request->imagesids));
                        if(!empty($imagesarray)){
                            foreach ($imagesarray as $images){
                                $punchfile = ManpowerLogFiles::find($images);
                                if(!empty($punchfile->id)){
                                    $punchfile->manpower_id = $punchitem->id;
                                    $punchfile->reply_id = $pi->id;
                                    $punchfile->save();
                                }
                            }
                        }

                        $medium = $request->medium ?: 'android';
                        $createlog = new ProjectsLogs();
                        $createlog->company_id = $user->company_id;
                        $createlog->added_id = $user->id;
                        $createlog->module_id = $pi->id;
                        $createlog->module = 'manpower_reply';
                        $createlog->modulename = 'manpower_comment';
                        $createlog->project_id = $punchitem->project_id;
                        $createlog->subproject_id = $punchitem->title_id ?: 0;
                        $createlog->segment_id = $punchitem->segment_id ?: 0;
                        $createlog->medium = $medium;
                        $createlog->heading =  $request->comment;
                        $createlog->mentionusers = $mentionusers;
                        if(empty($manpowerid)) {
                            $createlog->description = 'Manpower log reply created by ' . $user->name . ' for the project ' . get_project_name($punchitem->project_id);
                        }else{
                            $createlog->description = 'Manpower log reply updated by ' . $user->name . ' for the project ' . get_project_name($punchitem->project_id);
                        }
                        $createlog->save();
                        $response = array();
                        $response['status'] = 200;
                        $response['manpowerid'] = $punchitem->id;
                        $response['replyid'] = $pi->id;
                        $response['message'] = 'Reply Updated Successfully';
                        return $response;
                    }
                }
                $response['status'] = 300;
                $response['message'] = 'Manpower log not Found';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'manpower-reply',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function replyList(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                if(!empty($request->manpowerid)){
                    $id = $request->manpowerid;
                    $punchitem = ManpowerLog::find($id);
                    if($punchitem){
                        $page = $request->page;
                        $response = array();
                        if(empty($page)||$page=='all'){
                            $pi = ManpowerLogReply::where('manpower_id',$punchitem->id)->get();
                        }else{
                            $count = pagecount();
                            $skip = 0;
                            if($page){
                                $skip = $page*$count;
                            }
                            $pi = ManpowerLogReply::where('manpower_id',$punchitem->id)->offset($skip)->take($count)->get();
                        }
                        $response = array();
                        $response['status'] = 200;
                        $response['responselist'] = $pi;
                        $response['message'] = 'Reply Updated Successfully';
                        return $response;
                    }
                }
                $response['status'] = 301;
                $response['message'] = 'Manpower log not Found';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'manpower-reply-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

}