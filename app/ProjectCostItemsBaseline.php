<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ProjectCostItemsBaseline extends Model
{
    protected $table = 'project_cost_item_baseline';

    protected static function boot()
    {
        parent::boot();

    }
}
