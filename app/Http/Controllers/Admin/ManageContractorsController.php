<?php

namespace App\Http\Controllers\Admin;

use App\ClientDetails;
use App\Helper\Reply;
use App\Http\Requests\Admin\Contractors\StoreContractorsRequest;
use App\Http\Requests\Admin\Contractors\UpdateContractorsRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;
use App\Invoice;
use App\Lead;
use App\Notifications\NewUser;
use App\PurposeConsent;
use App\PurposeConsentUser;
use App\Role;
use App\Contractors;
use App\RoleUser;
use App\UniversalSearch;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class ManageContractorsController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.contractors';
        $this->pageIcon = 'icon-people';
        $this->activeMenu = 'hr';
        $this->middleware(function ($request, $next) {
            if (!in_array('contractors', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company_id = Auth::user()->company_id;
        $this->contractors = Contractors::where('added_by', $company_id)->get();
        if($company_id == '' || $company_id == null){
            $this->contractors = Contractors::all();
        }

        $this->totalContractor = count($this->contractors);

        return view('admin.contractors.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($leadID = null)
    {
        return view('admin.contractors.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreContractorsRequest $request)
    {
        //dd($request);
        $user = $this->user;

        $newuser = new User();
        $newuser->company_id = $user->company_id;
        $newuser->name = $request->name;
        $newuser->email = $request->email;
        $newuser->mobile = $request->mobile;
        $newuser->password = bcrypt(trim($request->password));
        $newuser->save();

        $contractors = new Contractors();
        $contractors->user_id = $newuser->id;
        $contractors->firmname = $request->firmname;
        $contractors->name = $request->name;
        $contractors->email = $request->email;
        $contractors->mobile = $request->mobile;
        $contractors->landline = $request->landline;
        $contractors->address = $request->address;
        $contractors->gst_no = $request->gst_number;
        $contractors->added_by = $user->id;
        $contractors->company_id = $user->company_id;
        $contractors->status = 'active';
        $contractors->save();

        $roles = Role::where('company_id',$user->company_id)->where('name','contractor')->first();
        if(!empty($roles->id)){

        $roleuser = RoleUser::where('user_id',$newuser->id)->where('role_id',$roles->id)->first();
        if(empty($roleuser->id)){
            $roleuser = new RoleUser();
            $roleuser->user_id = $newuser->id;
            $roleuser->role_id = $roles->id;
            $roleuser->save();
        }
        }
        return Reply::redirect(route('admin.contractors.index'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->contractor = Contractors::find($id);
        return view('admin.contractors.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateContractorsRequest $request, $id)
    {
        $user = $this->user;
        $contractors = Contractors::find($id);
        $contractors->firmname = $request->firmname;
        $contractors->name = trim($request->name);
        $contractors->email = trim($request->email);
        $contractors->mobile = trim($request->mobile);
        $contractors->landline = $request->landline;
        $contractors->address = $request->address;
        $contractors->gst_no = $request->gst_number;
        $contractors->added_by = $user->id;
        $contractors->company_id = $user->company_id;
        $contractors->save();

        $user = User::find($contractors->user_id);
        if(!empty($user->id)){
            $user->name = trim($request->name);
            $user->email = trim($request->email);
            $user->mobile = trim($request->mobile);
            if($request->password){
                $user->password = bcrypt(trim($request->password));
            }
            $user->save();
        }
        return Reply::redirect(route('admin.contractors.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        $contractor = Contractors::find($id);
        User::destroy($contractor->user_id);
        $contractor->delete();
        RoleUser::where('user_id',$contractor->user_id)->delete();
        DB::commit();
        return Reply::success(__('messages.contractorsDeleted'));
    }

    public function data(Request $request)
    {
        $users = Contractors::where('company_id', Auth::user()->company_id);
        if ($request->contractors != 'all' && $request->contractors != '') {
            $users = $users->where('users.id', $request->contractors);
        }

        $users = $users->get();

        return DataTables::of($users)
            ->addColumn('action', function ($row) {
                return '<a href="' . route('admin.contractors.edit', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a> 
                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->editColumn(
                'name',
                function ($row) {
                    return '<a href="' . route('admin.contractors.edit', $row->id) . '">' . ucfirst($row->name) . '</a>';
                }
            )
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->addIndexColumn()
            ->rawColumns(['name', 'action', 'status'])
            ->make(true);
    }
    public function export($status, $client)
    {
        $rows = User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->withoutGlobalScope('active')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->where('roles.name', 'client')
            ->where('roles.company_id', company()->id)
            ->leftJoin('client_details', 'users.id', '=', 'client_details.user_id')
            ->select(
                'users.id',
                'client_details.name',
                'client_details.email',
                'client_details.mobile',
                'client_details.firmname',
                'client_details.address',
                'client_details.gst_no',
                'client_details.created_at'
            )
            ->where('client_details.company_id', company()->id);

        if ($status != 'all' && $status != '') {
            $rows = $rows->where('users.status', $status);
        }

        if ($client != 'all' && $client != '') {
            $rows = $rows->where('users.id', $client);
        }

        $rows = $rows->get()->makeHidden(['image']);

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Name', 'Email', 'Mobile', 'Company Name', 'Address', 'Website', 'Created at'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($rows as $row) {
            $exportArray[] = $row->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('clients', function ($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Clients');
            $excel->setCreator('Worksuite')->setCompany($this->companyName);
            $excel->setDescription('clients file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));
                });
            });
        })->download('xlsx');
    }
}
