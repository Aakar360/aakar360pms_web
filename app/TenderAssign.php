<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class TenderAssign extends Model
{
    protected $table = 'tender_assign';

    protected static function boot()
    {
        parent::boot();
    }
}
