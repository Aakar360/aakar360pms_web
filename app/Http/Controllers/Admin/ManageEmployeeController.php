<?php

namespace App\Http\Controllers\Admin;

use App\Allowance;
use App\Attendance;
use App\AttendanceSetting;
use App\Company;
use App\Currency;
use App\Designation;
use App\DirectReportTeam;
use App\DocumentType;
use App\Employee;
use App\EmployeeDetails;
use App\EmployeeDocs;
use App\EmployeeEducationDetails;
use App\EmployeeEmergencyContactDetail;
use App\EmployeeSkill;
use App\EmployeeTeam;
use App\Helper\Reply;
use App\Http\Requests\Admin\Employee\StoreRequest;
use App\Http\Requests\Admin\Employee\UpdateRequest;
use App\Http\Requests\Admin\User\StoreUser;
use App\Http\Requests\Admin\User\UpdateEmployee;
use App\Leave;
use App\LeaveType;
use App\Module;
use App\ModuleSetting;
use App\Notifications\NewUser;
use App\Overtime;
use App\Package;
use App\PaypalInvoice;
use App\PayrollVerify;
use App\Permission;
use App\PermissionRole;
use App\PfandEsiSettings;
use App\Project;
use App\ProjectMember;
use App\ProjectPermission;
use App\ProjectTimeLog;
use App\PtSettings;
use App\RazorpayInvoice;
use App\Role;
use App\RoleUser;
use App\Skill;
use App\StripeInvoice;
use App\Task;
use App\Team;
use App\UniversalSearch;
use App\User;
use App\UserActivity;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use App\TaskboardColumn;

class ManageEmployeeController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.employee';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'hr';
        $this->middleware(function ($request, $next) {
            if (!in_array('employees', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->user;
        $pids = ProjectMember::where('user_id', $user->id)->get()->pluck('project_id');
        $this->projectlist = Project::where('company_id', $user->company_id)->orWhereIn('id', $pids)->get();
        $this->employees = Employee::where('company_id',$this->companyid)->where('user_type','employee')->get();
        $this->skills = Skill::all();
        $this->departments = Team::all();
        $this->designations = Designation::all();
        $this->totalEmployees = count($this->employees);
        $this->roles = Role::where('roles.name', '<>', 'client')->get();
        $whoseProjectCompleted = ProjectMember::join('projects', 'projects.id', '=', 'project_members.project_id')
            ->join('users', 'users.id', '=', 'project_members.user_id')
            ->select('users.*')
            ->groupBy('project_members.user_id')
            ->havingRaw("min(projects.completion_percent) = 100 and max(projects.completion_percent) = 100")
            ->orderBy('users.id')
            ->get();

        $notAssignedProject = User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->select('users.id', 'users.name')->whereNotIn('users.id', function ($query) {
                $query->select('user_id as id')->from('project_members');
            })
            ->where('roles.name', '<>', 'client')
            ->get();
        $this->freeEmployees = $whoseProjectCompleted->merge($notAssignedProject)->count();
        return view('admin.employee.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employee = new EmployeeDetails();
        $this->fields = $employee->getCustomFieldGroupsWithFields()->fields;
        $this->skills = Skill::all()->pluck('name')->toArray();
        $this->teams = Team::all();
        $this->designations = Designation::all();
        return view('admin.employee.create', $this->data);
    }

    /**
     * @param StoreRequest $request
     * @return array
     */
    public function store(Request $request)
    {

        $company = company();
        if (!is_null($company->employees) && $company->employees->count() >= $company->package->max_employees) {
            return Reply::error(__('messages.upgradePackageForAddEmployees', ['employeeCount' => company()->employees->count(), 'maxEmployees' => $company->package->max_employees]));
        }

        if (!is_null($company->employees) && $company->package->max_employees < $company->employees->count()) {
            return Reply::error(__('messages.downGradePackageForAddEmployees', ['employeeCount' => company()->employees->count(), 'maxEmployees' => $company->package->max_employees]));
        }


        $user = new Employee();
        $user->company_id = $company->id;
        $user->added_by = $this->user->id;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->user_type = 'employee';
        $user->password = Hash::make(trim($request->password));
        $user->gender = $request->gender;
        $user->status = 'active';
        if ($request->hasFile('image')) {
                $storage = storage();
                $image = $request->image->hashName();
                switch($storage) {
                    case 'local':
                        $request->image->storeAs('user-uploads/avatar', $image);
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs('avatar/', $request->image, $request->image->getClientOriginalName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('image', '=', 'avatar')
                            ->first();

                        if(!$dir) {
                            Storage::cloud()->makeDirectory('avatar');
                        }

                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('image', '=', $request->image)
                            ->first();

                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/');
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('image', '=', $request->image)
                                ->first();
                        }

                        Storage::cloud()->putFileAs($directory['basename'], $request->image, $request->image->getClientOriginalName());

                        $user->google_url = Storage::cloud()->url($directory['path'].'/'.$request->image->getClientOriginalName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('task-files/'.'/', $request->image, $request->image->getClientOriginalName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/avtar/'.'/'.$request->image->getClientOriginalName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $user->dropbox_link = $dropboxResult['url'];
                        break;
                }

                $user->image= $request->image->getClientOriginalName();
        }
        $user->save();
        $employeedetail = new EmployeeDetails();
        $employeedetail->company_id = $company->id;
        $employeedetail->employee_id = $user->id;
        $employeedetail->address = $request->address;
        $employeedetail->save();
        return Reply::redirect(route('admin.employee.index'), __('messages.employeeAdded'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->employee = Employee::findOrFail($id);
        $this->employeeDetail = EmployeeDetails::where('employee_id', '=', $this->employee->id)->first();
       /* $this->employeeDocs = EmployeeDocs::where('employee_id', '=', $this->employee->id)->get();*/

        if (!is_null($this->employeeDetail)) {
            $this->employeeDetail = $this->employeeDetail->withCustomFields();
            $this->fields = $this->employeeDetail->getCustomFieldGroupsWithFields()->fields;
        }

        $completedTaskColumn = TaskboardColumn::where('slug', 'completed')->first();
        $this->taskCompleted = Task::where('user_id', $id)
            ->where('board_column_id', $completedTaskColumn->id)
            ->count();

        $hoursLogged = ProjectTimeLog::where('user_id', $id)->sum('total_minutes');
        $timeLog = intdiv($hoursLogged, 60) . ' hrs ';

        if (($hoursLogged % 60) > 0) {
            $timeLog .= ($hoursLogged % 60) . ' mins';
        }

        $this->hoursLogged = $timeLog;

        $this->activities = UserActivity::where('user_id', $id)->orderBy('id', 'desc')->get();
        $this->projects = Project::select('projects.id', 'projects.project_name', 'projects.deadline', 'projects.completion_percent')
            ->join('project_members', 'project_members.project_id', '=', 'projects.id')
            ->where('project_members.user_id', '=', $id)
            ->get();
        $this->leaves = Leave::byUser($id);

        $this->leaveTypes = LeaveType::byUser($id);
        $this->allowedLeaves = LeaveType::sum('no_of_leaves');
        $this->roles = Role::where('company_id', Auth::user()->company_id)->where('name', '<>', 'admin')->get();
        $this->projects = Project::where('company_id', Auth::user()->company_id)->get();
        $this->user_roles = RoleUser::where('user_id', $this->employee->id)->pluck('role_id')->toArray();
        $this->user_rolesx = Role::whereIn('id', $this->user_roles)->pluck('name')->toArray();
        $this->totalPermissions = Permission::count();
        $user = Auth::user();
        $company = Company::find($user->company_id);
        $package = Package::find($company->package_id);
        $mods = $package->module_in_package;
        //dd();
        $ackageModules = ModuleSetting::whereIn('module_name', (array)json_decode($mods))->pluck('module_name')->toArray();
        $this->modulesData = Module::whereIn('module_name', $ackageModules)->get();
        $this->firstAdmin = User::firstAdmin();
        return view('admin.employee.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->userDetail = Employee::findOrFail($id);
        $this->employeeDetail = EmployeeDetails::where('employee_id', '=', $this->userDetail->id)->first();
        $this->skills = Skill::all()->pluck('name')->toArray();
        $this->teams = Team::all();
        $this->designations = Designation::all();
        if (!is_null($this->employeeDetail)) {
            $this->employeeDetail = $this->employeeDetail->withCustomFields();
            $this->fields = $this->employeeDetail->getCustomFieldGroupsWithFields()->fields;
        }
        return view('admin.employee.edit', $this->data);
    }

    /**
     * @param UpdateRequest $request
     * @param $id
     * @return array
     */
    public function update(Request $request, $id)
    {
        $user = $this->user;
        $employee = Employee::findOrFail($id);
        $employee->name = $request->name;
        $employee->email = $request->email;
        if ($request->password != '') {
            $employee->password = Hash::make($request->password);
        }
        $employee->mobile = $request->mobile;
        $employee->gender = $request->gender;
        $employee->status = $request->status;
        if ($request->hasFile('image')) {
            $storage = storage();
            $image = $request->image->hashName();
            switch($storage) {
                case 'local':
                    $request->image->storeAs('user-uploads/avatar', $image);
                    break;
                case 's3':
                    Storage::disk('s3')->putFileAs('avatar/', $request->image, $request->image->getClientOriginalName(), 'public');
                    break;
                case 'google':
                    $dir = '/';
                    $recursive = false;
                    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                    $dir = $contents->where('type', '=', 'dir')
                        ->where('image', '=', 'avatar')
                        ->first();

                    if(!$dir) {
                        Storage::cloud()->makeDirectory('avatar');
                    }

                    $directory = $dir['path'];
                    $recursive = false;
                    $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                    $directory = $contents->where('type', '=', 'dir')
                        ->where('image', '=', $request->image)
                        ->first();

                    if ( ! $directory) {
                        Storage::cloud()->makeDirectory($dir['path'].'/');
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('image', '=', $request->image)
                            ->first();
                    }

                    Storage::cloud()->putFileAs($directory['basename'], $request->image, $request->image->getClientOriginalName());

                    $user->google_url = Storage::cloud()->url($directory['path'].'/'.$request->image->getClientOriginalName());

                    break;
                case 'dropbox':
                    Storage::disk('dropbox')->putFileAs('task-files/'.'/', $request->image, $request->image->getClientOriginalName());
                    $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                    $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                        [\GuzzleHttp\RequestOptions::JSON => ["path" => '/avtar/'.'/'.$request->image->getClientOriginalName()]]
                    );
                    $dropboxResult = $res->getBody();
                    $dropboxResult = json_decode($dropboxResult, true);
                    $user->dropbox_link = $dropboxResult['url'];
                    break;
            }

            $employee->image= $request->image->getClientOriginalName();
        }
        $employee->save();

        $employeedetails = EmployeeDetails::where('employee_id', '=', $user->id)->first();
        if (empty($employeedetails)) {
            $employeedetails = new EmployeeDetails();
            $employeedetails->employee_id = $user->id;
        }
        $employeedetails->address = $request->address;
        $employeedetails->save();
        return Reply::redirect(route('admin.employee.index'), __('messages.employeeUpdated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Employee::findOrFail($id);
        $user->delete();
        return Reply::success(__('messages.employeeDeleted'));
    }

    public function data(Request $request)
    {

        $user = $this->user;

        $users =  Employee::where('company_id',$this->companyid)->where('user_type','employee')->get();

        return DataTables::of($users)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {

                return '<a href="' . route('admin.employee.edit', [$row->id]) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit & Add Details"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                      <a href="' . route('admin.employee.show', [$row->id]) . '" class="btn btn-success btn-circle"
                      data-toggle="tooltip" data-original-title="View Employee Details"><i class="fa fa-search" aria-hidden="true"></i></a>

                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->editColumn(
                'created_at',
                function ($row) {
                    return Carbon::parse($row->created_at)->format($this->global->date_format);
                }
            )
            ->editColumn(
                'email',
                function ($row) {
                    return $row->email;
                }
            )
            ->editColumn(
                'status',
                function ($row) {
                    if ($row->status == 'active') {
                        return '<label class="label label-success">' . __('app.active') . '</label>';
                    } else {
                        return '<label class="label label-danger">' . __('app.inactive') . '</label>';
                    }
                }
            )
            ->editColumn('name', function ($row) {

              return  '<div class="row"><div class="col-sm-3 col-xs-4">'.get_employee_images($row->id).'</div><div class="col-sm-9 col-xs-8"><a href="' . route('admin.employee.show', $row->id) . '">'.ucwords($row->name).'</a></div></div>';

            })
            ->rawColumns(['name', 'action', 'status'])
            ->make(true);
    }
    public function addJobProfile($id){

        $this->user = User::find(Auth::user()->id);
        $employeedetails = new EmployeeDetails();
        $this->userDetail = Employee::findOrFail($id);
        $this->employeeDetail = EmployeeDetails::where('employee_id', '=', $this->userDetail->id)->first();
        $this->fields = $employeedetails->getCustomFieldGroupsWithFields()->fields;
        $this->skills = Skill::all()->pluck('name')->toArray();
        $this->teams = Team::all();
        $this->designations = Designation::all();
        $this->employeeSkill = EmployeeSkill::where('employee_id', '=', $this->userDetail->id)->get();
        $this->id = $id;
        return view('admin.employee.create_job_profile',$this->data);
    }

    public function storeJobProfile(Request $request){
        $tags = json_decode($request->tags);
        if (!empty($tags)) {
            EmployeeSkill::where('employee_id', $request->id)->delete();
            foreach ($tags as $tag) {
                $skillData = Skill::firstOrCreate(['name' => strtolower($tag->value)]);
                $skill = new EmployeeSkill();
                $skill->employee_id = $request->id;
                $skill->skill_id = $skillData->id;
                $skill->save();
            }
        }
        $employee = EmployeeDetails::where('employee_id', '=', $request->id)->first();

        if (empty($employee)) {
            $employee = new EmployeeDetails();
            $employee->user_id = $request->id;
            $employee->unique_id = $request->unique_id;
        }else{
            $employee->unique_id = $request->unique_id;
        }
        if($request->workingrate == "hourly"){
            $employee->hourly_rate = $request->hourly_rate ? trim($request->hourly_rate) :'';
        }elseif ($request->workingrate == "monthly"){
            $employee->monthly_rate = $request->hourly_rate ? trim($request->hourly_rate) :'';
        }else{
            $employee->yearly_rate = $request->hourly_rate ? trim($request->hourly_rate) :'';
        }
        $employee->joining_date = Carbon::parse($request->startDate)->format('Y-m-d');
        $employee->last_date = null;
        if ($request->last_date != '') {
            $employee->last_date = Carbon::parse($request->startDate)->format('Y-m-d');
        }
        $employee->department_id = $request->department;
        $employee->designation_id = $request->designation;
        $employee->save();
        if ($request->get('custom_fields_data')) {
            $employee->updateCustomFieldData($request->get('custom_fields_data'));
        }
        return Reply::success(__('Job Profile Added Successfully'));
    }

    public function  addProfileDocuments ($id){
        $this->user = User::find(Auth::user()->id);
        $employee = new EmployeeDetails();
        $this->fields = $employee->getCustomFieldGroupsWithFields()->fields;
        $this->skills = Skill::all()->pluck('name')->toArray();
        $this->teams = Team::all();
        $this->designations = Designation::all();
        $this->userDetail = Employee::findOrFail($id);
        $this->employeeDetail = EmployeeDetails::where('employee_id', '=', $this->userDetail->id)->first();
        $this->skills = Skill::all()->pluck('name')->toArray();
        $this->teams = Team::all();
        if (!is_null($this->employeeDetail)) {
            $this->employeeDetail = $this->employeeDetail->withCustomFields();
            $this->fields = $this->employeeDetail->getCustomFieldGroupsWithFields()->fields;
        }
        $this->docs = EmployeeDocs::where('employee_id',$id)->get();
        return view('admin.employee.create_document_profile',$this->data);
    }

    public function addId($id){
        $this->types = DocumentType::all();
        $this->id = $id;
        return  view('admin.employee.add_id', $this->data);
    }

    public function uploadId(Request $request, $id){
        $user = new EmployeeDocs();
        if ($request->hasFile('file')) {
            $storage = storage();
            $image = $request->file->hashName();
            switch($storage) {
                case 'local':
                    $request->file->storeAs('user-uploads/documents', $image);
                    break;
                case 's3':
                    $st = Storage::disk('s3')->putFileAs('documents/', $request->file, $request->file->hashName(), 'public');
                    $user->filename =  $image;
                    break;
                case 'google':
                    $dir = '/';
                    $recursive = false;
                    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                    $dir = $contents->where('type', '=', 'dir')
                        ->where('image', '=', 'documents')
                        ->first();

                    if(!$dir) {
                        Storage::cloud()->makeDirectory('documents');
                    }

                    $directory = $dir['path'];
                    $recursive = false;
                    $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                    $directory = $contents->where('type', '=', 'dir')
                        ->where('image', '=', $request->file)
                        ->first();

                    if ( ! $directory) {
                        Storage::cloud()->makeDirectory($dir['path'].'/');
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('image', '=', $request->file)
                            ->first();
                    }

                    Storage::cloud()->putFileAs($directory['basename'], $request->file, $request->file->getClientOriginalName());

                    $user->google_url = Storage::cloud()->url($directory['path'].'/'.$request->file->getClientOriginalName());
                    $user->filename = $user->google_url;
                    break;
                case 'dropbox':
                    Storage::disk('dropbox')->putFileAs('task-files/'.'/', $request->file, $request->file->getClientOriginalName());
                    $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                    $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                        [\GuzzleHttp\RequestOptions::JSON => ["path" => '/documents/'.'/'.$request->file->getClientOriginalName()]]
                    );
                    $dropboxResult = $res->getBody();
                    $dropboxResult = json_decode($dropboxResult, true);
                    $user->dropbox_link = $dropboxResult['url'];
                    $user->filename = $user->dropbox_link;
                    break;
            }
            $user->company_id = $this->user->company_id;
            $user->employee_id = $id;
            $user->doc_type = $request->document_type;
            $user->filename = $request->file->hashName();
            $user->save();
        }
        return Reply::redirect(route('admin.employee.employee_profile_documents', [$id]), __('Document Uploaded Successfully'));
    }
    public function deleteId($id)
    {
        EmployeeDocs::destroy($id);
        return Reply::success(__('Document Deleted'));
    }
    public function editId($id){
        $this->types = DocumentType::all();
        $this->doc = EmployeeDocs::where('id',$id)->first();
        $this->id = $id;
        return  view('admin.employee.edit_id', $this->data);
    }

    public function updateId(Request $request, $id){
        $user = EmployeeDocs::find($id);
        if ($request->hasFile('file')) {
            $storage = storage();
            $image = $request->file->hashName();
            switch($storage) {
                case 'local':
                    $request->file->storeAs('user-uploads/documents', $image);
                    break;
                case 's3':
                    $st = Storage::disk('s3')->putFileAs('documents/', $request->file, $request->file->getClientOriginalName(), 'public');
                    $user->filename = "https://" . config('filesystems.disks.s3.bucket') . ".s3.amazonaws.com/".str_replace('//', '/',$st);
                    break;
                case 'google':
                    $dir = '/';
                    $recursive = false;
                    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                    $dir = $contents->where('type', '=', 'dir')
                        ->where('image', '=', 'documents')
                        ->first();

                    if(!$dir) {
                        Storage::cloud()->makeDirectory('documents');
                    }

                    $directory = $dir['path'];
                    $recursive = false;
                    $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                    $directory = $contents->where('type', '=', 'dir')
                        ->where('image', '=', $request->file)
                        ->first();

                    if ( ! $directory) {
                        Storage::cloud()->makeDirectory($dir['path'].'/');
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('image', '=', $request->file)
                            ->first();
                    }

                    Storage::cloud()->putFileAs($directory['basename'], $request->file, $request->file->getClientOriginalName());

                    $user->google_url = Storage::cloud()->url($directory['path'].'/'.$request->file->getClientOriginalName());
                    $user->filename = $user->google_url;
                    break;
                case 'dropbox':
                    Storage::disk('dropbox')->putFileAs('task-files/'.'/', $request->file, $request->file->getClientOriginalName());
                    $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                    $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                        [\GuzzleHttp\RequestOptions::JSON => ["path" => '/documents/'.'/'.$request->file->getClientOriginalName()]]
                    );
                    $dropboxResult = $res->getBody();
                    $dropboxResult = json_decode($dropboxResult, true);
                    $user->dropbox_link = $dropboxResult['url'];
                    $user->filename = $user->dropbox_link;
                    break;
            }
            $user->doc_type = $request->document_type;
            $user->filename = $request->file->getClientOriginalName();
            $user->save();
        }
        return Reply::redirect(route('admin.employee.employee_profile_documents', [$id]), __('Document Updated Successfully'));
    }
    public function reportTeam($id){
        $user = User::find(Auth::user()->id);
        $this->user =$user;
        $employees = Employee::where('company_id',$user->company_id)->get();
        $this->employeelists =  view('admin.employee.employeelist',['employees'=>$employees])->render();
        $this->userDetail = Employee::findOrFail($id);
        $this->employeeDetail = EmployeeDetails::where('employee_id', '=', $this->userDetail->id)->first();
        $this->employeeTeam = EmployeeTeam::where('employee_id',$this->userDetail->id)->get();
        $this->employeeDirect = DirectReportTeam::where('employee_id',$this->userDetail->id)->get();
        return view('admin.employee.report_team',$this->data);
    }
    public function educationDetail($id){
        $user = User::find(Auth::user()->id);
        $this->user =$user;
        $employees = Employee::where('company_id',$user->company_id)->get();
        $this->employeelists =  view('admin.employee.employeelist',['employees'=>$employees])->render();
        $this->userDetail = Employee::findOrFail($id);
        $this->employeeDetail = EmployeeDetails::where('employee_id', '=', $this->userDetail->id)->first();
        return view('admin.employee.educationdetail',$this->data);
    }
    public function familyDetail($id){
        $user = User::find(Auth::user()->id);
        $this->user =$user;
        $employees = Employee::where('company_id',$user->company_id)->get();
        $this->employeelists =  view('admin.employee.employeelist',['employees'=>$employees])->render();
        $this->userDetail = Employee::findOrFail($id);
        $this->employeeDetail = EmployeeDetails::where('employee_id', '=', $this->userDetail->id)->first();
        $this->familyDetail = EmployeeEmergencyContactDetail::where('employee_id',$id)->get();
        return view('admin.employee.familydetail',$this->data);
    }
    public function storeReportTeam(Request $request){
        $user = User::find(Auth::user()->id);
        $team_id = $request->employee;
        foreach ($team_id as $key=>$ti) {
            $et = new EmployeeTeam();
            $et->team_id = $ti;
            $et->company_id = $user->company_id;
            $et->employee_id = $request->user_id;
            $et->type = $request->type[$key];
            $et->save();
        }
        return Reply::success(__('Added Successfully'));
    }

    public function storeDirectTeam(Request $request){
        $user = User::find(Auth::user()->id);
        $team_id = $request->employee;
        foreach ($team_id as $key=>$ti) {
            $et = new DirectReportTeam();
            $et->team_id = $ti;
            $et->company_id = $user->company_id;
            $et->employee_id = $request->user_id;
            $et->save();
        }
        return Reply::success(__('Added Successfully'));
    }
    public function storeReportManager(Request $request){
        $reportManager = array_combine($request->date, $request->occasion);
        foreach ($reportManager as $index => $value) {
            if ($index){
                $add = Holiday::firstOrCreate([
                    'user_id' => $value->user_id,
                    'type' => $value->type,
                ]);
            }
        }
        return Reply::redirect(route('member.employee.team'), __('messages.managerReportAddedSuccess'));
    }
    public function storeFamilyDetail(Request $request){
         $user = $this->user;
        if($request->emg_name > 0){
            foreach ($request->emg_name as $index => $value) {

                $add = EmployeeEmergencyContactDetail::firstOrCreate([
                    'company_id' => $user->company_id,
                    'employee_id' => $request->employee_id,
                    'user_id' => $user->id,
                    'family_memeber_name' => $request->emg_name[$index],
                    'family_memeber_relation' => $request->emg_relation[$index],
                    'family_memeber_number' => $request->number[$index],
                ]);
            }
        }
        return Reply::redirect(route('admin.employee.family',[$request->employee_id]), __('messages.managerReportAddedSuccess'));
    }
    public function storeEducationDetail(Request $request){

        $date =  Carbon::today()->timezone($this->global->timezone);
        $user = $this->user;
        if($request->qualification_type > 0){
            foreach ($request->qualification_type as $index => $value) {

                $add = EmployeeEducationDetails::firstOrCreate([
                    'company_id' => $user->company_id,
                    'employee_id' => $request->employee_id,
                    'user_id' => $user->id,
                    'qualification_type' => $request->qualification_type[$index],
                    'course_name' => $request->course_name[$index],
                    'course_type' => $request->course_type[$index],
                    'stream' => $request->stream[$index],
                    'course_start_date' => $request->startDate[$index],
                    'course_end_date' => $request->endDate[$index],
                    'college_name' => $request->college_name[$index],
                    'univercity_name' => $request->univercity_name[$index],
                ]);
            }
        }
        return Reply::redirect(route('admin.employee.family',[$request->employee_id]), __('messages.managerReportAddedSuccess'));
    }
}
