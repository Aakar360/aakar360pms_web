<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class SubmittalsFile extends Model
{
    protected $table = 'submittals_files';

    protected static function boot()
    {
        parent::boot();
    }
}
