<?php

namespace App\Http\Controllers\Admin;

use App\EmployeeDetails;
use App\EmployeeTeam;
use App\Helper\Reply;
use App\Http\Requests\ProjectMembers\SaveGroupMembers;
use App\Http\Requests\ProjectMembers\StoreProjectMembers;
use App\Notifications\NewProjectMember;
use App\Project;
use App\ProjectMember;
use App\Team;
use App\TempUser;
use App\User;
use Illuminate\Http\Request;

class ManageProjectMembersController extends AdminBaseController
{

    public function __construct() {
        parent::__construct();
        $this->pageIcon = 'icon-layers';
        $this->pageTitle = 'app.menu.project-members';
        $this->activeMenu = 'pms';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->projectlist = Project::orderBy('id','desc')->get();
        return view('admin.projects.project-member.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProjectMembers $request)
    {
        $user = $this->user;
        $users = $request->user_id;

        foreach($users as $nuser){
            $member = new ProjectMember();
            $member->user_id = $nuser;
            $member->project_id = $request->project_id;
            $member->user_type = $request->user_type;
            $member->company_id = $user->company_id;
            $member->save();

            /*$notifyUser = User::withoutGlobalScope('active')->findOrFail($user);
            $notifyUser->notify(new NewProjectMember($member));

            $this->logProjectActivity($request->project_id, ucwords($member->user->name).' '.__('messages.isAddedAsProjectMember'));*/
        }

        return Reply::success(__('messages.membersAddedSuccessfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->user;
        $this->projectlist = Project::orderBy('id','desc')->get();
        if(is_numeric($id)){
            $this->project = Project::findOrFail($id);
            $this->employees = User::doesntHave('member', 'and', function($query) use ($id){
                $query->where('project_id', $id);
            })->join('role_user', 'role_user.user_id', '=', 'users.id')
                ->join('roles', 'roles.id', '=', 'role_user.role_id')
                ->select('users.id', 'users.name', 'users.email', 'users.created_at')
                ->where('roles.name', 'employee')
                ->groupBy('users.id')
                ->get();
            $this->invitiesarray =  TempUser::where('company_id',$user->company_id)->get();
            $this->contractorarray =  ProjectMember::where('project_id',$id)->where('user_type','contractor')->get();
            $this->clientarray =  ProjectMember::where('project_id',$id)->where('user_type','client')->get();
            $this->employeearray =  ProjectMember::where('project_id',$id)->where('user_type','employee')->get();
        }
        $this->groups = Team::all();
        $this->invitelist = Team::all();
        return view('admin.projects.project-member.create', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $projectMember = ProjectMember::findOrFail($id);

        $project = Project::findOrFail($projectMember->project_id);

        if($project->project_admin == $projectMember->user_id){
            $project->project_admin = null;
            $project->save();
        }
        $projectMember->delete();

        return Reply::success(__('messages.memberRemovedFromProject'));
    }
    public function inviteDestroy($id)
    {
        $projectMember = TempUser::findOrFail($id);

        $projectMember->delete();

        return Reply::success(__('messages.invitationRemoved'));
    }

    public function storeGroup(SaveGroupMembers $request)
    {
        $groups = $request->group_id;
        $project = Project::find($request->project_id);

        foreach($groups as $group){

            $members = EmployeeDetails::where('department_id', $group)->get();

            foreach ($members as $user){
                $check = ProjectMember::where('user_id', $user->user_id)->where('project_id', $request->project_id)->first();

                if(is_null($check)){
                    $member = new ProjectMember();
                    $member->company_id = $user->company_id;
                    $member->user_id = $user->user_id;
                    $member->project_id = $request->project_id;
                    $member->user_type = $request->user_type;
                    $member->save();
                    if($project->publish_status == 'published') {
                        $notifyUser = User::find($user->user_id);
                        $notifyUser->notify(new NewProjectMember($member));

                        $this->logProjectActivity($request->project_id, ucwords($member->user->name) . __('messages.isAddedAsProjectMember'));
                    }
                }
            }
        }

        return Reply::success(__('messages.membersAddedSuccessfully'));
    }
    public function inviteMember(Request $request){

        $user = $this->user;
        $username = $request->mobile;
        $id = $request->id;
        if(!empty($id)){
            $tc = TempUser::where('id',$id)->first();
        }
        if(!empty($username)){
            $tc = TempUser::where('mobile',$username)->first();
        }
        if(empty($tc->id)){
            $tc = new TempUser();
            $tc->company_id = $user->company_id;
            $tc->mobile = $request->mobile;
            $tc->project_id = $request->project_id;
            $tc->user_type = $request->user_type;
            $tc->invited_by = $user->id;
            $tc->save();
        }
        $pn = Project::find($request->project_id);
        if (filter_var($username, FILTER_VALIDATE_EMAIL)){
            $data = 'You are invited to '.$pn->project_name.' project by '.$user->name.'. Please download the application using below given link to join this project.\n';
            $data .= 'Join now\n';
            $data .= 'https://play.google.com/store/apps/details?id=com.pms.akar360\n';
            $data .= 'Aakar360 project\n';
            $data .= 'Construction and interior management app\n';
            $data .= '✅ Replace Whasapp and massages\n';
            $data .= '✅ Manage work digitally\n';
            $data .= '✅ Track progress efficiently\n';
            $data .= '✅ Automate site update\n';
            $data .= '✅ Invite member to project';
            if(isset($user->id)){
                $mailarray = array();
                $mailarray['email'] = $tc->mobile;
                $mailarray['subject'] = 'Invitation for the project - '.$pn->project_name;
                $mailarray['message'] = $data;
                $user->sendEmail($mailarray);
            }
        }else{

        }
        return Reply::success(__('messages.inviteSentSuccessfully'));
    }
}
