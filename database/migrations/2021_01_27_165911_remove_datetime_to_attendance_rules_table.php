<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveDatetimeToAttendanceRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attendance_rules', function (Blueprint $table) {
            $table->string('anomaly_grace_in');
            $table->string('anomaly_grace_out');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attendance_rules', function (Blueprint $table) {
            $table->datetime('anomaly_grace_in_time');
            $table->datetime('anomaly_grace_out_time');
        });
    }
}
