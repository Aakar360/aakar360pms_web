<?php

namespace App\Http\Controllers\Api;

use App\AppProject;
use App\BoqCategory;
use App\Company;
use App\CostItems;
use App\Helper\Files;
use App\Helper\Reply;
use App\Http\Controllers\Controller;
use App\ManpowerLog;
use App\Notifications\NewClientTask;
use App\Notifications\NewTask;
use App\ProjectActivity;
use App\ProjectAttachmentDesigns;
use App\ProjectAttachmentFiles;
use App\ProjectCostItemsFinalQty;
use App\ProjectCostItemsPosition;
use App\ProjectCostItemsProduct;
use App\ProjectMember;
use App\ProjectMilestone;
use App\ProjectSegmentsPosition;
use App\ProjectSegmentsProduct;
use App\ProjectsLogs;
use App\ProjectTemplate;
use App\ProjectTimeLog;
use App\PunchItem;
use App\Rfi;
use App\Task;
use App\TaskboardColumn;
use App\TaskFile;
use App\TaskPercentage;
use App\Title;
use App\Traits\ProjectProgress;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\FileManager;
use App\Project;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use DB;
use Intervention\Image\Facades\Image;
use  View;
use Maatwebsite\Excel\Facades\Excel;

class AppTaskController extends Controller
{
    use AuthenticatesUsers;
    use ProjectProgress;

    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }

    protected function validatetToken($apikey, $token)
    {
        if (isset($apikey)) {
            if (!$this->validateAPI($apikey)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            $response = array();
            if (isset($token)) {
                if (!empty($token)) {
                    $user = User::where('appid', $token)->first();
                    if ($user === null) {
                        $response['message'] = 'Invalid token';
                        $response['status'] = 401;
                        return $response;
                    }
                    $response['user'] = $user;
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    return $response;
                }
                $response['message'] = 'Api Token cannot be empty';
                $response['status'] = 401;
                return $response;
            }
            $response['message'] = 'Api Token not sent';
            $response['status'] = 401;
            return $response;

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function __construct(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];

            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', $user->company_id)->first();
            $this->url = "https://" . config('filesystems.disks.s3.bucket') . ".s3.amazonaws.com/";

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function appTasks(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $workid = $request->workid;
                if ($workid) {
                    $projects = Task::where('task_category_id', $workid)->get()->toArray();
                } else {
                    $projects = Task::get()->toArray();
                }
                $response['status'] = 200;
                $response['message'] = 'Tasks List Fetched';
                $response['response'] = $projects;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'store-indents-image',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function createTask(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            $new = 0;
            try{
                $response = array();
                $projectid = $request->project_id;
                $subprojectid = $request->subproject_id;
                $segmentid = $request->segment_id;
                $taskid = $request->taskid;
                $boqid = $request->boqid;
                $memberExistsInTemplate = false;
                if (!empty($taskid)) {
                    $task = Task::find($taskid);
                    $task->company_id = $user->company_id;
                    $task->heading = $request->heading;
                }else{
                    $task = new Task();
                    $new = 1;
                    $task->heading = $request->heading;
                    $costitemheading = $request->costitemheading;
                    if($costitemheading=='other'){
                        $task->heading = $request->heading;
                    }
                    $task->company_id = $user->company_id;
                }
                if($request->description != ''){
                    $task->description = $request->description;
                }
                $task->start_date = Carbon::createFromFormat($this->global->date_format, $request->start_date, $this->global->timezone)->format('Y-m-d');
                $task->due_date = Carbon::createFromFormat($this->global->date_format, $request->due_date, $this->global->timezone)->format('Y-m-d');
                $task->user_id = $request->user_id;
                $task->project_id = $projectid;
                $task->title = $subprojectid;
                $task->segment = $segmentid ?: 0;
                $task->boqinclude = $request->boq ?: 0;
                $task->task_category_id = $request->itemid;
                $task->priority = $request->priority ?: 'medium';
                $task->board_column_id = 0;
                $task->status = $request->status;
                $task->created_by = $user->id;
                $task->dependent_task_id = $request->has('dependent') && $request->dependent == 'yes' && $request->has('dependent_task_id') && $request->dependent_task_id != '' ? $request->dependent_task_id : null;
                if ($request->board_column_id) {
                    $task->board_column_id = $request->board_column_id;
                }
                if ($request->milestone_id != '') {
                    $task->milestone_id = $request->milestone_id;
                }
                $task->save();

                if($request->boq=='1'){
                    $costitemheading = $request->costitemheading;
                    if ($costitemheading == 'other') {
                        $costitem = new CostItems();
                        $costitem->cost_item_name = $request->heading;
                        $costitem->save();
                        $costitemheading = $costitem->id;
                    }
                    if(!empty($segmentid)){
                        if(!empty($boqid)){
                            $productcostitem = ProjectSegmentsProduct::find($boqid);
                        }else{
                            $productcostitem = new ProjectSegmentsProduct();
                        }
                        $maxcostitemid = ProjectSegmentsProduct::where('title',$subprojectid)->where('project_id',$projectid)->where('segment',$segmentid)->max('inc');
                        $newid = $maxcostitemid+1;
                        $productcostitem->inc = $newid;
                        $productcostitem->segment = $segmentid;
                    }else{
                        if(!empty($boqid)){
                            $productcostitem = ProjectCostItemsProduct::find($boqid);
                        }else{
                            $productcostitem = new ProjectCostItemsProduct();
                            $maxcostitemid = ProjectCostItemsProduct::where('title',$subprojectid)->where('project_id',$projectid)->max('inc');
                            $newid = $maxcostitemid+1;
                            $productcostitem->inc = $newid;
                        }
                    }

                    $productcostitem->cost_items_id = $costitemheading;
                    $productcostitem->category = $request->catlevel;
                    $productcostitem->project_id = $projectid;
                    $productcostitem->assign_to = $request->user_id;
                    $productcostitem->contractor = $request->contractor;
                    $productcostitem->title = $subprojectid;
                    $productcostitem->position_id = $request->position_id ?: 0;
                    $productcostitem->description = strip_tags($request->description);
                    $startDate = Carbon::createFromFormat($this->global->date_format, $request->start_date, $this->global->timezone)->format('Y-m-d');
                    $dueDate = Carbon::createFromFormat($this->global->date_format, $request->due_date, $this->global->timezone)->format('Y-m-d');
                    $productcostitem->start_date = $startDate;
                    $productcostitem->deadline = $dueDate;
                    $productcostitem->save();


                    $task->cost_item_id = $productcostitem->id ?: 0;
                    $task->save();

                }

                $medium = $request->medium ?: 'android';
                $createlog = new ProjectsLogs();
                $createlog->company_id = $user->company_id;
                $createlog->added_id = $user->id;
                $createlog->module_id = $task->id;
                $createlog->module = 'tasks';
                $createlog->project_id = $projectid;
                $createlog->subproject_id = $subprojectid ?: 0;
                $createlog->segment_id = $segmentid ?: 0;
                $createlog->heading = $task->heading;
                if (!empty($taskid)) {
                    $createlog->modulename = 'task_updated';
                }else{
                    $createlog->modulename = 'task_created';
                }
                $createlog->description = $task->heading.' task created by '.$user->name.' for the project '.get_project_name($productcostitem->project_id);
                $createlog->medium = $medium;
                $createlog->save();
                //calculate project progress if enabled
                $this->calculateProjectProgress($request->project_id);
                $response['status'] = 200;
                if($new){
                    $response['message'] = 'Task Added Successfully';
                }else{
                    $response['message'] = 'Task Updated Successfully';
                }
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'store-indents-image',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteTask(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $id = $request->taskid;
                $manpower = ManpowerLog::where('task_id',$id)->first();
                if(!empty($manpower->id)){
                    $response['message'] = 'Please delete Manpower logs';
                    $response['status'] = 300;
                    return $response;
                }
                $manpower = PunchItem::where('task_id',$id)->first();
                if(!empty($manpower->id)){
                    $response['message'] = 'Please delete issues';
                    $response['status'] = 300;
                    return $response;
                }
                $task = Task::findOrFail($id);

                if ($request->has('recurring') && $request->recurring == 'yes') {
                    Task::where('recurring_task_id', $id)->delete();
                }

                $taskFiles = TaskFile::where('task_id', $id)->get();

                foreach ($taskFiles as $file) {
                    Files::deleteFile($file->hashname, 'task-files/' . $file->task_id);
                    $file->delete();
                }

                Task::destroy($id);

                ProjectsLogs::where('module_id',$task->id)->where('module','tasks')->delete();
                TaskFile::where('task_id',$task->id)->delete();
                $manpowerreply = TaskPercentage::where('task_id',$task->id)->get();
                foreach ($manpowerreply as $manpowerre){
                    ProjectsLogs::where('module_id',$manpowerre->id)->where('module','task_percentage')->delete();
                    $manpowerre->delete();
                }

                $response['status'] = 200;
                $response['message'] = 'Task deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-tasks',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function boqCategories(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projects = BoqCategory::where('company_id', $user->company_id)->get()->toArray();
                $response['status'] = 200;
                $response['message'] = 'Category List Fetched';
                $response['response'] = $projects;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'store-indents-image',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function updatePercentage(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $id = $request->taskid;
                $mentionusers = !empty($request->mentionusers) ? $request->mentionusers : '';
                $tasks = Task::find($id);
                if ($tasks) {
                    $percentage = 0;
                    if (!empty($request->percentage) && $request->percentage > 0) {
                        $percentage = $request->percentage;
                        $tasks->percentage = $percentage;
                    }
                    if (!empty($request->status)) {
                        $tasks->status = $request->status;
                    }
                    if (!empty($request->taskqty)) {
                        $tasks->taskqty = $request->taskqty;
                    }
                    $tasks->save();
                    $taskspercent = new TaskPercentage();
                    $taskspercent->company_id = $user->company_id;
                    $taskspercent->task_id = $tasks->id;
                    $taskspercent->added_by = $user->id;
                    $taskspercent->percentage = $percentage;
                    $taskspercent->comment = $request->comment ?: '';
                    $taskspercent->status = $request->status ?: '';
                    $taskspercent->mentionusers = $mentionusers;
                    $taskspercent->save();
                    $msgtext = 'Task status updated by ' . $user->name;
                    $headingtext = '';
                    if ($percentage > 0) {
                        $msgtext .= ' with ' . $percentage;
                        $headingtext .= 'Task Updated to '.$percentage.'%';
                    }
                    if (!empty($request->status)) {
                        if($headingtext){
                            $headingtext .= ' with status '.$request->status;
                        }else{
                            $headingtext .= $request->status;
                        }
                    }
                    $medium = !empty($request->medium) ? $request->medium : 'android';
                    $createlog = new ProjectsLogs();
                    $createlog->company_id = $user->company_id;
                    $createlog->added_id = $user->id;
                    $createlog->module_id = $taskspercent->id;
                    $createlog->module = 'task_percentage';
                    $createlog->project_id = $tasks->project_id;
                    $createlog->subproject_id = $tasks->title ?: 0;
                    $createlog->segment_id = $tasks->segment ?: 0;
                    if(!empty($percentage)){
                        $createlog->heading = $headingtext;
                        $createlog->modulename = 'task_percentage_updated';
                    }
                    if (!empty($request->comment)) {
                        $createlog->heading = $headingtext;
                        $createlog->modulename = 'task_comment';
                    }
                    if (!empty($request->status)) {
                        $createlog->heading = $headingtext;
                        $createlog->modulename = 'task_status';
                    }
                    $createlog->description = $msgtext;
                    $createlog->medium = $medium;
                    $createlog->mentionusers = $mentionusers;
                    $createlog->save();
                    $response['status'] = 200;
                    $response['percentageid'] = $taskspercent->id;
                    $response['message'] = 'Task updated';
                    return $response;
                }
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'store-indents-image',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function taskTimelines(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try {
                $response = array();
                $alltimeline = collect();
                $id = $request->taskid;
                $task = Task::find($id);
                if (!empty($task->id)) {
                    $taskpercentagearray = TaskPercentage::select('*', 'id as percentid')->where('task_id', $task->id)->orderBy('created_at', 'desc')->get();
                    foreach ($taskpercentagearray as $taskpercentage) {
                        $alltimeline = $alltimeline->push($taskpercentage);
                    }
                    $manpowerlogsarray = ManpowerLog::select('*', 'id as manpowerid')->where('task_id', $task->id)->orderBy('created_at', 'desc')->get();
                    foreach ($manpowerlogsarray as $manpowerlogs) {
                        $alltimeline = $alltimeline->push($manpowerlogs);
                    }
                    $punchitemlogsarray = PunchItem::select('*', 'id as punchitemid')->where('task_id', $task->id)->orderBy('created_at', 'desc')->get();
                    foreach ($punchitemlogsarray as $punchitemlogs) {
                        $alltimeline = $alltimeline->push($punchitemlogs);
                    }
                    $page = $request->page;
                    $response = array();
                    if($page=='all'){
                        $alltimelinearray = $alltimeline->sortByDesc('created_at');
                    }else{
                        $count = pagecount();
                        $skip = 0;
                        if($page){
                            $skip = $page*$count;
                        }
                        $alltimelinearray = $alltimeline->sortByDesc('created_at')->forPage($page,$count);
                    }
                    $awsurl = awsurl();
                    $url = uploads_url();
                    $companyid = $user->company_id;
                    $storage = storage();
                    $timelinearray = array();
                    if ($alltimelinearray) {
                        foreach ($alltimelinearray as $timeline) {
                            $tablename = $timeline->getTable();
                            $tablearray = array();
                            $tablearray['id'] = $timeline->id;
                            $tablearray['username'] = get_user_name($timeline->added_by);
                            $tablearray['userimage'] = get_users_image_link($timeline->added_by);
                            switch ($tablename) {
                                case 'task_percentage':
                                    $tablearray['module'] = 'taskupdate';
                                    $tablearray['status'] = $timeline->status;
                                    if ($timeline->comment) {
                                        $tablearray['comment'] = $timeline->comment;
                                    }
                                    if ($timeline->percentage) {
                                        $tablearray['percentage'] = $timeline->percentage;
                                    }
                                    $files = \App\TaskFile::where('task_id', $timeline->task_id)->where('task_percentage_id', $timeline->percentid)->get();
                                    foreach ($files as $file) {
                                        $fx = explode('.', $file->hashname);
                                        $ext = $fx[(count($fx) - 1)];
                                        $html5class = '';
                                        if ($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg') {
                                            $html5class = 'html5lightbox';
                                        }
                                        $filename['name'] = $file->filename;
                                        switch ($storage) {
                                            case 'local':
                                                $filename['image'] = $url.'/task-files/' . $timeline->task_id . '/' . $file->hashname;
                                                break;
                                            case 's3':
                                                $filename['image'] = $awsurl . '/task-files/' . $timeline->task_id . '/' . $file->hashname;
                                                break;
                                            case 'google':
                                                $filename['image'] = $file->google_url;
                                                break;
                                            case 'dropbox':
                                                $filename['image'] = $file->dropbox_link;
                                                break;
                                        }
                                        $filename['created_at'] = Carbon::parse($file->created_at)->format('d-M-Y h:i A');
                                         $tablearray['images'][] = $filename;
                                    }
                                    $tablearray['created_at'] = Carbon::parse($timeline->created_at)->format('d-M-Y h:i A');
                                      break;
                                case 'manpower_logs':
                                    $tablearray['module'] = 'manpower';
                                    $tablearray['manpower'] = $timeline->manpower;
                                    $tablearray['workinghours'] = $timeline->workinghours;

                                    $files = \App\ManpowerLogFiles::where('manpower_id', $timeline->manpowerid)->where('reply_id','0')->get();
                                    foreach ($files as $file) {
                                        $fx = explode('.', $file->hashname);
                                        $ext = $fx[(count($fx) - 1)];
                                        $html5class = '';
                                        if ($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg') {
                                            $html5class = 'html5lightbox';
                                        }
                                        $filename['name'] = $file->filename;
                                        switch ($storage) {
                                            case 'local':
                                                $filename['image'] = $url.'manpower-log-files/' . $timeline->manpowerid . '/' . $file->hashname;
                                                break;
                                            case 's3':
                                                $filename['image'] = $awsurl . '/manpower-log-files/' . $timeline->manpowerid . '/' . $file->hashname;
                                                break;
                                            case 'google':
                                                $filename['image'] = $file->google_url;
                                                break;
                                            case 'dropbox':
                                                $filename['image'] = $file->dropbox_link;
                                                break;
                                        }
                                        $filename['created_at'] = Carbon::parse($file->created_at)->format('d-M-Y h:i A');
                                        $tablearray['images'][] = $filename;
                                    }
                                    $tablearray['created_at'] = Carbon::parse($timeline->created_at)->format('d-M-Y h:i A');
                                    break;
                                case 'punch_item':
                                    $tablearray['module'] = 'issue';
                                    $tablearray['title'] = $timeline->title;
                                    $tablearray['status'] = $timeline->status;
                                    $files = \App\PunchItemFiles::where('task_id', $timeline->punchitemid)->where('reply_id','0')->get();
                                    foreach ($files as $file) {
                                        $fx = explode('.', $file->hashname);
                                        $ext = $fx[(count($fx) - 1)];
                                        $html5class = '';
                                        if ($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg') {
                                            $html5class = 'html5lightbox';
                                        }
                                        $filename['name'] = $file->filename;
                                        switch ($storage) {
                                            case 'local':
                                                $filename['image'] = $url.'punch-files/' . $timeline->punchitemid . '/' . $file->hashname;
                                                break;
                                            case 's3':
                                                $filename['image'] = $awsurl . '/punch-files/' . $timeline->punchitemid . '/' . $file->hashname;
                                                break;
                                            case 'google':
                                                $filename['image'] = $file->google_url;
                                                break;
                                            case 'dropbox':
                                                $filename['image'] = $file->dropbox_link;
                                                break;
                                        }
                                        $filename['created_at'] = Carbon::parse($file->created_at)->format('d-M-Y h:i A');
                                        $tablearray['images'][] = $filename;
                                    }
                                    $tablearray['created_at'] = Carbon::parse($timeline->created_at)->format('d-M-Y h:i A');

                                    break;
                            }
                            $timelinearray[] = $tablearray;
                        }
                    }
                    $response['status'] = 200;
                    $response['message'] = 'Task Percentage';
                    $response['responselist'] = $timelinearray;
                    return $response;
                }else{
                    $response['status'] = 300;
                    $response['message'] = 'Task Not found';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'tasks-timelines',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function storeImage(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $taskid = $request->taskid;
                $percentageid = $request->percentageid;
                if ($request->hasFile('file')) {
                    $storage = storage();
                    $companyid = $user->company_id;
                    $fileData =  $request->file('file');
                        $file = new TaskFile();
                        $file->user_id = $user->id;
                        $file->company_id = $user->company_id;
                        $file->task_id = $taskid ?: 0;
                        $file->task_percentage_id = $percentageid ?: 0;
                        switch($storage) {
                            case 'local':
                                $destinationPath = 'uploads/task-files/'.$taskid;
                                if (!file_exists('public/'.$destinationPath)) {
                                    mkdir('public/'.$destinationPath, 0777, true);
                                }
                                $fileData->storeAs($destinationPath, $fileData->hashName());
                                break;
                            case 's3':
                                Storage::disk('s3')->putFileAs('/task-files/'.$taskid, $fileData, $fileData->hashName(), 'public');
                                break;
                            case 'google':
                                $dir = '/';
                                $recursive = false;
                                $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                                $dir = $contents->where('type', '=', 'dir')
                                    ->where('filename', '=', 'task-files')
                                    ->first();

                            if (!$dir) {
                                Storage::cloud()->makeDirectory('task-files');
                            }

                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $taskid)
                                ->first();

                            if (!$directory) {
                                Storage::cloud()->makeDirectory($dir['path'] . '/' . $taskid);
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('filename', '=', $taskid)
                                    ->first();
                            }

                            Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());
                            $file->google_url = Storage::cloud()->url($directory['path'] . '/' . $fileData->hashName());
                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('task-files/' . $taskid . '/', $fileData, $fileData->hashName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer " . config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/task-files/' . $taskid . '/' . $fileData->hashName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $file->dropbox_link = $dropboxResult['url'];
                            break;
                    }
                    $file->filename = $fileData->getClientOriginalName();
                    $file->hashname = $fileData->hashName();
                    $file->size = $fileData->getSize();
                    $file->save();
                    $response['status'] = 200;
                    $response['imageid'] = $file->id;
                    $response['message'] = 'Task Image Uploaded';
                    return $response;

                }
                $response['status'] = 300;
                $response['message'] = 'Uploading failed. Please try again';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'store-indents-image',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function activityList(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
            $response = array();
            $projectid = $request->project_id;
            $subprojectid = $request->subproject_id ?: 0;
            $segmentid = $request->segment_id ?: 0;
            $activityarray = array();
            $activityarray['projectid'] = $projectid;
            $activityarray['subprojectid'] = $subprojectid;
            $activityarray['segmentid'] = $segmentid;
            $activityarray['level'] = 0;
            $activityarray['parent'] = 0;
            $activitlist = $this->activityloop($activityarray);
            $response['status'] = 200;
            $response['message'] = 'Category List Fetched';
            $response['response'] = $activitlist;
            return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'activitylist',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    protected function activityloop($actarray){
        $projectid = $actarray['projectid'];
        $subprojectid = $actarray['subprojectid'];
        $segmentid = $actarray['segmentid'];
        $level = $actarray['level'];
        $parent = $actarray['parent'];
        if(!empty($segmentid)) {
            $projectcostitem = ProjectSegmentsPosition::where('project_id',$projectid)->where('segment',$segmentid);
        }else{
            $projectcostitem = ProjectCostItemsPosition::where('project_id',$projectid);
        }
        if(!empty($subprojectid)){
            $projectcostitem = $projectcostitem->where('title',$subprojectid);
        }
        $projectcostitemarray = $projectcostitem->where('position','row')->where('parent',$parent)->where('level',$level)->get();
        $categorynames = $catraray = array();
        foreach ($projectcostitemarray as $projectactivty){
            $explodeact =  array_filter(explode(',',$projectactivty->catlevel));
            $explodeact =  count($explodeact)+1;
            $catlevel = $projectactivty->itemid;
            if(!empty($projectactivty->catlevel)){
                $catlevel = $projectactivty->catlevel.','.$projectactivty->itemid;
            }
            $categorynames['id'] = $projectactivty->id;
            $categorynames['level'] = $projectactivty->level;
            $categorynames['dotcount'] = $explodeact;
            $categorynames['itemid'] = $projectactivty->itemid;
            $categorynames['name'] = $projectactivty->itemname;
            $categorynames['newlevel'] = (int)$projectactivty->level+1;
            $categorynames['catlevel'] = $catlevel;
            $catraray[] = $categorynames;

            $newlevel = (int)$projectactivty->level+1;
            $activityarray = array();
            $activityarray['projectid'] = $projectid;
            $activityarray['subprojectid'] = $subprojectid;
            $activityarray['segmentid'] = $segmentid;
            $activityarray['level'] = $newlevel;
            $activityarray['parent'] = $projectactivty->itemid;
            $boqcategory = $this->activityloop($activityarray);
            $catraray = array_merge($catraray, $boqcategory);
        }
        return $catraray;
    }
    public function boqList(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] == 401) {
                return response()->json(['error' => $validate['message']], 401);
            }
            $user = $validate['user'];
            try{
                    $response = array();
                    $projectid = explode(',',$request->project_id);
                    if (!empty($projectid)) {
                        $subprojectid = $request->subproject_id ?: 0;
                        $segmentid = $request->segment_id ?: 0;
                        $boqarray = array();
                        $boqlist['projectid'] = $projectid;
                        $boqlist['subprojectid'] = $subprojectid;
                        $boqlist['segmentid'] = $segmentid;
                        $boqlist['level'] = 0;
                        $boqlist['parent'] = 0;
                        $boqarray = $this->boqdata($boqlist);
                    }
                } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'boqlist',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
              }
            $response['status'] = 200;
            $response['message'] = 'BOQ List Fetched';
            $response['response'] = $boqarray;
            return $response;
        }
        return response()->json(['error' => 'Unauthorised'], 401);
    }
    protected function boqdata($boqarray)
    {
        $projectid = $boqarray['projectid'];
        $subprojectid = $boqarray['subprojectid'];
        $segmentid = $boqarray['segmentid'];
        $level = $boqarray['level'];
        $parent = $boqarray['parent'];
        if (!empty($segmentid)) {
            $subcategorycostitemarray = \App\ProjectSegmentsPosition::whereIn('project_id', $projectid)->where('title', $subprojectid)->where('segment', $segmentid)->where('position', 'row')->where('parent', $parent)->where('level', $level)->orderBy('inc', 'asc')->get();
        } else {
            $subcategorycostitemarray = \App\ProjectCostItemsPosition::whereIn('project_id', $projectid)->where('title', $subprojectid)->where('position', 'row')->where('parent', $parent)->where('level', $level)->orderBy('inc', 'asc')->get();
        }
        if (!empty($subcategorycostitemarray)) {
            $boqcategoryarary = array();
            foreach ($subcategorycostitemarray as $subcategorycostitem) {
                $boqsubcategoryarary = array();
                $boqsubcategoryarary['module'] = 'activity';
                $boqsubcategoryarary['id'] = $subcategorycostitem->id;
                $boqsubcategoryarary['itemid'] = $subcategorycostitem->itemid;
                $boqsubcategoryarary['itemname'] = $subcategorycostitem->itemname;
                $boqsubcategoryarary['level'] = $subcategorycostitem->level;
                $boqsubcategoryarary['catlevel'] = $subcategorycostitem->catlevel;
                $boqsubcategoryarary['count'] = !empty($subcategorycostitem->catlevel) ? count(array_filter(explode(',', $subcategorycostitem->catlevel))) : 0;
                $boqcategoryarary[] = $boqsubcategoryarary;
                $catvalue = $subcategorycostitem->itemid;
                if (!empty($subcategorycostitem->catlevel)) {
                    $catvalue = $subcategorycostitem->catlevel . ',' . $subcategorycostitem->itemid;
                }
                if (!empty($segmentid)) {
                    $costitemarray = \App\ProjectSegmentsProduct::whereIn('project_id', $projectid)->where('title', $subprojectid)->where('segment', $segmentid)->where('category', $catvalue)->where('position_id', $subcategorycostitem->id)->get();
                } else {
                    $costitemarray = \App\ProjectCostItemsProduct::whereIn('project_id', $projectid)->where('title', $subprojectid)->where('category', $catvalue)->where('position_id', $subcategorycostitem->id)->get();
                }
                if (!empty($costitemarray)) {
                    foreach ($costitemarray as $costitem) {
                        $task = Task::whereIn('project_id', $projectid)->where('title', $subprojectid)->where('cost_item_id', $costitem->id)->first();
                        if ($task) {
                            $boqsubcategoryarary = array();
                            $boqsubcategoryarary['module'] = 'task';
                            $boqsubcategoryarary['id'] = $task->id;
                            $boqsubcategoryarary['boqid'] = $costitem->id;
                            $boqsubcategoryarary['category'] = $costitem->category;
                            $boqsubcategoryarary['position_id'] = (int)$costitem->position_id;
                            $boqsubcategoryarary['heading'] = $task->heading;
                            $boqsubcategoryarary['start_date'] = !empty($costitem->start_date) ? date('d/m/Y', strtotime($costitem->start_date)) : '';
                            $boqsubcategoryarary['due_date'] = !empty($costitem->deadline) ? date('d/m/Y', strtotime($costitem->deadline)) : '';
                            $boqsubcategoryarary['percentage'] = !empty($task) ? $task->percentage : 0;
                            $boqsubcategoryarary['quantity'] = !empty($costitem) ? $costitem->qty : 0;
                            $boqsubcategoryarary['rate'] = !empty($costitem) ? $costitem->rate : 0;
                            $boqsubcategoryarary['description'] = !empty($costitem) ? $costitem->description : '';
                            $boqsubcategoryarary['contractor_name'] = !empty($costitem->contractor) ? get_user_name($costitem->contractor) : '';
                            $boqsubcategoryarary['project_name'] = !empty($costitem->project_id) ? get_project_name($costitem->project_id) : '';
                            $boqsubcategoryarary['subproject_name'] = !empty($costitem->title) ? get_title($costitem->title) : '';
                            $boqsubcategoryarary['segment_name'] = !empty($costitem->segment) ? get_segment($costitem->segment) : '';
                            $boqsubcategoryarary['assign_to'] = !empty($costitem->assign_to) ? $costitem->assign_to : '';
                            $boqsubcategoryarary['assign_to_name'] = !empty($costitem->assign_to) ? get_user_name($costitem->assign_to) : '';
                            $boqsubcategoryarary['status'] = !empty($task->status) ? $task->status : '';
                            $boqcategoryarary[] = $boqsubcategoryarary;
                        }
                    }
                }
                $newlevel = (int)$subcategorycostitem->level + 1;
                $boqlist['projectid'] = $projectid;
                $boqlist['subprojectid'] = $subprojectid;
                $boqlist['segmentid'] = $segmentid;
                $boqlist['level'] = $newlevel;
                $boqlist['parent'] = $subcategorycostitem->itemid;
                $boqcategory = $this->boqdata($boqlist);
                $boqcategoryarary = array_merge($boqcategoryarary, $boqcategory);
            }
            return $boqcategoryarary;
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function boq(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status']==401) {
                return response()->json(['error' => $validate['message']], 401);
            }
            $user = $validate['user'];
            try{
                $response = array();
                $boqsubcategoryarary = array();
                $taskid = $request->taskid;
                $costitemid = $request->boqid;
                    if(!empty($taskid)&&!empty($costitemid)){
                    $costitem = \App\ProjectCostItemsProduct::where('id',$costitemid)->first();
                        if(!empty($costitem->id)){
                        $task = Task::where('cost_item_id',$costitem->id)->first();
                            if(!empty($task->id)){
                                $boqsubcategoryarary['id'] = $task->id;
                                $boqsubcategoryarary['boqid'] = $costitem->id;
                                $boqsubcategoryarary['heading'] = $task->heading;
                                $boqsubcategoryarary['start_date'] = !empty($costitem->start_date) ? date('d/m/Y', strtotime($costitem->start_date)) : '';
                                $boqsubcategoryarary['due_date'] = !empty($costitem->deadline) ? date('d/m/Y', strtotime($costitem->deadline)) : '';
                                $boqsubcategoryarary['percentage'] = !empty($task) ? $task->percentage : 0;
                                $boqsubcategoryarary['quantity'] = !empty($costitem) ? $costitem->qty : 0;
                                $boqsubcategoryarary['rate'] = !empty($costitem) ? $costitem->rate : 0;
                                $boqsubcategoryarary['description'] = !empty($costitem) ? $costitem->description : '';
                                $boqsubcategoryarary['contractor_name'] = !empty($costitem->contractor) ? get_user_name($costitem->contractor) : '';
                                $boqsubcategoryarary['contractor_id'] = $costitem->contractor;
                                $boqsubcategoryarary['project_id'] = !empty($costitem->project_id) ? (int)$costitem->project_id : '';
                                $boqsubcategoryarary['subproject_id'] = !empty($costitem->title) ? (int)$costitem->title : '';
                                $boqsubcategoryarary['segment_id'] = !empty($costitem->segment_id) ? (int)$costitem->segment_id : '';
                                $boqsubcategoryarary['project_name'] = !empty($costitem->project_id) ? get_project_name($costitem->project_id) : '';
                                $boqsubcategoryarary['subproject_name'] = !empty($costitem->title) ? get_title($costitem->title) : '';
                                $boqsubcategoryarary['segment_name'] = !empty($costitem->segment) ? get_segment($costitem->segment) : '';
                                $boqsubcategoryarary['assign_to'] = !empty($costitem->assign_to) ?  $costitem->assign_to : '';
                                $boqsubcategoryarary['assign_to_name'] = !empty($costitem->assign_to) ? get_user_name($costitem->assign_to) : '';
                                $boqsubcategoryarary['status'] = !empty($task->status) ? $task->status : '';
                                $boqsubcategoryarary['totalissues'] = !empty($task->id) ? PunchItem::where('task_id', $task->id)->count() : '';
                                $boqsubcategoryarary['totallabourlog'] = !empty($task->id) ? ManpowerLog::where('task_id', $task->id)->count() : '';

                                $response['status'] = 200;
                                $response['message'] = 'Category List Fetched';
                                $response['response'] = $boqsubcategoryarary;
                            }else{
                                $response['status'] = 300;
                                $response['message'] = 'Task not found';
                            }
                        }else{
                            $response['status'] = 300;
                            $response['message'] = 'Task not found';
                        }
                    }else{
                        $response['status'] = 300;
                        $response['message'] = 'Task not found';
                    }
                     return $response;

                    } catch (\Exception $e) {
                        $userid = !empty($user->id) ? $user->id : 0;
                        app_log($e,'boq',$userid);
                        $response['status'] = 500;
                        $response['message'] = $e->getMessage();
                        $response['line'] = $e->getLine();
                        $response['file'] = $e->getFile();
                        return $response;
                    }
            $response['status'] = 300;
            $response['message'] = 'Task not found';
            return $response;
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function boqUpdate(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                    $response = array();
                    if ($request->taskid && $request->boqid) {
                        $taskid = $request->taskid;
                        $costitemid = $request->boqid;
                        $parameter = $request->parameter;
                        $value = $request->value;
                        $taskdetails = Task::find($taskid);
                        $taskdetails->$parameter = $value;
                        $taskdetails->save();
                        $costdetails = ProjectCostItemsProduct::find($costitemid);
                        if ($parameter == 'due_date') {
                            $costdetails->deadline = $value;
                        } elseif ($parameter == 'user_id') {
                            $costdetails->assign_to = $value;
                        } else {
                            $costdetails->$parameter = $value;
                        }
                        $costdetails->save();
                        $response['status'] = 200;
                        $response['message'] = 'Task Updated Successfully';
                        return $response;
                    }
                } catch (\Exception $e) {
                    $userid = !empty($user->id) ? $user->id : 0;
                    app_log($e,'boqupdate',$userid);
                    $response['status'] = 500;
                    $response['message'] = $e->getMessage();
                    $response['line'] = $e->getLine();
                    $response['file'] = $e->getFile();
                    return $response;
               }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function costItemList(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
                 try{
                    $user = $validate['user'];
                    $pi = CostItems::where('company_id', $user->company_id)->get();
                    $response = array();
                    $response['status'] = 200;
                    $response['responselist'] = $pi;
                    $response['message'] = 'Cositem list Successfully';
                    return $response;
                } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'costitemlist',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
                 }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function taskStatusList(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                    $statusarray = array();
                    $statusarray[] = array('id'=>1,'slug'=>'notstarted','name'=>'Not Started');
                    $statusarray[] = array('id'=>2,'slug'=>'inprogress','name'=>'In Progress');
                    $statusarray[] = array('id'=>3,'slug'=>'inproblem','name'=>'In Problem');
                    $statusarray[] = array('id'=>4,'slug'=>'delayed','name'=>'Delayed');
                    $statusarray[] = array('id'=>5,'slug'=>'completed','name'=>'Completed');
                    $response = array();
                    $response['status'] = 200;
                    $response['responselist'] = $statusarray;
                    $response['message'] = 'Status list Successfully';
                    return $response;
                } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'task-status-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function progressReport(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $statusarray = array();
                $projectid = $request->project_id;
                $subprojectid = $request->subproject_id;
                $startdate = $request->start_date;
                $enddate = $request->end_date;
                $segmentid = $request->segment_id;
                if($projectid&&$startdate&&$enddate){
                    $contractorarray = \App\Contractors::where('company_id',$user->company_id)->pluck('name','user_id')->toArray();
                    $contr = array('0'=>'Company');
                    $contractorarray = $contractorarray+$contr;
                    $reportarray = array();
                    foreach ($contractorarray as $contracid => $contraname){
                         $reportarray[]  = array('module'=>'contractor','name'=>$contraname);
                         $progreearray = array();
                         $progreearray['projectid'] = $projectid;
                         $progreearray['subprojectid'] = $subprojectid;
                         $progreearray['segmentid'] = $segmentid;
                         $progreearray['level'] = '0';
                         $progreearray['parent'] = '0';
                         $progreearray['contractor'] = $contracid;
                         $progreearray['startdate'] = $startdate;
                         $progreearray['enddate'] = $enddate;
                         $progreearray['exportype'] = 'mobile';
                         $report = $this->progreesreportdata($progreearray);
                        $reportarray  = array_merge($reportarray,$report);
                    }
                    $response = array();
                    $response['status'] = 200;
                    $response['responselist'] = $reportarray;
                    $response['message'] = 'Report list fetched Successfully';
                    return $response;

                }
                $response['status'] = 301;
                $response['message'] = 'Project Or date range not found';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'progress-report',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        return response()->json(['error' => 'Unauthorised'], 401);
    }
    protected function progreesreportdata($reportarray){

        $projectid = $reportarray['projectid'];
        $subprojectid = $reportarray['subprojectid'];
        $segmentid = $reportarray['segmentid'];
        $level = $reportarray['level'];
        $parent = $reportarray['parent'];
        $contractor = $reportarray['contractor'];
        $startdate = $reportarray['startdate'];
        $enddate = $reportarray['enddate'];
        if(!empty($segmentid)) {
            $proproget = \App\ProjectSegmentsPosition::where('project_id',$projectid)->where('segment',$segmentid);
        }else{
            $proproget = \App\ProjectCostItemsPosition::where('project_id',$projectid);
        }
        if($subprojectid){
            $proproget = $proproget->where('title',$subprojectid);
        }
        $proproget = $proproget->where('position','row')->where('level',$level)->where('parent',$parent)->orderBy('inc','asc')->get();
        $progressreport = array();
        foreach ($proproget as $propro) {
            $catvalue = $propro->itemid;
            if(!empty($propro->catlevel)){
                $catvalue = $propro->catlevel.','.$propro->itemid;
            }
            $catarary = array();
            $explodeact =  explode(',',$catvalue);
            $catarary['module'] = 'activity';
            $catarary['name'] = ucwords($propro->itemname);
            $catarary['level'] = $propro->level;
            $catarary['dotcount'] =  count($explodeact);
            if(!empty($segmentid)) {
                $projectcostitemsarray = \App\ProjectSegmentsProduct::where('project_id',$projectid)->where('segment',$segmentid);
            }else{
                $projectcostitemsarray = \App\ProjectCostItemsProduct::where('project_id',$projectid);
            }
            if(!empty($subprojectid)){
                $projectcostitemsarray = $projectcostitemsarray->where('title',$subprojectid);
            }
            if(!empty($contractor)){
                $projectcostitemsarray = $projectcostitemsarray->where('contractor',$contractor);
            }else{
                $projectcostitemsarray = $projectcostitemsarray->whereNull('contractor');
            }
            $projectcostitemsarray = $projectcostitemsarray->where('category',$catvalue)->where('position_id',$propro->id)->get();
            $tasklist = array();
            foreach ($projectcostitemsarray as $projectcostitems){
                if($projectcostitems->id){
                    $tasks = \App\Task::where('cost_item_id',$projectcostitems->id)->first();
                    $totalquantity = $projectcostitems->qty;
                    $percentage = $cumiliquantity = $cumiliquantitytill = $achivied=  $balance=  0;
                    if(!empty($tasks->id)){
                        $comarchive = 0;
                        $percentage =  $tasks->percentage;
                        $start_date = $startdate.' 00:00:00';
                        $end_date = $enddate.' 23:59:59';
                        $prevarchived = \App\TaskPercentage::where('task_id',$tasks->id)->where('created_at','<',$start_date)->orderBy('id','desc')->max('percentage');
                        $prevarchived = !empty($prevarchived) ? (int) $prevarchived : 0;
                        $perachivied = \App\TaskPercentage::where('task_id',$tasks->id)->where('created_at','<=',$end_date)->orderBy('id','desc')->max('percentage');
                        $perachivied = !empty($perachivied) ? (int) $perachivied : 0;
                        $comarchive = $perachivied-$prevarchived;
                        $costarray = array();
                        $costarray['module'] = 'task';
                        $costarray['name'] = ucwords($tasks->heading);
                        $costarray['percentageprogress'] = !empty($comarchive) ? (int)$comarchive : 0;
                        $costarray['percentagecomplete'] = $percentage;
                        $tasklist[] = $costarray;
                    }
                }
            }
            $catarary['tasklist'] =  $tasklist;
            $progressreport[] =  $catarary;
            $progreearray = array();
            $newlevel = (int)$propro->level+1;
            $progreearray['projectid'] = $projectid;
            $progreearray['subprojectid'] = $subprojectid;
            $progreearray['segmentid'] = $segmentid;
            $progreearray['level'] = $newlevel;
            $progreearray['parent'] = $propro->itemid;
            $progreearray['contractor'] = $contractor;
            $progreearray['startdate'] = $startdate;
            $progreearray['enddate'] = $enddate;
            $progreearray['exportype'] = 'mobile';
            $report = $this->progreesreportdata($progreearray);
            $progressreport  = array_merge($progressreport,$report);
        }
        return $progressreport;
    }

    public function progressReportExport(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
            $dataarray = array();
            $dataarray['projectarray'] = Project::all();
            $dataarray['contractorsarray'] = User::getAllContractors($user);
            $exporttype = $request->exporttype;
            $dataarray['projectid'] = $request->project_id;
            $dataarray['subprojectid'] = $request->subproject_id;
            $dataarray['segmentid'] = $request->segment_id;
            $dataarray['startdate'] = $request->start_date;
            $dataarray['enddate'] = $request->end_date;
            $dataarray['dateformat'] = $this->global->date_format;
            $dataarray['exportype'] = 'mobile';
            $dataarray['user'] = $user;
            $filename = '';
            if ($exporttype == 'pdf') {
                $pdf = PDF::loadView('admin.projects.reports.progress-reporthtml', $dataarray)->setPaper('a4', 'landscape');
                $filename = uniqid();
                $filename .= '.pdf';
                if (!file_exists('public/pdflogfiles/')) {
                    mkdir('public/pdflogfiles/', 0755);
                }
                $pdf->save('public/pdflogfiles/'.$filename);
            }
            if ($exporttype == 'excell') {
                $filename = uniqid();
                $report = View::make('admin.projects.reports.progress-reportexcell', $dataarray)->render();
                $exportArray = json_decode($report);
                Excel::create($filename, function ($excel) use ($user, $exportArray) {
                    $excel->setTitle('Progress report');
                    $excel->setCreator('Worksuite')->setCompany($user->company_id);
                    $excel->setDescription('Progress report');
                    $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                        $sheet->fromArray($exportArray, null, 'A1', false, false);
                        $sheet->row(1, function ($row) {
                            $row->setFont(array(
                                'bold' => true
                            ));
                        });
                    });
                })->store('xlsx', 'public/pdflogfiles/');
                $filename .= '.xlsx';
            }

            $response = array();
            $response['status'] = 200;
            $response['responselist'] = url('public/pdflogfiles/' . $filename);
            $response['message'] = 'Status list Successfully';
            return $response;

                    } catch (\Exception $e) {
                    $userid = !empty($user->id) ? $user->id : 0;
                    app_log($e,'progress-report-export',$userid);
                    $response['status'] = 500;
                    $response['message'] = $e->getMessage();
                    $response['line'] = $e->getLine();
                    $response['file'] = $e->getFile();
                    return $response;
                }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
}