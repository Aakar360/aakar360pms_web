<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignWorkRules extends Model
{
    protected $fillable = ['user_id','workrules_id','effective_date'];
}
