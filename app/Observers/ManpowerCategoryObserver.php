<?php

namespace App\Observers;

use App\ManpowerCategory; 

class ManpowerCategoryObserver
{

    public function saving(ManpowerCategory $log)
    {
        // Cannot put in creating, because saving is fired before creating. And we need company id for check bellow
        if (company()) {
            $log->company_id = company()->id;
        }
    }

}
