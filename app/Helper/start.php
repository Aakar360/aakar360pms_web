<?php

use App\Company;
use App\Helper\Reply;
use App\InputFields;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

if (!function_exists('superAdmin')) {
    function superAdmin()
    {
        return auth()->user();
    }
}
if (!function_exists('company')) {
    function company()
    {
        if(auth()->user()) {
            $companyId = auth()->user()->company_id;
            $company = \App\Company::find($companyId);
            return $company;
        }
        return false;
    }
}

if (!function_exists('asset_url')) {
    function asset_url($path)
    {
        $path = 'public/user-uploads/' . $path;
        $storageUrl = $path;
        if (!Str::startsWith($storageUrl, 'http')) {
            return url($storageUrl);
        }
        return $storageUrl;
    }
}

if (!function_exists('get_cat_name')) {
    function get_cat_name($id)
    {
        $bc = \App\BoqCategory::find($id);
        if($bc !== null){
            return $bc->title;
        }
        return 'NA';
    }
}

if (!function_exists('get_cost_name')) {
    function get_cost_name($id)
    {
        $bc = \App\CostItems::find($id);
        if($bc !== null){
            return $bc->cost_item_name;
        }
        return 'NA';
    }
}
if (!function_exists('get_work_type_name')) {
    function get_work_type_name($id)
    {
        $bc = \App\Type::find($id);
        if($bc !== null){
            return $bc->title;
        }
    }
}

if (!function_exists('get_type_name')) {
    function get_type_name($id)
    {
        $bc = \App\Type::find($id);
        if($bc !== null){
            return $bc->title;
        }
        return 'NA';
    }
}
if (!function_exists('get_resource_name')) {
    function get_resource_name($id)
    {
        $bc = \App\Resource::find($id);
        if($bc !== null){
            return $bc->description;
        }
        return 'NA';
    }
}
if (!function_exists('get_resource_category')) {
    function get_resource_category($id)
    {
        $bc = \App\Resource::find($id);
        if($bc !== null){
            return $bc->category;
        }
        return 'NA';
    }
}
if (!function_exists('get_ins_type_name')) {
    function get_ins_type_name($id)
    {
        $bc = \App\InspectionType::find($id);
        if($bc !== null){
            return $bc->title;
        }
        return 'NA';
    }
}

if (!function_exists('get_field_name')) {
    function get_field_name($id)
    {
        $bc = \App\InputFields::find($id);
        if($bc !== null){
            return $bc->title;
        }
        return 'NA';
    }
}

if (!function_exists('get_user_name')) {
    function get_user_name($users)
    {
        $name = '';
        $userarray = explode(',',$users);
        foreach ($userarray as $user){
            $bc = \App\User::find($user);
            if($bc !== null){
                if(!empty($name)){
                    $name .= ',';
                }
                $name .= $bc->name;
            }
        }
        return $name;
    }
}
if (!function_exists('get_users_images')) {
    function get_users_images($users)
    {
        $image = '';
        if($users){
            $usersarray = explode(',',$users);
            foreach ($usersarray as $user){
                $userdetails = \App\User::where('id',$user)->first();
                if(!empty($userdetails->id)){
                    $name = !empty($userdetails->name) ? $userdetails->name : '';
                    if(!empty($userdetails->image)&&!empty($name)){
                        $storage = storage();
                        $imagename  = $userdetails->image;
                        $url = "https://" . config('filesystems.disks.s3.bucket') . ".s3.amazonaws.com/";
                        switch($storage){
                            case 'local':
                                $imagelink = asset('public/uploads/avatar/' . $imagename);
                                break;
                            case 's3':
                                $imagelink = $url.'avatar/'.$imagename;
                                break;
                            case 'google':
                                $imagelink = $imagename;
                                break;
                            case 'dropbox':
                                $imagelink = $imagename;
                                break;
                        }
                        $image .= '<img data-toggle="tooltip" data-original-title="'.$name.'" src="'.$imagelink.'" alt="user" class="img-circle" width="30">';
                    }else{
                        $image .= '<img data-toggle="tooltip" data-original-title="'.$name.'" src="'.asset_url('user.png').'" alt="user" class="img-circle" width="30">';
                    }
                }
            }
        }
        return $image;
    }
}
if (!function_exists('get_users_image_link')) {
    function get_users_image_link($users)
    {
        $image = '';
        if($users){
            $usersarray = explode(',',$users);
            foreach ($usersarray as $user){
                $userdetails = \App\User::where('id',$user)->first();
                if(!empty($userdetails->image)){
                    $storage = storage();
                    $url = uploads_url();
                    $imagename  = $userdetails->image;
                    switch($storage){
                        case 'local':
                            $image = $url.'avatar/' . $imagename;
                            break;
                        case 's3':
                            $image = $url.'avatar/'.$imagename;
                            break;
                        case 'google':
                            $image = $imagename;
                            break;
                        case 'dropbox':
                            $image = $imagename;
                            break;
                    }
                }
            }
        }
        return $image;
    }
}
if (!function_exists('get_employee_name')) {
    function get_employee_name($users)
    {
        $name = '';
        $userarray = explode(',',$users);
        foreach ($userarray as $user){
            $bc = \App\Employee::find($user);
            if($bc !== null){
                if(!empty($name)){
                    $name .= ',';
                }
                $name .= $bc->name;
            }
        }
        return $name;
    }
}
if (!function_exists('get_employee_images')) {
    function get_employee_images($users)
    {
        $image = '';
        if($users){
            $usersarray = explode(',',$users);
            foreach ($usersarray as $user){
                $userdetails = \App\Employee::where('id',$user)->first();
                if(!empty($userdetails->id)){
                    $name = !empty($userdetails->name) ? $userdetails->name : '';
                    if(!empty($userdetails->image)&&!empty($name)){
                        $storage = storage();
                        $imagename  = $userdetails->image;
                        $url = "https://" . config('filesystems.disks.s3.bucket') . ".s3.amazonaws.com/";
                        switch($storage){
                            case 'local':
                                $imagelink = asset('public/uploads/avatar/' . $imagename);
                                break;
                            case 's3':
                                $imagelink = $url.'avatar/'.$imagename;
                                break;
                            case 'google':
                                $imagelink = $imagename;
                                break;
                            case 'dropbox':
                                $imagelink = $imagename;
                                break;
                        }
                        $image .= '<img data-toggle="tooltip" data-original-title="'.$name.'" src="'.$imagelink.'" alt="user" class="img-circle" width="30">';
                    }else{
                        $image .= '<img data-toggle="tooltip" data-original-title="'.$name.'" src="'.asset_url('user.png').'" alt="user" class="img-circle" width="30">';
                    }
                }
            }
        }
        return $image;
    }
}
if (!function_exists('get_employee_image_link')) {
    function get_employee_image_link($users)
    {
        $image = '';
        if($users){
            $usersarray = explode(',',$users);
            foreach ($usersarray as $user){
                $userdetails = \App\Employee::where('id',$user)->first();
                if(!empty($userdetails->image)){
                    $storage = storage();
                    $url = awsurl();
                    $imagename  = $userdetails->image;
                    switch($storage){
                        case 'local':
                            $image = asset('public/uploads/avatar/' . $imagename);
                            break;
                        case 's3':
                            $image = $url.'avatar/'.$imagename;
                            break;
                        case 'google':
                            $image = $imagename;
                            break;
                        case 'dropbox':
                            $image = $imagename;
                            break;
                    }
                }
            }
        }
        return $image;
    }
}

if (!function_exists('get_product_category_name')) {
    function get_product_category_name($id)
    {
        $bc = \App\ProductCategory::find($id);
        if($bc !== null){
            return $bc->name;
        }
        return 'NA';
    }
}

if (!function_exists('get_title')) {
    function get_title($id)
    {
        $title = \App\Title::find($id);
        if($title !== null){
            return $title->title;
        }
        return 'NA';
    }
}
if (!function_exists('get_segment')) {
    function get_segment($id)
    {
        $title = \App\Segment::find($id);
        if($title !== null){
            return $title->name;
        }
        return 'NA';
    }
}

if (!function_exists('get_category')) {
    function get_category($id)
    {
        $cn = \App\BoqCategory::find($id);
        if($cn !== null){
            return $cn->title;
        }
        return 'NA';
    }
}

if (!function_exists('get_manpower_category')) {
    function get_manpower_category($id)
    {
        $cn = \App\ManpowerCategory::find($id);
        if($cn !== null){
            return $cn->title;
        }
        return 'NA';
    }
}

if (!function_exists('get_store_name')) {
    function get_store_name($id)
    {
        $cn = \App\Store::find($id);
        if($cn !== null){
            return $cn->company_name;
        }
        return 'NA';
    }
}

if (!function_exists('get_department_name')) {
    function get_department_name($id)
    {
        $cn = \App\EmployeeDetails::where('user_id',$id)->first();
        if($cn !== null){
            $dn = \App\Team::find($cn->department_id);
            if($dn !== null) {
                return $dn->team_name;
            }else {
                return 'NA';
            }
        }
        return 'NA';
    }
}

if (!function_exists('get_designation_name')) {
    function get_designation_name($id)
    {
        $cn = \App\EmployeeDetails::where('user_id',$id)->first();
        if($cn !== null){
            $dn = \App\Designation::find($cn->designation_id);
            if($dn !== null) {
                return $dn->team_name;
            }else {
                return 'NA';
            }
        }
        return 'NA';
    }
}

if (!function_exists('get_supplier_name')) {
    function get_supplier_name($id)
    {
        $cn = \App\Supplier::find($id);
        if($cn !== null){
            return $cn->company_name;
        }
        return 'NA';
    }
}
if (!function_exists('get_contractors_name')) {
    function get_contractors_name($id)
    {
        $cn = \App\Contractors::find($id);
        if($cn !== null){
            return $cn->name;
        }
        return 'NA';
    }
}
if (!function_exists('filter_string')) {
    function filter_string($string)
    {
        $string = preg_replace('/\s+/', '-', $string);
        $string = preg_replace('/[^A-Za-z0-9-]/', '', $string);
        return strtolower($string);
    }
}
if (!function_exists('get_rfq_name')) {
    function get_rfq_name($id)
    {
        $cn = \App\Rfq::find($id);
        if($cn !== null){
            return $cn->rfq_no;
        }
        return 'NA';
    }
}
if (!function_exists('get_store_name_by_rfq')) {
    function get_store_name_by_rfq($id)
    {
        $cn = \App\Rfq::find($id);
        if($cn !== null){
            return get_store_name($cn->store_id);
        }
        return 'NA';
    }
}
if (!function_exists('getRemainingQty')) {
    function getRemainingQty($po, $cid, $bid, $act_qty)
    {
        $qty = \App\Stock::where('po', $po)->where('cid', $cid)->where('bid', $bid)->sum('quantity');
        return ($act_qty-$qty);
    }
}
if (!function_exists('getStock')) {
    function getStock($store, $bid)
    {
        $qty = 0;
        $os = 0;
        if($store == 0){
            $o = \App\Stock::where('bid', $bid)->first();
            $qty = \App\Stock::where('bid', $bid)->sum('stock');
            $os = $o->opening_stock;
        }else{
            $o = \App\Stock::where('bid', $bid)->first();
            $qty = \App\Stock::where('bid', $bid)->where('store_id', $store)->sum('stock');
            $os = $o->opening_stock;
        }
        return $qty+$os;
    }
}

if (!function_exists('getProjectStoreStock')) {
    function getProjectStoreStock($project_id, $store, $cid)
    {
        $qty = \App\Stock::where('project_id', $project_id)->where('store_id', $store)->where('cid', $cid)->sum('stock');
        return $qty;
    }
}

if (!function_exists('get_est_qty')) {
    function get_est_qty($project_id, $store, $cid)
    {
        $qt = 0;
        $qty = \App\Bom::where('project_id', $project_id)->where('store_id', $store)->where('product_id', $cid)->select('est_qty')->first();
        if($qty !== null){
            $qt = $qty->est_qty;
        }
        return $qt;
    }
}

if (!function_exists('get_req_qty')) {
    function get_req_qty($project_id, $store, $cid)
    {
        $qt = 0;
        $qty = \App\ProductIssue::where('project_id', $project_id)->where('store_id', $store)->where('product_id', $cid)->sum('quantity');
        if($qty !== null){
            $qt = $qty;
        }
        return $qt;
    }
}

if (!function_exists('get_unit')) {
    function get_unit($project_id, $store, $cid)
    {
        $qt = '';
        $qty = \App\Bom::where('project_id', $project_id)->where('store_id', $store)->where('product_id', $cid)->select('unit_id')->first();
        if($qty !== null){
            $ex = explode(',',$qty->unit_id);
            foreach ($ex as $e){
                $qt .= get_unit_name($e).',';
            }
        }
        return $qt;
    }
}

if (!function_exists('get_unit_data')) {
    function get_unit_data($project_id, $store, $cid)
    {
        $qt = '';
        $qty = \App\Bom::where('project_id', $project_id)->where('store_id', $store)->where('product_id', $cid)->select('unit_id')->first();
        if($qty !== null){
            $ex = explode(',',$qty->unit_id);
            foreach ($ex as $e){
                $qt .= '<input type="hidden" name="unit[]" readonly value="'.$e.'" class="form-control quantity" placeholder="Enter Quantity">';
            }
        }
        return $qt;
    }
}

if (!function_exists('get_est_rate')) {
    function get_est_rate($project_id, $store, $cid)
    {
        $qt = 0;
        $qty = \App\Bom::where('project_id', $project_id)->where('store_id', $store)->where('product_id', $cid)->select('est_rate')->first();
        if($qty !== null){
            $qt = $qty->est_rate;
        }
        return $qt;
    }
}

if (!function_exists('get_project_name')) {
    function get_project_name($project_id)
    {
        $qt = 'NA';
        $qty = \App\Project::where('id', $project_id)->first();
        if($qty !== null){
            $qt = $qty->project_name;
        }
        return $qt;
    }
}
if (!function_exists('get_project_category_name')) {
    function get_project_category_name($project_id)
    {
        $qt = 'NA';
        $qty = \App\ProjectCategory::where('id', $project_id)->first();
        if($qty !== null){
            $qt = $qty->category_name;
        }
        return $qt;
    }
}
if (!function_exists('sub_project_access')) {
    function sub_project_access($project_id)
    {
        $qt = false;
        $qty = \App\Project::where('id', $project_id)->first();
        if($qty->subproject=='enable'){
            $qt = true;
        }
        return $qt;
    }
}
if (!function_exists('segment_access')) {
    function segment_access($project_id)
    {
        $qt = false;
        $qty = \App\Project::where('id', $project_id)->first();
        if($qty->segment=='enable'){
            $qt = true;
        }
        return $qt;
    }
}
if(!function_exists('datediffrence')){
    function datediffrence($date1,$date2){

        $datetime1 = new DateTime($date1);
        $datetime2 = new DateTime($date2);
        $difference = $datetime1->diff($datetime2);
        if($difference->d>0){
            $datevalue = '';
            if($difference->y>0){
                $datevalue .= $difference->y.' year ';
            }
            if($difference->m>0){
                $datevalue .= $difference->m.' month ';
            }
            $datevalue .= $difference->d.' days ';
            return  $datevalue;
        }else{
            return  $difference->h.' hours '.$difference->i.' mins '.$difference->s.' secs';
        }
    }
}
if (!function_exists('get_indented_qty')) {
    function get_indented_qty($project_id, $store, $cid)
    {
        if($project_id !== 0) {
            $qty = \App\Indent::where('indents.store_id', '=', $store)
//                ->join('stores', 'stores.project_id', '=', $project_id)
                ->where(function ($query) {
                    $query->where('indents.status', '=', '0')->orWhere('indents.status', '=', '1')->orWhere('indents.status', '=', '2');
                })
                ->get();
            $in = 0;
            foreach ($qty as $q) {
                $in += \App\IndentProducts::where('indent_id', $q->id)->where('cid', $cid)->sum('quantity');
            }
            return $in;
        }else{
            return '0';
        }
    }
}

if (!function_exists('get_procat_name')) {
    function get_procat_name($id)
    {
        $cn = \App\ProductCategory::find($id);
        if($cn !== null){
            return $cn->name;
        }else{
            $pro = \App\Product::find($id);
            if($pro !== null){
                return $pro->name;
            }else{
                return 'NA';
            }
        }
    }
}

if (!function_exists('get_pcat_name')) {
    function get_pcat_name($id)
    {
        $cn = \App\ProductCategory::find($id);
        if($cn !== null){
            return $cn->name;
        }
        return 'NA';
    }
}

if (!function_exists('get_pbrand_name')) {
    function get_pbrand_name($id)
    {
        $cn = \App\ProductBrand::find($id);
        if($cn !== null){
            return $cn->name;
        }
        return 'NA';
    }
}

if (!function_exists('get_min_quote')) {
    function get_min_quote($rfqId, $product)
    {
        $quotes = \App\Quotes::where('rfq_id', $rfqId)->pluck('id');
        $min = \App\QuoteProducts::whereIn('quote_id', $quotes)->where('bid', $product->bid)->where('quantity', $product->quantity)->where('unit', $product->unit)->orderBy('price', 'ASC')->first();
        return $min->price;
    }
}

if (!function_exists('get_min_price')) {
    function get_min_price($rfqId, $product)
    {
        $quotes = \App\TenderBidding::where('tender_id', $rfqId)->pluck('id');
        $min = \App\TenderBiddingProduct::where('products',$product->id)->where('tender_id', $rfqId)->whereIn('bidding_id', $quotes)->orderBy('price', 'ASC')->first();
        return !empty($min) ? $min->price : '';
    }
}

if (!function_exists('get_unit_symbol')) {
    function get_unit_symbol($id)
    {
        $cn = \App\Units::find($id);
        if($cn !== null){
            return $cn->symbol;
        }
        return 'NA';
    }
}
if (!function_exists('get_unit_name')) {
    function get_unit_name($id)
    {
        $cn = \App\Units::find($id);
        if($cn !== null){
            return $cn->name;
        }
        return 'NA';
    }
}
if (!function_exists('get_cost_item_unit_name')) {
    function get_cost_item_unit_name($id)
    {
        $cn = \App\CostItems::find($id);
        if($cn !== null){
            return $cn->unit;
        }
        return '';
    }
}
if (!function_exists('get_trade_name')) {
    function get_trade_name($id)
    {
        $cn = \App\ProductTrade::find($id);
        if($cn !== null){
            return $cn->name;
        }
        return 'NA';
    }
}

if (!function_exists('get_local_product_name')) {
    function get_local_product_name($id)
    {
        $cn = \App\Product::find($id);
        if($cn !== null){
            return $cn->name;
        }
        return 'NA';
    }
}
if (!function_exists('get_city_name')) {
    function get_city_name($id)
    {
        $cn = \App\Cities::find($id);
        if($cn !== null){
            return $cn->name;
        }
        return 'NA';
    }
}


if (!function_exists('get_state_name')) {
    function get_state_name($id)
    {
        $cn = \App\State::find($id);
        if($cn !== null){
            return $cn->name;
        }
        return 'NA';
    }
}

if (!function_exists('get_status')) {
    function get_status($id)
    {
        switch($id){
            case '0':
                return 'Open';
                break;
            case '1':
                return 'Closed';
                break;
            case '2':
                return 'Pending';
                break;
            case '3':
                return 'Completed';
                break;
            case '4':
                return 'Hold';
                break;
        }
    }
}

if (!function_exists('get_priority')) {
    function get_priority($id)
    {
        switch($id){
            case '0':
                return 'High';
                break;
            case '1':
                return 'Medium';
                break;
            case '2':
                return 'Low';
                break;
        }
    }
}


if (!function_exists('get_country_name')) {
    function get_country_name($id)
    {
        $cn = \App\Country::find($id);
        if ($cn !== null) {
            return $cn->name;
        }
        return 'NA';
    }
}
if (!function_exists('get_location_name')) {
    function get_location_name($id)
    {
        $cn = \App\Location::find($id);
        if($cn !== null){
            return $cn->location_name;
        }
        return 'NA';
    }
}

if (!function_exists('get_count_sub_location')) {
    function get_count_sub_location($location)
    {
        $cn = \App\SubLocation::where('location_id',$location)->get();
        if($cn !== null){
            return count($cn);
        }
        return 'NA';
    }
}

if (!function_exists('get_asset_category')) {
    function get_asset_category($id)
    {
        $cn = \App\AssetCategory::find($id);
        if($cn !== null){
            return $cn->name;
        }
        return 'NA';
    }
}

if (!function_exists('get_asset_name')) {
    function get_asset_name($id)
    {
        $cn = \App\Asset::find($id);
        if($cn !== null){
            return $cn->name;
        }
        return 'NA';
    }
}

if (!function_exists('get_response_field')) {
    function get_response_field($id,$question,$userid,$formid=false)
    {
        if($id){
            $field = InputFields::findOrFail($id);
            $qid = $question->id;
            $answers = \App\InspectionAnswer::where('inspection_question_id',$question->id)->where('inspection_id',$question->inspection_name_id)->where('answered_by',$userid)->orderBy('id','desc')->first();
            $replychange = $replyclick = '';
            $color = !empty($field->colors) ? $field->colors : 'blue';
            if($qid){
                $replychange = 'onChange="submitAnswer('.$qid.','.$formid.')"';
                $replyclick = 'onClick="submitAnswer('.$qid.','.$formid.')"';
            }
            $html = '';
            if($field->type == 'checkbox'){
                $opt = json_decode($field->options);
                foreach($opt as $o ) {
                    $checked = '';
                    if(!empty($answers->id)&&$answers==$o){
                        $checked = 'checked';
                    }
                    $html .= '<input  id="answer'.$qid.'" data-color="'.$color.'" '.$checked.' type="checkbox" '.$replychange.' $checked value="'.$o.'" id="check'.$o.'"> <label for="check'.$o.'">'.$o.'</label> &nbsp;';
                }
            }
            if($field->type == 'dropdown'){
                $html .= '<div class="col-md-9">';
                $opt = explode(',',$field->options);
                $html .= '<select  id="answer'.$qid.'"  '.$replychange.' class="form-control">';
                $html .= '<option value="">--Select--</option>';
                foreach($opt as $o ) {
                    $checked = '';
                    if(!empty($answers->id)&&$answers==$o){
                        $checked = 'selected';
                    }
                    $html .= '<option value="'.$o.'" '.$checked.'> '.$o.'</option>';
                }
                $html .='</select>';
                $html .= '</div>';
            }
            if($field->type == 'date'){
                $checked = '';
                if(!empty($answers->id)){
                    $checked = $answers->answer;
                }
                $html .= '<div class="col-md-9">';
                $html .= '<input type="date" '.$replychange.' data-color="'.$color.'" id="answer'.$qid.'" value="'.$checked.'"  placeholder="Select '.$field->title.'" class="form-control">';
                $html .= '</div>';
            }

            if($field->type == 'email'){
                $checked = '';
                if(!empty($answers->id)){
                    $checked = $answers->answer;
                }
                $html .= '<div class="col-md-9">';
                $html .= '<input type="email" '.$replychange.' data-color="'.$color.'" id="answer'.$qid.'" value="'.$checked.'"  placeholder="'.$field->title.'" class="form-control">';
                $html .= '</div>';
            }

            if($field->type == 'number'){
                $checked = '';
                if(!empty($answers->id)){
                    $checked = $answers->answer;
                }
                $html .= '<div class="col-md-9">';
                $html .= '<input type="number" '.$replychange.' data-color="'.$color.'" id="answer'.$qid.'" value="'.$checked.'"  placeholder="'.$field->title.'" class="form-control">';
                $html .= '</div>';
            }

            if($field->type == 'radio'){
                $html .= '<div class="col-md-9">';
                $opt = json_decode($field->options);
                foreach($opt as $o ) {
                    $checked = '';
                    if(!empty($answers->id)&&$answers->answer==$o){
                        $checked = 'checked';
                    }
                    $html .= '<input type="radio" '.$replychange.' '.$checked.' data-color="'.$color.'" id="answer'.$qid.'"  value="'.$o.'" id="check'.$o.'"> <label for="check'.$o.'">'.$o.'</label> &nbsp;';
                }
                $html .= '</div>';
            }

            if($field->type == 'text'){
                $checked = '';
                if(!empty($answers->id)&&$answers->answer){
                    $checked = $answers->answer;
                }
                $html .= '<div class="col-md-9">';
                $html .= '<input type="text" '.$replychange.' data-color="'.$color.'" value="'.$checked.'" id="answer'.$qid.'" placeholder="Enter '.$field->title.'" class="form-control">';
                $html .= '</div>';
            }
            if($field->type == 'button'){
                $opt = explode(',',$field->options);
                $colours = explode(',',$field->colors);
                $c=0; foreach($opt as $o ) {
                    $coloropt = '';
                    $styledesign = 'style="border: 1px solid #686868;color: #686868;"';
                    if(!empty($colours[$c])){
                        $coloropt = $colours[$c];
                    }
                    $r = $c+1;
                    if(!empty($answers->id)&&$answers->answer==$o){
                        $styledesign = 'style="background-color: '.$coloropt.';color: #fff;"';
                    }
                    $html .= '<a  onClick="submitAnswer('.$qid.','.$formid.','.$r.')" class="btn m-rl-5 submitoption'.$qid.$r.'" id="answer'.$qid.'" data-value="'.$o.'" data-color="'.$coloropt.'" '.$styledesign.'> '.$o.'</a>';
                    $c++;
                }
            }
            $styledesign = 'style="border: 1px solid #686868;color: #686868;"';
            if(!empty($answers->id)&&$answers->answer=='N/A'){
                $styledesign = 'style="background-color: grey;color: #fff;"';
            }
            $c = '10000000';
            $html .= '<a  onClick="submitAnswer('.$qid.','.$formid.','.$c.')" data-color="grey"  class="btn submitoption'.$qid.$c.'" data-value="N/A" '.$styledesign.' > N/A</a>';
            return $html;
        }
    }

    if (!function_exists('get_input_field')) {
        function get_input_field($id,$row)
        {
            $field = InputFields::find($id);
            if(!empty($field->id)){

                $html = '';
                $html .= '<div class="col-md-8">';
                if($field->type == 'checkbox'){
                    $opt = json_decode($field->options);
                    foreach($opt as $o ) {
                        $html .= '<input type="checkbox" name="answer[' . $row . ']" value="'.$o.'" id="check'.$o.'"> <label for="check'.$o.'">'.$o.'</label> &nbsp;';
                    }
                }

                if($field->type == 'dropdown'){
                    $opt = explode(',',$field->options);
                    $html .= '<select name="answer['.$row.']" class="form-control">';
                    $html .= '<option value="">--Select--</option>';
                    foreach($opt as $o ) {
                        $html .= '<option value="'.$o.'"> '.$o.'</option>';
                    }
                    $html .='</select>';
                }

                if($field->type == 'button'){
                    $opt = explode(',',$field->options);
                    $colours = explode(',',$field->colors);
                    $c=0; foreach($opt as $o ) {
                        $coloropt = '';
                        if(!empty($colours[$c])){
                            $coloropt = $colours[$c];
                        }
                        $html .= '<button type="button" class="btn btn-info m-rl-5" style="background-color: '.$coloropt.';" > '.$o.'</button>';
                        $c++; }
                }

                if($field->type == 'date'){
                    $html .= '<input type="date" name="answer['.$row.']" placeholder="'.$field->title.'" class="form-control">';
                }

                if($field->type == 'email'){
                    $html .= '<input type="email" name="answer['.$row.']" placeholder="'.$field->title.'" class="form-control">';
                }

                if($field->type == 'number'){
                    $html .= '<input type="number" name="answer['.$row.']" placeholder="'.$field->title.'" class="form-control">';
                }

                if($field->type == 'radio'){
                    $opt = json_decode($field->options);
                    foreach($opt as $o ) {
                        $html .= '<input type="radio" name="answer[' . $row . ']" value="'.$o.'" id="check'.$o.'"> <label for="check'.$o.'">'.$o.'</label> &nbsp;';
                    }
                }

                if($field->type == 'text'){
                    $html .= '<input type="text" name="answer['.$row.']" placeholder="'.$field->title.'" class="form-control">';
                }
                $html .= '</div>';
                $html .= '<button type="button" class="btn btn-info m-rl-5" style="background-color: grey;" > N/A</button>';
                return $html;
            }
        }
    }
    if (!function_exists('get_delivered_quantity')) {
        function get_delivered_quantity($id)
        {
            $qty = \App\Stock::where('po', $id)->sum('quantity');
            return $qty;
        }
    }

    if (!function_exists('get_issued_qty')) {
        function get_issued_qty($pid,$sid,$cid)
        {
            $qty = \App\ProductIssue::where('project_id', $pid)->where('store_id',$sid)->where('product_id',$cid)->sum('quantity');
            return $qty;
        }
    }
}

if (!function_exists('get_user_email')) {
    function get_user_email($id)
    {
        $qty = \App\User::where('id', $id)->first();
        return $qty->email;
    }
}


if (!function_exists('getResourceType')) {
    function getResourceType($id)
    {
        $res = \App\Resource::find($id);
        if($res !== null){
            $type = \App\Type::find($res->type);
            if($type !== null){
                return $type->symbol;
            }
            return 'NA';
        }
        return 'NA';
    }
}
if (!function_exists('getMin')) {
    function getMin($str)
    {
        $ar = explode(',', $str);
        return min($ar);
    }
}
if (!function_exists('getMax')) {
    function getMax($str)
    {
        $ar = explode(',', $str);
        return max($ar);
    }
}
if (!function_exists('getSum')) {
    function getSum($str)
    {
        $ar = explode(',', $str);
        return array_sum($ar);
    }
}
if (!function_exists('getAverage')) {
    function getAverage($str)
    {
        $ar = explode(',', $str);
        $ave = number_format(getSum($str)/count($ar), 2, '.', '');
        return $ave;
    }
}
if (!function_exists('getAbs')) {
    function getAbs($number)
    {
        return abs($number);
    }
}
if (!function_exists('getSqrt')) {
    function getSqrt($number)
    {
        return sqrt($number);
    }
}

if (!function_exists('getRound')) {
    function getRound($n)
    {
        $number = round($n);
        return $number;
    }
}
if (!function_exists('getRoundUp')) {
    function getRoundUp($n)
    {
        $number = round(ceil($n));
        return $number;
    }
}
if (!function_exists('getRoundDown')) {
    function getRoundDown($n)
    {
        $number = round(floor($n));
        return $number;
    }
}
if (!function_exists('getRoundX')) {
    function getRoundX($string)
    {
        $number = $string;
        if(!empty($string)){
            $array = explode(',',$string);
            if(!empty($array[0])&&!empty($array[1])){
                $number = round($array[0],$array[1]);
            }
        }
        return $number;
    }
}
if (!function_exists('getRoundUpX')) {
    function getRoundUpX($string)
    {
        $number = $string;
        if(!empty($string)){
            $array = explode(',',$string);
            if(!empty($array[0])&&!empty($array[1])){
                $number = round(ceil($array[0]),$array[1]);
            }
        }
        return $number;
    }
}
if (!function_exists('getRoundDown')) {
    function getRoundDown($n)
    {

        $number = round(floor($n));
        return $number;
    }
}
if (!function_exists('getRoundDownX')) {
    function getRoundDownX($string)
    {
        $number = $string;
        if(!empty($string)){
            $array = explode(',',$string);
            if(!empty($array[0])&&!empty($array[1])){
                $number = round(floor($array[0]),$array[1]);
            }
        }
        return $number;
    }
}
if (!function_exists('getCeil')) {
    function getCeil($n)
    {
        $number =  ceil($n);
        return $number;
    }
}
if (!function_exists('getFloor')) {
    function getFloor($n)
    {
        $number =  floor($n);
        return $number;
    }
}
if (!function_exists('getvolPyr')) {
    function getVolPyr($string)
    {
        $number = $string;
        if(!empty($string)){
            $array = explode(',',$string);
            if(!empty($array[0])&&!empty($array[1]&&$array[2])){
                $number = $array[0]*$array[1]*$array[2];
                return $number/3;
            }
        }
        return $number;
    }
}
if (!function_exists('getVolCyl')) {
    function getVolCyl($string)
    {
        $pi = 3.141592653589;
        $number = $string;
        if(!empty($string)){
            $array = explode(',',$string);
            if(!empty($array[0])&&!empty($array[1])){
                $number = $pi * $array[0] * $array[0] * $array[1];
                return $number/3;
            }
        }
        return $number;
    }
}
if (!function_exists('getVolCone')) {
    function getVolCone($string)
    {
        $pi = 3.141592653589;
        $number = $string;
        if(!empty($string)){
            $array = explode(',',$string);
            if(!empty($array[0])&&!empty($array[1])){
                /*$height = $height/3;
                $volume =  $pi * $radius * $radius * $height;
                return $volume;*/
                $height = $array[1]/3;
                $volume =  $pi * $array[0] * $array[0] * $height;
                return $volume;
            }
        }
        return $number;
    }
}
if (!function_exists('getVolSph')) {
    function getVolSph($string)
    {
        $pi = 3.141592653589;
        $volume = 4/3;
        $number = $string;
        if(!empty($string)){
            $array = explode(',',$string);
            if(!empty($array[0])){
                /* $volume =  $pi * $radius * $radius *$radius * $volume;*/
                $number = $pi * $array[0] * $array[0] * $array[0]* $volume;
                return $number;
            }
        }
        return $number;
    }
}
if (!function_exists('getPerimCir')) {
    function getPerimCir($string)
    {
        $pi = 3.141592653589;
        $number = $string;
        if(!empty($string)){
            $array = explode(',',$string);
            if(!empty($array[0])){
                /*$volume =  2*$pi * $radius;*/
                $number = 2*$pi * $array[0];
                return $number;
            }
        }
        return $number;
    }
}
if (!function_exists('getPerimTriR')) {
    function getPerimTriR($string)
    {
        $number = $string;
        if(!empty($string)){
            $array = explode(',',$string);
            if(!empty($array[0])&&!empty($array[1])){
                /*$volume =  2*($l+$w);*/
                $number = 2*($array[0]+$array[1]);
                return $number;
            }
        }
        return $number;
    }
}
if (!function_exists('getPerimRec')) {
    function getPerimRec($string)
    {
        $number = $string;
        if(!empty($string)){
            $array = explode(',',$string);
            if(!empty($array[0])&&!empty($array[1])){
                /*$volume =  2*($l+$w);*/
                $number = 2*($array[0]+$array[1]);
                return $number;
            }
        }
        return $number;
    }
}
if (!function_exists('getAreaCir')) {
    function getAreaCir($radius)
    {
        $pi = 3.141592653589;
        $volume =  2 *$pi * $radius* $radius;
        return $volume;
    }
}
if (!function_exists('getAreaTri')) {
    function getAreaTri($string)
    {
        $number = $string;
        if(!empty($string)){
            $array = explode(',',$string);
            if(!empty($array[0])&&!empty($array[1])){
                /* $volume = ($b* $h)/2;*/
                $number = ($array[0]*$array[1])/2;
                return $number;
            }
        }
        return $number;
    }
}
if (!function_exists('getAreaTri')) {
    function getAreaRect($string)
    {
        $number = $string;
        if(!empty($string)){
            $array = explode(',',$string);
            if(!empty($array[0])&&!empty($array[1])){
                /*  $volume = $w* $l; */
                $number = $array[0]*$array[1];
                return $number;
            }
        }
        return $number;
    }
}
if (!function_exists('getAreaPyr')) {
    function getAreaPyr($string)
    {
        $number = $string;
        if(!empty($string)){
            $array = explode(',',$string);
            if(!empty($array[0])&&!empty($array[1])&&!empty($array[2])){
                $l = $array[0];
                $b = $array[1];
                $h = $array[2];
                $number = $l*$b;
                $number += $l*sqrt(($b/2)*($b/2)+$h*$h);
                $number += $b*sqrt(($l/2)*($l/2)+$h*$h);
                return $number;
            }
        }
        return $number;
    }
}
if (!function_exists('getAreaCyl')) {
    function getAreaCyl($string)
    {
        $pi = 3.141592653589;
        $number = $string;
        if(!empty($string)){
            $array = explode(',',$string);
            if(!empty($array[0])&&!empty($array[1])){
                $r = $array[0];
                $h = $array[1];
                $number =  2 *$pi * $r* $h;
                $number +=  2 *$pi * $r* $r;
                return $number;
            }
        }
        return $number;
    }
}
if (!function_exists('getAreaCone')) {
    function getAreaCone($string)
    {
        $pi = 3.141592653589;
        $number = $string;
        if(!empty($string)){
            $array = explode(',',$string);
            if(!empty($array[0])&&!empty($array[1])){
                $r = $array[0];
                $h = $array[1];
                $number =   $r +  sqrt(($h*$h)+($r*$r));
                $number =  $pi * $r *  $number;
                return $number;
            }
        }
        return $number;
    }
}
if (!function_exists('getAreaSph')) {
    function getAreaSph($r)
    {

        $pi = 3.141592653589;
        $number =  4 *$pi * $r* $r;
        return $number;
    }
}
if (!function_exists('getSinDeg')) {
    function getSinDeg($n)
    {
        if($n>0){
            $number = 0.01745240643*$n;
            return $number;
        }
    }
}
if (!function_exists('getCosDeg')) {
    function getCosDeg($n)
    {
        if($n>0){
            $number = 0.99984769515*$n;
            return $number;
        }
    }
}
if (!function_exists('getTanDeg')) {
    function getTanDeg($n)
    {
        if($n>0){
            $number = 0.01745506492 *$n;
            return $number;
        }
    }
}
if (!function_exists('getSinDeg')) {
    function getSinDeg($n)
    {
        if($n>0){
            $number = 0.01745240643*$n;
            return $number;
        }
    }
}
if (!function_exists('get_input_date')) {
    function get_input_date($date)
    {
        if(!empty($date)){
            return date('d-m-Y',strtotime($date));
        }
    }
}
if (!function_exists('carbon_date')) {
    function carbon_date($date)
    {
        $global = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', Auth::user()->company_id)->first();
        $dateformat = '';
        if(!empty($date)){
            $dateformat =  Carbon::createFromFormat($global->date_format, $date, $global->timezone)->format('Y-m-d');
        }
        return $dateformat;
    }
}
if (!function_exists('hasPermission')) {
    function hasPermission($name, $user_id){
        $perm = \App\Permission::where('name', $name)->first();
        $user = \App\User::find($user_id);
        if($perm !== null){
            $roles = \App\RoleUser::where('user_id', $user_id)->pluck('role_id')->toArray();
            $role = 1;
            if(in_array(0, $roles)){
                $role = 0;
            }
            $rp = null;
            if($role) {
                foreach ($roles as $r) {
                    $roleD = \App\Role::find($r);
                    if($roleD->name == 'admin'){
                        $module = \App\Module::where('id', $perm->module_id)->first();
                        if(in_array($module->module_name, modulesByUser($user)))
                            return true;
                        else
                            return false;
                    }
                    $rp = \App\PermissionRole::where('permission_id', $perm->id)->where('role_id', $r)->first();
                    if($rp !== null){
                        return true;
                    }
                }
                return false;
            }else{
                $rp = \App\PermissionRole::where('permission_id', $perm->id)->where('role_id', '0_'.$user_id)->first();
                if($rp !== null){
                    return true;
                }
                return false;
            }
        }
        return false;
    }
}

if (!function_exists('modulesByUser')) {
    function modulesByUser($user){

        $module = new \App\ModuleSetting();

        if ($user->hasRole('admin')) {
            $module = $module->where('type', 'admin');

        } elseif ($user->hasRole('client')) {
            $module = $module->where('type', 'client');

        } elseif ($user->hasRole('employee')) {
            $module = $module->where('type', 'employee');
        }

        $module = $module->where('status', 'active');
        $module->select('module_name');

        $module = $module->get();
        $moduleArray = [];
        foreach ($module->toArray() as $item) {
            array_push($moduleArray, array_values($item)[0]);
        }

        return $moduleArray;
    }
}
if (!function_exists('permissions_by_user')) {
    function permissions_by_modules($module){
        $modulesarry  = explode(',',$module);
        $module = \App\Module::whereIn('module_name',$modulesarry)->pluck('id')->toArray();
        if(!empty($module)){
            $permissions = \App\Permission::whereIn('module_id',$module)->pluck('id')->toArray();
            return $permissions;
        }
    }
}
if (!function_exists('storage')){
    function storage()
    {
        return config('filesystems.default');
    }
}
if (!function_exists('uploads_url')){
    function uploads_url()
    {

        return  url('/').'/public/uploads/';
    }
}
if (!function_exists('pagecount')){
    function pagecount()
    {
        return  10;
    }
}
if (!function_exists('awsurl')){
    function awsurl()
    {
        return "https://" . config('filesystems.disks.s3.bucket') . ".s3.amazonaws.com/";
    }
}

if (!function_exists('get_doc_type')) {
    function get_doc_type($id)
    {
        $res = \App\DocumentType::find($id);
        if($res !== null){
            return $res->name;
        }
        return 'NA';
    }
}

if (!function_exists('get_skill_name')) {
    function get_skill_name($id)
    {
        $res = \App\Skill::find($id);
        if($res !== null){
            return $res->name;
        }
        return 'NA';
    }
}
if (!function_exists('get_dots_by_level')) {
    function get_dots_by_level($level)
    {
        $dots = '';
        for($i=0;$i<=$level;$i++){
            $dots .= '.';
        }
        return  $dots;
    }
}
if (!function_exists('nestedToSingle')) {
    function nestedToSingle(array $array)
    {
        $singleDimArray = [];

        foreach ($array as $item) {

            if (is_array($item)) {
                $singleDimArray = array_merge($singleDimArray, nestedToSingle($item));

            } else {
                $singleDimArray[] = $item;
            }
        }

        return $singleDimArray;
    }
}
if (!function_exists('checkEmail')) {
    function checkEmail($str) {
        return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
    }
}
if (!function_exists('checkMobile')) {
    function checkMobile($mobile)
    {
        return preg_match('/^[0-9]{10}+$/', $mobile);
    }
}
if (!function_exists('send_sms')) {
    function send_sms($mobile,$message)
    {
        $sms = urlencode($message);
        $no = urlencode($mobile);
        $url = "https://merasandesh.com/api/sendsms?username=Aakar_360&password=Ak@12345&senderid=AKAARR&message=".$sms."&numbers=" . $no . "&unicode=0&template_id=1707161684573368493";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_exec($ch);
        curl_error($ch);
        curl_close($ch);
    }
}

if (!function_exists('app_log')) {
    function app_log($e,$apiname=false,$userid=false)
    {
        $logs = new \App\AppLogs();
        $logs->status = 500;
        $logs->user_id = !empty($userid) ?: 0;
        $logs->message = $e->getMessage();
        $logs->line = $e->getLine();
        $logs->file = $e->getFile();
        $logs->api_name = $apiname;
        $logs->medium = 'api';
        $logs->save();
    }
}

if(!function_exists('sendFcmNotification')){
    function sendFcmNotification($token, $note){
        if (!defined('API_ACCESS_KEY')) define('API_ACCESS_KEY','AAAAZs3kkrA:APA91bH1TsyEUhjgSzNs8-CBs6LCnoow0sZSxi6j7rv3Lvn6M7BKnh21sQ1M1Igp1oKjsxvAwHE3Sfk2AWsmS0Na3OtHOVnbKlCVgTxn64FnJJR4TMCT33pHpEXVL0J46qfXyKmkG24d');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        /* $note = [
             'title' =>'Project Assignment',
             'body' => 'You have been added to '.$projectName.' project by '.$usaerName,
             'activity' => 'projects'
         ];*/
        $extraNotificationData = ["message" => $note,"moredata" =>'dd'];
        $fcmNotification = [
            'to'        => $token, //single token
            'notification' => $note
        ];
        $headers = [
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        curl_exec($ch);
        curl_close($ch);
    }
}

if (!function_exists('validate_url')) {
    function validate_url($url)
    {
        $headers = @get_headers($url);
        if($headers && strpos( $headers[0], '200')) {
            return true;
        }
    }
}

if (!function_exists('mentionusers_html')) {
    function mentionusers_html($mentionusers=false)
    {
        $mentionhtml = '';
        if(!empty($mentionusers)){
            $mentionusersarray = explode(',',$mentionusers);
            $mentionhtml .= '<ul class="mentiontags">';
            foreach ($mentionusersarray as $mentionuser){
                $mentionhtml .= '<li><a href="#" class="mentiontag">'.get_user_name($mentionuser).'</a></li>';
            }
            $mentionhtml .= '</ul>';
        }
        return $mentionhtml;
    }

}

if (!function_exists('mimetype_thumbnail')) {
    function mimetype_thumbnail($filename,$url)
    {
        $mimetype =  pathinfo($filename, PATHINFO_EXTENSION);
        $htmlcontent = '';
        switch ($mimetype){
            case "jpg":
                $htmlcontent .= '<a target="_blank" class="html5lightbox"  href="'.$url.'" data-toggle="tooltip" data-original-title="View" >';
                $htmlcontent .= '<img src="'.$url.'"  />';
                $htmlcontent .= '</a>';
                break;
            case "png":
                $htmlcontent .= '<a target="_blank" class="html5lightbox"  href="'.$url.'" data-toggle="tooltip" data-original-title="View" >';
                $htmlcontent .= '<img src="'.$url.'"  />';
                $htmlcontent .= '</a>';
                break;
            case "pdf":
                $htmlcontent .= '<a target="_blank" class="html5lightbox"  href="'.$url.'" data-toggle="tooltip" data-original-title="View" >';
                $htmlcontent .= '<img src="'.asset_url("pdf.png").'"  />';
                $htmlcontent .= '</a>';
                break;
            case "xls":
                $htmlcontent .= '<a target="_blank" class="html5lightbox"  href="'.$url.'" data-toggle="tooltip" data-original-title="View" >';
                $htmlcontent .= '<img src="'.asset_url("excell.png").'"  />';
                $htmlcontent .= '</a>';
                break;
            case "xlsx":
                $htmlcontent .= '<a target="_blank" class="html5lightbox"  href="'.$url.'" data-toggle="tooltip" data-original-title="View" >';
                $htmlcontent .= '<img src="'.asset_url("excell.png").'"  />';
                $htmlcontent .= '</a>';
                break;
            case "doc":
                $htmlcontent .= '<a target="_blank" class="html5lightbox"  href="'.$url.'" data-toggle="tooltip" data-original-title="View" >';
                $htmlcontent .= '<img src="'.asset_url("msdoc.png").'"  />';
                $htmlcontent .= '</a>';
                break;
        }
        return $htmlcontent;
    }

    if (!function_exists('getworkdayoff')) {
        function getworkdayoff($id)
        {
            $sundayworking = [];
            $sunday = array("17","27","37","47","57");
            $monday = array("11","21","31","41","51");
            $tuesday = array("12","22","32","42","52");
            $wednessday = array("13","23","33","43","53");
            $thrusday = array("14","24","34","44","54");
            $friday = array("15","25","35","45","55");
            $saturdayday = array("16","26","36","46","56");

            $workDay = \App\LeaveRulesSettings::join('assign_work_rules', 'assign_work_rules.workrules_id', '=', 'leave_rules_settings.id')->where('assign_work_rules.user_id',$id)->first();
            if(!empty($workDay)){
                $fields = json_decode($workDay->field_id_class);
                foreach ($fields as $key=>$value){
                    if(in_array($key,$sunday) && $value == "box red"){
                        $sundayworking[] = "Sunday Off";
                    }
                }
                return array_count_values($sundayworking);
                foreach ($fields as $key=>$value){
                    if(in_array($key,$monday) && $value == "box red"){
                        return "Monday Off";
                    }

                }foreach ($fields as $key=>$value){
                    if(in_array($key,$tuesday) && $value == "box red"){
                        return "Tuesday Off";
                    }

                }foreach ($fields as $key=>$value){
                    if(in_array($key,$wednessday) && $value == "box red"){
                        return "Wednesday Off";
                    }

                }foreach ($fields as $key=>$value){
                    if(in_array($key,$thrusday) && $value == "box red"){
                        return "Thrusday Off";
                    }

                }foreach ($fields as $key=>$value){
                    if(in_array($key,$friday) && $value == "box red"){
                        return "Friday Off";
                    }

                }foreach ($fields as $key=>$value){
                    if(in_array($key,$saturdayday) && $value == "box red"){
                        return "Saturday Off";
                    }

                }
            }
        }
    }
}
