<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class BoqTemplateProduct extends Model
{
    protected $table = 'boq_template_product';

    protected static function boot()
    {
        parent::boot();

    }
}
