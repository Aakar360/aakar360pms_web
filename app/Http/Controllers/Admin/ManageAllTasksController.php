<?php

namespace App\Http\Controllers\Admin;

use App\BoqCategory;
use App\Contractors;
use App\CostItems;
use App\Helper\Files;
use App\Helper\Reply;
use App\Http\Requests\Tasks\StoreTask;
use App\ManpowerCategory;
use App\ManpowerLog;
use App\Notifications\NewClientTask;
use App\Notifications\NewTask;
use App\Notifications\TaskCompleted;
use App\Notifications\TaskReminder;
use App\Notifications\TaskUpdated;
use App\Notifications\TaskUpdatedClient;
use App\Product;
use App\Project;
use App\ProjectCostItemsProduct;
use App\ProjectMember;
use App\ProjectsLogs;
use App\PunchItem;
use App\Task;
use App\TaskboardColumn;
use App\TaskCategory;
use App\TaskFile;
use App\TaskPercentage;
use App\Title;
use App\Traits\ProjectProgress;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class ManageAllTasksController extends AdminBaseController
{
    use ProjectProgress;

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.tasks';
        $this->pageIcon = 'ti-layout-list-thumb';
        $this->activeMenu = 'pms';
        $this->middleware(function ($request, $next) {
            if (!in_array('tasks', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    public function index()
    {
        $this->projects = Project::all();
        $this->clients = User::allClients();
        $this->employees = User::allEmployees();
        $this->taskBoardStatus = TaskboardColumn::all();
        return view('admin.tasks.index', $this->data);
    }

    public function data(Request $request, $startDate = null, $endDate = null, $hideCompleted = null, $projectId = null)
    {
        $taskBoardColumn = TaskboardColumn::where('slug', 'incomplete')->first();

        $tasks = Task::leftJoin('projects', 'projects.id', '=', 'tasks.project_id')
            ->leftJoin('users as client', 'client.id', '=', 'projects.client_id')
            ->leftJoin('users', 'users.id', '=', 'tasks.user_id')
            ->select('tasks.id','tasks.user_id', 'projects.project_name', 'tasks.heading','tasks.percentage', 'client.name as clientName','tasks.due_date','tasks.project_id')
            ->whereNull('projects.deleted_at');
        if(!empty($startDate)&&!empty($endDate)){
            $startDate = Carbon::createFromFormat($this->global->date_format, $startDate)->format('Y-m-d');
            $endDate = Carbon::createFromFormat($this->global->date_format, $endDate)->format('Y-m-d');
            $tasks->where(function ($q) use ($startDate, $endDate) {
                $q->whereBetween(DB::raw('DATE(tasks.`due_date`)'), [$startDate, $endDate]);

                $q->orWhereBetween(DB::raw('DATE(tasks.`start_date`)'), [$startDate, $endDate]);
            });
        }
        if ($projectId != 0 && $projectId !=  null && $projectId !=  'all') {
            $tasks->where('tasks.project_id', '=', $projectId);
        }

        if ($request->clientID != '' && $request->clientID !=  null && $request->clientID !=  'all') {
            $tasks->where('projects.client_id', '=', $request->clientID);
        }

        if ($request->assignedTo != '' && $request->assignedTo !=  null && $request->assignedTo !=  'all') {
            $tasks->where('tasks.user_id', '=', $request->assignedTo);
        }

        if ($request->assignedBY != '' && $request->assignedBY !=  null && $request->assignedBY !=  'all') {
            $tasks->where('creator_user.id', '=', $request->assignedBY);
        }

        if ($request->status != '' && $request->status !=  null && $request->status !=  'all') {
            $tasks->where('tasks.status', '=', $request->status);
        }

        if ($hideCompleted == '1') {
            $tasks->where('tasks.board_column_id', $taskBoardColumn->id);
        }

        $tasks->get();
        return DataTables::of($tasks)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $action = '<a href="' . route('admin.all-tasks.updateTask', $row->id) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Update"><i class="fa fa-eye" aria-hidden="true"></i></a>';
                $action .= '&nbsp;&nbsp;<a href="' . route('admin.all-tasks.edit', $row->id) . '" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>';

                $recurringTaskCount = Task::where('recurring_task_id', $row->id)->count();
                $recurringTask = $recurringTaskCount > 0 ? 'yes' : 'no';
                $action .= '&nbsp;&nbsp;<a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-task-id="' . $row->id . '" data-recurring="' . $recurringTask . '" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';

                return $action;
            })
            ->editColumn('due_date', function ($row) {
                if ($row->due_date->isPast()) {
                    return '<span class="text-danger">' . $row->due_date->format($this->global->date_format) . '</span>';
                }
                return '<span class="text-success">' . $row->due_date->format($this->global->date_format) . '</span>';
            })
            ->editColumn('name', function ($row) {
                return ($row->user_id) ? '<img src="' . get_users_image_link($row->user_id) . '" alt="user" class="img-circle" width="30"> ' . get_user_name($row->user_id) : '';
            })
            ->editColumn('created_by', function ($row) {
                if (!is_null($row->created_by)) {
                    return ($row->created_by) ? '<img src="' . get_users_image_link($row->created_by) . '" alt="user" class="img-circle" width="30"> ' . get_user_name($row->created_by) : '';
                }
            })
            ->editColumn('heading', function ($row) {
                return '<a href="javascript:;" data-task-id="' . $row->id . '" class="show-task-detail">' . ucfirst($row->heading) . '</a>';
            })
            ->editColumn('percentage', function ($row) {
                return   $row->percentage.'%';
            })
            ->editColumn('status', function ($row) {
                $statusicon = '<img data-toggle="tooltip" data-original-title="Not Started" class="img-circle" width="30" src="'.asset('public/user-uploads/not-started.png').'" /><br/><label>Not Started</label>';
                switch ($row->status){
                    case 'not started':
                        $statusicon = '<img data-toggle="tooltip" data-original-title="Not Started"  class="img-circle" width="30" src="'.asset('public/user-uploads/not-started.png').'" /><br/><label>Not Started</label>';
                        break;
                    case 'in progress':
                        $statusicon = '<img data-toggle="tooltip" data-original-title="In Progress"  class="img-circle" width="30" src="'.asset('public/user-uploads/in-progress.png').'" /><br/><label>In Progress</label>';
                        break;
                    case 'in problem':
                        $statusicon = '<img data-toggle="tooltip" data-original-title="In Problem"  class="img-circle" width="30" src="'.asset('public/user-uploads/in-problem.png').'" /><br/><label>In Problem</label>';
                        break;
                    case 'delayed':
                        $statusicon = '<img data-toggle="tooltip" data-original-title="Delayed"  class="img-circle" width="30" src="'.asset('public/user-uploads/delay.png').'" /><br/><label>Delay</label>';
                        break;
                    case 'completed':
                        $statusicon = '<img data-toggle="tooltip" data-original-title="Completed"  class="img-circle" width="30" src="'.asset('public/user-uploads/completed.png').'" /><br/><label>Completed</label>';
                        break;
                }
                return $statusicon;
            })
            ->editColumn('project_name', function ($row) {
                if (is_null($row->project_id)) {
                    return "";
                }
                return '<a href="' . route('admin.projects.show', $row->project_id) . '">' . ucfirst($row->project_name) . '</a>';
            })
            ->rawColumns(['status', 'action', 'project_name','clientName', 'due_date', 'name', 'created_by', 'heading'])
            ->removeColumn('project_id')
            ->removeColumn('image')
            ->removeColumn('created_image')
            ->removeColumn('label_color')
            ->make(true);
    }
    public function edit($id)
    {
        $task = Task::findOrFail($id);
        $this->task = $task;
        $this->projects = Project::all();
        $this->titlesarray = !empty($task->project_id) ? Title::where('project_id',$task->project_id)->get() : array();
        $this->costitemsarray = !empty($task->title) ?  ProjectCostItemsProduct::where('project_id',$task->project_id)->where('title',$task->title)->pluck('cost_items_id','id') : array();
        $this->employees = User::allEmployees();
        $this->categories = BoqCategory::all();
        $this->taskBoardColumns = TaskboardColumn::all();
        $completedTaskColumn = TaskboardColumn::where('slug', '!=', 'completed')->first();
        if($completedTaskColumn)
        {
            $this->allTasks = Task::where('board_column_id', $completedTaskColumn->id)
                ->where('id', '!=', $id);

            if($this->task->project_id != '')
            {
                $this->allTasks = $this->allTasks ->where('project_id', $this->task->project_id);
            }

            $this->allTasks = $this->allTasks->get();
        }else {
            $this->allTasks = [];
        }

        return view('admin.tasks.edit', $this->data);
    }

    public function update(StoreTask $request, $id)
    {
        $task = Task::findOrFail($id);
        $oldStatus = TaskboardColumn::findOrFail($task->board_column_id);

        $task->heading = $request->heading;
        if ($request->description != '') {
            $task->description = $request->description;
        }
        $task->start_date = Carbon::createFromFormat($this->global->date_format, $request->start_date,$this->global->timezone)->format('Y-m-d');
        $task->due_date = Carbon::createFromFormat($this->global->date_format, $request->due_date,$this->global->timezone)->format('Y-m-d');
        $task->user_id = $request->user_id;
        $task->task_category_id = $request->category_id;
        $task->priority = $request->priority;
        $task->board_column_id = $request->status;
        $task->dependent_task_id = $request->has('dependent') && $request->dependent == 'yes' && $request->has('dependent_task_id') && $request->dependent_task_id != '' ? $request->dependent_task_id : null;

        $taskBoardColumn = TaskboardColumn::findOrFail($request->status);

        if ($taskBoardColumn->slug == 'completed') {
            $task->completed_on = Carbon::now()->format('Y-m-d H:i:s');
        } else {
            $task->completed_on = null;
        }

        $task->project_id = $request->project_id;
        $task->save();

        if ($oldStatus->slug == 'incomplete'  && $taskBoardColumn->slug == 'completed') {
            // notify user
            $notifyUser = User::withoutGlobalScope('active')->findOrFail($request->user_id);
            $notifyUser->notify(new TaskCompleted($task));

            if ($task->project_id != null) {
                if ($task->project->client_id != null  && $task->project->allow_client_notification == 'enable') {
                    //calculate project progress if enabled
                    $this->calculateProjectProgress($request->project_id);
                    $notifyClient = User::findOrFail($task->project->client_id);
                    $notifyClient->notify(new TaskCompleted($task));
                }
            }
        } else {
            //Send notification to user
            $notifyUser = User::findOrFail($request->user_id);
            $notifyUser->notify(new TaskUpdated($task));

            if ($task->project_id != null) {
                if ($task->project->client_id != null && $task->project->allow_client_notification == 'enable') {
                    //calculate project progress if enabled
                    $this->calculateProjectProgress($request->project_id);
                    $notifyUser = User::withoutGlobalScope('active')->findOrFail($task->project->client_id);
                    $notifyUser->notify(new TaskUpdatedClient($task));
                }
            }
        }
        if($request->boq=='1'){
            $cost_item_id = $task->cost_item_id;
            $productcostitem = ProjectCostItemsProduct::find($cost_item_id);
            if(!empty($productcostitem->id)){
                $productcostitem->category = $task->category_id;
                $productcostitem->project_id = $task->project_id;
                $productcostitem->assign_to = $task->user_id;
                $productcostitem->title = $task->title_id;
                $productcostitem->description = strip_tags($task->description);
                $startDate = Carbon::createFromFormat($this->global->date_format, $task->start_date, $this->global->timezone)->format('Y-m-d');
                $dueDate = Carbon::createFromFormat($this->global->date_format, $task->due_date, $this->global->timezone)->format('Y-m-d');
                $productcostitem->start_date = $startDate;
                $productcostitem->deadline = $dueDate;
                $productcostitem->save();
            }
        }
        return Reply::dataOnly(['taskID' => $task->id]);
        //        return Reply::redirect(route('admin.all-tasks.index'), __('messages.taskUpdatedSuccessfully'));
    }

    public function destroy(Request $request, $id)
    {
        $task = Task::findOrFail($id);

        // If it is recurring and allowed by user to delete all its recurring tasks
        if ($request->has('recurring') && $request->recurring == 'yes') {
            Task::where('recurring_task_id', $id)->delete();
        }

        $taskFiles = TaskFile::where('task_id', $id)->get();

        foreach ($taskFiles as $file) {
            Files::deleteFile($file->hashname, 'task-files/' . $file->task_id);
            $file->delete();
        }

        Task::destroy($id);
        //calculate project progress if enabled
        $this->calculateProjectProgress($task->project_id);

        return Reply::success(__('messages.taskDeletedSuccessfully'));
    }

    public function create()
    {
        $this->projects = Project::all();
        $this->employees = User::allEmployees();
        $this->categories = BoqCategory::all();
        $this->costitems = CostItems::all();
        $completedTaskColumn = TaskboardColumn::where('slug', '!=', 'completed')->first();
        if($completedTaskColumn)
        {
            $this->allTasks = Task::where('board_column_id', $completedTaskColumn->id)->get();
        }else {
            $this->allTasks = [];
        }
        return view('admin.tasks.create', $this->data);
    }

    public function membersList($projectId)
    {
        $this->members = ProjectMember::byProject($projectId);
        $list = view('admin.tasks.members-list', $this->data)->render();
        return Reply::dataOnly(['html' => $list]);
    }

    public function dependentTaskLists($projectId, $taskId = null)
    {
        $completedTaskColumn = TaskboardColumn::where('slug', '!=', 'completed')->first();
        if($completedTaskColumn)
        {
            $this->allTasks = Task::where('board_column_id', $completedTaskColumn->id)
                ->where('project_id', $projectId);

            if($taskId != null)
            {
                $this->allTasks = $this->allTasks->where('id', '!=', $taskId);
            }

            $this->allTasks = $this->allTasks->get();
        }else {
            $this->allTasks = [];
        }

        $list = view('admin.tasks.dependent-task-list', $this->data)->render();
        return Reply::dataOnly(['html' => $list]);
    }

    public function store(StoreTask $request)
    {

        $ganttTaskArray = [];
        $gantTaskLinkArray = [];
        $taskBoardColumn = TaskboardColumn::where('slug', 'incomplete')->first();
        $task = new Task();
        $task->heading = $request->heading;
        if($request->costitemheading=='other'){
            $task->heading =  $request->heading;
        }
        if ($request->description != '') {
            $task->description = $request->description;
        }
        $task->start_date = Carbon::createFromFormat($this->global->date_format, $request->start_date, $this->global->timezone)->format('Y-m-d');
        $task->due_date = Carbon::createFromFormat($this->global->date_format, $request->due_date, $this->global->timezone)->format('Y-m-d');
        $task->user_id = $request->user_id;
        $task->project_id = $request->project_id;
        $task->title = $request->title_id;
        $task->boqinclude = $request->boq ?: 0;
        $task->task_category_id = $request->category_id;
        $task->priority = $request->priority;
        $task->board_column_id = $taskBoardColumn->id;
        $task->created_by = $this->user->id;
        $task->status = $request->status_id;
        $task->private = $request->private ?: 0;
        $task->dependent_task_id = $request->has('dependent') && $request->dependent == 'yes' && $request->has('dependent_task_id') && $request->dependent_task_id != '' ? $request->dependent_task_id : null;

        if ($request->board_column_id) {
            $task->board_column_id = $request->board_column_id;
        }

        if ($taskBoardColumn->slug == 'completed') {
            $task->completed_on = Carbon::now()->format('Y-m-d H:i:s');
        } else {
            $task->completed_on = null;
        }

        $task->save();

        if($request->boq=='1'){
            $costitemheading = $request->costitemheading;
            if($costitemheading=='other'){
                $costitem = new CostItems();
                $costitem->cost_item_name = $request->heading;
                $costitem->save();
                $costitemheading = $costitem->id;
            }
            $productcostitem = new ProjectCostItemsProduct();
            $productcostitem->cost_items_id = $costitemheading;
            $productcostitem->category = $request->category_id;
            $productcostitem->project_id = $request->project_id;
            $productcostitem->assign_to = $request->user_id;
            $productcostitem->title = $request->title_id;
            $productcostitem->description = strip_tags($request->description);
            $startDate = Carbon::createFromFormat($this->global->date_format, $request->start_date, $this->global->timezone)->format('Y-m-d');
            $dueDate = Carbon::createFromFormat($this->global->date_format, $request->due_date, $this->global->timezone)->format('Y-m-d');
            $productcostitem->start_date = $startDate;
            $productcostitem->deadline = $dueDate;
            $productcostitem->save();

            $task->cost_item_id = $productcostitem->id;
            $task->save();
        }
        // For gantt chart
        if ($request->page_name && $request->page_name == 'ganttChart') {
            $parentGanttId = $request->parent_gantt_id;

            $taskDuration = $task->due_date->diffInDays($task->start_date);
            $taskDuration = $taskDuration + 1;

            $ganttTaskArray[] = [
                'id' => $task->id,
                'text' => $task->heading,
                'start_date' => $task->start_date->format('Y-m-d'),
                'duration' => $taskDuration,
                'parent' => $parentGanttId,
                'users' => [
                    ucwords($task->user->name)
                ],
                'taskid' => $task->id
            ];

            $gantTaskLinkArray[] = [
                'id' => 'link_' . $task->id,
                'source' => $parentGanttId,
                'target' => $task->id,
                'type' => 1
            ];
        }

        // Add repeated task
        if ($request->has('repeat') && $request->repeat == 'yes') {
            $repeatCount = $request->repeat_count;
            $repeatType = $request->repeat_type;
            $repeatCycles = $request->repeat_cycles;
            $startDate = Carbon::createFromFormat($this->global->date_format, $request->start_date, $this->global->timezone)->format('Y-m-d');
            $dueDate = Carbon::createFromFormat($this->global->date_format, $request->due_date, $this->global->timezone)->format('Y-m-d');


            for ($i = 1; $i < $repeatCycles; $i++) {
                $repeatStartDate = Carbon::createFromFormat($this->global->date_format, $startDate, $this->global->timezone)->format('Y-m-d');
                $repeatDueDate = Carbon::createFromFormat($this->global->date_format, $dueDate, $this->global->timezone)->format('Y-m-d');

                if ($repeatType == 'day') {
                    $repeatStartDate = $repeatStartDate->addDays($repeatCount);
                    $repeatDueDate = $repeatDueDate->addDays($repeatCount);
                } else if ($repeatType == 'week') {
                    $repeatStartDate = $repeatStartDate->addWeeks($repeatCount);
                    $repeatDueDate = $repeatDueDate->addWeeks($repeatCount);
                } else if ($repeatType == 'month') {
                    $repeatStartDate = $repeatStartDate->addMonths($repeatCount);
                    $repeatDueDate = $repeatDueDate->addMonths($repeatCount);
                } else if ($repeatType == 'year') {
                    $repeatStartDate = $repeatStartDate->addYears($repeatCount);
                    $repeatDueDate = $repeatDueDate->addYears($repeatCount);
                }

                $newTask = new Task();
                $newTask->heading = $request->heading;
                if ($request->description != '') {
                    $newTask->description = $request->description;
                }
                $newTask->start_date = $repeatStartDate->format('Y-m-d');
                $newTask->due_date = $repeatDueDate->format('Y-m-d');
                $newTask->user_id = $request->user_id;
                $newTask->project_id = $request->project_id;
                $newTask->task_category_id = $request->category_id;
                $newTask->priority = $request->priority;
                $task->title = $request->title_id;
                $task->cost_item_id = $request->costitem;
                $task->boqinclude = $request->boq ?: 0;
                $newTask->board_column_id = $taskBoardColumn->id;
                $newTask->created_by = $this->user->id;
                $newTask->recurring_task_id = $task->id;

                if ($request->board_column_id) {
                    $newTask->board_column_id = $request->board_column_id;
                }

                if ($taskBoardColumn->slug == 'completed') {
                    $newTask->completed_on = Carbon::now()->format('Y-m-d H:i:s');
                } else {
                    $newTask->completed_on = null;
                }

                $newTask->save();

                // For gantt chart
                if ($request->page_name && $request->page_name == 'ganttChart') {
                    $parentGanttId = $request->parent_gantt_id;
                    $taskDuration = $newTask->due_date->diffInDays($newTask->start_date);
                    $taskDuration = $taskDuration + 1;

                    $ganttTaskArray[] = [
                        'id' => $newTask->id,
                        'text' => $newTask->heading,
                        'start_date' => $newTask->start_date->format('Y-m-d'),
                        'duration' => $taskDuration,
                        'parent' => $parentGanttId,
                        'users' => [
                            ucwords($newTask->user->name)
                        ],
                        'taskid' => $newTask->id
                    ];

                    $gantTaskLinkArray[] = [
                        'id' => 'link_' . $newTask->id,
                        'source' => $parentGanttId,
                        'target' => $newTask->id,
                        'type' => 1
                    ];
                }

                $startDate = $newTask->start_date->format('Y-m-d');
                $dueDate = $newTask->due_date->format('Y-m-d');
            }
        }

        //calculate project progress if enabled
        $this->calculateProjectProgress($request->project_id);

        //      Send notification to user
        /*$notifyUser = User::withoutGlobalScope('active')->findOrFail($request->user_id);
        $notifyUser->notify(new NewTask($task));*/

        if ($task->project_id != null) {
            if ($task->project->client_id != null && $task->project->allow_client_notification == 'enable') {
                $notifyUser = User::withoutGlobalScope('active')->findOrFail($task->project->client_id);
                $notifyUser->notify(new NewClientTask($task));
            }
        }

        if (!is_null($request->project_id)) {
            $this->logProjectActivity($request->project_id, __('messages.newTaskAddedToTheProject'));
        }

        //log search
        $this->logSearchEntry($task->id, 'Task ' . $task->heading, 'admin.all-tasks.edit', 'task');

        if ($request->page_name && $request->page_name == 'ganttChart') {

            return Reply::successWithData(
                'messages.taskCreatedSuccessfully',
                [
                    'tasks' => $ganttTaskArray,
                    'links' => $gantTaskLinkArray
                ]
            );
        }

        if ($request->board_column_id) {
            return Reply::redirect(route('admin.taskboard.index'), __('messages.taskCreatedSuccessfully'));
        }

        return Reply::dataOnly(['taskID' => $task->id]);
        //        return Reply::redirect(route('admin.all-tasks.index'), __('messages.taskCreatedSuccessfully'));
    }

    public function ajaxCreate($columnId)
    {
        $this->projects = Project::all();
        $this->columnId = $columnId;
        $this->employees = User::allEmployees();
        $completedTaskColumn = TaskboardColumn::where('slug', '!=', 'completed')->first();
        if($completedTaskColumn)
        {
            $this->allTasks = Task::where('board_column_id', $completedTaskColumn->id)->get();
        }else {
            $this->allTasks = [];
        }
        return view('admin.tasks.ajax_create', $this->data);
    }

    public function remindForTask($taskID)
    {
        $task = Task::with('user')->findOrFail($taskID);

        // Send  reminder notification to user
        $notifyUser = $task->user;
        $notifyUser->notify(new TaskReminder($task));

        return Reply::success('messages.reminderMailSuccess');
    }

    public function show($id)
    {
        $this->task = Task::with('board_column')->findOrFail($id);
        $view = view('admin.tasks.show', $this->data)->render();
        return Reply::dataOnly(['status' => 'success', 'view' => $view]);
    }

    public function showFiles($id)
    {
        $this->taskFiles = TaskFile::where('task_id', $id)->get();
        return view('admin.tasks.ajax-file-list', $this->data);
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param $projectId
     * @param $hideCompleted
     */
    public function export($startDate, $endDate, $projectId, $hideCompleted)
    {

        $tasks = Task::leftJoin('projects', 'projects.id', '=', 'tasks.project_id')
            ->join('users', 'users.id', '=', 'tasks.user_id')
            ->join('taskboard_columns', 'taskboard_columns.id', '=', 'tasks.board_column_id')
            ->select('tasks.id', 'projects.project_name', 'tasks.heading', 'users.name', 'users.image', 'taskboard_columns.column_name', 'tasks.due_date', 'tasks.start_date');

        $tasks->where(function ($q) use ($startDate, $endDate) {
            $q->whereBetween(DB::raw('DATE(tasks.`due_date`)'), [$startDate, $endDate]);

            $q->orWhereBetween(DB::raw('DATE(tasks.`start_date`)'), [$startDate, $endDate]);
        });

        if ($projectId != 0) {
            $tasks->where('tasks.project_id', '=', $projectId);
        }

        if ($hideCompleted == '1') {
            $tasks->where('tasks.status', '=', 'incomplete');
        }

        $attributes =  ['image', 'due_date'];

        $tasks = $tasks->get()->makeHidden($attributes);

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'Project', 'Title', 'Assigned To', 'Status', 'Due Date'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($tasks as $row) {
            $exportArray[] = $row->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('task', function ($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Task');
            $excel->setCreator('Worksuite')->setCompany($this->companyName);
            $excel->setDescription('task file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function ($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));
                });
            });
        })->download('xlsx');
    }
    public function projectTitles(Request $request){
        $projectid = $request->projectid;
        $titleoption = '<option value="">Select Title</option>';
        if($projectid){
            $titlearray = Title::where('project_id',$projectid)->get();
            foreach ($titlearray as $item) {
                $titleoption .= '<option value="'.$item->id.'">'.$item->title.'</option>';
            }
        }
        return $titleoption;
    }
    public function costitemBytitle(Request $request){
        $projectid = $request->projectid;
        $title = $request->title;
        $titleoption = '<option value="">Select Task</option>';
        if($projectid){
            $titlearray = ProjectCostItemsProduct::where('project_id',$projectid)->where('title',$title)->pluck('cost_items_id','id');
            foreach ($titlearray as $key => $item) {
                $titleoption .= '<option value="'.$key.'">'.get_cost_name($item).'</option>';
            }
        }
        return $titleoption;
    }

    public function updateTask($id)
    {
        $user = $this->user;
        $task = Task::findOrFail($id);
        $this->task = $task;
        $this->projectcostitem = !empty($task->cost_item_id) ? ProjectCostItemsProduct::find($task->cost_item_id) : array();
        $this->projectarray  = Project::get();
        $this->contractorsarray =User::getAllContractors($user);
        $this->employees = User::allEmployees();
        $this->manpowercategory = ManpowerCategory::get();
        $alltimeline = collect();
        $taskpercentagearray = TaskPercentage::select('*','id as percentid')->where('task_id',$task->id)->orderBy('created_at','desc')->get();
        foreach ($taskpercentagearray as $taskpercentage){
            $alltimeline =  $alltimeline->push($taskpercentage);
        }
        $manpowerlogsarray = ManpowerLog::select('*','id as manpowerid')->where('task_id',$task->id)->orderBy('created_at','desc')->get();
        foreach ($manpowerlogsarray as $manpowerlogs){
            $alltimeline =  $alltimeline->push($manpowerlogs);
        }
        $punchitemlogsarray = PunchItem::select('*','id as punchitemid')->where('task_id',$task->id)->orderBy('created_at','desc')->get();
        foreach ($punchitemlogsarray as $punchitemlogs){
            $alltimeline =  $alltimeline->push($punchitemlogs);
        }
        $this->timelinearray =  $alltimeline->sortByDesc('created_at');
        return view('admin.tasks.updateTask', $this->data);
    }
    public function updatePercentage(Request $request,$id){
        $user = $this->user;
        $percentage = 0;
        $tasks = Task::find($id);
        if(!empty($request->percentage)&&$request->percentage>0){
            $percentage = $request->percentage;
            if((int)$tasks->percentage!=(int)$percentage){
                $tasks->percentage = $percentage;
            }else{
                $percentage = 0;
            }
        }
        if(!empty($request->status)){
            $tasks->status = $request->status;
        }
        if(!empty($request->taskqty)){
            $tasks->taskqty = $request->taskqty;
        }
        $tasks->save();
        $mentionusers = !empty($request->mentionusers) ? array_unique(array_filter(explode(',',$request->mentionusers))) : null;
            $taskspercent = new TaskPercentage();
            $taskspercent->task_id = $tasks->id;
            $taskspercent->added_by = $user->id;
            $taskspercent->percentage = $percentage;
            $taskspercent->taskqty = $request->taskqty ?: 0;
            $taskspercent->comment = $request->comment ?: '';
            $taskspercent->status = $request->status ?: '';
            $taskspercent->mentionusers = !empty($mentionusers) ? implode(',',$mentionusers) : null;
            $taskspercent->save();
            $msgtext = 'Task status updated by ' . $user->name;
            $headingtext = '';
            if ($percentage > 0) {
                $msgtext .= ' with ' . $percentage;
                $headingtext .= 'Task Updated to '.$percentage.'%';
            }
            if (!empty($request->status)) {
                if($percentage > 0){
                    $msgtext .= ' with ' . $request->status . ' status  update';
                    $headingtext .= ' and ' . $request->status.' status';
                }else{
                    $msgtext .=  $request->status . ' status  update';
                    $headingtext .=  $request->status.' status';
                }
                if (!empty($request->comment)) {
                    $headingtext .= ' and ' . $request->status.' status';
                }else{
                    $headingtext .= ' with ' . $request->status.' status';
                }
            }
            $medium = 'web';
            $createlog = new ProjectsLogs();
            $createlog->company_id = $user->company_id;
            $createlog->added_id = $user->id;
            $createlog->module_id = $taskspercent->id;
            $createlog->module = 'task_percentage';
            $createlog->project_id = $tasks->project_id;
            $createlog->subproject_id = $tasks->title ?: 0;
            $createlog->segment_id = $tasks->segment ?: 0;
            if(!empty($percentage)){
                $createlog->heading = $headingtext;
                $createlog->modulename = 'task_percentage_updated';
            }
            if (!empty($request->comment)) {
                $createlog->heading = $headingtext;
                $createlog->modulename = 'task_comment';
            }
            if (!empty($request->status)) {
                $createlog->heading = $headingtext;
                $createlog->modulename = 'task_status';
            }
            $createlog->description = $msgtext;
            $createlog->medium = $medium;
            $createlog->mentionusers =  !empty($mentionusers) ? implode(',',$mentionusers) : null;
            $createlog->save();


            return Reply::dataOnly(['taskID' => $id,'percentageID' => $taskspercent->id]);
    }


}
