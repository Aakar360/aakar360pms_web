<?php

namespace App;

use App\Observers\FileManagerObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class FileManager extends Model
{
    protected $table = 'file_manager';
    protected static function boot()
    {
        parent::boot();

        static::observe(FileManagerObserver::class);

        $company = company();

        static::addGlobalScope('company', function (Builder $builder) use($company) {
            if ($company) {
                $builder->where('file_manager.company_id', '=', $company->id);
            }
        });
    }
}
