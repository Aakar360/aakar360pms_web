
@if($contractor)
    <h3>{{ ucwords($contractor->name) }}</h3>
    <div class="combo-sheet">
        <div class="table-wrapper">

                 <table class="table table-bordered" >
                        <thead>
                        <tr>
                            <th></th>
                            <?php
                            $curmonth = '10-'.$curmonth.'-'.$curyear;
                            $day = date('t',strtotime($curmonth));
                            $month = date('m',strtotime($curmonth));
                            $year = date('Y',strtotime($curmonth));
                            for($x=1;$x<=$day;$x++){
                             $date = date('d M Y',mktime( 0, 0, 0, $month, $x, $year));
                                ?>
                                      <th>{{ $date }}</th>
                             <?php }   ?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $manpowercategoryarray = \App\ManpowerCategory::all();
                        if(!empty($manpowercategoryarray)){
                            foreach ($manpowercategoryarray as $manpowercategory){
                            ?>
                        <tr>
                            <td class="cell-inp">{{ ucwords($manpowercategory->title) }}</td>
                            <?php  for($x=1;$x<=$day;$x++){
                            $date = date('Y-m-d',mktime( 0, 0, 0, $month, $x, $year));
                            $mancount = \App\ManpowerLog::where('manpower_category',$manpowercategory->id)->where('work_date',$date);
                            if(!empty($contractor->id)){
                                $mancount = $mancount->where('contractor',$contractor->id);
                            }
                            if(!empty($projectid)){
                                $mancount = $mancount->where('project_id',$projectid);
                            }
                            if(!empty($subprojectid)){
                                $mancount = $mancount->where('title_id',$subprojectid);
                            }
                            if(!empty($segmentid)){
                                $mancount = $mancount->where('segment_id',$segmentid);
                            }
                            $mancount = $mancount->count();
                            ?>
                            <td class="cell-inp">{{ $mancount ?: '' }}</td>
                            <?php }   ?>
                        </tr>
                        <?php }}?>
                        </tbody>
                    </table>

        </div>
    </div>
    @else
        <p>Contractor not found</p>
    @endif
