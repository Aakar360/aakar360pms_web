<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class MeetingMinutes extends Model
{
    protected $table = 'meeting_minutes';

    protected static function boot()
    {
        parent::boot();
    }
}
