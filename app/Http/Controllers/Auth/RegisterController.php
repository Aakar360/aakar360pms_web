<?php

namespace App\Http\Controllers\Auth;

use App\Company;
use App\Currency;
use App\GlobalCurrency;
use App\GlobalSetting;
use App\Helper\Reply;
use App\InvitedUser;
use App\Notifications\NewUser;
use App\PermissionRole;
use App\ProjectMember;
use App\ProjectPermission;
use App\Role;
use App\TempUser;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'member/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //send welcome email notification
        $user->notify(new NewUser($user));
        $user->attachRole(2);

    }

    public function index()
    {
        if (!$this->isLegal()) {
            return redirect('verify-purchase');
        }
        $setting = GlobalSetting::first();
        return view('auth.register', compact('setting'));
    }
    public function store(Request $request)
    {
        if (!$this->isLegal()) {
            return redirect('verify-purchase');
        }
        $name = trim($request->name);
        $email = trim($request->email);
        $mobile = trim($request->mobile);
        $password = trim($request->password);
        if(empty($name)){
            return Reply::error(__('messages.nameEmpty'));
        }
        if(empty($mobile)){
            return Reply::error(__('messages.mobileEmpty'));
        }
        if(empty($email)){
            return Reply::error(__('messages.emailEmpty'));
        }
        $prevemail = User::where('email',$email)->first();
        if(empty($prevemail->id)){
            $prevemail = User::where('mobile',$mobile)->first();
            if(empty($prevemail->id)){
                $com = new Company();
                $com->company_name = $name;
                $com->company_email = $email;
                $com->company_phone = $mobile;
                $com->address = 'NA';
                $com->package_type = 'annual';
                $com->package_id = 1;
                $com->save();

                $globalCurrency = GlobalCurrency::find(4);
                $currency = Currency::where('currency_code', $globalCurrency->currency_code)
                    ->where('company_id', $com->id)->first();

                if(is_null($currency)) {
                    $currency = new Currency();
                    $currency->currency_name = $globalCurrency->currency_name;
                    $currency->currency_symbol = $globalCurrency->currency_symbol;
                    $currency->currency_code = $globalCurrency->currency_code;
                    $currency->is_cryptocurrency = $globalCurrency->is_cryptocurrency;
                    $currency->usd_price = $globalCurrency->usd_price;
                    $currency->company_id = $com->id;
                    $currency->save();
                }

                $com->currency_id = $currency->id;
                $com->save();
                if($com) {
                    $appid = md5(microtime());
                    $adminRole = Role::where('name', 'admin')->where('company_id', $com->id)->withoutGlobalScope('active')->first();
                    $user = new User();
                    $user->name = trim($name);
                    $user->email = trim($email);
                    $user->mobile = trim($mobile);
                    $user->gender = trim($request->gender);
                    $user->password = trim(bcrypt($password));
                    $user->appid = $appid;
                    $user->save();

                    $tmp = TempUser::where('mobile',$mobile)->orWhere('mobile',$email)->first();
                    if ($user) {
                        $user->roles()->attach($adminRole->id);
                        if($tmp) {
                            $iu = new InvitedUser();
                            $iu->company_id = $tmp->company_id;
                            $iu->user_id = $user->id;
                            $iu->invited_by = $user->id;
                            $iu->mobile = $email;
                            $iu->save();

                            $pm = new ProjectMember();
                            $pm->company_id = $tmp->company_id;
                            $pm->project_id = $tmp->project_id;
                            $pm->user_id = $user->id;
                            $pm->user_type = $tmp->user_type;
                            $pm->assigned_by = $tmp->invited_by;

                            $pm->save();

                            $urole = Role::where('name', $tmp->user_type)->where('company_id', $tmp->company_id)->first();
                            if($urole === null){
                                $emessage = 'Account created. The company invited you do not have user role type '.$tmp->user_type;
                                return Reply::error($emessage);
                            }
                            $perms = PermissionRole::where('role_id', $urole->id)->get()->pluck('permission_id');
                            foreach ($perms as $perm) {
                                $pprm = new ProjectPermission();
                                $pprm->project_id = $tmp->project_id;
                                $pprm->user_id = $user->id;
                                $pprm->permission_id = $perm;
                                $pprm->save();
                            }
                            TempUser::where('mobile',$mobile)->orWhere('mobile',$email)->delete();
                        }
                    }
                    return Reply::success(__('messages.accountCreated'));

                }
            }
            return Reply::error(__('messages.mobileExists'));
        }
        return Reply::error(__('messages.emailExists'));
    }
}
