<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneratedUserPayslip extends Model
{
     protected $fillable = ['compony_id','user_id','month','year','status'];
}
