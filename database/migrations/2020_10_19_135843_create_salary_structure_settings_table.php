<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryStructureSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_structure_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('structure_name');
            $table->string('structure_desc')->nullable();
            $table->string('basic');
            $table->string('hra');
            $table->string('structure_uuid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_structure_settings');
    }
}
