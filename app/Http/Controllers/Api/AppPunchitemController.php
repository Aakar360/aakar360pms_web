<?php

namespace App\Http\Controllers\Api;

use App\AppLogs;
use App\AppProject;
use App\BoqCategory;
use App\Company;
use App\CostItems;
use App\Helper\Files;
use App\Helper\Reply;
use App\Http\Controllers\Controller;
use App\Notifications\NewClientTask;
use App\Notifications\NewTask;
use App\ProjectActivity;
use App\ProjectAttachmentDesigns;
use App\ProjectAttachmentFiles;
use App\ProjectCostItemsFinalQty;
use App\ProjectCostItemsPosition;
use App\ProjectCostItemsProduct;
use App\ProjectMember;
use App\ProjectMilestone;
use App\ProjectsLogs;
use App\ProjectTemplate;
use App\ProjectTimeLog;
use App\PunchItem;
use App\PunchItemFiles;
use App\PunchItemReply;
use App\Rfi;
use App\Task;
use App\TaskboardColumn;
use App\TaskFile;
use App\Title;
use App\Traits\ProjectProgress;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\FileManager;
use App\Project;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use DB;
use Intervention\Image\Facades\Image;

class AppPunchitemController extends Controller
{
    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }

    protected function validatetToken($apikey,$token)
    {
        if (isset($apikey)) {
            if (!$this->validateAPI($apikey)) {
                return response()->json(['error' => 'Unauthorised'], 401);
            }
            $response = array();
            if(isset($token)) {
                if (!empty($token)) {
                    $user = User::where('appid', $token)->first();
                    if($user === null){
                        $response['message'] = 'Invalid token';
                        $response['status'] = 401;
                        return $response;
                    }
                    $response['user'] = $user;
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    return $response;
                }
                $response['message'] = 'Api Token cannot be empty';
                $response['status'] = 401;
                return $response;
            }
            $response['message'] = 'Api Token not sent';
            $response['status'] = 401;
            return $response;

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function __construct(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];

            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', $user->company_id)->first();

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function createPunchitem(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $punchitemid = $request->punchitemid;
                $memberExistsInTemplate = false;
                if(!empty($punchitemid)){
                    $task =  PunchItem::find($punchitemid);
                }else{
                    $task = new PunchItem();
                    $task->company_id = $user->company_id;
                }
                $task->title = $request->title;
                if ($request->description != '') {
                    $task->description = $request->description;
                }
                $task->start_date = Carbon::createFromFormat($this->global->date_format, $request->start_date, $this->global->timezone)->format('Y-m-d');
                $task->due_date = Carbon::createFromFormat($this->global->date_format, $request->due_date, $this->global->timezone)->format('Y-m-d');

                $task->assign_to = $request->assign_to;
                $task->priority = $request->priority;
                $task->status = $request->status;
                $task->type = $request->type;
                if($request->distribution != '') {
                    $task->distribution = implode(',', $request->distribution);
                }
                $task->location = $request->location;
                if ($request->private != '') {
                    $task->private = $request->private;
                }
                if ($request->reference != '') {
                    $task->reference = $request->reference;
                }
                $task->added_by = $user->id;
                $task->schimpact = $request->schimpact ?: '';
                $task->schimpact_days = $request->schimpact_days ?: '';
                $task->costimpact = $request->costimpact ?: '';
                $task->costimpact_days = $request->costimpact_days ?: '';
                if(!empty($request->task_id)){
                    $taskdetails = Task::find($request->task_id);
                    $task->task_id = $taskdetails->id ?: 0;
                    $task->projectid = $taskdetails->project_id ?: '';
                    $task->titleid = $taskdetails->title ?: '';
                    $task->segmentid = $taskdetails->segment ?: '';
                    $task->costitemid = $taskdetails->costitem ?: '';
                    $task->category_id = $taskdetails->task_category_id ?: 0;
                }else{
                    $task->projectid = $request->project_id ?: '';
                    $task->titleid = $request->subproject_id ?: '';
                    $task->segmentid = $request->segment_id ?: '';
                    $task->costitemid = $request->cost_item_id ?: '';
                }
                $task->save();

                    $imagesarray = array_filter(explode(',',$request->imagesids));
                    if(!empty($imagesarray)){
                        foreach ($imagesarray as $images){
                            $punchfile = PunchItemFiles::find($images);
                            if(!empty($punchfile->id)){
                                $punchfile->task_id = $task->id;
                                $punchfile->save();
                            }
                        }
                    }

                    $medium = $request->medium ?: 'android';
                    $createlog = new ProjectsLogs();
                    $createlog->company_id = $user->company_id;
                    $createlog->added_id = $user->id;
                    $createlog->module_id = $task->id;
                    $createlog->module = 'punch_item';
                    $createlog->project_id = $request->project_id ?: 0;
                    $createlog->subproject_id = $request->subproject_id ?: 0;
                    $createlog->segment_id = $request->segment_id ?: 0;
                    $createlog->heading =   $task->title;
                    if(!empty($punchitemid)){
                        $createlog->modulename = 'issue_updated';
                        $createlog->description = ' Issue Updated by '.$user->name.' with title '.$task->title.' for the project '.get_project_name($request->project_id);
                    }else{
                        $createlog->modulename = 'issue_created';
                        $createlog->description = ' Issue Raised by '.$user->name.' with title '.$task->title.' for the project '.get_project_name($request->project_id);
                     }
                    $createlog->medium = $medium;
                    $createlog->save();

                    $project_membersarray = User::join('project_members','project_members.user_id','=','users.id')
                        ->select('users.*')->where('project_members.project_id', $request->project_id)->where('users.fcm', '<>','')->groupBy('project_members.user_id')->orderBy('users.name','asc')->get();
                    foreach($project_membersarray as $project_members){
                        $notifmessage = array();
                        $notifmessage['title'] = 'Project Issue';
                        $notifmessage['body'] = 'New issue has been raised in '.get_project_name($request->project_id).' project by '.$user->name;
                        $notifmessage['activity'] = 'issues';
                        sendFcmNotification($project_members->fcm, $notifmessage);
                    }

                $response['status'] = 200;
                $response['punchitemid'] = $task->id;
                if(!empty($punchitemid)){
                    $response['message'] = 'Issue Updated Successfully';
                }else{
                    $response['message'] = 'Issue Raised Successfully';
                }
                $response['punch_item_id'] = $task->id;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-punch-item',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function appPunchitem(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try {
                $response = array();
                $proData = array();
                $taskid = $request->task_id;
                $projectselectarray = explode(',',$request->project_id);
                $subprojectid = $request->subproject_id;
                $segmentid = $request->segment_id;
                $fromdate = date('Y-m-d',strtotime($request->fromdate));
                $todate =  date('Y-m-d',strtotime($request->todate));
                $status = $request->status;
                $assignto = $request->assignto;
                $activity = $request->activity;
                if(!empty($projectselectarray)){
                    $prarray = $projectselectarray;
                }else{
                    $project = Project::where('company_id',$user->company_id)->get()->pluck('id')->toArray();
                    $pm = ProjectMember::where('user_id',$user->id)->get()->pluck('project_id')->toArray();
                    $prarray = array_filter(array_merge($project,$pm));
                }

                $projects = PunchItem::whereIn('projectid', $prarray);
                if(!empty($subprojectid)){
                    $projects = $projects->where('titleid',$subprojectid);
                }
                if(!empty($segmentid)){
                    $projects = $projects->where('segmentid',$segmentid);
                }
                if(!empty($taskid)){
                    $projects = $projects->where('task_id',$taskid);
                }
                if(!empty($assignto)){
                    $projects = $projects->where('assign_to',$assignto);
                }
                if(!empty($activity)){
                    $projects = $projects->where('category_id',$activity);
                }
                if(!empty($status)){
                    $projects = $projects->where('status',$status);
                }
                if(!empty($request->fromdate)){
                    $fromdate = date('Y-m-d',strtotime($request->fromdate));
                    $projects = $projects->where('created_at','>=',$fromdate.' 00:00:00');
                }
                if(!empty($request->todate)){
                    $todate =  date('Y-m-d',strtotime($request->todate));
                    $projects = $projects->where('created_at','<=',$todate.' 23:59:59');
                }
                $page = $request->page;
                if($page=='all'){
                    $projects = $projects->orderBy('id','desc')->get();
                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    $projects = $projects->offset($skip)->take($count)->orderBy('id','desc')->get();
                }
                foreach($projects as $project){
                    $proData[] = array(
                        'id' => $project->id,
                        'company_id' => $project->company_id,
                        'title' => $project->title,
                        'assign_to' => $project->assign_to,
                        'status' => $project->status,
                        'costimpact' => $project->costimpact,
                        'costimpact_days' => $project->costimpact_days,
                        'projectid' => $project->projectid,
                        'titleid' => $project->titleid,
                        'segmentid' => $project->segmentid,
                        'costitemid' => $project->costitemid,
                        'task_id' => $project->task_id,
                        'category_id' => $project->category_id,
                        'resolve' => $project->resolve,
                        'schimpact' => $project->schimpact,
                        'schimpact_days' => $project->schimpact_days,
                        'type' => $project->type,
                        'distribution' => $project->distribution,
                        'description' => $project->description,
                        'location' => $project->location,
                        'start_date' => Carbon::parse($project->start_date)->format('d-m-Y'),
                        'due_date' => Carbon::parse($project->due_date)->format('d-m-Y'),
                        'private' => $project->private,
                        'priority' => $project->priority,
                        'reference' => $project->reference,
                        'attachments' => $project->attachments,
                        'added_by' => get_user_name($project->added_by),
                        'created_at' => Carbon::parse($project->created_at)->format('d-m-Y'),
                        'updated_at' => Carbon::parse($project->updated_at)->format('d-m-Y'),
                        'assign_name' => $project->assign_name,
                        'category_name' => $project->category_name,
                        'project_name' => $project->project_name,
                        'subproject_name' => $project->subproject_name,
                        'distribution_name' => $project->distribution_name,
                        'task_name' => $project->task_name,
                        'images' => $project->images,
                        'datetime' => $project->datetime,
                    );
                }
                $response['status'] = 200;
                $response['message'] = 'Issues List Fetched';
                $response['response'] = $proData;
                return $response;

            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'punch-item-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function getPunchItem(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $taskid = $request->punchitemid;
                $projects = PunchItem::where('id',$taskid)->first();
                if(!empty($projects->id)){
                    $projects->addedby = !empty($projects->added_by) ? get_user_name($projects->added_by) : '';
                    $projects->start_date = !empty($projects->start_date) ? Carbon::parse($projects->start_date)->format('d-m-Y') : '';
                    $projects->due_date = !empty($projects->due_date) ? Carbon::parse($projects->due_date)->format('d-m-Y') : '';
                     $response['status'] = 200;
                    $response['message'] = 'Issues List Fetched';
                    $response['response'] = $projects;
                    return $response;
                }
                $response['message'] = 'Issue not Found';
                $response['status'] = 300;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'punch-item',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function deletePunchitem(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $punchitemarray = array_filter(explode(',',$request->punchitemid));
                foreach ($punchitemarray as $punchitem){
                    $task = PunchItem::findOrFail($punchitem);

                    ProjectsLogs::where('module_id',$task->id)->where('module','punch_item')->delete();
                    PunchItemFiles::where('task_id',$task->id)->delete();
                    $manpowerreply = PunchItemReply::where('punch_item_id',$task->id)->get();
                    foreach ($manpowerreply as $manpowerre){
                        ProjectsLogs::where('module_id',$manpowerre->id)->where('module','punch_item_reply')->delete();
                        $manpowerre->delete();
                    }
                    $task->delete();
                }
                $response['status'] = 200;
                $response['message'] = 'Issue deleted Successfully';
                return $response;

            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-punch-item',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function storeImage(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
            $punchitemid = $request->punchitemid;
            $replyid = $request->replyid;
                if ($request->hasFile('file')) {
                    $fileData = $request->file;
                     $storage = storage();
                        $companyid = $user->company_id;
                        $file = new PunchItemFiles();
                        $file->company_id = $companyid;
                        $file->user_id = $user->id;
                        $file->task_id = $punchitemid ?: 0;
                        $file->reply_id =  $replyid ?: 0;
                        switch($storage) {
                            case 'local':

                                $destinationPath = 'uploads/punch-files/'.$punchitemid;
                                if (!file_exists('public/'.$destinationPath)) {
                                    mkdir('public/'.$destinationPath, 0777, true);
                                }
                                $fileData->storeAs($destinationPath, $fileData->hashName());
                                $filename = $fileData->hashName();
                                break;
                            case 's3':
                                Storage::disk('s3')->putFileAs('/punch-files/'.$punchitemid, $fileData, $fileData->hashName(), 'public');
                                break;
                            case 'google':
                                $dir = '/';
                                $recursive = false;
                                $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                                $dir = $contents->where('type', '=', 'dir')
                                    ->where('filename', '=', 'punch-files')
                                    ->first();
                                if(!$dir) {
                                    Storage::cloud()->makeDirectory('punch-files');
                                }

                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $punchitemid)
                                ->first();

                            if ( ! $directory) {
                                Storage::cloud()->makeDirectory($dir['path'].'/'.$punchitemid);
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('filename', '=', $punchitemid)
                                    ->first();
                            }
                            Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());

                            $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());

                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('punch-files/'.$punchitemid.'/', $fileData, $fileData->hashName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/punch-files/'.$punchitemid.'/'.$fileData->hashName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $file->dropbox_link = $dropboxResult['url'];
                            break;
                    }

                    $file->filename = $fileData->getClientOriginalName();
                    $file->hashname = $fileData->hashName();
                    $file->size = $fileData->getSize();
                    $file->save();
                    $response['status'] = 200;
                    $response['imageid'] = $file->id;
                    $response['message'] = 'Issue Image Uploaded';
                    return $response;
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'store-punch-item-image',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
            $response['status'] = 300;
            $response['message'] = 'Uploading failed. Please try again';
            return $response;
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function replyPost(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
            if(!empty($request->punchitemid)){
                $id = $request->punchitemid;
                $mentionusers = !empty($request->mentionusers) ? $request->mentionusers : '';
                $punchitem = PunchItem::find($id);
                if($punchitem){
                    $punchitem->status = $request->status ?: 'Open';
                    $punchitem->save();
                    $pi = new PunchItemReply();
                    $pi->comment = $request->comment;
                    $pi->punch_item_id = $punchitem->id;
                    $pi->added_by = $user->id;
                    $pi->type = 'reply';
                    $pi->mentionusers = $mentionusers;
                    $pi->save();

                    $imagesarray = array_filter(explode(',',$request->imagesids));
                    if(!empty($imagesarray)){
                        foreach ($imagesarray as $images){
                            $punchfile = PunchItemFiles::find($images);
                            if(!empty($punchfile->id)){
                                $punchfile->task_id = $punchitem->id;
                                $punchfile->reply_id = $pi->id;
                                $punchfile->save();
                            }
                        }
                    }
                    $medium = $request->medium ?: 'android';
                    $createlog = new ProjectsLogs();
                    $createlog->company_id = $user->company_id;
                    $createlog->added_id = $user->id;
                    $createlog->module_id = $pi->id;
                    $createlog->module = 'punch_item_reply';
                    $createlog->modulename = 'issue_comment';
                    $createlog->project_id = $punchitem->projectid ?: 0;
                    $createlog->subproject_id = $punchitem->titleid ?: 0;
                    $createlog->segment_id = $punchitem->segmentid ?: 0;
                    $createlog->heading =  $request->comment;
                    $createlog->description = ' Issue Commented by '.$user->name.' with title '.$punchitem->title.' for the project '.get_project_name($punchitem->project_id);
                    $createlog->medium = $medium;
                    $createlog->mentionusers = $mentionusers;
                    $createlog->save();

                    $response = array();
                    $response['status'] = 200;
                    $response['punchitemid'] = $punchitem->id;
                    $response['replyid'] = $pi->id;
                    $response['message'] = 'Reply Updated Successfully';
                    return $response;
                }
            }
                } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'punch-item-reply',$userid);
                    $response['status'] = 500;
                    $response['message'] = $e->getMessage();
                    $response['line'] = $e->getLine();
                    $response['file'] = $e->getFile();
                    return $response;
                }
            $response['status'] = 300;
            $response['message'] = 'Issue not Found';
            return $response;
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function replyList(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                if(!empty($request->punchitemid)){
                    $id = $request->punchitemid;
                    $punchitem = PunchItem::find($id);
                    if($punchitem){
                        $page = $request->page;
                        $response = array();
                        if($page=='all'){
                            $punchitemarray = PunchItemReply::where('punch_item_id',$punchitem->id)->get();
                        }else{
                            $count = pagecount();
                            $skip = 0;
                            if($page){
                                $skip = $page*$count;
                            }
                            $punchitemarray = PunchItemReply::where('punch_item_id',$punchitem->id)->offset($skip)->take($count)->get();
                        }
                        $replyarray = array();
                        foreach ($punchitemarray as $punchitemreply){
                            $punchite = array();
                            $punchite['id'] = $punchitemreply->id;
                            $punchite['comment'] = $punchitemreply->comment;
                            $punchite['userdetails'] = $punchitemreply->userdetails;
                            $punchite['images'] = $punchitemreply->images;
                            $punchite['alignment'] = 'right';
                            if($punchitemreply->added_by==$user->id){
                                $punchite['alignment'] = 'left';
                            }
                            $punchite['type'] = $punchitemreply->type;
                            $punchite['created_at'] =  $punchitemreply->created_at;
                            $punchite['created_at'] = Carbon::parse($punchitemreply->created_at)->format('d-M-Y, h:i A');
                            $replyarray[] = $punchite;
                        }
                        $response = array();
                        $response['status'] = 200;
                        $response['responselist'] = $replyarray;
                        $response['message'] = 'Reply Updated Successfully';
                        return $response;
                    }
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'punch-item-reply-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
            $response['status'] = 300;
            $response['message'] = 'Issue not Found';
            return $response;
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function punchStatusList(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
                try{
                    $statusarray = array();
                    $statusarray[] = array('id'=>1,'slug'=>'open','name'=>'Open');
                    $statusarray[] = array('id'=>2,'slug'=>'resolved','name'=>'Resolved');
                    $response = array();
                    $response['status'] = 200;
                    $response['responselist'] = $statusarray;
                    $response['message'] = 'Status list Fetched';
                    return $response;
                } catch (\Exception $e) {
                    $userid = !empty($user->id) ? $user->id : 0;
                    app_log($e,'punch-status-list',$userid);
                    $response['status'] = 500;
                    $response['message'] = $e->getMessage();
                    $response['line'] = $e->getLine();
                    $response['file'] = $e->getFile();
                    return $response;
                }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function punchItemStatus(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                if(!empty($request->punchitemid)){
                    $id = $request->punchitemid;
                    $punchitem = PunchItem::find($id);
                    if($punchitem){
                        $punchitem->status = $request->status ?: 'Open';
                        $punchitem->save();
                        $pi = new PunchItemReply();
                        $status = '';
                        if($request->status == 'Open'){
                            $status = 'Open';
                        }else{
                            $status = 'Resolve';
                        }
                        $pi->comment = 'Status changed to '.$status.' by '.$user->name;
                        $pi->punch_item_id = $punchitem->id;
                        $pi->added_by = $user->id;
                        $pi->type = 'status';
                        $pi->save();

                        $response = array();
                        $response['status'] = 200;
                        $response['punchitemid'] = $punchitem->id;
                        $response['replyid'] = $pi->id;
                        $response['type'] = 'status';
                        $response['message'] = 'Status Updated Successfully';
                        return $response;
                    }
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'punch-item-reply',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
            $response['status'] = 300;
            $response['message'] = 'Issue not Found';
            return $response;
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
}