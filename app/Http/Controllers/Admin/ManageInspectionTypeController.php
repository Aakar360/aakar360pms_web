<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use App\InspectionType;
use Illuminate\Http\Request;

class ManageInspectionTypeController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Inspection Type';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'pms';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->types = InspectionType::all();
        return view('admin.inspection-type.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->categories = InspectionType::all();
        return view('admin.inspection-type.create', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createType()
    {
        return view('admin.inspection-type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
//    public function store(Request $request)
//    {
//        $category = new InspectionType();
//        $category->title = $request->title;
//        $category->save();
//
//        return Reply::success(__('messages.categoryAdded'));
//    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeType(Request $request)
    {
        $category = new InspectionType();
        $category->title = $request->title;
        $category->save();
        $categoryData = InspectionType::all();
        return Reply::successWithData(__('Type added successfully'),['data' => $categoryData]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editType($id)
    {
        $this->types = InspectionType::all();
        $this->type = InspectionType::where('id',$id)->first();
        return view('admin.inspection-type.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateType(Request $request, $id)
    {
        $category = InspectionType::find($id);
        $category->title = $request->title;
        $category->save();

        return Reply::success(__('Type updated successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyType($id)
    {
        InspectionType::destroy($id);
        $categoryData = InspectionType::all();
        return Reply::successWithData(__('Type deleted successfully'),['data' => $categoryData]);
    }
}
