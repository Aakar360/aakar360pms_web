<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollVerify extends Model
{
    protected $fillable = [
        'verified_date',
        'verifion_month',
        'verified_by',
        'verified_values'
    ];
}
