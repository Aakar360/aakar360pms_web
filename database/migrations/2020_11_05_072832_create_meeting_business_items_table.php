<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingBusinessItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meeting_business_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('meeting_detail_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->string('title');
            $table->string('assigned_user')->nullable();
            $table->string('due_date');
            $table->integer('status')->default(0)->nullable();
            $table->integer('priority')->default(0)->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meeting_business_items');
    }
}
