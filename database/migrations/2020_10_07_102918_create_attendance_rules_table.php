<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance_rules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->dateTime('shift_in_time');
            $table->dateTime('shift_out_time');
            $table->dateTime('anomaly_grace_in_time');
            $table->dateTime('anomaly_grace_out_time');
            $table->string('work_full_time');
            $table->string('work_half_time');
            $table->boolean('auto_clock_out')->default(false);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *

      @return void */

    public function down()
    {
        Schema::dropIfExists('attendance_rules');
    }
}
