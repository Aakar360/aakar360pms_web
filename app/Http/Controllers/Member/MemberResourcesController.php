<?php

namespace App\Http\Controllers\Member;

use App\ClientDetails;
use App\Helper\Reply;
use App\Http\Requests\Admin\Rfq\StoreRfqRequest;
use App\Http\Requests\Admin\Rfq\UpdateRfqRequest;
use App\Http\Requests\Admin\Store\StoreStoreRequest;
use App\Http\Requests\Admin\Store\UpdateStoreRequest;
use App\Http\Requests\Gdpr\SaveConsentUserDataRequest;
use App\Indent;
use App\Invoice;
use App\Lead;
use App\Notifications\NewUser;
use App\PoProducts;
use App\Product;
use App\ProductBrand;
use App\ProductCategory;
use App\PurchaseOrder;
use App\PurposeConsent;
use App\PurposeConsentUser;
use App\QuoteProducts;
use App\Quotes;
use App\Role;
use App\Rfq;
use App\RfqProducts;
use App\Store;
use App\Supplier;
use App\TmpRfq;
use App\Units;
use App\UniversalSearch;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class MemberResourcesController extends MemberBaseController
{

    public function __construct()
    {
        parent::__construct();

        $this->pageTitle = 'app.menu.resources';
        $this->pageIcon = 'icon-layers';

        $this->middleware(function ($request, $next) {
            if (!in_array('resources', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->pos = PurchaseOrder::all();
        $this->stores = Store::all();
        $this->pos = count($this->pos);

        return view('member.resources.index', $this->data);
    }
}
