<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuestionidToInspectionFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inspection_files', function (Blueprint $table) {
            $table->bigInteger('question_id')->nullable()->after('inspection_id');
            $table->bigInteger('answer_id')->nullable()->after('question_id');
            $table->string('mimetype')->nullable()->after('answer_id');
            $table->string('filetype')->nullable()->after('mimetype');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inspection_files', function (Blueprint $table) {
            $table->dropIfExists('question_id');
            $table->dropIfExists('answer_id');
            $table->dropIfExists('mimetype');
            $table->dropIfExists('filetype');
        });
    }
}
