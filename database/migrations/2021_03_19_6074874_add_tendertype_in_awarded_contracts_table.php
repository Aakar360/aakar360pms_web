<?php

use App\ClientDetails;
use App\CreditNotes;
use App\EmployeeDetails;
use App\Estimate;
use App\Invoice;
use App\Lead;
use App\Notice;
use App\Project;
use App\Proposal;
use App\Company;
use App\Task;
use App\Ticket;
use App\UniversalSearch;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTendertypeInAwardedContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('awarded_contracts', function (Blueprint $table) {
            $table->string('title')->nullable()->after('contractor_id');
            $table->string('number')->nullable()->after('title');
            $table->string('status')->nullable()->after('number');
            $table->string('tendertype')->nullable()->after('contractor_id');
            $table->string('distribution')->nullable()->after('tendertype');
            $table->string('project_id')->nullable()->after('distribution');
            $table->string('subproject_id')->nullable()->after('project_id');
            $table->string('segment_id')->nullable()->after('subproject_id');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('awarded_contracts', function (Blueprint $table) {
            $table->dropColumn(['title']);
            $table->dropColumn(['number']);
            $table->dropColumn(['status']);
            $table->dropColumn(['tendertype']);
            $table->dropColumn(['distribution']);
        });
    }
}
