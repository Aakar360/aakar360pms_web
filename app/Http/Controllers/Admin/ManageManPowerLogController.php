<?php

namespace App\Http\Controllers\Admin;

use App\Contractors;
use App\DailyLogFiles;
use App\Helper\Reply;
use App\Http\Requests\TimeLogs\StoreTimeLog;
use App\LogTimeFor;
use App\ManpowerCategory;
use App\ManpowerLog;
use App\ManpowerLogFiles;
use App\ManpowerLogReply;
use App\Project;
use App\ProjectCostItemsProduct;
use App\ProjectMember;
use App\ProjectsLogs;
use App\Segment;
use App\Task;
use App\Title;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use  View;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class ManageManPowerLogController extends AdminBaseController
{


    public function __construct() {
        parent::__construct();
        $this->pageTitle = 'Man Power Logs';
        $this->pageIcon = 'icon-clock';
        $this->activeMenu = 'pms';
        $this->middleware(function ($request, $next) {
            if(!in_array('timelogs',$this->user->modules)){
                abort(403);
            }
            return $next($request);
        });

    }

    public function index(){
        $user = $this->user;
        $this->employees = User::allEmployees();
        $this->projectarray = Project::all();
        $this->contractorsarray = User::getAllContractors($user);

        return view('admin.manpower-logs.index', $this->data);
    }
    public function create(){
        $user = $this->user;
        $this->employees = User::allEmployees();
        $this->projectarray = Project::all();
        $this->contractorsarray =User::getAllContractors($user);
        $this->manpowercategoryarray = ManpowerCategory::all();
        return view('admin.manpower-logs.create', $this->data);
    }

    public function edit($id){
        $user = $this->user;
        $manpower = ManpowerLog::findOrFail($id);
        $this->manpower = $manpower;
        $this->employees = User::allEmployees();
        $this->projectarray = Project::all();
        $this->contractorsarray = User::getAllContractors($user);
        $this->manpowercategoryarray = ManpowerCategory::all();
        $this->titlesarray = !empty($manpower->project_id) ? Title::where('project_id',$manpower->project_id)->get() : array();
        $this->costitemsarray = !empty($manpower->title_id) ? ProjectCostItemsProduct::where('project_id',$manpower->project_id)->where('title',$manpower->title_id)->get() : array();
        $this->segmentsarray = !empty($manpower->title_id) ? Segment::where('projectid',$manpower->project_id)->where('titleid',$manpower->title_id)->get() : array();

        return view('admin.manpower-logs.edit', $this->data);
    }


    public function data($startDate = null, $endDate = null, $projectId = null, $employee = null) {
        $user = Auth::user();
        $timeLogs = ManpowerLog::leftjoin('users', 'users.id', '=', 'manpower_logs.added_by');
        $timeLogs = $timeLogs->join('projects', 'projects.id', '=', 'manpower_logs.project_id');
        $timeLogs = $timeLogs->leftjoin('manpower_category', 'manpower_category.id', '=', 'manpower_logs.manpower_category');
        $timeLogs = $timeLogs->select('manpower_logs.*', 'manpower_category.title as cattitle','projects.project_name');
        if(!is_null($employee) && $employee !== 'all'){
            $timeLogs->where('manpower_logs.added_by', $employee);
        }
        if(!is_null($projectId) && $projectId !== 'all'){
                $timeLogs->where('manpower_logs.project_id', '=', $projectId);
        }
        $timeLogs = $timeLogs->get();
        $urole = \App\RoleUser::where('user_id',$user->id)->first();
        $userRole = \App\Role::where('id',$urole->role_id)->first();
        return DataTables::of($timeLogs)
            ->addIndexColumn()
            ->addColumn('action', function ($row) use ($user,$userRole) {
                $actionlink = '';
            $actionlink .= '<a href="'.route('admin.man-power-logs.reply', $row->id).'" class="btn btn-info btn-circle"
                   data-toggle="tooltip" data-original-title="Reply"><i class="fa fa-reply" aria-hidden="true"></i></a>';
        if($row->added_by==$user->id||$userRole->name == 'admin'){
            $actionlink .= '<a href="'.route('admin.man-power-logs.edit',[$row->id]).'" data-cat-id="'.$row->id.'" class="btn btn-info btn-circle"
                               data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
            $actionlink .='<a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                                   data-toggle="tooltip" data-time-id="'.$row->id.'" data-original-title="Delete">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </a>';
                }
                return $actionlink;
            })
            ->editColumn('name', function($row){
                return '<a href="'.route('admin.employees.show', $row->added_by).'" target="_blank" >'.ucwords($row->name).'</a>';
            })
            ->editColumn('project_name', function ($row) {
                    return '<a href="' . route('admin.projects.show', $row->project_id) . '">' . ucfirst($row->project_name) . '</a>';

            })
            ->editColumn('contractor', function ($row) {
                if($row->contractor){
                    return '<a href="' . route('admin.contractors.show', $row->contractor) . '">' . ucfirst(get_user_name($row->contractor)) . '</a>';
                }

            })
            ->editColumn('files', function ($row) {
                $files = ManpowerLogFiles::where('manpower_id',$row->id)->get();
                foreach ($files as $file) {
                    if ($file->external_link != '') {
                        return '<a target="_blank" href="' . $file->external_link . '" >'.ucfirst($file->filename).'</a ><br>';
                    } elseif (config('filesystems.default') == 'local') {
                        return '<a target="_blank" href="'.asset_url('manpower-log-files/'.$row->id.'/'.$file->hashname).'">'.ucfirst($file->filename).'</a ><br>';
                    } elseif (config('filesystems.default') == 's3') {
                        return '<a target="_blank" href="'.$this->url.$this->company->id.'/manpower-log-files/'.$row->id.'/'.$file->hashname.'">'.ucfirst($file->filename).'</a ><br>';
                    } elseif (config('filesystems.default') == 'google') {
                        return '<a target="_blank" href="'.$file->google_url.'">'.ucfirst($file->hashname).'</a><br>';
                    } elseif (config('filesystems.default') == 'dropbox') {
                        return '<a target="_blank" href="'.$file->dropbox_link.'">'.ucfirst($file->hashname).'</a><br>';
                    }
                }
//                    return '<a href="' . route('admin.projects.show', $row->project_id) . '">' . ucfirst($row->project_name) . '</a>';
            })
            ->rawColumns(['contractor','action', 'project_name', 'name', 'files'])
            ->removeColumn('project_id')
            ->removeColumn('task_id')
            ->make(true);
    }
    public function store(Request $request)
    {
            $timeLog = new ManpowerLog();
            $timeLog->contractor = $request->contractor;
            $timeLog->manpower_category = $request->category;
            $timeLog->manpower = $request->manpower;
            $timeLog->workinghours = $request->workinghours;
            $timeLog->work_date = !empty($request->workdate) ?  Carbon::createFromFormat($this->global->date_format, $request->workdate)->format('Y-m-d') : '';
            $timeLog->description = $request->description;
            if($request->task_id){
                $task = Task::find($request->task_id);
                $timeLog->project_id = $task->project_id;
                $timeLog->title_id = $task->title;
                $timeLog->costitem_id = $task->cost_item_id;
            }else{
                $timeLog->project_id = $request->project_id;
                $timeLog->title_id = $request->title_id;
                $timeLog->costitem_id = $request->costitem;
            }
            $timeLog->segment_id = $request->segment_id;
            $timeLog->task_id = $request->task_id ?: 0;
            $timeLog->added_by = $this->user->id;
            $timeLog->save();
            return Reply::dataOnly(['manpowerID' => $timeLog->id]);
    }
    public function update(Request $request,$id)
    {
            $timeLog = ManpowerLog::findorfail($id);
            $timeLog->contractor = $request->contractor;
            $timeLog->manpower_category = $request->category;
            $timeLog->manpower = $request->manpower;
            $timeLog->workinghours = $request->workinghours;
            $timeLog->work_date =  Carbon::createFromFormat($this->global->date_format, $request->workdate)->format('Y-m-d');
            $timeLog->description = $request->description;
            $timeLog->project_id = $request->project_id;
            $timeLog->title_id = $request->title_id;
            $timeLog->costitem_id = $request->costitem;
             $timeLog->segment_id = $request->segment_id;
            $timeLog->added_by = $this->user->id;
            $timeLog->save();
            return Reply::dataOnly(['manpowerID' => $timeLog->id]);
    }
    public function destroy($id) {
        ManpowerLog::destroy($id);
        return Reply::success(__('messages.timeLogDeleted'));
    }
    public function storeImage(Request $request)
    {
        if ($request->hasFile('file')) {
            $storage = storage();
            $companyid = $this->user->company_id;
            foreach ($request->file as $fileData){
                $file = new ManpowerLogFiles();
                $file->added_by = $this->user->id;
                $file->company_id = $companyid;
                $file->task_id = $request->task_id ?: 0;
                $file->reply_id = $request->reply_id ?: 0;
                $file->manpower_id = $request->manpower_id;
                switch($storage) {
                    case 'local':
                        $fileData->storeAs('user-uploads/manpower-log-files/'.$request->manpower_id, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs($companyid.'/manpower-log-files/'.$request->manpower_id, $fileData, $fileData->hashName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'task-files')
                            ->first();

                        if(!$dir) {
                            Storage::cloud()->makeDirectory('manpower-log-files');
                        }

                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->manpower_id)
                            ->first();

                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/'.$request->manpower_id);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->task_id)
                                ->first();
                        }

                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());

                        $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('manpower-log-files/'.$request->manpower_id.'/', $fileData, $fileData->hashName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/manpower-log-files/'.$request->manpower_id.'/'.$fileData->hashName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }
                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
//                $this->logProjectActivity($request->task_id, __('messages.newFileUploadedToTheProject'));
            }

        }
//        return Reply::redirect(route('admin.man-power-logs.index'), __('modules.projects.projectUpdated'));
    }
    /**
     * @param Request $request
     * @return array
     */
    public function stopTimer(Request $request){
        $timeId = $request->timeId;
        $timeLog = ManpowerLog::findOrFail($timeId);
        $timeLog->end_time = Carbon::now();
        $timeLog->edited_by_user = $this->user->id;
        $timeLog->save();

        $timeLog->total_hours = ($timeLog->end_time->diff($timeLog->start_time)->format('%d')*24)+($timeLog->end_time->diff($timeLog->start_time)->format('%H'));

        if($timeLog->total_hours == 0){
            $timeLog->total_hours = round(($timeLog->end_time->diff($timeLog->start_time)->format('%i')/60),2);
        }
        $timeLog->total_minutes = ($timeLog->total_hours*60)+($timeLog->end_time->diff($timeLog->start_time)->format('%i'));

        $timeLog->save();

        $this->activeTimers = ManpowerLog::whereNull('end_time')
            ->get();
        $view = view('admin.projects.man-power-logs.active-timers', $this->data)->render();
        return Reply::successWithData(__('messages.timerStoppedSuccessfully'), ['html' => $view, 'activeTimers' => count($this->activeTimers)]);
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param $id
     */
    public function export($startDate, $endDate, $id, $employee = null) {

        $projectName = 'projects.project_name'; // Set default name for select in mysql
        $timeLogs = ManpowerLog::join('users', 'users.id', '=', 'manpower_logs.added_by');

        $this->logTimeFor = LogTimeFor::first();

        // Check for apply join Task Or Project
        if($this->logTimeFor != null && $this->logTimeFor->log_time_for == 'task'){
            $timeLogs = $timeLogs->join('tasks', 'tasks.id', '=', 'manpower_logs.task_id');
            $projectName = 'tasks.heading as project_name';
        }else{
            $timeLogs = $timeLogs->join('projects', 'projects.id', '=', 'manpower_logs.project_id');
        }

        // Fields selecting  For excel
        $timeLogs = $timeLogs->select('manpower_logs.id', 'users.name', $projectName, 'manpower_logs.start_time', 'manpower_logs.end_time', 'manpower_logs.memo', 'manpower_logs.total_minutes');

        // Condition according start_date
        if(!is_null($startDate)){
            $timeLogs->where(DB::raw('DATE(manpower_logs.`start_time`)'), '>=', "$startDate");
        }

        // Condition according start_date
        if(!is_null($endDate)){
            $timeLogs->where(DB::raw('DATE(manpower_logs.`end_time`)'), '<=', "$endDate");
        }

        // Condition according employee
        if(!is_null($employee) && $employee !== 'all'){
            $timeLogs->where('manpower_logs.added_by', $employee);
        }

        // Condition according select id
        if(!is_null($id) && $id !== 'all'){
            if($this->logTimeFor != null && $this->logTimeFor->log_time_for == 'task'){
                $timeLogs->where('manpower_logs.task_id', '=', $id);
            }else{
                $timeLogs->where('manpower_logs.project_id', '=', $id);
            }
        }
        $attributes =  ['total_minutes','duration','timer'];
        $timeLogs = $timeLogs->get()->makeHidden($attributes);

        // Initialize the array which will be passed into the Excel
        // generator.
        $exportArray = [];

        // Define the Excel spreadsheet headers
        $exportArray[] = ['ID', 'User','Log For','Start Time','End Time', 'Memo', 'Total Hours'];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($timeLogs as $row) {
            $exportArray[] = $row->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('timelog', function($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Time Log');
            $excel->setCreator('Worksuite')->setCompany($this->companyName);
            $excel->setDescription('time log file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));

                });

            });

        })->download('xlsx');
    }
    public function exportReport($projectId = null, $subproject = null, $segment = null, $curmonth = null, $contractor = null) {

        $datesarray[] = '';
                            $day = date('t',strtotime($curmonth));
                            $month = date('m',strtotime($curmonth));
                            $year = date('Y',strtotime($curmonth));
                            for($x=1;$x<=$day;$x++){
                             $date = date('d M Y',mktime( 0, 0, 0, $month, $x, $year));
                             $datesarray[] = $date;
                            }
        $exportArray[] = $datesarray;
        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        $manpowercategoryarray = \App\ManpowerCategory::all();
        if(!empty($manpowercategoryarray)) {
            foreach ($manpowercategoryarray as $manpowercategory) {
                $listarray = array();
                $listarray[] = ucwords($manpowercategory->title);
                for($x=1;$x<=$day;$x++) {
                    $date = date('Y-m-d', mktime(0, 0, 0, $month, $x, $year));
                    $mancount = \App\ManpowerLog::where('manpower_category', $manpowercategory->id)->where('work_date', $date);
                    if (!empty($contractor)) {
                        $mancount = $mancount->where('contractor', $contractor);
                    }
                    if (!empty($projectId)) {
                        $mancount = $mancount->where('project_id', $projectId);
                    }
                    if (!empty($subproject)) {
                        $mancount = $mancount->where('title_id', $subproject);
                    }
                    if (!empty($segment)) {
                        $mancount = $mancount->where('segment_id', $segment);
                    }
                    $listarray[] = $mancount->count();
                }
                $exportArray[] = $listarray;
            }
        }

        // Generate and return the spreadsheet
        Excel::create('manpowerreport', function($excel) use ($exportArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Man Power report');
            $excel->setCreator('Worksuite')->setCompany($this->companyName);
            $excel->setDescription('Man Power report');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function($sheet) use ($exportArray) {
                $sheet->fromArray($exportArray, null, 'A1', false, false);

                $sheet->row(1, function($row) {

                    // call row manipulation methods
                    $row->setFont(array(
                        'bold'       =>  true
                    ));

                });

            });

        })->download('xlsx');
    }
    public function manpowerReport(){
        $user = $this->user;
        $contractors = User::getAllContractors($user);
        $this->employees = User::allEmployees();
        $this->projectarray = Project::all();
        $this->contractorsarray = $contractors;
        $this->manpowercategoryarray = ManpowerCategory::all();
        $dataarray = array();
        $dataarray['contractor'] = !empty($contractors[0]) ? $this->contractorsarray[0] : '';
        $dataarray['curmonth'] =   date('m');
        $dataarray['curyear'] = date('Y');
        $messageview = View::make('admin.manpower-logs.reporthtml',$dataarray);
        $mailcontent = $messageview->render();
        $this->reporthtml = $mailcontent;
        return view('admin.manpower-logs.report', $this->data);
    }
    public function manpowerReportHtml(Request $request){
        $contractors = User::find($request->contractor);
        $dataarray = array();
        $dataarray['projectid'] = $request->project_id;
        $dataarray['subprojectid'] = $request->subproject_id;
        $dataarray['segmentid'] = $request->segment_id;
        $dataarray['contractor'] = $contractors;
        $dataarray['curmonth'] = $request->month ?: date('m');
        $dataarray['curyear'] = $request->year ?: date('Y');
        $messageview = View::make('admin.manpower-logs.reporthtml',$dataarray);
        $mailcontent = $messageview->render();
        return $mailcontent;
    }
    public function manpowerReportPdf(Request $request){
        $contractors = User::find($request->contractor);
        $dataarray = array();
        $exporttype = $request->exporttype;
        $this->projectid = $request->project_id;
        $this->subprojectid = $request->subproject_id;
        $this->segmentid = $request->segment_id;
        $this->contractor = $contractors;
        $this->curmonth = $request->month ?: date('m');
        $this->curyear = $request->year ?: date('Y');
        if($exporttype=='pdf'){
            $pdf = PDF::loadView('admin.manpower-logs.reporthtml',$this->data)->setPaper('a4', 'landscape');
            return $pdf->download('progress-report.pdf');
        }
        if($exporttype=='excell'){
            $report = View::make('admin.manpower-logs.reportexcell',$this->data)->render();
            $exportArray =  json_decode($report);
            Excel::create('Man-power-report', function ($excel) use ($exportArray) {
                $excel->setTitle('Man power report');
                $excel->setCreator('Worksuite')->setCompany($this->companyName);
                $excel->setDescription('Progress report');
                $excel->sheet('sheet1', function ($sheet) use ($exportArray) {
                    $sheet->fromArray($exportArray, null, 'A1', false, false);
                    $sheet->row(1, function ($row) {
                        $row->setFont(array(
                            'bold'       =>  true
                        ));
                    });
                });
            })->download('xlsx');
        }
    }
    public function reply($id)
    {
        $this->manpower = ManpowerLog::findOrFail($id);
        $this->employees = User::allEmployees();
        $this->files = ManpowerLogFiles::where('manpower_id',$id)->where('reply_id','0')->get();
        $this->replies = ManpowerLogReply::where('manpower_id',$id)->get();
        return view('admin.manpower-logs.reply', $this->data);
    }
    public function replyPost(Request $request, $id)
    {
        $user = $this->user;
        $mentionusers = !empty($request->mentionusers) ? array_unique(array_filter(explode(',',$request->mentionusers))) : null;
        $manpower = ManpowerLog::find($id); 

        $pi = new ManpowerLogReply();
        $pi->company_id = $user->company_id;
        $pi->comment = $request->comment;
        $pi->manpower_id = $manpower->id;
        $pi->added_by = $this->user->id;
        $pi->mentionusers = !empty($mentionusers) ? implode(',',$mentionusers) : null;
        $pi->save();
        $createlog = new ProjectsLogs();
        $createlog->company_id = $user->company_id;
        $createlog->added_id = $user->id;
        $createlog->module_id = $pi->id;
        $createlog->module = 'manpower_reply';
        $createlog->modulename = 'manpower_comment';
        $createlog->project_id = $manpower->project_id ?: '';
        $createlog->subproject_id = $manpower->title ?: '';
        $createlog->segment_id = $manpower->segment ?: '';
        $createlog->heading =  $request->comment;
        if(empty($manpowerid)) {
            $createlog->description = 'Manpower log reply created by ' . $user->name . ' for the project ' . get_project_name($manpower->project_id);
        }else{
            $createlog->description = 'Manpower log reply updated by ' . $user->name . ' for the project ' . get_project_name($manpower->project_id);
        }
        $createlog->medium = 'web';
        $createlog->mentionusers =  !empty($mentionusers) ? implode(',',$mentionusers) : null;
        $createlog->save();

        return Reply::dataOnly(['manpowerID' => $manpower->id,'replyID' => $pi->id]);
    }

}
