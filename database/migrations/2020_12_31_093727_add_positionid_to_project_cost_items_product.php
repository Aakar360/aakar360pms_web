<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPositionidToProjectCostItemsProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_cost_items_product', function (Blueprint $table) {
            $table->string('position_id')->default(0)->after('cost_items_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_cost_items_product', function (Blueprint $table) {
            $table->dropColumn(['position_id']);
        });
    }
}
