<?php

namespace App\Http\Controllers\Member;

use App\Helper\Reply;
use App\InputFields;
use App\InspectionAnswer;
use App\InspectionAssignedUser;
use App\InspectionFile;
use App\InspectionName;
use App\InspectionQuestion;
use App\InspectionType;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ManageInspectionNameController extends MemberBaseController
{
    private $mimeType = [
        'txt' => 'fa-file-text',
        'htm' => 'fa-file-code-o',
        'html' => 'fa-file-code-o',
        'php' => 'fa-file-code-o',
        'css' => 'fa-file-code-o',
        'js' => 'fa-file-code-o',
        'json' => 'fa-file-code-o',
        'xml' => 'fa-file-code-o',
        'swf' => 'fa-file-o',
        'flv' => 'fa-file-video-o',

        // images
        'png' => 'fa-file-image-o',
        'jpe' => 'fa-file-image-o',
        'jpeg' => 'fa-file-image-o',
        'jpg' => 'fa-file-image-o',
        'gif' => 'fa-file-image-o',
        'bmp' => 'fa-file-image-o',
        'ico' => 'fa-file-image-o',
        'tiff' => 'fa-file-image-o',
        'tif' => 'fa-file-image-o',
        'svg' => 'fa-file-image-o',
        'svgz' => 'fa-file-image-o',

        // archives
        'zip' => 'fa-file-o',
        'rar' => 'fa-file-o',
        'exe' => 'fa-file-o',
        'msi' => 'fa-file-o',
        'cab' => 'fa-file-o',

        // audio/video
        'mp3' => 'fa-file-audio-o',
        'qt' => 'fa-file-video-o',
        'mov' => 'fa-file-video-o',
        'mp4' => 'fa-file-video-o',
        'mkv' => 'fa-file-video-o',
        'avi' => 'fa-file-video-o',
        'wmv' => 'fa-file-video-o',
        'mpg' => 'fa-file-video-o',
        'mp2' => 'fa-file-video-o',
        'mpeg' => 'fa-file-video-o',
        'mpe' => 'fa-file-video-o',
        'mpv' => 'fa-file-video-o',
        '3gp' => 'fa-file-video-o',
        'm4v' => 'fa-file-video-o',

        // adobe
        'pdf' => 'fa-file-pdf-o',
        'psd' => 'fa-file-image-o',
        'ai' => 'fa-file-o',
        'eps' => 'fa-file-o',
        'ps' => 'fa-file-o',

        // ms office
        'doc' => 'fa-file-text',
        'rtf' => 'fa-file-text',
        'xls' => 'fa-file-excel-o',
        'ppt' => 'fa-file-powerpoint-o',
        'docx' => 'fa-file-text',
        'xlsx' => 'fa-file-excel-o',
        'pptx' => 'fa-file-powerpoint-o',

        // open office
        'odt' => 'fa-file-text',
        'ods' => 'fa-file-text',
    ];

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Inspection';
        $this->pageIcon = 'icon-user';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->names = InspectionName::all();
        return view('member.inspection-name.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->categories = InspectionName::all();
        return view('member.inspection-name.create', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createName()
    {
        $this->types = InspectionType::get();
        $this->inputs = InputFields::get();
        $this->fields = InputFields::get();
        return view('member.inspection-name.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
//    public function store(Request $request)
//    {
//        $category = new InspectionType();
//        $category->title = $request->title;
//        $category->save();
//
//        return Reply::success(__('messages.categoryAdded'));
//    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeName(Request $request)
    {
        $category = new InspectionName();
        $category->name = $request->title;
        $category->type = $request->type;
        $category->description = $request->description;
        $category->added_by = $this->user->id;
        $category->save();

        if($category->id){
            foreach($request->question as $key=>$value ){
                $question = new InspectionQuestion();
                $question->inspection_name_id = $category->id;
                if($request->category[$key]) {
                    $question->category = $request->category[$key];
                }
                $question->input_field_id = $request->input_field_id[$key];
                $question->question = $request->question[$key];
                $question->save();
            }
        }

        $categoryData = InspectionName::all();
        return Reply::dataOnly(['taskID' => $category->id]);
//        return Reply::successWithData(__('Inspection added successfully'),['data' => $categoryData]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editName($id)
    {
        $this->names = InspectionName::all();
        $this->name = InspectionName::where('id',$id)->first();
        $this->types = InspectionType::get();
        $this->files = InspectionFile::where('inspection_id',$id)->get();
        $this->fields = InputFields::get();
        $this->ques = InspectionQuestion::where('inspection_name_id',$id)->get();
        return view('member.inspection-name.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateName(Request $request, $id)
    {
        $category = InspectionName::find($id);
        $category->name = $request->title;
        $category->type = $request->type;
        $category->description = $request->description;
        $category->added_by = $this->user->added_by;
        $category->save();

        if($request->question){
            InspectionQuestion::where('inspection_name_id',$id)->delete();
            foreach($request->question as $key=>$value ){
                $question = new InspectionQuestion();
                $question->inspection_name_id = $category->id;
                if($request->category[$key]) {
                    $question->category = $request->category[$key];
                }
                $question->input_field_id = $request->input_field_id[$key];
                $question->question = $request->question[$key];
                $question->save();
            }
        }

        return Reply::success(__('Inspection updated successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyName($id)
    {
        InspectionName::destroy($id);
        $categoryData = InspectionName::all();
        return Reply::successWithData(__('Inspection deleted successfully'),['data' => $categoryData]);
    }

    public function storeImage(Request $request)
    {
        if ($request->hasFile('file')) {
            foreach ($request->file as $fileData){
                $storage = config('filesystems.default');
                $file = new InspectionFile();
                $file->user_id = $this->user->id;
                $file->inspection_id = $request->task_id;
                switch($storage) {
                    case 'local':
                        $fileData->storeAs('user-uploads/task-files/'.$request->task_id, $fileData->hashName());
                        break;
                    case 's3':
                        Storage::disk('s3')->putFileAs('task-files/'.$request->task_id, $fileData, $fileData->getClientOriginalName(), 'public');
                        break;
                    case 'google':
                        $dir = '/';
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $dir = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', 'task-files')
                            ->first();

                        if(!$dir) {
                            Storage::cloud()->makeDirectory('task-files');
                        }

                        $directory = $dir['path'];
                        $recursive = false;
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', $request->task_id)
                            ->first();

                        if ( ! $directory) {
                            Storage::cloud()->makeDirectory($dir['path'].'/'.$request->task_id);
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $request->task_id)
                                ->first();
                        }

                        Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->getClientOriginalName());

                        $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->getClientOriginalName());

                        break;
                    case 'dropbox':
                        Storage::disk('dropbox')->putFileAs('task-files/'.$request->task_id.'/', $fileData, $fileData->getClientOriginalName());
                        $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                        $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                            [\GuzzleHttp\RequestOptions::JSON => ["path" => '/task-files/'.$request->task_id.'/'.$fileData->getClientOriginalName()]]
                        );
                        $dropboxResult = $res->getBody();
                        $dropboxResult = json_decode($dropboxResult, true);
                        $file->dropbox_link = $dropboxResult['url'];
                        break;
                }

                $file->filename = $fileData->getClientOriginalName();
                $file->hashname = $fileData->hashName();
                $file->size = $fileData->getSize();
                $file->save();
//                $this->logProjectActivity($request->task_id, __('messages.newFileUploadedToTheProject'));
            }

        }
        return Reply::redirect(route('admin.inspection-name.index'), __('modules.projects.projectUpdated'));
    }

    public function inspectionAnswer()
    {
        $this->fields = InputFields::get();
        $this->names = InspectionName::get();
        return view('member.inspection-name.answer', $this->data);
    }

    public function inspectionAnswerForm($id)
    {
        $this->inspection = InspectionName::where('id',$id)->first();
        $this->files = InspectionFile::where('inspection_id',$id)->get();
        $this->questions = InspectionQuestion::where('inspection_name_id',$id)->groupBy('category')->get();
        $this->fields = InputFields::get();
        $this->names = InspectionName::get();
//        $this->names = InspectionName::whereIn('assign_to',$this->user->id, $this->insp->assign_to)->get();
        return view('member.inspection-name.answer-form', $this->data);
    }

    public function inspectionQuestion($id)
    {
        $this->inspection = InspectionName::where('id',$id)->first();
        $this->files = InspectionFile::where('inspection_id',$id)->get();
        $this->questions = InspectionQuestion::where('inspection_name_id',$id)->groupBy('category')->get();
        $this->fields = InputFields::get();
        return view('member.inspection-name.question', $this->data);
    }

    public function getData(Request $request){
        $id = $request->id;
        $row = $request->row_id;

        $field = InputFields::findOrFail($id);
        $html = '';

        if($field->type == 'checkbox'){
            $opt = json_decode($field->options);
            foreach($opt as $o ) {
                $html .= '<input type="checkbox" name="answer[' . $row . ']" value="'.$o.'" id="check'.$o.'"> <label for="check'.$o.'">'.$o.'</label> &nbsp;';
            }
        }

        if($field->type == 'select'){
            $opt = json_decode($field->options);
            $html = '<select name="answer['.$row.']" class="form-control">';
            foreach($opt as $o ) {
                $html .= '<option value="'.$o.'"> '.$o.'</option>';
            }
            $html .='</select>';
        }

        if($field->type == 'select'){
            $opt = json_decode($field->options);
            $html = '<select name="answer['.$row.']" class="form-control">';
            foreach($opt as $o ) {
                $html .= '<option value="'.$o.'"> '.$o.'</option>';
            }
            $html .='</select>';
        }

        if($field->type == 'button'){
            $html .= '<input type="button" name="answer['.$row.']" value="'.$field->title.'" class="form-control">';
        }

        if($field->type == 'date'){
            $html .= '<input type="date" name="answer['.$row.']" placeholder="'.$field->title.'" class="form-control">';
        }

        if($field->type == 'email'){
            $html .= '<input type="email" name="answer['.$row.']" placeholder="'.$field->title.'" class="form-control">';
        }

        if($field->type == 'number'){
            $html .= '<input type="number" name="answer['.$row.']" placeholder="'.$field->title.'" class="form-control">';
        }

        if($field->type == 'radio'){
            $opt = json_decode($field->options);
            foreach($opt as $o ) {
                $html .= '<input type="radio" name="answer[' . $row . ']" value="'.$o.'" id="check'.$o.'"> <label for="check'.$o.'">'.$o.'</label> &nbsp;';
            }
        }

        if($field->type == 'text'){
            $html .= '<input type="text" name="answer['.$row.']" placeholder="'.$field->title.'" class="form-control">';
        }

        return Reply::dataOnly(['html' => $html]);
    }

    public function storeQuestion(Request $request){
        //dd($request->question);

        foreach($request->question as $key=>$value ){
            $question = new InspectionQuestion();
            $question->inspection_name_id = $request->inspection_id;
            if($request->category) {
                $question->category = $request->category;
            }
            $question->input_field_id = $request->input_field_id[$key];
            $question->question = $request->question[$key];
            $question->save();
        }

        return Reply::success(__('Question updated successfully'));
    }

    public function storeAnswer(Request $request){
        //dd($request->question);

        foreach($request->answer as $key=>$value ){
            if ($request->answer != null) {
                $question = new InspectionAnswer();
                $question->inspection_id = $request->inspection_id;
                $question->inspection_question_id = $request->question_id[$key];
                $question->answer = $request->answer[$key];
                $question->answered_by = $this->user->id;
                $question->save();
            }
        }

        return Reply::success(__('Answer updated successfully'));
    }

    public function assignUser(){
        $this->employees = User::allEmployees();
        $this->fields = InputFields::get();
        $this->names = InspectionName::get();
        return view('member.inspection-name.assign-user', $this->data);
    }

    public function assignUserStore(Request $request){
//        $x = explode(',', $request->assign_to);
        foreach($request->assign_to as $a) {
            $category = new InspectionAssignedUser();
            $category->inspection_id = $request->id;
            $category->user_id = $a;
            $category->save();
        }


        return Reply::success(__('User Assigned successfully'));
    }

    public function removeFile($id){
        $taskFiles = InspectionFile::findOrFail($id);

        Files::deleteFile($taskFiles->hashname, 'task-files/' . $taskFiles->inspection_id);
        $taskFiles->delete();

        return Reply::success(__('image deleted successfully'));
    }

    public function inspectionReplies(){
        $this->employees = User::allEmployees();
        $this->fields = InputFields::get();
        $this->namess = InspectionName::get();
        $this->answers = InspectionAssignedUser::get();
        return view('member.inspection-name.inspection-replies', $this->data);
    }

    public function inspectionRepliesView($id, $userId){
        $this->inspection = InspectionName::where('id',$id)->first();
        $this->files = InspectionFile::where('inspection_id',$id)->get();
        $this->questions = InspectionQuestion::where('inspection_name_id',$id)->groupBy('category')->get();
        $this->userId = $userId;
        return view('member.inspection-name.inspection-replies-view', $this->data);
    }
}
