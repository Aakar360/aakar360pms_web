<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class InspectionFile extends Model
{
    protected $table = 'inspection_files';

    protected static function boot()
    {
        parent::boot();
    }
}
