<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAwardedContractsProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('awarded_contracts_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->nullable();
            $table->integer('added_by')->unsigned();
            $table->integer('awarded_contract_id')->unsigned();
            $table->string('category')->default(0);
            $table->string('cost_items');
            $table->text('products')->nullable();
            $table->string('qty')->nullable();
            $table->string('unit')->nullable();
            $table->string('rate')->nullable();
            $table->string('inc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('awarded_contracts_products');
    }
}
