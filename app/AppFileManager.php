<?php

namespace App;

use App\Observers\FileManagerObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class AppFileManager extends Model
{
    protected $table = 'file_manager';

}
