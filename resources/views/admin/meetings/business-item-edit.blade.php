<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Edit Product</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'editProduct','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="form-body">
            <div class="row">
                <input type="hidden" name="id" id="title" value="{{ $businessItem->id }}" class="form-control">
                <div class="col-md-6 ">
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" name="title" id="title" class="form-control" value="{{ $businessItem->title }}">
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="form-group">
                        <label class="control-label">@lang('modules.tasks.assignTo')</label>
                        <select class="form-control" name="assigned_user[]" id="user_id" multiple>
                            <option value="">@lang('modules.tasks.chooseAssignee')</option>
                            @foreach($employees as $employee)
                                <option value="{{ $employee->id }}"<?php
                                    $ass = explode(',',$businessItem->assigned_user);
                                    if (in_array($employee->id, $ass)){
                                        echo 'selected'; } ?>>{{ ucwords($employee->name) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="form-group">
                        <label>Due Date</label>
                        <input type="text" name="due_date" id="due_date2" class="form-control" autocomplete="off" value="{{ $businessItem->due_date }}">
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="form-group">
                        <label class="control-label">Status</label>
                        <select class="form-control" name="status">
                            <option value="">Select Status</option>
                            <option value="0" @if($businessItem->status=='0') selected @endif>Open</option>
                            <option value="1" @if($businessItem->status=='1') selected @endif>Closed</option>
                            <option value="2" @if($businessItem->status=='2') selected @endif>Pending</option>
                            <option value="3" @if($businessItem->status=='3') selected @endif>Completed</option>
                            <option value="4" @if($businessItem->status=='4') selected @endif>Hold</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="form-group">
                        <label class="control-label">@lang('modules.tasks.priority')</label>
                        <select class="form-control" name="priority">
                            <option value="">Select Priority</option>
                            <option value="0" @if($businessItem->priority=='0') selected @endif>High</option>
                            <option value="1" @if($businessItem->priority=='1') selected @endif>Medium</option>
                            <option value="2" @if($businessItem->priority=='2') selected @endif>Low</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="form-group">
                        <label class="control-label">Category</label>
                        <select class="form-control" name="category_id">
                            <option value="">Select Category</option>
                            @foreach($meetingCat as $mcat)
                                <option value="{{ $mcat->id }}" @if($businessItem->category_id==$mcat->id) selected @endif>{{ ucwords($mcat->title) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">Description</label>
                        <textarea id="description" name="description" class="form-control summernote">{{ $businessItem->description }}</textarea>
                    </div>
                </div>
                <div class="row m-b-20">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                        <div id="file-upload-box">
                            <div class="row" id="file-dropzone">
                                <div class="col-md-12">
                                    <div class="dropzone"
                                         id="file-upload-dropzone">
                                        {{ csrf_field() }}
                                        <div class="fallback">
                                            <input name="file" type="file" multiple/>
                                        </div>
                                        <input name="image_url" id="image_url"type="hidden" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="rifID" id="rifID">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="float: left; padding-right: 20px;">&nbsp;</div>
        <div class="form-actions">
            <button type="button" id="update-product" data-cat-id="{{ $businessItem->id }}" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>



<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/multiselect/css/multi-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>
<script>
    Dropzone.autoDiscover = false;
    //Dropzone class
    myDropzone = new Dropzone("div#file-upload-dropzone", {
        url: "{{ route('admin.meetings.storeBusinessImage') }}",
        headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
        paramName: "file",
        maxFilesize: 10,
        maxFiles: 10,
        acceptedFiles: "image/*,application/pdf",
        autoProcessQueue: false,
        uploadMultiple: true,
        addRemoveLinks:true,
        parallelUploads:10,
        init: function () {
            myDropzone = this;
        }
    });

    myDropzone.on('sending', function(file, xhr, formData) {
        console.log(myDropzone.getAddedFiles().length,'sending');
        var ids = $('#rifID').val();
        formData.append('rfi_id', ids);
        formData.append('rfitype', 'rfi');
    });

    myDropzone.on('completemultiple', function () {
        var msgs = "Business Items Created";
        $.showToastr(msgs, 'success');
        $('.createBusinessItem').click();
    });
    $('.summernote').summernote({
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ["view", ["fullscreen"]]
        ]
    });
    $(".select2").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });
    $(".date-picker").datepicker({
        todayHighlight: true,
        autoclose: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });
    jQuery('#due_date2, #start_date2').datepicker({
        autoclose: true,
        todayHighlight: true,
        weekStart:'{{ $global->week_start }}',
        format: '{{ $global->date_picker_format }}',
    });
    function getCo() {
        var qt = $('#qt').val();
        var rat = $('#rat').val();
        var total = (parseFloat(qt)*parseFloat(rat)).toFixed(2);
        $('#co').val(total);
    }

    $('#update-product').click(function () {

        $.easyAjax({

            url : '{{route('admin.meetings.updateBusinessItem')}}',
            container: '#editProduct',
            type: "POST",
            data: $('#editProduct').serialize(),
            success: function (response) {
                if(response.meetingID){
                    window.location.reload();
                }
            }
        })
    });
    $(function () {
        $('.selectpicker').selectpicker();
    });

</script>