<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use App\Http\Requests\ChatStoreRequest;
use App\MessageSetting;
use App\Notifications\NewChat;
use App\Team;
use App\User;
use App\UserChat;
use Illuminate\Support\Facades\Input;

/**
 * Class MemberChatController
 * @package App\Http\Controllers\Member
 */
class AdminChatController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.messages';
        $this->pageIcon = 'icon-envelope';
        $this->activeMenu = 'hr';
        $this->middleware(function ($request, $next) {
            if(!in_array('messages',$this->user->modules)){
                abort(403);
            }
            return $next($request);
        });

    }

    public function index()
    {
        $user = $this->user;
        $this->userList = User::where('company_id',$user->company_id)->where('id','<>',$user->id)->get();
        $this->teamList = Team::all();

        $userID = Input::get('userID');
        $id     = $userID;
        $name   = '';

        if(count($this->userList) != 0)
        {
            if(($userID == '' || $userID == null)){
                $id   = $this->userList[0]->id;
                $name = $this->userList[0]->name;

            }else{
                $id = $userID;
                $name = User::findOrFail($userID)->name;
            }

            $updateData = ['message_seen' => 'yes'];
            UserChat::messageSeenUpdate($this->user->id, $id, $updateData);
        }

        $this->dpData = $id;
        $this->dpName = $name;

        $this->chatDetails = UserChat::chatDetail($id, $this->user->id);

        if (request()->ajax()) {
            return $this->userChatData($this->chatDetails, 'user');
        }
        return view('admin.user-chat.index', $this->data);
    }
    public function indexold()
    {

        $this->userList = $this->userListLatest();
        $this->teamList = Team::all();

        $userID = Input::get('userID');
        $id     = $userID;
        $name   = '';

        if(count($this->userList) != 0)
        {
            if(($userID == '' || $userID == null)){
                $id   = $this->userList[0]->id;
                $name = $this->userList[0]->name;

            }else{
                $id = $userID;
                $name = User::findOrFail($userID)->name;
            }

            $updateData = ['message_seen' => 'yes'];
            UserChat::messageSeenUpdate($this->user->id, $id, $updateData);
        }

        $this->dpData = $id;
        $this->dpName = $name;
        $this->chatDetails = UserChat::chatDetail($id, $this->user->id);
        if (request()->ajax()) {
            return $this->userChatData($this->chatDetails, 'user');
        }
        return view('admin.user-chat.index', $this->data);
    }

    /**
     * @param $chatDetails
     * @param $type
     * @return string
     */
    public function userChatData($chatDetails)
    {
        $chatMessage = '';

        $this->chatDetails = $chatDetails;
        $chatMessage .= view('admin.user-chat.ajax-chat-list', $this->data)->render();
        $chatMessage .= '<li id="scrollHere"></li>';

        return Reply::successWithData(__('messages.fetchChat'), ['chatData' => $chatMessage]);

    }

    /**
     * @return mixed
     */
    public function postChatMessage(ChatStoreRequest $request)
    {
        $this->user = auth()->user();

        $message = $request->get('message');

        if($request->user_type == 'client'){
            $userID = $request->get('client_id');
        }
        else{
            $userID = $request->get('user_id');
        }

        $allocatedModel = new UserChat();
        if ($request->hasFile('image')) {
            File::delete('user-uploads/chat_image/' . $allocatedModel->image);

            $allocatedModel->image = $request->image->hashName();
            $request->image->store('user-uploads/chat_image');

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make('user-uploads/chat_image/' . $allocatedModel->image);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $allocatedModel->message = $img;
        }elseif ($request->hasFile('doc')){
            File::delete('user-uploads/chat_doc/' . $allocatedModel->image);

            $allocatedModel->image = $request->image->hashName();
            $request->image->store('user-uploads/chat_doc');

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $doc = Image::make('user-uploads/chat_doc/' . $allocatedModel->image);

            $allocatedModel->message = $doc;
        }
        else{
            $allocatedModel->message         = $message;
        }
        $allocatedModel->user_one        = $this->user->id;
        $allocatedModel->user_id         = $userID;
        $allocatedModel->from            = $this->user->id;
        $allocatedModel->to              = $userID;
        $allocatedModel->save();

        // Notify User
        $notifyUser = User::withoutGlobalScope('active')->findOrFail($allocatedModel->user_id);
        $notifyUser->notify(new NewChat($allocatedModel));

        $this->userLists = $this->userListLatest();

        $this->userID = $userID;

        $users = view('admin.user-chat.ajax-user-list', $this->data)->render();

        $lastLiID = '';
        return Reply::successWithData(__('messages.fetchChat'), ['chatData' => $this->index(), 'dataUserID' => $this->user->id, 'userList' => $users, 'liID' => $lastLiID]);
    }

    /**
     * @return mixed
     */
    public function userListLatest($term = null)
    {
        $result = User::userListLatest($this->user->id, $term );

        return $result;
    }



    public function getUserSearch()
    {
        $term = Input::get('term');
        $this->userLists = $this->userListLatest($term);

        $users = '';

        $users = view('admin.user-chat.ajax-user-list', $this->data)->render();

        return Reply::dataOnly(['userList' => $users]);
    }


    public function create() {
        $this->members = User::allEmployees($this->user->id);
        $this->clients = User::allClients();
        $this->messageSetting = MessageSetting::first();
        return view('admin.user-chat.create', $this->data);
    }
    public function userChatDemo(){
        $this->members = User::allEmployees($this->user->id);
        $this->clients = User::allClients();
        $this->messageSetting = MessageSetting::first();
        return view('admin.user-chat.demo', $this->data);
    }
}
