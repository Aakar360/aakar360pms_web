<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Assign Employee</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'assignWorkRequestf','class'=>'ajax-form','method'=>'POST']) !!}
        @csrf
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label for="inputName">Assign Employee</label>
                       <select class="form-control "  name="assigned_emp">
                            <option data-tokens="ketchup mustard">Select User</option>
                            @foreach($users as $user)
                                <option data-tokens="ketchup mustard" value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach

                        </select>
                    </div>
                </div>
            </div>

        </div>
        <div class="form-actions">
            <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<script>
    $('#save-category').click(function () {
        $.easyAjax({
            url: '{{route('admin.work-request.assign-update', [$workrequest->id])}}',
            container: '#assignWorkRequestf',
            type: "POST",
            data: $('#assignWorkRequestf').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });

</script>