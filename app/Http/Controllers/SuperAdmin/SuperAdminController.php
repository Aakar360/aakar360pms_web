<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Helper\Reply;
use App\Http\Requests\SuperAdmin\SuperAdmin\CreateSuperAdmin;
use App\Http\Requests\SuperAdmin\SuperAdmin\UpdateSuperAdmin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Yajra\DataTables\Facades\DataTables;

class SuperAdminController extends SuperAdminBaseController
{

    public function __construct() {
        parent::__construct();
        $this->pageTitle = 'app.superAdmin';
        $this->pageIcon = 'icon-people';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->superAdmin = User::allSuperAdmin();
        $this->totalSuperAdmin = count($this->superAdmin);

        return view('super-admin.super-admin.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($leadID = null)
    {
        $superadmin = new User();
        return view('super-admin.super-admin.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSuperAdmin $request)
    {
        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->mobile = $request->input('mobile');
        $user->login = 'enable';
        $user->status = 'active';
        $user->super_admin = '1';

        if ($request->hasFile('image')) {
            File::delete('user-uploads/avatar/'.$user->image);

            $user->image = $request->image->hashName();
            $request->image->store('user-uploads/avatar');

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make('user-uploads/avatar/'.$user->image);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
        }

        $user->save();

        return Reply::redirect(route('super-admin.super-admin.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->userDetail = User::withoutGlobalScope('active')->findOrFail($id);

        return view('super-admin.super-admin.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSuperAdmin $request, $id)
    {
        $user = User::withoutGlobalScope('active')->findOrFail($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        if ($request->password != '') {
            $user->password = Hash::make($request->input('password'));
        }
        $user->mobile = $request->input('mobile');
        if ($this->user->id != $user->id)
        {
            $user->status = $request->input('status');
        }

        if ($request->hasFile('image')) {
            File::delete('user-uploads/avatar/'.$user->image);

            $user->image = $request->image->hashName();
            $request->image->store('user-uploads/avatar');

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $img = Image::make('user-uploads/avatar/'.$user->image);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save();
        }
        $user->save();

        return Reply::redirect(route('super-admin.super-admin.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return Reply::success(__('messages.userDeleted'));
    }

    public function data(Request $request)
    {
        $users = User::allSuperAdmin();
        return DataTables::of($users)
            ->addColumn('action', function($row){
                $action = '<a href="'.route('super-admin.super-admin.edit', [$row->id]).'" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a> ';
                if($this->user->id != $row->id){
                    $action .=   ' <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="'.$row->id.'" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
                }
                return $action;

            })
            ->editColumn(
                'name',
                function ($row) {
                    return ucfirst($row->name);
                }
            )
            ->editColumn(
                'status',
                function ($row) {
                    if($row->status == 'active'){
                        return '<label class="label label-success">'.__('app.active').'</label>';
                    }
                    else{
                        return '<label class="label label-danger">'.__('app.deactive').'</label>';
                    }
                }
            )
            ->rawColumns(['name', 'action', 'status'])
            ->make(true);
    }

    public function appNotification(){
        return view('super-admin.send-app-notification', $this->data);
    }
    public function sendNotification(Request $request){
        if (!defined('API_ACCESS_KEY')) define('API_ACCESS_KEY','AAAAZs3kkrA:APA91bH1TsyEUhjgSzNs8-CBs6LCnoow0sZSxi6j7rv3Lvn6M7BKnh21sQ1M1Igp1oKjsxvAwHE3Sfk2AWsmS0Na3OtHOVnbKlCVgTxn64FnJJR4TMCT33pHpEXVL0J46qfXyKmkG24d');
        $title = $request->title;
        $text = $request->text;
        $surl = '';
        $storage = storage();

        if ($request->hasFile('logo')) {
            $fileData = $request->file('logo');
            switch($storage) {
                case 'local':

                    $destinationPath = 'uploads/app-notification/';
                    if (!file_exists('public/'.$destinationPath)) {
                        mkdir('public/'.$destinationPath, 0777, true);
                    }
                    $fileData->storeAs($destinationPath, $fileData->hashName());
                    $surl = uploads_url().'app-notification/'.$fileData->hashName();
                    break;
                case 's3':
                    $st = Storage::disk('s3')->putFileAs('app-notification/', $request->logo, $request->logo->hashName(), 'public');
                    $surl = awsurl().'app-notification/'.$request->logo->hashName();
                    break;
                case 'google':
                    $dir = '/';
                    $recursive = false;
                    $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                    $dir = $contents->where('type', '=', 'dir')
                        ->where('filename', '=', 'punch-files')
                        ->first();
                    if(!$dir) {
                        Storage::cloud()->makeDirectory('punch-files');
                    }

                    $directory = $dir['path'];
                    $recursive = false;
                    $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                    $directory = $contents->where('type', '=', 'dir')
                        ->where('filename', '=', '')
                        ->first();

                    if ( ! $directory) {
                        Storage::cloud()->makeDirectory($dir['path']);
                        $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                        $directory = $contents->where('type', '=', 'dir')
                            ->where('filename', '=', '')
                            ->first();
                    }
                    Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());

                    $surl = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());

                    break;
                case 'dropbox':
                    Storage::disk('dropbox')->putFileAs('app-notification/', $fileData, $fileData->hashName());
                    $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                    $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                        [\GuzzleHttp\RequestOptions::JSON => ["path" => '/app-notification/'.$fileData->hashName()]]
                    );
                    $dropboxResult = $res->getBody();
                    $dropboxResult = json_decode($dropboxResult, true);
                    $surl = $dropboxResult['url'];
                    break;
            }
        }
        $users = User::all();
        foreach ($users as $user) {
            if ($user->fcm) {
                $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                $note = [
                    'title' => $title,
                    'body' => $text,
                    'image' => $surl
                ];
                $fcmNotification = [
                    'to' => $user->fcm, //single token
                    'notification' => $note
                ];
                $headers = [
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $fcmUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                curl_exec($ch);
                curl_close($ch);
            }
        }
        return Reply::success('Notification Sent successfully.');
    }
}
