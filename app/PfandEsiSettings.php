<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PfandEsiSettings extends Model
{
    protected $fillable = ['effective_date','min_pf_calc_amount','emp_pf_contribution',	'fixed_amount','custum_formula_select',	'custum_formula_percent','employer_pf_contibution','enable_min_ceilig','lop_dependent',	'emploer_ctc_contribution',	'allow_override','admin_ctc_charges','employer_contribution','include_empl_ctc','allow_esi_override'];
}
