<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">@lang('modules.attendance.assignedrules')</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">
        {!! Form::open(['id'=>'assignLeaveRules','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="row">

            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >Leave Rules</label>

                    <select id="rules_id" name="rules_id" class="form-control">
                        @foreach($rules as $rule)
                            <option value="{{$rule->id}}">{{$rule->leave_rule_name}}</option>
                        @endforeach
                    </select>

                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >Effective Date</label>
                    <input type="date" class="form-control" name="effective_date" id="effective_date" value="{{ Carbon\Carbon::today()->format($global->date_format) }}">
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" id="assignleave-form" class="btn btn-success"><i class="fa fa-check"></i>
                    @lang('app.save')
                </button>
                <button type="reset" class="btn btn-default">@lang('app.cancel')</button>
            </div>
        </div>
        {!! Form::hidden('ids', $ids) !!}
        {!! Form::close() !!}

    </div>
</div>
<script>
    $('#assignleave-form').click(function () {
        $.easyAjax({
            url: '{{route('admin.leave.postAssignLeaveRules')}}',
            container: '#assignLeaveRules',
            type: "POST",
            data: $('#assignLeaveRules').serialize(),
            success: function (response) {
                $('#leaveRuleAssignModal').modal('hide');
                window.location.reload();
            }
        });

        return false;
    });
</script>