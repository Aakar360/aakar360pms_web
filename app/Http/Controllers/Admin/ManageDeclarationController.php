<?php

namespace App\Http\Controllers\Admin;

use App\AdhocAction;
use App\AdhocDeduction;
use App\AdhocEarning;
use App\Admin\EmployeeTaxDetail;
use App\Admin\PayAction;
use App\Attendance;
use App\Designation;
use App\Employee;
use App\EmployeeLopDetail;
use App\EmployeeTeam;
use App\Holiday;
use App\Leave;
use App\LeaveRulesSettings;
use App\LeavesRules;
use App\PayrollRegister;
use App\PayrollVerify;
use App\PfandEsiSettings;
use App\SalaryComponentVariable;
use App\Team;
use App\User;
use Illuminate\Http\Request;
use App\Helper\Reply;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Controller;

class ManageDeclarationController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.salarystucture';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'hr';
        $this->middleware(function ($request, $next) {
            if(!in_array('payroll',$this->user->modules)){
                abort(403);
            }
            return $next($request);
        });

    }

    public function index()
    {
       //
    }
    public function view()
    {
        return view('admin.declaration.view',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function taxData(){
        // Getting View data
        $user = User::join('employee_details','employee_details.user_id','=','users.id')->get();
        return DataTables::of($user)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {

                return $row->name;
            })
            ->editColumn('location', function ($row)  {

                return $row->address;
            })
            ->editColumn('taxscheme', function ($row)  {

                return "";
            })
            ->addColumn('action', function ($row) {
                return '<a href="javascript:;" class="btn btn-danger"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Reject">Reject</a>
                <a href="javascript:;" class="btn btn-success "
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Approve">Approve</a>';
            })
            ->rawColumns(['name', 'location','taxscheme','action'])

            ->make(true);
    }
    public function hraData(){
        // Getting View data
        $user = User::join('employee_details','employee_details.user_id','=','users.id')->get();
        return DataTables::of($user)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {

                return $row->name;
            })
            ->editColumn('location', function ($row)  {

                return $row->address;
            })
            ->editColumn('daterang', function ($row)  {

                return "";
            })->editColumn('anualamount', function ($row)  {

                return $row->yearly_rate;
            })->editColumn('landlardpan', function ($row)  {

                return "";
            })->editColumn('documents', function ($row)  {

                return "";
            })->editColumn('comments', function ($row)  {

                return "";
            })
            ->addColumn('action', function ($row) {
                return '<a href="javascript:;" class="btn btn-danger"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Reject">Reject</a>
                <a href="javascript:;" class="btn btn-success "
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Approve">Approve</a>';
            })
            ->rawColumns(['name', 'location','taxscheme','action'])

            ->make(true);
    }
    public function declareData(){
        // Getting View data
        $user = User::join('employee_details','employee_details.user_id','=','users.id')->get();
        return DataTables::of($user)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {

                return $row->name;
            })
            ->editColumn('location', function ($row)  {

                return $row->address;
            })
            ->editColumn('declareamount', function ($row)  {

                return "";
            })->editColumn('documents', function ($row)  {

                return "";
            })->editColumn('comments', function ($row)  {

                return "";
            })
            ->addColumn('action', function ($row) {
                return '<a href="javascript:;" class="btn btn-danger"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Reject">Reject</a>
                <a href="javascript:;" class="btn btn-success "
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Approve">Approve</a>';
            })
            ->rawColumns(['name', 'location','taxscheme','action'])

            ->make(true);
    }
    public function incomelossData(){
        // Getting View data
        $user = User::join('employee_details','employee_details.user_id','=','users.id')->get();
        return DataTables::of($user)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {

                return $row->name;
            })
            ->editColumn('location', function ($row)  {

                return $row->address;
            })
            ->editColumn('type', function ($row)  {

                return "";
            })->editColumn('incomeloss', function ($row)  {

                return "";
            })->editColumn('documents', function ($row)  {

                return "";
            })->editColumn('comments', function ($row)  {

                return "";
            })
            ->addColumn('action', function ($row) {
                return '<a href="javascript:;" class="btn btn-danger"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Reject">Reject</a>
                <a href="javascript:;" class="btn btn-success "
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Approve">Approve</a>';
            })
            ->rawColumns(['name', 'location','taxscheme','action'])

            ->make(true);
    }
    public function ltaData(){
        // Getting View data
        $user = User::join('employee_details','employee_details.user_id','=','users.id')->get();
        return DataTables::of($user)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {

                return $row->name;
            })
            ->editColumn('location', function ($row)  {

                return $row->address;
            })->editColumn('type', function ($row)  {

                return $row->address;
            })->editColumn('component', function ($row)  {

                return $row->address;
            })
            ->editColumn('declareamount', function ($row)  {

                return "";
            })->editColumn('documents', function ($row)  {

                return "";
            })->editColumn('comments', function ($row)  {

                return "";
            })
            ->addColumn('action', function ($row) {
                return '<a href="javascript:;" class="btn btn-danger"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Reject">Reject</a>
                <a href="javascript:;" class="btn btn-success "
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Approve">Approve</a>';
            })
            ->rawColumns(['name', 'location','taxscheme','action'])

            ->make(true);
    }
    public function incomePreviousData(){
        // Getting View data
        $user = User::join('employee_details','employee_details.user_id','=','users.id')->get();
        return DataTables::of($user)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {

                return $row->name;
            })
            ->editColumn('location', function ($row)  {

                return $row->address;
            })
            ->editColumn('income', function ($row)  {

                return "";
            }) ->editColumn('incometax', function ($row)  {

                return "";
            }) ->editColumn('pf', function ($row)  {

                return "";
            }) ->editColumn('pt', function ($row)  {

                return "";
            })->editColumn('documents', function ($row)  {

                return "";
            })->editColumn('comments', function ($row)  {

                return "";
            })
            ->addColumn('action', function ($row) {
                return '<a href="javascript:;" class="btn btn-danger"
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Reject">Reject</a>
                <a href="javascript:;" class="btn btn-success "
                      data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Approve">Approve</a>';
            })
            ->rawColumns(['name', 'location','taxscheme','action'])

            ->make(true);
    }

    public function auditHistory(){
        return view('admin.auditHistory.index',$this->data);
    }


    public function getAttendanceData(Request $request)
    {

        $ruleassigneduser = [];
        $workDayrules = [];
        // Getting View data
        $leaveRules = LeavesRules::join('leave_assigneds', 'leave_assigneds.leave_rule_id', '=', 'leaves_rules.id')
            ->join('employee', 'employee.id', '=', 'leave_assigneds.user_id')
            ->select('employee.id', 'employee.name', 'leaves_rules.weekend_bet_leave', 'leaves_rules.hollyday_bet_leaves'
                , 'leaves_rules.allow_probation', 'leaves_rules.leave_carrey_forward', 'leaves_rules.leave_backdated_allow',
                'leaves_rules.backdated_allow_upto', 'leaves_rules.backdated_allow_till', 'leaves_rules.leave_allowed_in_year',
                'leaves_rules.leave_allowed_in_month', 'leaves_rules.continue_leave_allow', 'leaves_rules.max_leave_to_carry_forword', 'leaves_rules.all_remaining_leaves')->get();

        $workDay = LeaveRulesSettings::join('assign_work_rules', 'assign_work_rules.workrules_id', '=', 'leave_rules_settings.id')
            ->join('employee', 'employee.id', '=', 'assign_work_rules.user_id')->select('employee.id', 'employee.name', 'leave_rules_settings.type')->get();

        if (!empty($leaveRules) || $leaveRules != 0) {
            foreach ($leaveRules as $rule) {
                $ruleassigneduser[] = $rule->id;
            }

        }
        if (!empty($workDay) || $workDay != 0) {
            foreach ($workDay as $workrule) {
                $workDayrules[] = $workrule->id;
            }
        }

        $employees = Employee::with(
            ['attendance' => function ($query) use ($request) {
                $query->whereRaw('MONTH(attendances.clock_in_time) = ?', [$request->month])
                    ->whereRaw('YEAR(attendances.clock_in_time) = ?', [$request->year]);
            }]
        )->join('rules_users', 'rules_users.user_id', '=', 'employee.id')
            ->join('attendance_rules', 'attendance_rules.id', '=', 'rules_users.rules_id')
            ->select('employee.id', 'employee.name', 'attendance_rules.name as rulename',
                'attendance_rules.description', 'attendance_rules.shift_in_time',
                'attendance_rules.shift_out_time', 'attendance_rules.anomaly_grace_in',
                'attendance_rules.anomaly_grace_out', 'attendance_rules.work_full_time',
                'attendance_rules.work_half_time', 'attendance_rules.auto_clock_out','rules_users.effective_date')
            ->groupBy('employee.id');
        $employees = $employees->get();
        $this->holidays = Holiday::whereRaw('MONTH(holidays.date) = ?', [$request->month])->whereRaw('YEAR(holidays.date) = ?', [$request->year])->get();
        $final = [];
        $this->daysInMonth = cal_days_in_month(CAL_GREGORIAN, $request->month, $request->year);
        $now = Carbon::now()->timezone($this->global->timezone);
        $requestedDate = Carbon::parse(Carbon::parse('01-' . $request->month . '-' . $request->year))->endOfMonth();
        foreach ($employees as $employee) {
            $dataTillToday = array_fill(1, $now->copy()->format('d'), 'Absent');
            $dataFromTomorrow = [];

            if (($now->copy()->addDay()->format('d') != $this->daysInMonth) && !$requestedDate->isPast()) {
                $dataFromTomorrow = array_fill($now->copy()->addDay()->format('d'), ($this->daysInMonth - $now->copy()->format('d')), '-');
            } else {
                $dataFromTomorrow = array_fill($now->copy()->addDay()->format('d'), ($this->daysInMonth - $now->copy()->format('d')), 'Absent');
            }

            $final[$employee->id . '#' . $employee->name] = array_replace($dataTillToday, $dataFromTomorrow);

            foreach ($employee->attendance as $attendance) {
                $shift_in = Carbon::parse($employee->shift_in_time)->timezone($this->global->timezone)->format('H:i:s');
                $shift_out = Carbon::parse($employee->shift_out_time)->timezone($this->global->timezone)->format('H:i:s');
                $clockintime = Carbon::parse($attendance->clock_in_time)->timezone($this->global->timezone)->format('H:i:s');
                $clockouttime = Carbon::parse($attendance->clock_out_time)->timezone($this->global->timezone)->format('H:i:s');
                $start_datetime = new \DateTime(date('Y-m-d').' '.$clockintime);
                $end_datetime = new \DateTime(date('Y-m-d').' '.$clockouttime);
                $totaltime = $start_datetime->diff($end_datetime)->format('%h');
                //Effective date to Attendance Rule Apply
                if(!empty($employee->effective_date)) {
                    $effective_date = Carbon::parse($employee->effective_date)->timezone($this->global->timezone);
                    if ($effective_date->lte($now)) {
                        if ($totaltime == $employee->work_full_time){
                            $final[$employee->id . '#' . $employee->name][Carbon::parse($attendance->clock_in_time)->timezone($this->global->timezone)->day] = '<a href="javascript:;" class="view-attendance" data-attendance-id="' . $attendance->id . '"><i class="fa fa-check text-success"></i></a>';
                        }
                        if ($totaltime >= $employee->work_half_time && $totaltime < $employee->work_full_time){
                            $final[$employee->id . '#' . $employee->name][Carbon::parse($attendance->clock_in_time)->timezone($this->global->timezone)->day] = 'Half Day';
                        }
                        if ($totaltime < $employee->work_half_time){
                            $final[$employee->id . '#' . $employee->name][Carbon::parse($attendance->clock_in_time)->timezone($this->global->timezone)->day] = 'Anomaly Absent';
                        }
                    } else {
                        $final[$employee->id . '#' . $employee->name][Carbon::parse($attendance->clock_in_time)->timezone($this->global->timezone)->day] = '<a href="javascript:;" class="view-attendance" data-attendance-id="' . $attendance->id . '"><i class="fa fa-check text-success"></i></a>';
                    }
                }
                //End Effective date to Attendance Rule Apply
            }
            foreach ($this->holidays as $holiday) {
                $final[$employee->id . '#' . $employee->name][$holiday->date->day] = 'Holiday';
            }
        }

        $this->employeeAttendence  = $final;

        //Anamoly Calculation//
        //End Anamoly Calculation//
        return DataTables::of($employees)
            ->addIndexColumn()
            ->editColumn('name', function ($row) {
                return $row->name;
            })->editColumn('absentdates', function ($row) use($final,$request){
                $id = $row->id;
                $name = $row->name;
                return Carbon::createFromDate($request->month)->format('F').view('admin.attendance.absent_days', compact('id','name','final'))->render();

            })->editColumn('absentdays', function ($row) use($final){
                $id = $row->id;
                $name = $row->name;
                $view =  view('admin.attendance.count_absent_days', compact('id','name','final'))->render();
                return $view;
            })->editColumn('autstandingannamoly', function ($row) use($final){
                $id = $row->id;
                $name = $row->name;
                $view =  view('admin.attendance.count_anomaly', compact('id','name','final'))->render();
                return $view;
            })->editColumn('lopdays', function ($row) use($final){
                $id = $row->id;
                $name = $row->name;
                $view =  view('admin.attendance.count_lop_days', compact('id','name','final'))->render();
                $lop = EmployeeLopDetail::where('user_id',$row->id)->select('lop')->first();
                if(!empty($lop)){
                    $lopval = $lop->lop;
                    return $lopval;
                }else{
                    return $view;
                }

            })
            ->addColumn('action', function ($row) {
                return '<a   class="btn btn-info btn-circle"  onclick="addLop('.$row->id.')"
                      data-toggle="tooltip" data-original-title="addLop"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
            })
            ->rawColumns(['name', 'absentdates', 'absentdays', 'autstandingannamoly', 'lopdays', 'action'])
            ->make(true);

    }
    public function getLeaveData(Request $request){

        // Getting View data
        $month  = $request->month;
        $year  = $request->year;
        $monthDt = '';
        $yearDt = '';
        $ruleassigneduser = [];
        $workDayrules = [];

        $leaveRules = LeavesRules::join('leave_assigneds','leave_assigneds.leave_rule_id','=','leaves_rules.id')
            ->join('employee','employee.id','=','leave_assigneds.user_id')
            ->select('employee.id','employee.name','leaves_rules.weekend_bet_leave','leaves_rules.hollyday_bet_leaves'
                ,'leaves_rules.allow_probation','leaves_rules.leave_carrey_forward','leaves_rules.leave_backdated_allow',
                'leaves_rules.backdated_allow_upto','leaves_rules.backdated_allow_till','leaves_rules.leave_allowed_in_year',
                'leaves_rules.leave_allowed_in_month','leaves_rules.continue_leave_allow','leaves_rules.max_leave_to_carry_forword','leaves_rules.all_remaining_leaves')->get();

        $workDay = LeaveRulesSettings::join('assign_work_rules','assign_work_rules.workrules_id','=','leave_rules_settings.id')
            ->join('employee','employee.id','=','assign_work_rules.user_id')->select('employee.id' ,'employee.name','leave_rules_settings.type')->get();

        if (!is_null($month)) {
            $monthDt = 'MONTH(leaves.`leave_date`) = ' . '"' . $month . '"';
        }

        if (!is_null($year)) {
            $yearDt = 'YEAR(leaves.`leave_date`) = ' . '"' . $year . '"';
        }

        if (!empty($leaveRules) || $leaveRules != 0) {
            foreach ($leaveRules as $rule) {
                $ruleassigneduser[]  = $rule->id;
            }
        }

        if ( !empty($workDay) || $workDay != 0) {
            foreach ($workDay as $workrule) {
                $workDayrules[] =$workrule->id;
            }
        }

        $leavesList = Leave::select('leaves.id','leaves.emp_id','employee.id as uid','employee.name', 'leaves.leave_date', 'leaves.status', 'leave_types.type_name','leave_types.no_of_leaves','leave_types.color')
            ->where('leaves.status', '<>', 'rejected')
            ->whereRaw($monthDt)
            ->whereRaw($yearDt)
            ->whereIn('leaves.emp_id',$ruleassigneduser)
            ->join('employee', 'employee.id', '=', 'leaves.emp_id')
            ->join('leave_types', 'leave_types.id', '=', 'leaves.leave_type_id')
            ->groupBy('employee.id');

        $leaves = $leavesList->get();

        return DataTables::of($leaves)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {
                return ucwords($row->name);
            })
            ->editColumn('date', function ($row)  {
                return date('F').$row->leave_date->format('Y-m-d');
            })->editColumn('totaldays', function ($row)  {
                return $row->no_of_leaves;
            })->addColumn('leave_type', function ($row)  {
                return '<div class="label-' . $row->color . ' label">' . $row->type_name . '</div>';
            })->addColumn('status', function ($row)  {
                $label = $row->status == 'pending' ? 'warning' : 'success';
                return '<div class="label label-' . $label . '">' . $row->status . '</div>';
            })->editColumn('manager', function ($row)  {
                $manager = EmployeeTeam::where('employee_id',$row->uid)->where('type','primary')->first();
                if(!empty($manager)){
                    $mng = Employee::where('id',$manager->team_id)->first();
                    return $mng->name.'<a href="javascript:;"
                            data-leave-id=' . $row->id . '
                            class="btn btn-info  nofitytomanager"
                            data-toggle="tooltip"
                            data-original-title="' . __('app.notify') . '">
                                Notify
                            </a>';
                }
                return 'NA';

            })
            ->addColumn('action', function ($row) {
                if ($row->status == 'pending') {
                    return '<a href="javascript:;"
                            data-leave-id=' . $row->id . ' 
                            data-leave-action="approved" 
                            class="btn btn-success  leave-action"
                            data-toggle="tooltip"
                            data-original-title="' . __('app.approved') . '">
                                Approved
                            </a>
                            <a href="javascript:;" 
                            data-leave-id=' . $row->id . '
                            data-leave-action="rejected"
                            class="btn btn-danger  leave-action-reject"
                            data-toggle="tooltip"
                            data-original-title="' . __('app.reject') . '">
                                Reject
                            </a>
                            
                            <a href="javascript:;"
                            data-leave-id=' . $row->id . '
                            class="btn btn-info  show-leave"
                            data-toggle="tooltip"
                            data-original-title="' . __('app.details') . '">
                                detail
                            </a>';
                }

                return '<a href="javascript:;"
                        data-leave-id=' . $row->id . '
                        class="btn btn-info btn-circle show-leave"
                        data-toggle="tooltip"
                        data-original-title="' . __('app.details') . '">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </a>';
            })
            ->rawColumns(['name','date','status','manager','leave_type','absentdates','absentdays','autstandingannamoly','lopdays','action'])

            ->make(true);
    }
    public function getSalaryRevision(){
        // Getting View data
        $attencedata = Employee::join('employee_ctc_details','employee_ctc_details.user_id','=','employee.id')->get();

        return DataTables::of($attencedata)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {

                return $row->name;
            })
            ->editColumn('oldctc', function ($row)  {

                return $row->oldctc?$row->oldctc:0;
            })->editColumn('newctc', function ($row)  {

                return $row->newctc?$row->newctc:'';
            })->editColumn('changes', function ($row)  {

                return $row->changes?$row->changes:'0%';
            })->editColumn('effectivedate', function ($row)  {

                return $row->effectivedate?$row->effectivedate:'';
            })->editColumn('comment', function ($row)  {

                return $row->comment?$row->comment:'';
            })

            ->rawColumns(['name', 'oldctc','newctc','changes','effectivedate','comment'])

            ->make(true);
    }
    public function getVariableAdhoc(){
        // Getting View data
        $variabledata = SalaryComponentVariable::all();

        $employee = Employee::where('company_id',$this->companyid)->where('user_type','employee')->get();

        return DataTables::of($variabledata)
            ->addIndexColumn()
            ->editColumn('name', function ($row) use ($employee)  {
                foreach ($employee as $user) {
                    return $user->name;
                }
            })
            ->editColumn('variabletype', function ($row)  {

                return $row->component_name;
            })->editColumn('amount', function ($row)  {

                return $row->anual_amount;
            })->editColumn('payaction', function ($row)  {

                return $row->Payout_frequency;
            })->editColumn('comment', function ($row)  {

                return "";
            })

            ->addColumn('action', function ($row) {
                return '<a   class="btn btn-info btn-circle" 
                      data-toggle="tooltip" data-original-title="assignRules"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
            })
            ->rawColumns(['name', 'absentdates','absentdays','autstandingannamoly','lopdays','action'])

            ->make(true);
    }

    public function geSalaryOnHold(){
        // Getting View data
        $attencedata = Employee::join('pay_actions','pay_actions.user_id','=','employee.id')->get();

        return DataTables::of($attencedata)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {

                return $row->name;
            })
            ->editColumn('payaction', function ($row)  {

                return $row->payaction;
            })->editColumn('startperiod', function ($row)  {

                return $row->startperiod?$row->startperiod:'';
            })->editColumn('comment', function ($row)  {

                return $row->comment;
            })

            ->addColumn('action', function ($row) {
                return '<a   class="btn btn-info btn-circle" onclick="editSalaryOnHold('.$row->id.')"
                          data-toggle="tooltip" data-original-title="assignRules"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
            })
            ->rawColumns(['name', 'absentdates','absentdays','autstandingannamoly','lopdays','action'])

            ->make(true);
    }

    public function editSalaryOnHold(Request $request){
        $this->employees = User::all();
        $this->payaction = PayAction::find($request->id);
        return view('admin.declaration.edit_salary_on_hold',$this->data)->render();
    }


    public function updateSalaryOnHold(Request $request){

        $payaction = PayAction::find($request->id);
        $payaction->user_id = $request->user_id;
        $payaction->payaction = $request->pay_action;
        $payaction->comment = $request->comment;
        $payaction->save();
        return Reply::success(__('Salary On Hold Updated Successfully!!'));
    }

    public function geTaxOverride(){
        // Getting View data
        $attencedata = Employee::join('employee_tax_details','employee_tax_details.user_id','=','employee.id')->get();

        return DataTables::of($attencedata)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {

                return $row->name;
            })
            ->editColumn('incometax', function ($row)  {

                return $row->incometax;
            })->editColumn('incometax-ytd', function ($row)  {

                return $row->incometax_ytd?$row->incometax_ytd:0;
            })->editColumn('comment', function ($row)  {

                return $row->comment;
            })

            ->addColumn('action', function ($row) {
                return '<a   class="btn btn-info btn-circle" onclick="editTaxOverride('.$row->id.')"
                          data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
            })
            ->rawColumns(['name', 'absentdates','absentdays','autstandingannamoly','lopdays','action'])

            ->make(true);
    }

    public function editTaxOverride(Request $request){
        $this->employees = Employee::where('company_id',$this->companyid)->where('user_type','employee')->get();
        $this->taxdetail = EmployeeTaxDetail::find($request->id);
        return view('admin.declaration.edit_tax_detail',$this->data)->render();
    }

    public function updateTaxDetail(Request $request){
        $taxdetail = EmployeeTaxDetail::find($request->id);
        $taxdetail->user_id = $request->user_id;
        $taxdetail->incometax = $request->income_tax;
        $taxdetail->comment = $request->comment;
        $taxdetail->save();
        return Reply::success(__('Tax Detail Updated Successfully!!'));
    }

    public function getAdhocData(){
        // Getting View data
        $attencedata = Employee::join('adhoc_actions','adhoc_actions.user_id','=','employee.id')->get();

        return DataTables::of($attencedata)
            ->addIndexColumn()
            ->editColumn('name', function ($row)  {

                return $row->name;
            })
            ->editColumn('compotype', function ($row)  {

                return $row->compo_type;
            })->editColumn('componame', function ($row)  {
                $ername = AdhocEarning::where('id',$row->compo_name)->first();
                $didname = AdhocDeduction::where('id',$row->compo_name)->first();
                return $row->compo_name;
            })->editColumn('amount', function ($row)  {

                return $row->amount;
            })

            ->addColumn('action', function ($row) {
                return '<a   class="btn btn-info btn-circle" onclick="editAdhoc('.$row->id.')"
                          data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                          <a   class="btn btn-danger btn-circle" 
                          data-toggle="tooltip" data-user-id="' . $row->id . '" data-original-title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>';
            })
            ->rawColumns(['action'])

            ->make(true);
    }

    public function getPreview(Request $request){
        $now = Carbon::now()->timezone($this->global->timezone);
        $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $request->month, $request->year);
        //Work Day Calculation //
        $ruleassigneduser = [];
        $workDayrules = [];
        $leaveRules = LeavesRules::join('leave_assigneds', 'leave_assigneds.leave_rule_id', '=', 'leaves_rules.id')
            ->join('employee', 'employee.id', '=', 'leave_assigneds.user_id')
            ->select('employee.id', 'employee.name', 'leaves_rules.weekend_bet_leave', 'leaves_rules.hollyday_bet_leaves'
                , 'leaves_rules.allow_probation', 'leaves_rules.leave_carrey_forward', 'leaves_rules.leave_backdated_allow',
                'leaves_rules.backdated_allow_upto', 'leaves_rules.backdated_allow_till', 'leaves_rules.leave_allowed_in_year',
                'leaves_rules.leave_allowed_in_month', 'leaves_rules.continue_leave_allow', 'leaves_rules.max_leave_to_carry_forword', 'leaves_rules.all_remaining_leaves')->get();

        $workDay = LeaveRulesSettings::join('assign_work_rules', 'assign_work_rules.workrules_id', '=', 'leave_rules_settings.id')
            ->join('employee', 'employee.id', '=', 'assign_work_rules.user_id')->select('employee.id', 'employee.name', 'leave_rules_settings.type')->get();

        if (!empty($leaveRules) || $leaveRules != 0) {
            foreach ($leaveRules as $rule) {
                $ruleassigneduser[] = $rule->id;
            }
        }

        if (!empty($workDay) || $workDay != 0) {
            foreach ($workDay as $workrule) {
                $workDayrules[] = $workrule->id;
            }
        }
        $employees = Employee::with(
            ['attendance' => function ($query) use ($request) {
                $query->whereRaw('MONTH(attendances.clock_in_time) = ?', [$request->month])
                    ->whereRaw('YEAR(attendances.clock_in_time) = ?', [$request->year]);
            }]
        )->join('rules_users', 'rules_users.user_id', '=', 'employee.id')
            ->join('attendance_rules', 'attendance_rules.id', '=', 'rules_users.rules_id')
            ->select('employee.id', 'employee.name', 'attendance_rules.name as rulename',
                'attendance_rules.description', 'attendance_rules.shift_in_time',
                'attendance_rules.shift_out_time', 'attendance_rules.anomaly_grace_in',
                'attendance_rules.anomaly_grace_out', 'attendance_rules.work_full_time',
                'attendance_rules.work_half_time', 'attendance_rules.auto_clock_out')
            ->groupBy('employee.id');
        $employees = $employees->get();
        $this->holidays = Holiday::whereRaw('MONTH(holidays.date) = ?', [$request->month])->whereRaw('YEAR(holidays.date) = ?', [$request->year])->get();

        $final = [];

        $this->daysInMonth = $daysInMonth;
        $now = Carbon::now()->timezone($this->global->timezone);
        $requestedDate = Carbon::parse(Carbon::parse('01-' . $request->month . '-' . $request->year))->endOfMonth();

        foreach ($employees as $employee) {
            $dataTillToday = array_fill(1, $now->copy()->format('d'), 'Absent');
            $dataFromTomorrow = [];
            if (($now->copy()->addDay()->format('d') != $this->daysInMonth) && !$requestedDate->isPast()) {
                $dataFromTomorrow = array_fill($now->copy()->addDay()->format('d'), ($this->daysInMonth - $now->copy()->format('d')), '-');
            } else {
                $dataFromTomorrow = array_fill($now->copy()->addDay()->format('d'), ($this->daysInMonth - $now->copy()->format('d')), 'Absent');
            }
            $final[$employee->id . '#' . $employee->name] = array_replace($dataTillToday, $dataFromTomorrow);

            foreach ($employee->attendance as $attendance) {
                $final[$employee->id . '#' . $employee->name][Carbon::parse($attendance->clock_in_time)->timezone($this->global->timezone)->day] = '<a href="javascript:;" class="view-attendance" data-attendance-id="' . $attendance->id . '"><i class="fa fa-check text-success"></i></a>';
            }

            foreach ($this->holidays as $holiday) {
                $final[$employee->id . '#' . $employee->name][$holiday->date->day] = 'Holiday';
            }
        }

        $this->employeeAttendence  = $final;

        //End Work Day Calculation //

        // Getting View data
        $attencedata = Employee::join('employee_details','employee_details.employee_id','=','employee.id')
            ->select('employee.id','employee.name','employee_details.address','employee_details.department_id as deptid','employee_details.designation_id as desigid','employee_details.monthly_rate')
            ->groupBy('employee.id')->get();
       // dd($attencedata);
        $pfesidata = PfandEsiSettings::all();

        $datatable = Datatables::of($attencedata);

        $datatable->addIndexColumn()
            ->editColumn('user_id', function ($row)  {
                return $row->id;
            })->editColumn('name', function ($row)  {
                return $row->name;
            })
            ->editColumn('location', function ($row)  {
                if($row->address){
                    return $row->address;
                }
                return 'NA';
            })->editColumn('pt-location', function ($row)  {

                return 'NA';
            })->editColumn('department', function ($row)  {
                $team = Team::where('id',$row->deptid)->first();
                if(!empty($team)){
                    return $team->team_name;
                }
                return 'NA';
            })->editColumn('designation', function ($row)  {
                $desg   = Designation::where('id',$row->desigid)->first();
                if(!empty($desg)){
                    return $desg->name;
                }
                return 'NA';
            })->editColumn('pan', function ($row)  {
                if($row->comment){
                    return $row->comment;
                }
                return 'NA';
            });
        if(!empty($pfesidata) && $pfesidata[0]->pf_applicable == 'on') {
            $datatable->editColumn('pf_number', function ($row) use ($pfesidata) {
                foreach ($pfesidata as $pfdata) {
                    return $pfdata->pf_number;
                }
                return 'NA';
            });
        }
        if(!empty($pfesidata) && $pfesidata[0]->esi_applicable == 'on') {
            $datatable->editColumn('esic_number', function ($row) use ($pfesidata) {
                foreach ($pfesidata as $pfdata) {
                    return $pfdata->esi_number;
                }
                return 'NA';
            })->editColumn('uan_number', function ($row) use ($pfesidata) {
                foreach ($pfesidata as $pfdata) {
                    return $pfdata->uan;
                }
                return 'NA';
            });
        }
        $datatable->editColumn('workingdays', function ($row) use($final) {
                $id = $row->id;
                $name = $row->name;
                $workdays = getworkdayoff($id);

                $view =  view('admin.attendance.working_days', compact('id','name','final'))->render();
                if($view && !empty($workdays['Sunday Off'])){
                    return (int)$view + (int)$workdays['Sunday Off'];
                }
                if($view){
                    return $view;
                }
                return 'NA';
            })->editColumn('lop', function ($row)  {
                $lop = EmployeeLopDetail::where('user_id',$row->id)->select('lop')->first();
                if(!empty($lop)){
                    $lopval = $lop->lop;
                    return $lopval;
                }
                return 'NA';
            })->editColumn('payoutstatus', function ($row)  {
                $paystatus = PayAction::where('user_id',$row->id)->select('payaction')->first();
                if(!empty($paystatus)){
                    $paystatusval = $paystatus->payaction;
                    return $paystatusval;
                }
                return 'NA';

            })->editColumn('gross', function ($row)  {
                if($row->monthly_rate){
                    return $row->monthly_rate;
                }
                return 'NA';
            })->editColumn('pt', function ($row)  {
                return 'NA';
            })->editColumn('incometax', function ($row)  {
                $incometax = EmployeeTaxDetail::where('user_id',$row->id)->select('incometax')->first();
                if(!empty($incometax)){
                    $incometaxval = $incometax->incometax;
                    return $incometaxval;
                }
                return 'NA';
            })->editColumn('netpay', function ($row) use($final,$daysInMonth) {
                $id = $row->id;
                $name = $row->name;
                $eramount = '';
                $didamount = '';
                $ahdocearning = AdhocAction::where('user_id',$id)->where('compo_type','earning')->first();
                $ahdocdiduction = AdhocAction::where('user_id',$id)->where('compo_type','diduction')->first();
                //get Work weak Off
                $workdays = getworkdayoff($id);
                $amount = $row->monthly_rate?$row->monthly_rate:'0';
                if(!empty($ahdocearning->amount)){
                    $eramount = $ahdocearning->amount?$ahdocearning->amount:'0';
                }if(!empty($ahdocdiduction->amount)){
                    $didamount = $ahdocdiduction->amount?$ahdocdiduction->amount:'0';
                }
                $view =  view('admin.attendance.netpay', compact('id','name','final'))->render();
                if($view && !empty($workdays['Sunday Off']) && (!empty($eramount) || !empty($didamount))){
                    return ceil((((int)$view + (int)$workdays['Sunday Off'])*(int)$amount/(int)$daysInMonth + (int)$eramount) - (int)$didamount);
                }
                if($view && !empty($workdays['Sunday Off'])){
                    return ceil(((int)$view + (int)$workdays['Sunday Off'])*(int)$amount/(int)$daysInMonth);
                }
                if($view) {
                    return ceil((int)$view*(int)$amount/(int)$daysInMonth );
                }
                return 'NA';
            });
            return $datatable->make(true);

    }

    public function employeeTaxDetail(){
        $this->employees = Employee::where('company_id',$this->companyid)->where('user_type','employee')->get();
        return view('admin.declaration.employee_tax_detail',$this->data)->render();
    }

    public function addLop(){
        $this->employees = Employee::where('company_id',$this->companyid)->where('user_type','employee')->get();
        return view('admin.declaration.employee_addLop_detail',$this->data)->render();
    }

    public function postTaxDetail(Request $request){
        $taxdetails = new EmployeeTaxDetail();
        $taxdetails->user_id = $request->user_id;
        $taxdetails->incometax = $request->income_tax;
        $taxdetails->comment = $request->comment;
        $taxdetails->save();
        return Reply::success(__('messages.TaxDetailAdded'));
    }
    public function postLop(Request $request){
        $taxdetails = new EmployeeLopDetail();
        $taxdetails->user_id = $request->user_id;
        $taxdetails->lop = $request->lop;
        $taxdetails->save();
        return Reply::success(__('Lop Saved Successfully!!'));
    }
    public function salaryOnHold(){
        $this->employees = Employee::where('company_id',$this->companyid)->where('user_type','employee')->get();
        return view('admin.declaration.salary_on_hold',$this->data)->render();
    }
    public function adhocAction(){
        $this->employees = Employee::where('company_id',$this->companyid)->where('user_type','employee')->get();
        return view('admin.declaration.adhoc_action',$this->data)->render();
    }
    public function editAdhocAction(Request $request){
        $this->employees = Employee::where('company_id',$this->companyid)->where('user_type','employee')->get();
        $this->adhoc = AdhocAction::find($request->id);
        return view('admin.declaration.edit_adhoc_action',$this->data)->render();
    }

    public function updateAdhoc(Request $request){
        $adhocaction = AdhocAction::find($request->id);
        $adhocaction->user_id = $request->user_id;
        $adhocaction->compo_type = $request->compo_type;
        $adhocaction->compo_name = $request->compo_name;
        $adhocaction->amount = $request->amount;
        $adhocaction->save();
        return Reply::success(__('Adhoc Updated Successfully!!'));
    }
    public function postAdhoc(Request $request){
        $adhocaction = new AdhocAction();
        $adhocaction->user_id = $request->user_id;
        $adhocaction->compo_type = $request->compo_type;
        $adhocaction->compo_name = $request->compo_name;
        $adhocaction->amount = $request->amount;
        $adhocaction->save();
        return Reply::success(__('Adhoc Saved Successfully!!'));
    }
    public function previewSlip(Request $request){
        return view('admin.payroll.preview_slip',$this->data)->render();
    }

    public function payrollVerification(Request $request){
        $name = $this->user->name;
        $date =  Carbon::today()->timezone($this->global->timezone)->format('Y-m-d H:i:s');
        $payverify = new PayrollVerify();
        $payverify->verified_date = $date;
        $payverify->verifion_month = $request->month;
        $payverify->verified_by = $name;
        $payverify->verified_values = $request->value;
        $payverify->save();
    }

    public function payRegister(Request $request){

        $fields = json_decode($request->jsonstring);
        $field = array_chunk($fields,18);
        $payRegister = new PayrollRegister();
        foreach ($field as $value){
            $tmp[] = [
                'user_id' => $value['1'],
                'company_id' => $this->user->company_id,
                'month' => $request->month,
                'year' => $request->year,
                'working_days' => $value['11'],
                'lop' => $value['12'],
                'payout_stauts' => $value['13'],
                'gross' => $value['14'],
                'income_tax' => $value['16'],
                'net_pay' => $value['17'],
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ];



        }
        $payRegister->insert($tmp);
        return Reply::success(__('Saved Successfully!!'));
        abort(403);
    }

    public function getComponentName(Request $request){
        $component  = array();
        $option = '';
        if($request->val == 'earning'){
            $component = array_pluck(AdhocEarning::where('company_id',$this->user->company_id)->get(),'id','component_name');
        }else{
            $component = array_pluck(AdhocDeduction::where('company_id',$this->user->company_id)->get(),'id','component_name');
        }

        foreach ($component as $key=>$value){
            $option.='<option value="'.$value.'">'.$key.'</option>';
        }
        return json_encode($option);
    }
}