<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmittalsWorkflowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submittals_workflow', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_id')->nullable();
            $table->string('submittals_id')->nullable();
            $table->string('added_by')->nullable();
            $table->string('user_id')->nullable();
            $table->string('role')->nullable();
            $table->string('sentdate')->nullable();
            $table->string('duedate')->nullable();
            $table->string('status')->nullable();
            $table->string('forward_to')->nullable();
            $table->string('response')->nullable();
            $table->string('returneddate')->nullable();
            $table->string('comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('submittals_workflow');
    }
}
