<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEmployeeDocs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE employee_docs MODIFY name varchar(255) null");
        \DB::statement("ALTER TABLE employee_docs MODIFY filename varchar(255) null");
        \DB::statement("ALTER TABLE employee_docs MODIFY hashname varchar(255) null");
        \DB::statement("ALTER TABLE employee_docs MODIFY size varchar(255) null");
        Schema::table('employee_docs', function (Blueprint $table) {
            $table->integer('doc_type')->default(NULL)->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_docs', function (Blueprint $table) {
            $table->dropColumn('doc_type');
            $table->dropColumn('name');
            $table->dropColumn('filename');
            $table->dropColumn('hashname');
            $table->dropColumn('size');
        });
    }
}
