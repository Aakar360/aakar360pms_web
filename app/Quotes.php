<?php

namespace App;

use App\Observers\StoreObserver;
use Illuminate\Database\Eloquent\Model;


class Quotes extends Model
{
    protected $table = 'quotes';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public static function boot()
    {
        parent::boot();
        static::observe(StoreObserver::class);
    }
}
