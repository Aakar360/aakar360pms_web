<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectSegmentsProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_segments_product', function (Blueprint $table) {
            $table->increments('id');
            $table->string('project_id')->default(0);
            $table->string('title')->default(0);
            $table->string('segment')->default(0);
            $table->string('position')->default('')->comment('row,col');
            $table->string('category')->default(0);
            $table->string('cost_items_id')->nullable();
            $table->string('description')->nullable();
            $table->string('unit')->nullable();
            $table->string('qty')->nullable();
            $table->string('rate')->nullable();
            $table->string('start_date')->nullable();
            $table->string('deadline')->nullable();
            $table->string('assign_to')->nullable();
            $table->string('finalrate')->nullable();
            $table->string('finalamount')->nullable();
            $table->string('target')->nullable();
            $table->string('type')->nullable();
            $table->string('linked')->nullable();
            $table->string('baselinedate')->nullable();
            $table->string('baselinelabel')->nullable();
            $table->string('planned_start')->nullable();
            $table->string('planned_end')->nullable();
            $table->string('worktype')->nullable();
            $table->string('collock')->nullable();
            $table->string('inc')->nullable();
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_segments_product');

    }
}
