<?php

namespace App;

use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ProjectsLogs extends Model
{
    protected $table = 'projects_logs';
    protected $appends = ['issueid','taskid','boqid','manpowerid','indentsid','status','username','userimage','project_name','subproject_name','taskpercentcomment','images','datetime'];

    public function getboqidAttribute()
    {
        $boqid = 0;
        $moduleid = $this->module_id;
        $module = $this->module;
        if($module=='tasks'){
            $taskdet = Task::find($moduleid);
            $boqid = !empty($taskdet->cost_item_id) ? $taskdet->cost_item_id : 0;
        }
        if($module=='task_percentage'){
            $taskper = TaskPercentage::find($moduleid);
            $taskdet = Task::find($taskper->task_id);
            $boqid = !empty($taskdet->cost_item_id) ? $taskdet->cost_item_id : 0;
        }
        return $boqid;
    }
    public function gettaskpercentcommentAttribute()
    {
        $taskid = '';
        $moduleid = $this->module_id;
        $module = $this->module;
        if($module=='task_percentage'){
            $taskper = TaskPercentage::find($moduleid);
            $taskid = !empty($taskper->id) ? $taskper->comment :'';
        }
        return $taskid;
    }
    public function gettaskidAttribute()
    {
        $taskid = 0;
        $moduleid = $this->module_id;
        $module = $this->module;
        if($module=='tasks'){
            $taskdet = Task::find($moduleid);
            $taskid = !empty($taskdet->id) ? $taskdet->id : 0;
        }
        if($module=='task_percentage'){
            $taskper = TaskPercentage::find($moduleid);
            $taskdet = Task::find($taskper->task_id);
            $taskid = !empty($taskdet->id) ? $taskdet->id : 0;
        }
        return $taskid;
    }
    public function getissueidAttribute()
    {
        $taskid = 0;
        $moduleid = $this->module_id;
        $module = $this->module;
        if($module=='punch_item_reply'){
            $taskdet = PunchItemReply::find($moduleid);
            $taskid = !empty($taskdet->punch_item_id) ? $taskdet->punch_item_id : 0;
        }
        if($module=='punch_item'){
            $taskper = PunchItem::find($moduleid);
            $taskid = !empty($taskdet->id) ? $taskdet->id : 0;
        }
        return $taskid;
    }
    public function getmanpoweridAttribute()
    {
        $taskid = 0;
        $moduleid = $this->module_id;
        $module = $this->module;
        if($module=='manpower_logs'){
            $taskdet = ManpowerLog::find($moduleid);
            $taskid = !empty($taskdet->id) ? $taskdet->id : 0;
        }
        if($module=='manpower_reply'){
            $taskper = ManpowerLogReply::find($moduleid);
            $taskid = !empty($taskper->manpower_id) ? $taskper->manpower_id : 0;
        }
        return $taskid;
    }
    public function getindentsidAttribute()
    {
        $taskid = 0;
        $moduleid = $this->module_id;
        $module = $this->module;
        if($module=='indents'){
            $taskdet = Indent::find($moduleid);
            $taskid = !empty($taskdet->id) ? $taskdet->id : 0;
        }
        if($module=='indents_reply'){
            $taskper = IndentsReply::find($moduleid);
            $taskid = !empty($taskper->indents_id) ? $taskper->indents_id : 0;
        }
        return $taskid;
    }
    public function getstatusAttribute()
    {
        $taskid = '';
        $moduleid = $this->module_id;
        $module = $this->module;
        if($module=='task_percentage'){
            $taskper = TaskPercentage::find($moduleid);
            $taskdet = Task::find($taskper->task_id);
            $taskid = !empty($taskdet->status) ? $taskdet->status : '';
        }
        return $taskid;
    }
    public function getDatetimeAttribute()
    {
        return date('d/m/Y h:i A',strtotime($this->created_at));
    }
    public function getProjectNameAttribute()
    {
        return get_project_name($this->project_id);
    }
    public function getSubProjectNameAttribute()
    {
        return !empty($this->subproject_id) ? get_title($this->subproject_id) : '';
    }
    //define accessor
    public function getUsernameAttribute()
    {
        $allign = '';
        if(!empty($this->added_id)){
            $userid = $this->added_id;
            $allign = get_user_name($userid);
        }
        return $allign;
    }
    public function getUserimageAttribute()
    {
        $allign = '';
        if(!empty($this->added_id)){
            $userid = $this->added_id;
            $allign = get_users_image_link($userid);
        }
        return $allign;
    }
    public function getImagesAttribute()
    {
        $imagesarray = $newimages = array();
        $company_id = $this->company_id;
        $moduleid = $this->module_id;
        $module = $this->module;
        $mainid = '';
        $storage = storage();
        $url = awsurl();
        switch ($module){
            case 'tasks':
                $filesarray = TaskFile::where('task_id',$moduleid)->where('task_percentage_id','0')->get();
                $localpath = uploads_url().'task-files/';
                $s3path = $url.'/task-files/';
                $mainid = $moduleid;
                break;
            case 'task_percentage':
                $filesarray = TaskFile::where('task_percentage_id',$moduleid)->get();
                $localpath = uploads_url().'task-files/';
                $s3path = $url.'/task-files/';
                $mainid = !empty($filesarray[0]) ? $filesarray[0]->task_id : '';
                break;
            case 'punch_item':
                $filesarray = PunchItemFiles::where('task_id',$moduleid)->where('reply_id','0')->get();
                $localpath = uploads_url().'punch-files/';
                $s3path = $url.'/punch-files/';
                $mainid = $moduleid;
                break;
            case 'punch_item_reply':
                $filesarray = PunchItemFiles::where('reply_id',$moduleid)->get();
                $localpath = uploads_url().'punch-files/';
                $s3path = $url.'/punch-files/';
                $mainid = !empty($filesarray[0]) ? $filesarray[0]->task_id : '';
                break;
            case 'indents':
                $filesarray = IndentsFiles::where('indents_id',$moduleid)->where('reply_id','0')->get();
                $localpath = uploads_url().'indents-files/';
                $s3path = $url.'/indents-files/';
                $mainid = $moduleid;
                break;
            case 'indents_reply':
                $filesarray = IndentsFiles::where('reply_id',$moduleid)->get();
                $localpath = uploads_url().'indents-files/';
                $s3path = $url.'/indents-files/';
                $mainid = !empty($filesarray[0]) ? $filesarray[0]->indents_id : '';
                break;
            case 'manpower_logs':
                $filesarray = ManpowerLogFiles::where('manpower_id',$moduleid)->where('reply_id','0')->get();
                $localpath = uploads_url().'manpower-log-files/';
                $s3path = $url.'/manpower-log-files/';
                $mainid = $moduleid;
                break;
            case 'manpower_reply':
                $filesarray = ManpowerLogFiles::where('reply_id',$moduleid)->get();
                $localpath = uploads_url().'manpower-log-files/';
                $s3path = $url.'/manpower-log-files/';
                $mainid = !empty($filesarray[0]) ? $filesarray[0]->manpower_id : '';
                break;
        }
        if(!empty($filesarray)){
            foreach ($filesarray as $files){
                $imagename  = $files->hashname;
                if($imagename){
                    if($storage){
                        switch($storage){
                            case 'local':
                                $filename = $localpath.$mainid.'/'.$imagename;
                                break;
                            case 's3':
                                $filename = $s3path.$mainid.'/'.$imagename;
                                break;
                            case 'google':
                                $filename = $files->google_url;
                                break;
                            case 'dropbox':
                                $filename = $files->dropbox_link;
                                break;
                        }
                    }
                $imagesarray['name'] = $files->filename;
                $imagesarray['image'] = $filename;
                $imagesarray['created_at'] = Carbon::parse($files->created_at)->format('d/m/Y');
                $newimages[] = $imagesarray;
                }
            }
        }
        return $newimages;
    }
}