<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class AwardContractCategory extends Model
{
    protected $table = 'awarded_contract_category';

    protected static function boot()
    {
        parent::boot();
    }
}
