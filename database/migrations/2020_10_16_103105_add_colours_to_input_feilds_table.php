<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColoursToInputFeildsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('input_fields', function (Blueprint $table) {
            $table->text('colors')->nullable()->after('options');
            $table->text('showtype')->nullable()->after('colors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('input_fields', function (Blueprint $table) {
            $table->dropColumn(['colors']);
            $table->dropColumn(['showtype']);
        });
    }
}
