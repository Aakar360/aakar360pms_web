<?php

namespace App\Http\Controllers\Admin;

use App\Company;
use App\Condition;
use App\Currency;
use App\Expense;
use App\Helper\Reply;
use App\Http\Requests\Project\StoreProject;
use App\Payment;
use App\ProjectActivity;
use App\ProjectAttachmentFiles;
use App\ProjectAttachmentDesigns;
use App\ProjectCategory;
use App\ProjectCostItemsFinalQty;
use App\ProjectFile;
use App\ProjectMember;
use App\ProjectTemplate;
use App\ProjectTimeLog;
use App\SchedulingDue;
use App\SourcingPackage;
use App\SourcingPackageProduct;
use App\Task;
use App\TaskboardColumn;
use App\TenderAssign;
use App\TenderBidding;
use App\TenderBiddingProduct;
use App\Tenders;
use App\TendersCondition;
use App\TendersFiles;
use App\TendersProduct;
use App\Title;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Project;
use App\ProjectMilestone;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use function PHPSTORM_META\type;
use Yajra\DataTables\Facades\DataTables;
use App\Traits\ProjectProgress;
use App\TaskCategory;
use App\CostItemsLavel;
use App\BoqCategory;
use App\CostItems;
use App\CostItemsProduct;
use App\ProjectCostItemsProduct;
use App\ProductCategory;
use App\ProductBrand;
use App\Units;
use App\FileManager;
use App\Helper\Files;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class BiddingController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $data['pageTitle'] = 'app.menu.projects';
        $data['pageIcon'] = 'icon-layers';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function bidding(Request $request, $company_id, $project_id, $tender_id, $sourcing_id, $user_id){
        $data['pageTitle'] = 'Tender Bidding';
        $data['pageIcon'] = 'icon-layers';
        $data['assign_tender'] = TenderAssign::where('company_id', $company_id)
            ->where('project_id', $project_id)
            ->where('tender_id', $tender_id)
            ->where('sourcing_id', $sourcing_id)
            ->where('user_id', $user_id)
            ->first();
        $data['tender'] = array();
        $data['company_id'] = $company_id;
        $data['project_id'] = $project_id;
        $data['tender_id'] = $tender_id;
        $data['sourcing_id'] = $sourcing_id;
        $data['user_id'] = $user_id;
        $data['conditions'] = array();
        $data['brands'] = array();

        $data['packages'] = array();
        $data['costitems'] = array();
        $data['package'] = array();
        $data['packageProducts'] = array();

        $data['cons'] = array();
        $data['pros'] = array();
        $data['files'] = array();
        if($data['assign_tender']){
            $data['tender'] = Tenders::where('company_id',$company_id)
                ->where('id',$tender_id)->first();
            $data['company_id'] = $company_id;
            $data['project_id'] = $project_id;
            $data['tender_id'] = $tender_id;
            $data['sourcing_id'] = $sourcing_id;
            $data['user_id'] = $user_id;
            $data['conditions'] = Condition::all();
            $data['brands'] = ProductBrand::all();

            $data['packages'] = SourcingPackage::where('company_id',$company_id)->get();
            $data['costitems'] = ProjectCostItemsProduct::where('project_cost_items_product.project_id',$project_id)
                ->join('title','title.id','=','project_cost_items_product.title')
                ->select('title.id','title.title')
                ->groupBy('project_cost_items_product.title')
                ->get();
            $data['package'] = SourcingPackage::where('id',$sourcing_id)->first();
            $data['packageProducts'] = SourcingPackageProduct::where('sourcing_packages_product.sourcing_package_id',$sourcing_id)
                ->join('category', 'category.id', '=', 'sourcing_packages_product.product_id')
                ->select('category.name as name', 'category.id as id')
                ->get();

            $data['cons'] = TendersCondition::where('tender_id', $tender_id)->get();
            $data['pros'] = TendersProduct::where('tender_id', $tender_id)->get();
            $data['files'] = TendersFiles::where('tender_id', $tender_id)->get();
        }
        return view('admin.projects.bidding', $data);
    }

    public function biddingSubmit(Request $request){
        $tender = new TenderBidding();
        $tender->company_id = $request->company_id;
        $tender->project_id = $request->project_id;
        $tender->sourcing_id = $request->sourcing_id;
        $tender->tender_id = $request->tender_id;
        $tender->user_id = $request->user_id;
        $tender->save();
        if($tender->id){
            foreach($request->products as $key=>$pi ) {
                $tp = new TenderBiddingProduct();
                $tp->tender_bidding_id = $tender->id;
                $tp->tender_id = $request->tender_id;
                $tp->products = $pi;
                $tp->qty = $request->qty[$key];
                $tp->brands = $request->brand_ids[$key];
                $tp->price = $request->price[$key];
                $tp->save();
            }
        }
        return Reply::redirect(route('projects.success'));
    }

    public function success(){
        $data['pageTitle'] = 'Success';
        $data['pageIcon'] = 'icon-tick';
        return view('admin.projects.success', $data);
    }
}
