<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class TendersCategory extends Model
{
    protected $table = 'tenders_category';

    protected static function boot()
    {
        parent::boot();
    }
}
