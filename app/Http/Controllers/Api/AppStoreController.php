<?php

namespace App\Http\Controllers\Api;

use App\AppProject;
use App\BoqCategory;
use App\Company;
use App\Helper\Reply;
use App\Http\Controllers\Controller;
use App\Indent;
use App\IndentProducts;
use App\IndentsFiles;
use App\IndentsReply;
use App\ManpowerLog;
use App\ManpowerLogFiles;
use App\ManpowerLogReply;
use App\Product;
use App\ProjectActivity;
use App\ProjectAttachmentDesigns;
use App\ProjectAttachmentFiles;
use App\ProjectCostItemsFinalQty;
use App\ProjectCostItemsPosition;
use App\ProjectCostItemsProduct;
use App\ProjectMember;
use App\ProjectMilestone;
use App\ProjectsLogs;
use App\ProjectTemplate;
use App\ProjectTimeLog;
use App\Rfi;
use App\Store;
use App\Task;
use App\Title;
use App\Units;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\FileManager;
use App\Project;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use DB;
use Intervention\Image\Facades\Image;

class AppStoreController extends Controller
{
    use AuthenticatesUsers;
    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }

    protected function validatetToken($apikey,$token)
    {
        if (isset($apikey)) {
            if (!$this->validateAPI($apikey)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            $response = array();
            if(isset($token)) {
                if (!empty($token)) {
                    $user = User::where('appid', $token)->first();
                    if($user === null){
                        $response['message'] = 'Invalid token';
                        $response['status'] = 401;
                        return $response;
                    }
                    $response['user'] = $user;
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    return $response;
                }
                $response['message'] = 'Api Token cannot be empty';
                $response['status'] = 401;
                return $response;
            }
            $response['message'] = 'Api Token not sent';
            $response['status'] = 401;
            return $response;

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function __construct(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];

            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', $user->company_id)->first();

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function unitsList(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $page = $request->page;
                $response = array();
                if($page=='all'){
                    $units = Units::where('added_by', '0')->orWhere('added_by',$user->id)->get()->toArray();
                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    $units = Units::where('added_by', '0')->orWhere('added_by',$user->id)->offset($skip)->take($count)->get()->toArray();
                }
                $response['status'] = 200;
                $response['message'] = 'Project List Fetched';
                $response['response'] = $units;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'units-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function indentsList(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $project = explode(',',$request->project_id);
                $response = array();
                $status = $request->status;
                $page = $request->page;
                $indents = Indent::whereIn('project_id', $project);
                if(!empty($status)){
                    $indents = $indents->where('status',$status);
                }
                if(!empty($request->fromdate)){
                    $fromdate = date('Y-m-d',strtotime($request->fromdate));
                    $indents = $indents->where('created_at','>=',$fromdate.' 00:00:00');
                }
                if(!empty($request->todate)){
                    $todate =  date('Y-m-d',strtotime($request->todate));
                    $indents = $indents->where('created_at','<=',$todate.' 23:59:59');
                }
                if($page=='all'){
                    $indents = $indents->orderBy('id','DESC')->get();
                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    $indents = $indents->offset($skip)->take($count)->orderBy('id','DESC')->get();
                }
                if(count($indents)){
                    foreach ($indents as $project){
                        $store = Indent::where('project_id', $project->id)->first();
                        $user = Store::where('id', $project->store_id)->first();
                        $proData[] = array(
                            'id' => $project->id,
                            'indent_no' => $project->indent_no,
                            'project_id' => $project->project_id,
                            'project_name' => get_project_name($project->project_id),
                            'added_by' => get_user_name($project->added_by),
                            'remark' => $project->remark,
                            'status' => ($project->status == '5') ? 'Received' : 'Pending',
                            'created_at' => Carbon::parse($project->created_at)->format('d-m-Y'),
                        );
                    }

                }else{
                    $proData = array();
                }
                $response['status'] = 200;
                $response['message'] = 'Indents List Fetched';
                $response['response'] = $proData;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'indents-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function indentProducts(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $proData = array();
                $page = $request->page;
                $indent = Indent::where('id', $request->indent_id)->first();
                if($page=='all'){
                    $indents = IndentProducts::where('indent_id', $request->indent_id)->get();
                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    $indents = IndentProducts::where('indent_id', $request->indent_id)->offset($skip)->take($count)->get();
                }
                foreach ($indents as $project){
                    $proData[] = array(
                        'id' => $project->id,
                        'cid' => get_procat_name($project->cid),
                        'bid' => get_pbrand_name($project->bid),
                        'quantity' => $project->quantity,
                        'unit_id' => $project->unit,
                        'unit' => get_unit_name($project->unit),
                        'expected_date' => $project->expected_date,
                        'created_at' => Carbon::parse($project->created_at)->format('d-m-Y'),
                        'remarks' => $project->remarks,
                    );
                }
                $status_name = array();
                /*$status_name[] = array(  'id' => '0', 'name' => 'Pending RFQ');
                $status_name[] = array(  'id' => '1', 'name' => 'Pending PO');
                $status_name[] = array(  'id' => '2', 'name' => 'Pending Purchase');
                $status_name[] = array(  'id' => '3', 'name' => 'Purchase Done');*/
                $status_name[] = array(  'id' => '4', 'name' => 'Pending');
                $status_name[] = array(  'id' => '5', 'name' => 'Received');
                $response['status'] = 200;
                $response['message'] = 'Indent Products List Fetched';
                $response['response'] = $proData;
                $response['status_name'] = $status_name;
                $response['indent_status'] = $indent->status;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'indent-products',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function productList(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $page = $request->page;
                if($page=='all'){
                    $projects = Product::where('company_id',$user->company_id)->get();
                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    $projects = Product::where('company_id',$user->company_id)->offset($skip)->take($count)->get();
                }
                $proData = array();
                foreach ($projects as $project){
                    $proData[] = array(
                        'id' => $project->id,
                        'name' => $project->name,
                        'unit_id' => $project->unit_id,
                        'unit_name' => get_unit_name($project->unit_id),
                    );
                }
                $response = array();
                $response['status'] = 200;
                $response['message'] = 'Product List Fetched';
                $response['response'] = $proData;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'product-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function createUnit(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $unit = new Units();
                $unit->name = $request->name;
                $unit->symbol = $request->symbol;
                $unit->added_by = $user->id;
                $unit->save();
                $response['status'] = 200;
                $response['message'] = 'Unit Added Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-unit',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function createIndent(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $sr = 1;
                $ind = Indent::orderBy('id', 'DESC')->first();
                if($ind !== null){
                    $in = explode('/', $ind->indent_no);
                    $sr = $in[2];
                    $sr++;
                }
                if($request->store_id == 0) {
                    $store = Store::where('project_id', $request->project_id)->first();
                    if($store == null){
                        $response['status'] = 200;
                        $response['message'] = 'No store assigned for this project.';
                        return $response;
                    }else {
                        $indent_no = 'IND/' . date("Y") . '/' . $sr;
                        $indent = new Indent();
                        $indent->company_id = $user->company_id;
                        $indent->indent_no = $indent_no;
                        $indent->store_id = $store->id;
                        $indent->project_id = $request->project_id;
                        $indent->status = '4';
                        $indent->added_by = $user->id;
                        $indent->save();
                    }
                }else{
                    $store = Store::where('project_id', $request->project_id)->first();
                    $indent_no = 'IND/' . date("Y") . '/' . $sr;
                    $indent = new Indent();
                    $indent->company_id = $user->company_id;
                    $indent->indent_no = $indent_no;
                    $indent->store_id = $request->store_id;
                    $indent->project_id = $request->project_id;
                    $indent->status = '4';
                    $indent->added_by = $user->id;
                    $indent->save();
                }

                $ex = json_decode($request->indent);
                foreach ($ex as $data){
                    $indPro = new IndentProducts();
                    $indPro->indent_id = $indent->id;
                    $indPro->cid = $data->product_id;
                    $indPro->unit = $data->unit_id;
                    $indPro->quantity = $data->qty;
                    $indPro->remarks = $data->remark;
                    $indPro->expected_date = $data->date;
                    $indPro->save();
                }
                $imagesarray = array_filter(explode(',',$request->imagesids));
                if(!empty($imagesarray)){
                    foreach ($imagesarray as $images){
                        $punchfile = IndentsFiles::find($images);
                        if(!empty($punchfile->id)){
                            $punchfile->indents_id = $indent->id;
                            $punchfile->reply_id = 0;
                            $punchfile->save();
                        }
                    }
                }
                $createlog = new ProjectsLogs();
                $createlog->company_id = $user->company_id;
                $createlog->added_id = $user->id;
                $createlog->module_id = $indent->id;
                $createlog->module = 'indents';
                $createlog->modulename = 'indents_created';
                $createlog->project_id = $request->project_id;
                $createlog->heading =  $indent->indent_no.' has created';
                $createlog->description = 'Indent with number '.$indent->indent_no.' created by ' . $user->name;
                $createlog->save();

                $project_membersarray = User::join('project_members','project_members.user_id','=','users.id')
                    ->select('users.*')->where('project_members.project_id', $indent->project_id)->where('project_members.share_project', '1')
                    ->where('project_members.user_id', '<>',$user->id)->where('users.fcm', '<>','')->groupBy('project_members.user_id')->orderBy('users.name','asc')->get();
                foreach($project_membersarray as $project_members){
                    $notifmessage = array();
                    $notifmessage['title'] = 'Store Indents';
                    $notifmessage['body'] = 'Indent with number '.$indent->indent_no.' created by ' . $user->name;
                    $notifmessage['activity'] = 'indents';
                    sendFcmNotification($project_members->fcm, $notifmessage);
                }

                $response['status'] = 200;
                $response['message'] = 'Indent Added Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-indent',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function replyPost(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                if(!empty($request->indentsid)){
                    $id = $request->indentsid;
                    $mentionusers = !empty($request->mentionusers) ? $request->mentionusers : '';
                    $punchitem = Indent::find($id);
                    if($punchitem){
                        $pi = new IndentsReply();
                        $pi->company_id = $user->company_id;
                        $pi->comment = $request->comment;
                        $pi->indents_id = $punchitem->id;
                        $pi->added_by = $user->id;
                        $pi->mentionusers = $mentionusers;
                        $pi->save();

                        $imagesarray = array_filter(explode(',',$request->imagesids));
                        if(!empty($imagesarray)){
                            foreach ($imagesarray as $images){
                                $punchfile = IndentsFiles::find($images);
                                if(!empty($punchfile->id)){
                                    $punchfile->indents_id = $punchitem->id;
                                    $punchfile->reply_id = $pi->id;
                                    $punchfile->save();
                                }
                            }
                        }
                        $medium = $request->medium ?: 'android';
                        $createlog = new ProjectsLogs();
                        $createlog->company_id = $user->company_id;
                        $createlog->added_id = $user->id;
                        $createlog->module_id = $pi->id;
                        $createlog->module = 'indents_reply';
                        $createlog->modulename = 'indents_comment';
                        $createlog->project_id = $punchitem->project_id ?: 0;
                        $createlog->subproject_id = $punchitem->subproject_id ?: 0;
                        $createlog->segment_id = $punchitem->segment_id ?: 0;
                        $createlog->heading =  $pi->comment;
                        $createlog->description = 'Indent Commented by '.$user->name.' with no '.$punchitem->indent_no.' for the project '.get_project_name($punchitem->project_id);
                        $createlog->medium = $medium;
                        $createlog->mentionusers = $mentionusers;
                        $createlog->save();

                        $response = array();
                        $response['status'] = 200;
                        $response['indentsid'] = $punchitem->id;
                        $response['replyid'] = $pi->id;
                        $response['message'] = 'Reply Updated Successfully';
                        return $response;
                    }
                }
                $response['status'] = 300;
                $response['message'] = 'Indents not Found';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'indents-reply',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function replyList(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                if(!empty($request->indentsid)){
                    $id = $request->indentsid;
                    $punchitem = Indent::find($id);
                    if($punchitem){
                        $page = $request->page;
                        $response = array();
                        if($page=='all'){
                            $pi = IndentsReply::where('indents_id',$punchitem->id)->get();
                        }else{
                            $count = pagecount();
                            $skip = 0;
                            if($page){
                                $skip = $page*$count;
                            }
                            $pi = IndentsReply::where('indents_id',$punchitem->id)->offset($skip)->take($count)->get();
                        }
                        $response = array();
                        $response['status'] = 200;
                        $response['responselist'] = $pi;
                        $response['message'] = 'Reply Updated Successfully';
                        return $response;
                    }
                }
                $response['status'] = 300;
                $response['message'] = 'Indents not Found';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'indents-reply-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function createProduct(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $pro = new Product();
                $pro->company_id = $user->company_id;
                $pro->name = $request->name;
                $pro->unit_id = $request->unit_id;
                $pro->save();
                $response['status'] = 200;
                $response['message'] = 'Product Added Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-product',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function updateUnit(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $unit = Units::find($request->id);
                $unit->name = $request->name;
                $unit->symbol = $request->symbol;
                $unit->save();
                $response['status'] = 200;
                $response['message'] = 'Unit Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'update-unit',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function updateIndentStatus(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $unit = Indent::find($request->id);
                $unit->status = $request->status;
                $unit->save();
                $response['status'] = 200;
                $response['message'] = 'Indent Status Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'update-indent-status',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteUnit(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $unit = Units::where('id',$request->id)->delete();
                $response['status'] = 200;
                $response['message'] = 'Unit Deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-unit',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function updateProduct(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $pro = Product::where('id',$request->id)->first();
                $pro->name = $request->name;
                $pro->unit_id = $request->unit_id;
                $pro->save();
                $response['status'] = 200;
                $response['message'] = 'Product Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'update-product',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteproduct(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $unit = Product::where('id',$request->id)->delete();
                $response['status'] = 200;
                $response['message'] = 'Product Deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-product',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteIndent(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                Indent::where('id',$request->id)->delete();
                IndentProducts::where('indent_id',$request->id)->delete();
                $indentsreplyarray = IndentsReply::where('indents_id',$request->id)->get();
                foreach($indentsreplyarray as $indentsreply){
                    ProjectsLogs::where('module_id',$indentsreply->id)->where('module','indents_reply')->delete();
                    $indentsreply->delete();
                }

                IndentsFiles::where('indents_id',$request->id)->delete();
                ProjectsLogs::where('module_id',$request->id)->where('module','indents')->delete();
                $response['status'] = 200;
                $response['message'] = 'Unit Deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-indent',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteIndentProduct(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $ip = IndentProducts::where('id',$request->id)->first();
                $ipc = IndentProducts::where('indent_id',$ip->indent_id)->get();
                if(count($ipc) < 2){
                    Indent::where('id',$ip->indent_id)->delete();
                    IndentProducts::where('id',$request->id)->delete();
                }else {
                    IndentProducts::where('id', $request->id)->delete();
                }
                $response['status'] = 200;
                $response['message'] = 'Indent Deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-indent-product',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function updateIndentProduct(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $indPro = IndentProducts::find($request->id);
                $indPro->unit = $request->unitid;
                $indPro->quantity = $request->qty;
                $indPro->remarks = $request->remark;
                $indPro->expected_date = $request->date;
                $indPro->save();
                $response['status'] = 200;
                $response['message'] = 'indent updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'update-indent-product',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function storesList(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $units = Store::where('added_by',$user->id)->get()->toArray();
                $response['status'] = 200;
                $response['message'] = 'Stores List Fetched';
                $response['response'] = $units;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'stores-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function createStore(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $pro = new Store();
                $pro->company_name = $request->company_name;
                $pro->project_id = $request->project_id;
                $pro->added_by = $user->id;
                $pro->address = $request->address;
                $pro->save();
                $response['status'] = 200;
                $response['message'] = 'Store Added Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-store',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function updateStore(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $pro = Store::find($request->id);
                $pro->company_name = $request->company_name;
                $pro->project_id = $request->project_id;
                $pro->address = $request->address;
                $pro->save();
                $response['status'] = 200;
                $response['message'] = 'Store Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'update-store',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteStore(Request $request){
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $unit = Store::where('id',$request->id)->delete();
                $response['status'] = 200;
                $response['message'] = 'Unit Deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-store',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function getStore(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $units = Store::where('project_id',$request->project_id)->where('added_by',$user->id)->get()->toArray();
                $response['status'] = 200;
                $response['message'] = 'Stores List Fetched';
                $response['response'] = $units;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'get-store',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function storeImage(Request $request)
    {
        if (isset($request->API_KEY)&&isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY,$request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $indentsid = $request->indentsid;
                if ($request->hasFile('file')) {
                    $storage = storage();
                    $companyid = $user->company_id;
                    $fileData =  $request->file('file');
                    $file = new IndentsFiles();
                    $file->added_by = $user->id;
                    $file->company_id = $companyid;
                    $file->indents_id = $request->indentsid ?: 0;
                    $file->reply_id = $request->replyid ?: 0;
                    switch($storage) {
                        case 'local':
                            $destinationPath = 'uploads/indents-files/'.$indentsid;
                            if (!file_exists('public/'.$destinationPath)) {
                                mkdir('public/'.$destinationPath, 0777, true);
                            }
                            $fileData->storeAs($destinationPath, $fileData->hashName());
                            break;
                        case 's3':
                            Storage::disk('s3')->putFileAs('/indents-files/'.$indentsid, $fileData, $fileData->hashName(), 'public');
                            break;
                        case 'google':
                            $dir = '/';
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                            $dir = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', 'indents-files')
                                ->first();
                            if(!$dir) {
                                Storage::cloud()->makeDirectory('indents-files');
                            }
                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $indentsid)
                                ->first();

                            if ( ! $directory) {
                                Storage::cloud()->makeDirectory($dir['path'].'/'.$indentsid);
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('filename', '=', $indentsid)
                                    ->first();
                            }
                            Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());
                            $file->google_url = Storage::cloud()->url($directory['path'].'/'.$fileData->hashName());
                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs('indents-files/'.$request->manpowerid.'/', $fileData, $fileData->getClientOriginalName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer ".config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/task-files/'.$request->manpowerid.'/'.$fileData->getClientOriginalName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $file->dropbox_link = $dropboxResult['url'];
                            break;
                    }

                    $file->filename = $fileData->getClientOriginalName();
                    $file->hashname = $fileData->hashName();
                    $file->size = $fileData->getSize();
                    $file->save();
                    $response['status'] = 200;
                    $response['message'] = 'Indents Image Uploaded';
                    return $response;

                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'store-indents-image',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
            $response['status'] = 300;
            $response['message'] = 'Uploading failed. Please try again';
            return $response;
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
}