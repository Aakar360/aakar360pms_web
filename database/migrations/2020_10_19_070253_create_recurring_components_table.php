<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecurringComponentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recurring_components', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('component_name');
            $table->string('component_type');
            $table->string('lop_dependent');
            $table->string('include_in_ctc');
            $table->string('individual_override');
            $table->string('proof_required');
            $table->string('taxable');
            $table->string('calc_type');
            $table->string('anual_amount')->nullable();
            $table->string('baded_on');
            $table->string('from')->nullable();
            $table->string('to')->nullable();
            $table->string('equal')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recurring_components');
    }
}
