<?php

namespace App\Http\Controllers\Admin;

use App\Combos;
use App\ProjectCostItemsProduct;
use App\ProjectCostItemsResourceRate;
use App\Resource;
use App\Type;
use App\FormulaParser;
use Illuminate\Http\Request;

class ManageResourcesController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'app.menu.resources';
        $this->pageIcon = 'ti-layout-list-thumb';
        $this->activeMenu = 'pms';
        $this->categories = array(
            0 => 'Simple',
            1 => 'Complex',
        );
        $this->types = Type::all();
        $this->middleware(function ($request, $next) {
            if (!in_array('resources', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->resources = Resource::all();
        return view('admin.resources.index', $this->data);
    }

    public function store(Request $request)
    {
        $resource = new Resource();
        $resource->category = $request->category;
        $resource->type = $request->type;
        $resource->description = $request->description;
        if (isset($request->unit)) {
            if ($request->unit !== 'undefined' && $request->unit !== null && $request->unit !== '') {
                $resource->unit = $request->unit;
            }
        }
        if (isset($request->rate)) {
            if ($request->rate !== 'undefined' && $request->rate !== null && $request->rate !== '') {
                $resource->rate = $request->rate;
            }
        }
        if (isset($request->remark)) {
            if ($request->remark !== 'undefined' && $request->remark !== null && $request->remark !== '') {
                $resource->remark = $request->remark;
            }
        }
        $resource->save();
        $ret['id'] = $resource->id;
        $ret['name'] = $resource->type.' - '.$resource->description;
        $ret['status'] = 'success';
        return json_encode($ret);
    }

    public function update(Request $request, $id)
    {
        $resource = Resource::find($id);
        $resource->category = $request->category;
        $resource->type = $request->type;
        $resource->description = $request->description;
        if (isset($request->unit)) {
            if ($request->unit != 'undefined' && $request->unit != null && $request->unit != '') {
                $resource->unit = $request->unit;
            }
        }
        if (isset($request->rate)) {
            if ($request->rate !== 'undefined' && $request->rate !== null && $request->rate !== '') {
                $resource->rate = $request->rate;
            }
        }
        if (isset($request->remark)) {
            if ($request->remark !== 'undefined' && $request->remark !== null && $request->remark !== '') {
                $resource->remark = $request->remark;
            }
        }
        $resource->save();
        $ret['id'] = $resource->id;
        $ret['status'] = 'success';
        return json_encode($ret);
    }

    public function getComboSheet(Request $request, $id)
    {
        $this->pageTitle = 'app.menu.combo';
        $this->pageIcon = 'ti-layout-list-thumb';
        $this->activeMenu = 'pms';
        $this->resources = Resource::orderBy('id','asc')->get();
        $this->combos = Combos::where('resource_id', $id)->orderBy('id','asc')->get();
        $this->resourceId = $id;
        $ret = array();
        $ret = view('admin.resources.combo-sheet', $this->data)->renderSections()['content'];
        return $ret;
    }

    public function getResource(Request $request)
    {
        $res = Resource::find($request->id);
        $ret = array();
        if ($res !== null) {
            $ret['rate'] = $res->rate;
            $ret['unit'] = $res->unit;
        }
        $combosheet = new Combos();
        $combosheet->resource_id = $request->resource;
        $combosheet->combo_resource_id = $res->id;
        $combosheet->rate = $res->rate;
        $combosheet->unit = $res->unit;
        $combosheet->formula = '';
        $combosheet->final_rate = $res->rate;
        $combosheet->save();
        $combosheet->category = get_resource_category($combosheet->combo_resource_id);
        return $combosheet;
    }
    public function removeResource(Request $request)
    {
        $res = Resource::find($request->id);
        $res->delete();
        ProjectCostItemsResourceRate::where('resource_id',$res->id)->delete();
        Combos::where('resource_id',$res->id)->delete();
        $ret = array();
        $ret['status'] = 'success';
        return  json_encode($ret);
    }
    public function comboSheetRemove(Request $request)
    {
        if($request->comboid){

            $res = Combos::find($request->comboid);
            $res->delete();
            $resoototal = Combos::where('resource_id',$res->resource_id)->sum('final_rate');
            Resource::where('id',$res->resource_id)->update(['rate'=>$resoototal]);
            $ret = array();
            $ret['status'] = 'success';
            $ret['result'] = $resoototal;
            return  json_encode($ret);
        }
        $ret = array();
        $ret['status'] = 'fail';
        return  json_encode($ret);
    }

    public function calculate(Request $request)
    {
        $ret = array();
        $str = $request->str . $request->applied;
        if (!ctype_digit($str[0]) && !ctype_alpha($str[0])) {
            $wastepercent = 0;
             $strlen = strlen($str);
                if(strpos($str, 'Waste(') !== false){
                    $wastepost =  strpos( $str,'Waste(');
                    if($strlen-9>$wastepost){
                        $ret['status'] = 'error';
                        $ret['result'] = '';
                        $ret['message'] = 'Waste Function should be in last position';
                        return json_encode($ret);
                    }else{
                        $wstg = substr($str,-9);
                        preg_match('#\((.*?)\)#', $wstg, $matchstr);
                        $wastepercent = $matchstr[1];
                        $str = substr($str,0,-9);
                    }
                }
                $str = $this->mathFunction($str);
                $pattern = '/([\/][a-zA-Z])\w/';
                $patternx = '/[a-z]/i';
                $nstr = preg_replace($pattern, '', $str);
                $nstr = preg_replace($patternx, '', $nstr);
                //dd(''.$request->rate.$nstr.')');
                $cal = new FormulaParser('' . $request->rate . $nstr, 2);
                $result = $cal->getResult();
                $ret['status'] = $result[0];
                if ($result[0] != 'done') {
                    $ret['message'] = 'Formula has some invalid expression or character.';
                } else {
                    $finalrate = $result[1];
                    if($wastepercent){
                        $finalrate += ($result[1]/100)*$wastepercent;
                    }
                    $result = number_format($finalrate, 2, '.', '');
                    /** @var update combo sheet here $resourceid */
                    $resourceid = $request->resource;
                    $comboresourceid = $request->comboresource;
                    $comboid = $request->comboid;
                    if(!empty($resourceid)){
                        if(!empty($comboid)){
                            $resourcedata = Combos::where('id',$comboid)->first();
                        }else{
                            $resourcedata = new Combos();
                            $resourcedata->resource_id = $resourceid;
                            $resourcedata->combo_resource_id = $comboresourceid;
                        }
                        $resourcedata->rate  = $request->rate;
                        $resourcedata->unit  = $request->unit;
                        $resourcedata->formula  = $request->str;
                        $resourcedata->final_rate  = $result;
                        $resourcedata->remark  = $request->remark;
                        $resourcedata->save();
                        $resourcefinalrate = Combos::where('resource_id',$resourceid)->sum('final_rate');
                        if($resourcefinalrate){
                            Resource::where('id',$resourceid)->update(['rate'=>$resourcefinalrate]);
                        }
                    }
                    $ret['comboid'] = $resourcedata->id;
                    $ret['result'] = $result;

                }
        } else {
            $ret['status'] = 'error';
            $ret['result'] = '';
            $ret['message'] = 'First Character of formula field must be an operator i.e +, -, *, /, ^ etc.';
        }

        return json_encode($ret);
    }
    public function getComboSheetStore(Request $request){
        $sheetid = $request->sheetid;
        $resourcearray = $request->resource;
        $rate = $request->rate;
        $unit = $request->unit;
        $formula = $request->formula;
        $result = $request->result;
        $remark = $request->remark;
        $resourceid = $request->resource_id;
        if(!empty($resourcearray)){
            foreach ($resourcearray as $key => $comboresource){
                $comid = $sheetid[$key];
                if(!empty($comid)){
                    $combosheet = Combos::where('id',$comid)->first();
                    if(!empty($combosheet->id)) {
                        $combosheet->unit = $unit[$key];
                        $combosheet->rate = $rate[$key];
                        $combosheet->formula = $formula[$key];
                        $combosheet->final_rate = $result[$key];
                        $combosheet->remark = $remark[$key];
                        $combosheet->save();
                    }
                }
            }
        }
        $rsrate = Combos::where('resource_id',$resourceid)->sum('final_rate');
         Resource::where('id',$resourceid)->update(['rate'=>$rsrate]);
        $ret = array();
        $ret['status'] = 'success';
        $ret['resourceamt'] = $rsrate;
        return json_encode($ret);
    }
    public function sheetcalculate(Request $request)
    {
        $ret = array();
        $str = $request->str . $request->applied;
        if (!ctype_digit($str[0]) && !ctype_alpha($str[0])) {
            $wastepercent = 0;
             $strlen = strlen($str);
                if(strpos($str, 'Waste(') !== false){
                    $wastepost =  strpos( $str,'Waste(');
                    if($strlen-9>$wastepost){
                        $ret['status'] = 'error';
                        $ret['result'] = '';
                        $ret['message'] = 'Waste Function should be in last position';
                        return json_encode($ret);
                    }else{
                        $wstg = substr($str,-9);
                        preg_match('#\((.*?)\)#', $wstg, $matchstr);
                        $wastepercent = $matchstr[1];
                        $str = substr($str,0,-9);
                    }
                }
                $str = $this->mathFunction($str);
                $pattern = '/([\/][a-zA-Z])\w/';
                $patternx = '/[a-z]/i';
                $nstr = preg_replace($pattern, '', $str);
                $nstr = preg_replace($patternx, '', $nstr);
                //dd(''.$request->rate.$nstr.')');
                $cal = new FormulaParser('' . $request->rate . $nstr, 2);
                $result = $cal->getResult();
                $ret['status'] = $result[0];
                if ($result[0] != 'done') {
                    $ret['message'] = 'Formula has some invalid expression or character.';
                } else {
                    $finalrate = $result[1];
                    if($wastepercent){
                        $finalrate += ($result[1]/100)*$wastepercent;
                    }
                    $result = number_format($finalrate, 2, '.', '');
                    /** @var update combo sheet here $resourceid */
                    $ratesheetid = $request->ratesheetid;
                    $productcostitem = $request->productcostitem;
                    if(!empty($ratesheetid)){
                        $resourcedata = ProjectCostItemsResourceRate::where('id',$ratesheetid)->first();
                        $resourcedata->rate  = $request->rate;
                        $resourcedata->unit  = $request->unit;
                        $resourcedata->formula  = $request->str;
                        $resourcedata->final_rate  = $result;
                        $resourcedata->save();
                    }
                    $resourcefinalrate = ProjectCostItemsResourceRate::where('project_cost_item',$productcostitem)->sum('final_rate');
                    if($resourcefinalrate){
                        ProjectCostItemsProduct::where('id',$productcostitem)->update(['rate'=>$resourcefinalrate]);
                    }
                    $ret['ratesheetid'] = $ratesheetid;
                    $ret['result'] = $result;
                    $ret['costitemrate'] = $resourcefinalrate;

                }
        } else {
            $ret['status'] = 'error';
            $ret['result'] = '';
            $ret['message'] = 'First Character of formula field must be an operator i.e +, -, *, /, ^ etc.';
        }

        return json_encode($ret);
    }
    public function calculateold(Request $request)
    {
        $ret = array();
        $str = $request->str . $request->applied;
        if (!ctype_digit($str[0]) && !ctype_alpha($str[0])) {
            $pattern = '/([\/][a-zA-Z])\w/';
            $patternx = '/[a-z]/i';
            $nstr = preg_replace($pattern, '', $str);
            $nstr = preg_replace($patternx, '', $nstr);
            //dd(''.$request->rate.$nstr.')');
            $cal = new FormulaParser('' . $request->rate . $nstr, 2);
            $result = $cal->getResult();
            $ret['status'] = $result[0];
            if ($result[0] != 'done') {
                $ret['message'] = 'Formula has some invalid expression or character.';
            } else {
                $ret['result'] = number_format($result[1], 2, '.', '');
            }
        } else {
            $ret['status'] = 'error';
            $ret['result'] = '';
            $ret['message'] = 'First Character of formula field must be an operator i.e +, -, *, /, ^ etc.';
        }

        return json_encode($ret);
    }

    public function validateFormula(Request $request)
    {
        $ret = array();
        $str = '1'.$request->str;
        if (!ctype_digit($str[1]) && !ctype_alpha($str[1])) {
            $str = $this->mathFunction($str);
            $pattern = '/([\/][a-zA-Z])\w/';
            $patternx = '/[a-z]/i';
            $nstr = preg_replace($pattern, '', $str);
            $nstr = preg_replace($patternx, '', $nstr);
            //dd(''.$request->rate.$nstr.')');
            $cal = new FormulaParser('' . $nstr, 2);
            $result = $cal->getResult();
            $ret['status'] = $result[0];
        } else {
            $ret['status'] = 'error';
        }

        return json_encode($ret);
    }
    private function mathFunction($ratestring){
        if($ratestring){
            $stringarray = preg_split("/[?&@#%+*\-\/]/", $ratestring);
            foreach($stringarray as $stringv){
                preg_match('#\((.*?)\)#', $stringv, $match);
                if(!empty($match[1])){
                    $matchstring = $match[1];
                    if(strpos(trim($stringv), 'Min(') !== false) {
                        $val = getMin($matchstring);
                    }elseif (strpos(trim($stringv), 'Max(') !== false) {
                        $val = getMin($matchstring);
                    }elseif (strpos(trim($stringv), 'Sum(') !== false) {
                        $val = getSum($matchstring);
                    }elseif (strpos(trim($stringv), 'Average(') !== false) {
                        $val = getAverage($matchstring);
                    }elseif (strpos(trim($stringv), 'Abs(') !== false) {
                        $val = getAbs($matchstring);
                    }elseif (strpos(trim($stringv), 'Sqrt(') !== false) {
                        $val = getSqrt($matchstring);
                    }elseif (strpos(trim($stringv), 'Sqrt(') !== false) {
                        $val = getSqrt($matchstring);
                    }elseif (strpos(trim($stringv), 'Round(') !== false) {
                        $val = getRound($matchstring);
                    }elseif (strpos(trim($stringv), 'RoundX(') !== false) {
                        $val = getRoundX($matchstring);
                    }elseif (strpos(trim($stringv), 'RoundUp(') !== false) {
                        $val = getRoundUp($matchstring);
                    }elseif (strpos(trim($stringv), 'RoundUpX(') !== false) {
                        $val = getRoundUpX($matchstring);
                    }elseif (strpos(trim($stringv), 'RoundDown(') !== false) {
                        $val = getRoundDown($matchstring);
                    }elseif (strpos(trim($stringv), 'RoundDownX(') !== false) {
                        $val = getRoundDownX($matchstring);
                    }elseif (strpos(trim($stringv), 'Ceiling(') !== false) {
                        $val = getCeil($matchstring);
                    }elseif (strpos(trim($stringv), 'Floor(') !== false) {
                        $val = getFloor($matchstring);
                    }elseif (strpos(trim($stringv), 'SinDeg(') !== false) {
                        $val = getSinDeg($matchstring);
                    }elseif (strpos(trim($stringv), 'CosDeg(') !== false) {
                        $val = getCosDeg($matchstring);
                    }elseif (strpos(trim($stringv), 'TanDeg(') !== false) {
                        $val = getTanDeg($matchstring);
                    }elseif (strpos(trim($stringv), 'AreaCir(') !== false) {
                        $val = getAreaCir($matchstring);
                    }elseif (strpos(trim($stringv), 'AreaTri(') !== false) {
                        $val = getAreaTri($matchstring);
                    }elseif (strpos(trim($stringv), 'AreaPyr(') !== false) {
                        $val = getAreaPyr($matchstring);
                    }elseif (strpos(trim($stringv), 'AreaRect(') !== false) {
                        $val = getAreaRect($matchstring);
                    }elseif (strpos(trim($stringv), 'AreaCyl(') !== false) {
                        $val = getAreaCyl($matchstring);
                    }elseif (strpos(trim($stringv), 'AreaCone(') !== false) {
                        $val = getAreaCone($matchstring);
                    }elseif (strpos(trim($stringv), 'AreaSph(') !== false) {
                        $val = getAreaSph($matchstring);
                    }elseif (strpos(trim($stringv), 'PerimCir(') !== false) {
                        $val = getPerimCir($matchstring);
                    }elseif (strpos(trim($stringv), 'PerimTriR(') !== false) {
                        $val = getPerimTriR($matchstring);
                    }elseif (strpos(trim($stringv), 'PerimRect(') !== false) {
                        $val = getPerimRec($matchstring);
                    }elseif (strpos(trim($stringv), 'VolPyr(') !== false) {
                        $val = getVolPyr($matchstring);
                    }elseif (strpos(trim($stringv), 'VolCyl(') !== false) {
                        $val = getVolCyl($matchstring);
                    }elseif (strpos(trim($stringv), 'VolCone(') !== false) {
                        $val = getVolCone($matchstring);
                    }elseif (strpos(trim($stringv), 'VolSph(') !== false) {
                        $val = getVolSph($matchstring);
                    }
                    $ratestring = str_replace($stringv,$val,$ratestring);
                }
            }
        }
        return $ratestring;
    }
}
