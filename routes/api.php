<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Api for RFI
Route::post('projects/get-rfi', ['uses' => '\App\Http\Controllers\Api\ApiController@getRfi']);
//Api for App
Route::post('homepage', ['uses' => '\App\Http\Controllers\Api\AppApiController@homePage'])->middleware('guest');
Route::post('app-login', ['uses' => '\App\Http\Controllers\Api\AppApiController@appLogin'])->middleware('guest');
Route::post('app-register', ['uses' => '\App\Http\Controllers\Api\AppApiController@appRegister'])->middleware('guest');
Route::post('forgot-password', ['uses' => '\App\Http\Controllers\Api\AppApiController@forgotPassword'])->middleware('guest');
Route::post('verify-user', ['uses' => '\App\Http\Controllers\Api\AppApiController@verifyUser'])->middleware('guest');
Route::post('verify-otp', ['uses' => '\App\Http\Controllers\Api\AppApiController@verifyOtp'])->middleware('guest');
Route::post('resend-otp', ['uses' => '\App\Http\Controllers\Api\AppApiController@resendOTP'])->middleware('guest');
Route::post('resend-reg-otp', ['uses' => '\App\Http\Controllers\Api\AppApiController@resendRegOTP'])->middleware('guest');
Route::post('reset-password', ['uses' => '\App\Http\Controllers\Api\AppApiController@resetPassword'])->middleware('guest');
Route::post('youtube-links', ['uses' => '\App\Http\Controllers\Api\AppApiController@youtubeLinks'])->middleware('guest');
Route::post('app-logs', ['uses' => '\App\Http\Controllers\Api\AppApiController@appLogs'])->middleware('guest');

Route::post('users-list', ['uses' => '\App\Http\Controllers\Api\AppUserController@usersList'])->middleware('guest');
Route::post('modules-list', ['uses' => '\App\Http\Controllers\Api\AppUserController@modulesList'])->middleware('guest');
Route::post('modules-by-name', ['uses' => '\App\Http\Controllers\Api\AppUserController@modulesByName'])->middleware('guest');
Route::post('package-modules-list', ['uses' => '\App\Http\Controllers\Api\AppUserController@modulesListByPackage']);


Route::post('employee-register', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@employeeRegister']);
Route::post('employee-list', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@employeeList']);
Route::post('project-employee-list', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@projectEmployeeList']);
Route::post('add-employee-to-project', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@addEmployeetoProject']);
Route::post('share-project', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@shareProject']);
Route::post('employee-details', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@employeeDetails']);
Route::post('employee-update', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@employeeUpdate']);
Route::post('employee-job-update', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@employeeJobUpdate']);
Route::post('delete-employee', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@deleteEmployee']);
Route::post('remove-project-employee', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@removeProjectEmployee']);
Route::post('remove-projects-by-employee', ['uses' => '\App\Http\Controllers\Api\AppEmployeeController@removeProjectsByEmployee']);


Route::post('app-projects-category', ['uses' => '\App\Http\Controllers\Api\AppProjectController@appProjectCategory']);
Route::post('projects-category-details', ['uses' => '\App\Http\Controllers\Api\AppProjectController@projectDetailsCategory']);
Route::post('create-projects-category', ['uses' => '\App\Http\Controllers\Api\AppProjectController@createProjectCategory']);
Route::post('delete-projects-category', ['uses' => '\App\Http\Controllers\Api\AppProjectController@deleteProjectCategory']);
Route::post('get-project', ['uses' => '\App\Http\Controllers\Api\AppProjectController@getProject']);
Route::post('get-projects', ['uses' => '\App\Http\Controllers\Api\AppProjectController@getProjects']);
Route::post('timeline-logs', ['uses' => '\App\Http\Controllers\Api\AppProjectController@timeLineLogs']);
Route::post('mention-logs', ['uses' => '\App\Http\Controllers\Api\AppProjectController@mentionLogs']);


Route::post('sub-projects-list', ['uses' => '\App\Http\Controllers\Api\AppProjectController@subProjectsList']);
Route::post('create-sub-projects', ['uses' => '\App\Http\Controllers\Api\AppProjectController@createSubProjects']);
Route::post('delete-sub-projects', ['uses' => '\App\Http\Controllers\Api\AppProjectController@deleteSubProjects']);


Route::post('segments-list', ['uses' => '\App\Http\Controllers\Api\AppProjectController@segmentsList']);
Route::post('create-segments', ['uses' => '\App\Http\Controllers\Api\AppProjectController@createSegments']);
Route::post('delete-segments', ['uses' => '\App\Http\Controllers\Api\AppProjectController@deleteSegments']);
Route::post('project-status-list', ['uses' => '\App\Http\Controllers\Api\AppProjectController@projectStatusList']);


Route::post('app-projects', ['uses' => '\App\Http\Controllers\Api\AppProjectController@appProjects']);
Route::post('app-projects-companies', ['uses' => '\App\Http\Controllers\Api\AppProjectController@appProjectsCompanies']);
Route::post('projects-details', ['uses' => '\App\Http\Controllers\Api\AppProjectController@projectDetails']);
Route::post('create-projects', ['uses' => '\App\Http\Controllers\Api\AppProjectController@createProject']);
Route::post('delete-projects', ['uses' => '\App\Http\Controllers\Api\AppProjectController@deleteProject']);
Route::post('leave-projects', ['uses' => '\App\Http\Controllers\Api\AppProjectController@leaveProjects']);
Route::post('launch-projects-list', ['uses' => '\App\Http\Controllers\Api\AppProjectController@launchProjectLists']);
Route::post('get-employee-projects', ['uses' => '\App\Http\Controllers\Api\AppProjectController@getEmployeeProject']);
Route::post('get-projects-by-employee', ['uses' => '\App\Http\Controllers\Api\AppProjectController@getProjectsEmployee']);


Route::post('app-works', ['uses' => '\App\Http\Controllers\Api\AppWorkController@appWorks']);
Route::post('create-works', ['uses' => '\App\Http\Controllers\Api\AppWorkController@createWork']);
Route::post('delete-works', ['uses' => '\App\Http\Controllers\Api\AppWorkController@deleteWork']);

Route::post('app-costitems', ['uses' => '\App\Http\Controllers\Api\AppWorkController@appCostitems']);

//Api for Task
Route::post('tasks-list', ['uses' => '\App\Http\Controllers\Api\AppTaskController@appTasks']);
Route::post('create-tasks', ['uses' => '\App\Http\Controllers\Api\AppTaskController@createTask']);
Route::post('delete-tasks', ['uses' => '\App\Http\Controllers\Api\AppTaskController@deleteTask']);
Route::post('tasks-percentage', ['uses' => '\App\Http\Controllers\Api\AppTaskController@updatePercentage']);
Route::post('tasks-timelines', ['uses' => '\App\Http\Controllers\Api\AppTaskController@taskTimelines']);
Route::post('store-task-image', ['uses' => '\App\Http\Controllers\Api\AppTaskController@storeImage']);


Route::post('activitylist', ['uses' => '\App\Http\Controllers\Api\AppTaskController@activityList']);
Route::post('boqlist', ['uses' => '\App\Http\Controllers\Api\AppTaskController@boqList']);
Route::post('boq', ['uses' => '\App\Http\Controllers\Api\AppTaskController@boq']);
Route::post('boqupdate', ['uses' => '\App\Http\Controllers\Api\AppTaskController@boqUpdate']);
Route::post('costitemlist', ['uses' => '\App\Http\Controllers\Api\AppTaskController@costItemList']);
Route::post('task-status-list', ['uses' => '\App\Http\Controllers\Api\AppTaskController@taskStatusList']);
Route::post('progress-report', ['uses' => '\App\Http\Controllers\Api\AppTaskController@progressReport']);
Route::post('progress-report-export', ['uses' => '\App\Http\Controllers\Api\AppTaskController@progressReportExport']);


//Api for punch item
Route::post('punch-item-list', ['uses' => '\App\Http\Controllers\Api\AppPunchitemController@appPunchitem']);
Route::post('create-punch-item', ['uses' => '\App\Http\Controllers\Api\AppPunchitemController@createPunchitem']);
Route::post('delete-punch-item', ['uses' => '\App\Http\Controllers\Api\AppPunchitemController@deletePunchitem']);
Route::post('get-type', ['uses' => '\App\Http\Controllers\Api\AppPunchitemController@getType']);
Route::post('store-punch-item-image', ['uses' => '\App\Http\Controllers\Api\AppPunchitemController@storeImage']);
Route::post('punch-item-reply', ['uses' => '\App\Http\Controllers\Api\AppPunchitemController@replyPost']);
Route::post('punch-item-reply-list', ['uses' => '\App\Http\Controllers\Api\AppPunchitemController@replyList']);
Route::post('punch-status-list', ['uses' => '\App\Http\Controllers\Api\AppPunchitemController@punchStatusList']);
Route::post('get-punch-item', ['uses' => '\App\Http\Controllers\Api\AppPunchitemController@getPunchItem']);
Route::post('punch-item-status', ['uses' => '\App\Http\Controllers\Api\AppPunchitemController@punchItemStatus']);


//Api for manpower
Route::post('man-power-list', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@appManpowerLog']);
Route::post('man-power', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@manpowerLog']);
Route::post('create-man-power', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@createManpowerLog']);
Route::post('delete-man-power', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@deleteManpowerLog']);
Route::post('store-man-power-image', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@storeImage']);
Route::post('manpower-reply', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@replyPost']);
Route::post('manpower-reply-list', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@replyList']);

//Api for category
Route::post('man-power-category-list', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@appManpowerCat']);
Route::post('create-man-power-category', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@createManpowerCat']);
Route::post('delete-man-power-category', ['uses' => '\App\Http\Controllers\Api\AppManpowerlogController@deleteManpowerCat']);

//store apis
Route::post('units-list', ['uses' => '\App\Http\Controllers\Api\AppStoreController@unitsList']);
Route::post('indents-list', ['uses' => '\App\Http\Controllers\Api\AppStoreController@indentsList']);
Route::post('store-indents-image', ['uses' => '\App\Http\Controllers\Api\AppStoreController@storeImage']);
Route::post('indents-reply', ['uses' => '\App\Http\Controllers\Api\AppStoreController@replyPost']);
Route::post('indents-reply-list', ['uses' => '\App\Http\Controllers\Api\AppStoreController@replyList']);
Route::post('product-list', ['uses' => '\App\Http\Controllers\Api\AppStoreController@productList']);
Route::post('create-unit', ['uses' => '\App\Http\Controllers\Api\AppStoreController@createUnit']);
Route::post('create-product', ['uses' => '\App\Http\Controllers\Api\AppStoreController@createProduct']);
Route::post('create-indent', ['uses' => '\App\Http\Controllers\Api\AppStoreController@createIndent']);
Route::post('indent-products', ['uses' => '\App\Http\Controllers\Api\AppStoreController@indentProducts']);
Route::post('update-unit', ['uses' => '\App\Http\Controllers\Api\AppStoreController@updateUnit']);
Route::post('delete-unit', ['uses' => '\App\Http\Controllers\Api\AppStoreController@deleteUnit']);
Route::post('update-product', ['uses' => '\App\Http\Controllers\Api\AppStoreController@updateProduct']);
Route::post('delete-product', ['uses' => '\App\Http\Controllers\Api\AppStoreController@deleteProduct']);
Route::post('delete-indent', ['uses' => '\App\Http\Controllers\Api\AppStoreController@deleteIndent']);
Route::post('delete-indent-product', ['uses' => '\App\Http\Controllers\Api\AppStoreController@deleteIndentProduct']);
Route::post('update-indent-product', ['uses' => '\App\Http\Controllers\Api\AppStoreController@updateIndentProduct']);
Route::post('stores-list', ['uses' => '\App\Http\Controllers\Api\AppStoreController@storesList']);
Route::post('create-store', ['uses' => '\App\Http\Controllers\Api\AppStoreController@createStore']);
Route::post('update-store', ['uses' => '\App\Http\Controllers\Api\AppStoreController@updateStore']);
Route::post('delete-store', ['uses' => '\App\Http\Controllers\Api\AppStoreController@deleteStore']);
Route::post('get-store', ['uses' => '\App\Http\Controllers\Api\AppStoreController@getStore']);
Route::post('update-indent-status', ['uses' => '\App\Http\Controllers\Api\AppStoreController@updateIndentStatus']);


//members apis
Route::post('add-client', ['uses' => '\App\Http\Controllers\Api\AppMemberController@addClient']);
Route::post('update-client', ['uses' => '\App\Http\Controllers\Api\AppMemberController@updateClient']);
Route::post('delete-client', ['uses' => '\App\Http\Controllers\Api\AppMemberController@deleteClient']);
Route::post('add-contractor', ['uses' => '\App\Http\Controllers\Api\AppMemberController@addContractor']);
Route::post('update-contractor', ['uses' => '\App\Http\Controllers\Api\AppMemberController@updateContractor']);
Route::post('delete-contractor', ['uses' => '\App\Http\Controllers\Api\AppMemberController@deleteContractor']);
Route::post('add-user', ['uses' => '\App\Http\Controllers\Api\AppMemberController@addEmployee']);
Route::post('update-user', ['uses' => '\App\Http\Controllers\Api\AppMemberController@updateEmployee']);
Route::post('delete-user', ['uses' => '\App\Http\Controllers\Api\AppMemberController@deleteEmployee']);
Route::post('get-client', ['uses' => '\App\Http\Controllers\Api\AppMemberController@getClient']);
Route::post('get-contractor', ['uses' => '\App\Http\Controllers\Api\AppMemberController@getContractor']);
Route::post('get-employee', ['uses' => '\App\Http\Controllers\Api\AppMemberController@getEmployee']);
Route::post('employee-search', ['uses' => '\App\Http\Controllers\Api\AppMemberController@employeeSearch']);
Route::post('client-search', ['uses' => '\App\Http\Controllers\Api\AppMemberController@clientSearch']);
Route::post('contractor-search', ['uses' => '\App\Http\Controllers\Api\AppMemberController@contractorSearch']);
Route::post('delete-member', ['uses' => '\App\Http\Controllers\Api\AppMemberController@deleteMember']);
Route::post('update-user-profile', ['uses' => '\App\Http\Controllers\Api\AppMemberController@updateUserProfile']);
Route::post('get-user-profile', ['uses' => '\App\Http\Controllers\Api\AppMemberController@getUserProfile']);
Route::post('get-members', ['uses' => '\App\Http\Controllers\Api\AppMemberController@getMembers']);
Route::post('save-permission', ['uses' => '\App\Http\Controllers\Api\AppMemberController@savePermission']);
Route::post('remove-member', ['uses' => '\App\Http\Controllers\Api\AppMemberController@removeMember']);
Route::post('send-notification', ['uses' => '\App\Http\Controllers\Api\AppMemberController@sendNotification']);

Route::post('invite-member', ['uses' => '\App\Http\Controllers\Api\AppMemberController@inviteMember']);
Route::post('add-noninvited-member', ['uses' => '\App\Http\Controllers\Api\AppMemberController@addNoninvitedMember']);


//Permission Role APIs
Route::post('check-permission', ['uses' => '\App\Http\Controllers\Api\AppApiController@checkPermission']);
Route::post('get-all-permissions', ['uses' => '\App\Http\Controllers\Api\AppApiController@getAllPermissions']);
Route::post('get-permissions-by-module', ['uses' => '\App\Http\Controllers\Api\AppApiController@getPermissionsByModule']);
Route::post('get-all-user-permissions', ['uses' => '\App\Http\Controllers\Api\AppApiController@getAllUserPermissions']);
Route::post('get-permission-by-user', ['uses' => '\App\Http\Controllers\Api\AppApiController@getPermissionByUser']);
Route::post('get-projects-check-permission', ['uses' => '\App\Http\Controllers\Api\AppApiController@getProjectCheckPermission']);
Route::post('get-roles', ['uses' => '\App\Http\Controllers\Api\AppMemberController@getRoles']);
Route::post('get-permissions-by-role', ['uses' => '\App\Http\Controllers\Api\AppMemberController@getPermissionsByRole']);
Route::post('save-permissions-by-role', ['uses' => '\App\Http\Controllers\Api\AppMemberController@savePermissionsByRole']);
Route::post('get-projects-permissions-roles', ['uses' => '\App\Http\Controllers\Api\AppMemberController@getProjectsPermissionsRoles']);

Route::post('folder-list', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@folderLists']);
Route::post('photos-list', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@photosLists']);
Route::post('move-pathtype', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@movePathType']);
Route::post('store-link', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@storeLink']);
Route::post('create-folder', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@createFolder']);
Route::post('edit-folder', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@editFolder']);
Route::post('download-photo', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@download']);
Route::post('multiple-upload', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@storeMultiple']);
Route::post('photos-store', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@photosStore']);
Route::post('update-filemanagername', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@updateFileManagerName']);
Route::post('delete-photo', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@deletePhoto']);
Route::post('delete-folder', ['uses' => '\App\Http\Controllers\Api\AppPhotosController@deleteFolder']);

Route::post('channel-list', ['uses' => '\App\Http\Controllers\Api\AppChannelController@appChannels']);
Route::post('create-channel', ['uses' => '\App\Http\Controllers\Api\AppChannelController@createChannel']);
Route::post('update-channel', ['uses' => '\App\Http\Controllers\Api\AppChannelController@updateChannel']);
Route::post('delete-channel', ['uses' => '\App\Http\Controllers\Api\AppChannelController@deleteChannel']);
Route::post('channel-users-list', ['uses' => '\App\Http\Controllers\Api\AppChannelController@channelUsersList']);
Route::post('add-channel-members', ['uses' => '\App\Http\Controllers\Api\AppChannelController@addChannelMembers']);
Route::post('remove-channel-members', ['uses' => '\App\Http\Controllers\Api\AppChannelController@removeChannelMembers']);
Route::post('set-channel-admin', ['uses' => '\App\Http\Controllers\Api\AppChannelController@setChannelAdmin']);
