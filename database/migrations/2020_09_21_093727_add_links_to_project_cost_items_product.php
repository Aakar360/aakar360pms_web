<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLinksToProjectCostItemsProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_cost_items_product', function (Blueprint $table) {
            $table->string('assign_to')->nullable();
            $table->string('target')->nullable();
            $table->string('type')->nullable();
            $table->string('linked')->nullable();
            $table->string('baselinedate')->nullable();
            $table->string('baselinelabel')->nullable();
            $table->string('planned_start')->nullable();
            $table->string('planned_end')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_cost_items_product', function (Blueprint $table) {
            $table->dropColumn(['assign_to']);
            $table->dropColumn(['target']);
            $table->dropColumn(['type']);
            $table->dropColumn(['linked']);
            $table->dropColumn(['baselinedate']);
            $table->dropColumn(['baselinelabel']);
            $table->dropColumn(['planned_start']);
            $table->dropColumn(['planned_end']);
        });
    }
}
