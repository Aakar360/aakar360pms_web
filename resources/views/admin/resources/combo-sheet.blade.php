@push('head-script')

@endpush

@section('content')
<style>
    .RowResource{
        width: 255px;
    }
    .RowRate{
        width: 90px;
    }
    .RowUnitx{
        width: 90px;
    }
    .RowFormula{
        width: 255px;
    }
    .RowResult{
        width: 100px;
    }
    .close-combo-sheet{
        font-size: 16px;
    }
    .formula-sheet .cell-inp[data-col="2"]{
        text-align: right;
    }
    .combo-sheet td[data-col="0"] {
        text-align: center;
    }
    .formula-sheet thead th{
        font-size: 12px;
        background-color: #C2DCE8;
        color: #3366CC;
        padding: 3px 5px !important;
        text-align: center;
        position: sticky;
        top: -1px;
        z-index: 1;

    &:first-of-type {
         left: 0;
         z-index: 3;
     }
    }
</style>
<form method="post" autocomplete="off" id="combo-sheet-form">
    @csrf
    <div class="row pdl10">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12" style="padding: 5px 15px;">
            <a href="javascript:;" class="close-combo-sheet pull-right"><i class="fa fa-times"></i> </a>
            <div class="col-md-3 pull-right">
                <select class="form-control selectfunction"  >
                    <option value="">Functions</option>
                    <option value="Waste()">Waste(n)%</option>
                    <option value="">---------------</option>
                    <option value="Min()">Min()</option>
                    <option value="Max()">Max()</option>
                    <option value="Sum()">Sum()</option>
                    <option value="Average()">Average()</option>
                    <option value="Abs()">Abs()</option>
                    <option value="Sqrt()">Sqrt()</option>
                    <option value="">---------------</option>
                    <option value="Round()">Round(n)</option>
                    <option value="RoundX()">Round(n;x)</option>
                    <option value="RoundUp()">RoundUp(n)</option>
                    <option value="RoundUpX()">RoundUp(n;x)</option>
                    <option value="RoundDown()">RoundDown(n)</option>
                    <option value="RoundDownX()">RoundDown(n;x)</option>
                    <option value="Ceiling()">Ceiling(n;x)</option>
                    <option value="Floor()">Floor(n;x)</option>
                    <option value="">---------------</option>
                    <option value="SinDeg()">SinDeg(n)</option>
                    <option value="CosDeg()">CosDeg(n)</option>
                    <option value="TanDeg()">TanDeg(n)</option>
                    <option value="">---------------</option>
                    <option value="AreaCir()">AreaCir(r)</option>
                    <option value="AreaTri()">AreaTri(b,h)</option>
                    <option value="AreaPyr()">AreaPyr(l,b,h)</option>
                    <option value="AreaRect()">AreaRect(x,y)</option>
                    <option value="AreaCyl()">AreaCyl(r,h)</option>
                    <option value="AreaCone()">AreaCone(r,h)</option>
                    <option value="AreaSph()">AreaSph(r)</option>
                    <option value="">---------------</option>
                    <option value="PerimCir()">PerimCir(r)</option>
                    <option value="PerimTriR()">PerimTriR(b,h)</option>
                    <option value="PerimRect()">PerimRect(x,y)</option>
                    <option value="">---------------</option>
                    <option value="VolPyr()">VolPyr(l,b,h)</option>
                    <option value="VolCyl()">VolCyl(r,h)</option>
                    <option value="VolCone()">VolCone(r,h)</option>
                    <option value="VolSph()">VolSph(r)</option>
                </select>
            </div>
            <div class="col-md-1 pull-right">
                <button class="btn btn-primary" id="combo-sheet-submit" >Save</button>
            </div>
        </div>
        <!-- /.breadcrumb -->
    </div>
    <div class="table-wrapper">
        <table class="table table-bordered default footable-loaded footable" id="combo-table" data-resizable-columns-id="users-table">
                @if(isset($resourceId))
                    <input type="hidden" class="resource_id" name="resource_id" value="{{ $resourceId }}"/>
                @endif
                <thead>
                    <tr>
                        <th class="RowRemove" data-resizable-column-id="unit" style="width: 2%;"></th>
                        <th class="RowResource" data-resizable-column-id="resource">@lang('modules.resources.resource')</th>
                        <th class="RowRate" data-resizable-column-id="rate">@lang('modules.resources.rate')</th>
                        <th class="RowUnitx" data-resizable-column-id="unitx">@lang('modules.resources.unit')</th>
                        <th class="RowFormula" data-resizable-column-id="formula">@lang('modules.resources.formula')</th>
                        <th class="RowResult" data-resizable-column-id="unit">@lang('modules.resources.result')</th>
                        <th class="RowRemark" data-resizable-column-id="unit">@lang('modules.resources.remark')</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $res_count = count($combos);
                        $totalRow = 15 + $res_count;
                        if($totalRow <= 1){
                            $totalRow = 2;
                        }
                        $row = 1;
                        $totalamt = 0;
                    ?>
                    @foreach($combos as $combo)
                        <?php
                        $totalamt += $combo->final_rate;
                        ?>
                        <tr class="resource_list" data-row="{{$row}}" data-id="{{ $combo->id }}">
                            <td data-col="0"><a href="javascript:void(0);" data-col="0" data-row="{{$row}}" class="removecombosheet" ><i class="fa fa-trash"></i></a></td>
                            <td data-col="1">
                                <span class="cubeicon" data-col="1" data-row="{{$row}}">@if(get_resource_category($combo->combo_resource_id)==0)<i class="fa fa-square"></i> @else <i class="fa fa-th-large"></i> @endif</span>
                                <input  list="resourcelist{{$row}}" data-row="{{$row}}" class="cell-inp" value="{{ get_resource_name($combo->combo_resource_id) }}"  />
                                <datalist id="resourcelist{{$row}}">
                                    @foreach($resources as $resource)
                                        <?php
                                        $type = getResourceType($resource->id);
                                        ?>
                                        <option value="{{$resource->id}}" @if($combo->combo_resource_id == $resource->id) selected @endif>{{ $type }} - {{ $resource->description }}</option>
                                    @endforeach
                                </datalist>
                                <input type="hidden" name="resource[]" value="{{ $combo->combo_resource_id }}" data-col="1" data-row="{{$row}}" class="cell-inp resourceList-hidden" />
                                <input type="hidden" name="sheetid[]" value="{{ $combo->id }}" data-row="{{$row}}" class="cell-inp sheetid" />
                             </td>
                            <td data-col="2"><input type="text" name="rate[]" data-col="2" data-row="{{$row}}" class="cell-inp cell-new" value="{{ $combo->rate }}" disabled="disabled"></td>
                            <td data-col="3"><input type="text" name="unit[]" data-col="3" data-row="{{$row}}" class="cell-inp cell-new" value="{{ $combo->unit }}" disabled="disabled"></td>
                            <td data-col="4"><input type="text" name="formula[]" data-col="4" data-row="{{$row}}" class="cell-inp cell-new" value="{{ $combo->formula }}"/></td>
                            <td data-col="5"><input type="text" name="result[]" data-col="5" data-row="{{$row}}" class="cell-inp cell-new" value="{{ $combo->final_rate }}" disabled="disabled"></td>
                            <td data-col="6"><input type="text" name="remark[]" data-col="6" data-row="{{$row}}" class="cell-inp cell-new" value="{{ $combo->remark }}"></td>
                        </tr>
                        <?php $row++ ?>
                    @endforeach
                    @for($i=0;$i<$totalRow;$i++)
                        @if($i==0)
                            <tr data-row="{{$row}}" class="resource_list">
                                <td data-col="0"><a href="javascript:;" class="removecombosheet" data-col="0" data-row="{{$row}}"  ></a></td>
                                <td data-col="1">
                                    <span class="cubeicon" data-col="1" data-row="{{$row}}"></span>
                                    <input  list="resourcelist{{$row}}" data-col="1" class="cell-inp resourceList" data-row="{{$row}}"  />
                                    <datalist id="resourcelist{{$row}}">
                                        @foreach($resources as $resource)
                                            <?php
                                            $type = getResourceType($resource->id);
                                            ?>
                                            <option value="{{$resource->id}}" >{{ $type }} - {{ $resource->description }}</option>
                                        @endforeach
                                    </datalist>
                                    <input type="hidden" name="resource[]" data-col="1" data-row="{{$row}}" class="cell-inp resourceList-hidden" />
                                    <input type="hidden" name="sheetid[]"  data-row="{{$row}}" class="cell-inp sheetid" />
                                </td>
                                <td data-col="2"><input type="text"  name="rate[]" data-col="2" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"></td>
                                <td data-col="3"><input type="text" name="unit[]"  data-col="3" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"></td>
                                <td data-col="4"><input type="text" name="formula[]"  data-col="4" data-row="{{$row}}" class="cell-inp cell-new" value=""/></td>
                                <td data-col="5"><input type="text"  name="result[]"  data-col="5" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"></td>
                                <td data-col="6"><input type="text"  name="remark[]"  data-col="6" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"></td>
                            </tr>
                        @elseif($i==1)
                            <tr data-row="{{$row}}" class="final_result">
                                <td data-col="0"><a href="javascript:;" class="removecombosheet" data-col="0" data-row="{{$row}}"  ></a></td>
                                <td data-col="1"><input type="text"  data-col="1" data-row="{{$row}}" class="cell-inp cell-new"></td>
                                <td data-col="2"><input type="text" name="rate[]"  data-col="2" data-row="{{$row}}" class="cell-inp cell-new" value="= APPLIED FACTOR" disabled="disabled"></td>
                                <td data-col="3"><input type="text"  name="unit[]" data-col="3" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"></td>
                                <td data-col="4"><input type="text" name="formula[]" data-col="4" data-row="{{$row}}" class="cell-inp cell-new" value=""/></td>
                                <td data-col="5"><input type="text" name="result[]" data-col="5" data-row="{{$row}}" class="cell-inp cell-new" value="{{ number_format($totalamt, 2, '.', '') }}" disabled="disabled"></td>
                                <td data-col="6"><input type="text" name="remark[]" data-col="6" data-row="{{$row}}" class="cell-inp cell-new"  disabled="disabled"></td>
                            </tr>
                        @else
                            <tr data-row="{{$row}}" class="extra_rows">
                                <td data-col="0"><a href="javascript:;" class="removecombosheet" data-col="0" data-row="{{$row}}"  ></a></td>
                                <td data-col="1"><input type="text" data-col="1" data-row="{{$row}}" class="cell-inp cell-new"></td>
                                <td data-col="2"><input type="text" name="rate[]" data-col="2" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"></td>
                                <td data-col="3"><input type="text" name="unit[]" data-col="3" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"></td>
                                <td data-col="4"><input type="text" name="formula[]" data-col="4" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"/></td>
                                <td data-col="5"><input type="text"  name="result[]" data-col="5" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"></td>
                                <td data-col="6"><input type="text"  name="remark[]" data-col="6" data-row="{{$row}}" class="cell-inp cell-new" value="" disabled="disabled"></td>
                             </tr>
                        @endif
                        <?php
                            $row++;
                        ?>
                    @endfor
                </tbody>
        </table>
    </div>
</form>
<script>
    "use strict";
    $(document).on("change",".resourceList",function () {
        var val =$(this).val();
        var row = $(this).attr('data-row');
        var  list = $(this).attr('list');
        var options = $('#' + list + ' option[value="'+val+'"]');
        $('.resourceList-hidden[data-row='+row+']').val(val);
        if (options.length > 0) {
            $(this).val(options[0].innerText);
        }
    });
    /* document.querySelector('.resourceList').addEventListener('input', function(e) {
        var input = e.target,
            row = input.getAttribute('data-row'),
            list = input.getAttribute('list'),
            options = document.querySelectorAll('#' + list + ' option[value="'+input.value+'"]'),
            hiddenInput = document.getElementById(input.getAttribute('id') + '-hidden');
        if (options.length > 0) {
            hiddenInput.value = input.value;
            input.value = options[0].innerText;
        }
    });*/
    function calcTotal(){
        var total = 0;
        $('.formula-sheet tr.resource_list input.cell-inp[data-col=5]').each(function () {
            var inp = $(this).val();
            if(inp){
                total = total + parseFloat(inp);
            }
        });
        var resource = $(".resource_id").val();
        $('.cs table tr[data-id="'+resource+'"] td[data-col=5] input.cell-inp[data-col=5]').val(total.toFixed(2));
        $('.formula-sheet tr.final_result input.cell-inp[data-col=5]').val(total.toFixed(2));
    }
    $(document).on('change', '.formula-sheet .cell-inp[data-col=1]', function (e) {
        var inp = $(this);
        var row = inp.attr('data-row');
        var id = $(".resourceList-hidden[data-row="+row+"]").val();
        var resource = $(".resource_id").val();
        if(id != ''){
            $.ajax({
                url: '{{ route('admin.resources.getResource') }}',
                type: 'POST',
                data: {_token:'{{ csrf_token() }}', id: id, resource: resource},
                success: function(rData){
                    /*var rData = JSON.parse(data);*/
                    if(rData.category=='0'){
                        $('.cubeicon[data-row='+row+']').html('<i class="fa fa-square"></i>');
                    }
                    if(rData.category=='1'){
                        $('.cubeicon[data-row='+row+']').html('<i class="fa fa-th-large"></i>');
                    }
                    $('tr.resource_list[data-row='+row+']').attr('data-id',rData.id);
                    $('.formula-sheet .sheetid[data-row='+row+']').val(rData.id);
                    $('.formula-sheet .removecombosheet[data-col=0][data-row='+row+']').html("<i class='fa fa-trash'></i>");
                    $('.formula-sheet .cell-inp[data-row='+row+'][data-col=2]').val(rData.rate);
                    $('.formula-sheet .cell-inp[data-row='+row+'][data-col=3]').val(rData.unit);
                    $('.formula-sheet .cell-inp[data-row='+row+'][data-col=5]').val(rData.rate);
                    $('.formula-sheet .cell-inp[data-row='+row+'][data-col=6]').val(rData.remark).prop('disabled', false);
                    var newrow = parseInt(row)+1;
                    $('.formula-sheet tr[data-row=' + newrow + '] td[data-col=1] input').val('');
                    $('.formula-sheet tr[data-row=' + newrow + ']').removeClass('resource_list').addClass('final_result');
                    $('.formula-sheet tr[data-row=' + newrow + '] td[data-col=2] input').val('');
                    $('.formula-sheet tr[data-row=' + newrow + '] td[data-col=3] input').val('');
                    $('.formula-sheet tr[data-row=' + newrow + '] td[data-col=4] input').prop('disabled', false);
                    $('.formula-sheet tr[data-row=' + newrow + '] td[data-col=5] input').val('');
                    $('.formula-sheet tr[data-row=' + newrow + '] td[data-col=6] input').val('');

                  var catBox='<input list="resourcelist'+newrow+'" data-col="0" class="cell-inp resourceList" data-row="'+newrow+'" />\n' +
                                 '<datalist id="resourcelist'+newrow+'">';
                         @foreach($resources as $resource)
                            <?php  $type = getResourceType($resource->id); ?>
                       catBox +='<option value="{{$resource->id}}" >{{ $type }} - {{ $resource->description }}</option>';
                             @endforeach
                     catBox += '</datalist>' +
                        '<input type="hidden" name="resource[]"  data-col="0" data-row="'+newrow+'" class="cell-inp resourceList-hidden">' +
                                 '<input type="hidden" name="sheetid[]"  data-col="0" data-row="'+newrow+'" class="cell-inp sheetid" />';

                    $('.formula-sheet tr[data-row=' + newrow + '] td[data-col=1]').html(catBox);
                    var nextRow = parseInt(newrow) + 1;
                        if(!$('.formula-sheet tr[data-row='+nextRow+']').hasClass('resource_list')) {
                            $('.formula-sheet tr[data-row=' + nextRow + '] td[data-col=1] input').val('');
                            $('.final_result').removeClass('final_result').addClass('resource_list');
                            $('.formula-sheet tr[data-row=' + nextRow + ']').removeClass('extra_rows');
                            $('.formula-sheet tr[data-row=' + nextRow + ']').removeClass('resource_list');
                            $('.formula-sheet tr[data-row=' + nextRow + ']').addClass('final_result');
                            $('.formula-sheet tr[data-row=' + nextRow + '] td[data-col=2] input').val('= APPLIED FACTOR');
                            $('.formula-sheet tr[data-row=' + nextRow + '] td[data-col=4] input').prop('disabled', false).val('');
                        }else{
                            $('.formula-sheet tr[data-row=' + row + '] td[data-col=4] input').val('');
                            $('.formula-sheet tr[data-row=' + nextRow + '] td[data-col=4] input').prop('disabled', false).val($('.formula-sheet tr[data-row=' + row + '] td[data-col=4] input').val());
                        }
                    $('.formula-sheet tr[data-row=' + row + '] td[data-col=4] input').trigger('change');
                    calcTotal();
                }
            });
            e.stopImmediatePropagation();
            return false;
        }else{
            $('.formula-sheet .cell-inp[data-row='+row+'][data-col=1]').val('');
            $('.formula-sheet .cell-inp[data-row='+row+'][data-col=3]').val('');
        }
    });
    $(document).on('change', '.formula-sheet tr.resource_list input.cell-inp[data-col=4],.formula-sheet tr.resource_list input.cell-inp[data-col=6]', function () {
        var inp = $(this);
        if(inp.val()){
        var row = inp.data('row');
        var resource = $(".resource_id").val();
        var comboid = $('.formula-sheet tr.resource_list[data-row=' + row + ']').attr('data-id');
        var comboresource = $('.formula-sheet .cell-inp[data-row=' + row + '][data-col=1]').prop('disabled', false).val();
        var rate = $('.formula-sheet .cell-inp[data-row=' + row + '][data-col=2]').prop('disabled', false).val();
        var unit = $('.formula-sheet .cell-inp[data-row=' + row + '][data-col=3]').prop('disabled', false).val();
        var remark = $('.formula-sheet .cell-inp[data-row=' + row + '][data-col=6]').prop('disabled', false).val();
        var applied = $('.formula-sheet tr.final_result .cell-inp[data-col=4]').val();
        $.ajax({
            url: '{{ route('admin.resources.calculate') }}',
            type: 'POST',
            data: {_token: '{{ csrf_token() }}', str: inp.val(), comboid: comboid, unit: unit, rate: rate,resource: resource, comboresource: comboresource, applied: applied, remark: remark},
            success: function (data) {
                var rData = JSON.parse(data);
                if (rData.status == 'done') {
                    $('.formula-sheet .cell-inp[data-row=' + row + '][data-col=3]').prop('disabled', true)
                    $('.formula-sheet tr.resource_list[data-row=' + row + ']').attr('data-id',rData.comboid);
                    $('.formula-sheet .cell-inp[data-row=' + row + '][data-col=5]').val(rData.result);
                    calcTotal();
                } else {
                    alert(rData.message);
                    $('.formula-sheet .cell-inp[data-row=' + row + '][data-col=5]').val('#error');
                }
            }
        });
        }
    });
    $(document).on('change', '.formula-sheet tr.final_result input.cell-inp[data-col=4]', function () {
        var app = $(this).val();
        if(app != '') {
            $.ajax({
                url: '{{ route('admin.resources.validate') }}',
                type: 'POST',
                data: {_token:'{{ csrf_token() }}', str: app},
                success: function(data){
                    var rData = JSON.parse(data);
                    if(rData.status == 'done') {
                        $('.formula-sheet tr.resource_list input.cell-inp[data-col=5]').each(function () {
                            $(this).trigger('change');
                        });
                    }else{
                        alert('Applied factor field has some error!');
                        $('.formula-sheet tr.final_result input.cell-inp[data-col=5]').val('#error');
                    }
                }
            });

        }else{
            $('.formula-sheet tr.resource_list input.cell-inp[data-col=5]').each(function () {
                $(this).trigger('change');
            });
        }
    });
    $(document).on('focus', '.formula-sheet tr.resource_list input.cell-inp[data-col=4]', function () {
        var app = $(this).data('row');
        $(".selectfunction").data('row',app);

      /*  if(app != '') {
            $(".selectfunction").data('row',app);
            $(".selectfunction").removeAttr('disabled');
        }else{
            $(".selectfunction").data('row','');
            $(".selectfunction").attr('disabled','disabled');
        }*/
    });
    $(document).on('change','.selectfunction',function (e) {
         var datarow = $(this).data('row');
         var val = $(this).val();
         var forrow = $('.formula-sheet tr.resource_list input[name="formula[]"][data-row='+datarow+']');
        var forrowval = forrow.val();
            var newtext = forrowval+''+val;
        forrow.val(newtext);
    });
    $('#combo-sheet-submit').click(function (e) {
        $('#combo-sheet-form .cell-inp').removeAttr('disabled');
        e.preventDefault();
        $.ajax({
            url: '{{ route('admin.resources.combosheetstore') }}',
            container: '#combo-sheet-form',
            type: "POST",
            data: $('#combo-sheet-form').serialize(),
            beforeSend: function(){
                var html = '<div class="loaderx">' +
                    '                        <div class="cssload-speeding-wheel"></div>' +
                    '                    </div>';
                $('.formula-sheet').html(html).show();
            },
            success: function (rData) {
                var response = JSON.parse(rData);
                if(response.status == 'success'){
                    $('.formula-sheet').html("").hide();
                    $('.rate-formula-sheet').show();
                }
            }
        })
    });
    $(document).on('click', '.removecombosheet[data-col=0]', function (e) {
        var row = $(this).attr('data-row');
        var comboid = $(this).closest('tr').attr('data-id');
        $.ajax({
            url: '{{ route('admin.resources.removecombosheet') }}',
            type: 'POST',
            data: {_token: '{{ csrf_token() }}',comboid: comboid },
            success: function (data) {
                var rData = JSON.parse(data);
                if (rData.status == 'success') {
                    $('tr.resource_list[data-row=' + row + ']').remove();
                    calcTotal();
                    $(this).html("");
                    $('tr.resourcelist .cell-inp[data-row=' + row + '][data-col=4]').val(rData.result);
                } else {
                    alert(rData.message);
                    $('.rate-formula-sheet .cell-inp[data-row=' + row + '][data-col=4]').val('#error');
                }
            }
        });
        e.stopImmediatePropagation();
        return false;
    });
</script>
@endsection

@push('footer-script')

@endpush