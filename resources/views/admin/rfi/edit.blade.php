@extends('layouts.app')
@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li><a href="{{ route('admin.rfi.index') }}">{{ __($pageTitle) }}</a></li>
                <li class="active">@lang('app.edit')</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">

    <style>
        .panel-black .panel-heading a,
        .panel-inverse .panel-heading a {
            color: unset!important;
        }
    </style>

@endpush
@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-inverse">
                <div class="panel-heading"> Update RFI</div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'updateTask','class'=>'ajax-form','method'=>'POST']) !!}
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">RFI</label>
                                        <input type="text" name="title" class="form-control" placeholder="Title *" value="{{ $rfi->title }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Subject</label>
                                        <input type="text" name="subject" class="form-control" placeholder="Subject *" value="{{ $rfi->subject }}" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <br>
                                    <div class="form-group">
                                        <label>Select Draft</label><br>
                                        <input type="checkbox" name="draft" value="1" @if($rfi->draft=='1') checked @endif />
                                        <label >Draft</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <br>
                                    <div class="form-group row">
                                        <label for="private">Private or Public</label><br>
                                        <input id="private" name="private" value="1" type="checkbox" @if($rfi->private=='1') checked @endif >
                                        <label for="private">Private</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">RFI Manager</label>
                                        <select class="selectpicker form-control" name="rfi_manager" id="user_id">
                                            <option value="">Choose RFI Manager</option>
                                            @foreach($employees as $employee)
                                                <?php
                                                $selected = '';
                                                if($employee->id == $rfi->rfi_manager){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option value="{{ $employee->id }}" {{ $selected }}>{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('modules.tasks.assignTo')</label>
                                        <select class="selectpicker form-control" name="assign_to[]" id="user_id" multiple>
                                            <option value="">@lang('modules.tasks.chooseAssignee')</option>
                                            @foreach($employees as $employee)
                                                <?php
                                                $selected = '';
                                                if($employee->id == $rfi->assign_to){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option value="{{ $employee->id }}" {{ $selected }}>{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">@lang('app.dueDate')</label>
                                        <?php
                                        $exd = explode('-',$rfi->due_date);
                                        ?>
                                        <input type="text" name="due_date"  id="due_date2" class="form-control" value="{{ $exd[2].'-'.$exd[1].'-'.$exd[0] }}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="control-label">Status
                                        </label>
                                        <select class="selectpicker form-control" name="status" data-style="form-control" required>
                                            <option value="">Please Select Status</option>
                                            <option value="Open" <?php if($rfi->status == 'Open'){ echo 'selected'; } ?>>Open</option>
                                            <option value="Closed" <?php if($rfi->status == 'Closed'){ echo 'selected'; } ?>>Closed</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Responsible Contractor</label>
                                        <select class="selectpicker form-control" name="rec_contractor" id="user_id">
                                            <option value="">Choose Responsible Contractor</option>
                                            @foreach($employees as $employee)
                                                <?php
                                                $selected = '';
                                                if($employee->id == $rfi->rec_contractor){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option value="{{ $employee->id }}" {{ $selected }}>{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Received From</label>
                                        <select class="selectpicker form-control" name="received_from" id="user_id">
                                            <option value="">Choose Responsible Contractor</option>
                                            @foreach($employees as $employee)
                                                <?php
                                                $selected = '';
                                                if($employee->id == $rfi->received_from){
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                <option value="{{ $employee->id }}" {{ $selected }}>{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="control-label">Distribution List
                                        </label>
                                        <select class="selectpicker form-control" name="distribution[]" data-style="form-control" multiple required>
                                            <option value="">Please Select Distribution</option>
                                            @foreach($employees as $employee)
                                                <option value="{{ $employee->id }}"
                                                <?php
                                                    $ass = explode(',',$rfi->distribution);
                                                    if (in_array($employee->id, $ass)){
                                                        echo 'selected'; } ?>>{{ ucwords($employee->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Location</label>
                                        <input type="text" name="location" class="form-control" placeholder="Location *" value="{{ $rfi->location }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Drawing Number</label>
                                        <input type="text" name="drawing_no" class="form-control" placeholder="Drawing Number *" value="{{ $rfi->drawing_no }}" required>
                                    </div>
                                </div>

                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="control-label">Reference
                                        </label>
                                        <input type="text" name="reference" class="form-control" value="{{ $rfi->reference }}">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label class="control-label">Schedule Impact days</label>
                                        </div>
                                        <div class="col-md-6">
                                            <select class="selectpicker form-control schimpact" name="schimpact" data-style="form-control"  required>
                                                <option value="">Please Select</option>
                                                <option value="yes" @if($rfi->schimpact=='yes') selected @endif>Yes</option>
                                                <option value="no" @if($rfi->schimpact=='no') selected @endif>No</option>
                                                <option value="TBD" @if($rfi->schimpact=='TBD') selected @endif>TBD</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 schimpactblock" style="display:  @if($rfi->schimpact=='yes') block @else none @endif;">
                                            <input type="text" name="schimpact_days" class="form-control" value="{{ $rfi->schimpact_days }}" placeholder="Impact days *" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label class="control-label">Cost Impact days</label>
                                        </div>
                                        <div class="col-md-6">
                                            <select class="selectpicker form-control costimpact" name="costimpact" data-style="form-control"  required>
                                                <option value="">Please Select</option>
                                                <option value="yes" @if($rfi->costimpact=='yes') selected @endif>Yes</option>
                                                <option value="no" @if($rfi->costimpact=='no') selected @endif>No</option>
                                                <option value="TBD" @if($rfi->costimpact=='TBD') selected @endif>TBD</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 costimpactblock"  style="display:  @if($rfi->costimpact=='yes') block @else none @endif;">
                                            <input type="text" name="costimpact_days" class="form-control" value="{{ $rfi->costimpact_days }}" placeholder="Cost Impact days *" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4 col-xs-4">
                                    <div class="form-group">
                                        <label for="project_id"><b>Projects</b></label>
                                        <select class="select2 form-control projectid" id="projectid" name="project_id" data-style="form-control" required>
                                            <option value="">Please Select Project</option>
                                            @foreach($projectlist as $project)
                                                <option value="{{ $project->id }}" @if($rfi->projectid==$project->id) selected @endif >{{ ucwords($project->project_name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-4">
                                    <div class="form-group">
                                        <label for="title_id">Project Title</label>
                                        <select class="select2 form-control titlelist" id="titlelist" name="title_id" data-style="form-control" required>
                                            <option value="">Please Select Title</option>
                                            @if($titlelist)
                                                @foreach($titlelist as $title)
                                                    <option value="{{ $title->id }}" <?php if($rfi->titleid==$title->id){ echo 'selected';}?>>{{ ucwords($title->title) }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-4">
                                    <div class="form-group">
                                        <label for="title_id">@lang('app.task')</label>
                                        <select class="select2 form-control costitemlist" id="costitemlist" name="costitem" data-style="form-control" required>
                                            <option value="">Please Select @lang('app.task')</option>
                                            @if($costitemlist)
                                                @foreach($costitemlist as $costitem)
                                                    <option value="{{ $costitem }}" <?php if($rfi->costitemid==$costitem){ echo 'selected';}?>>{{ ucwords(get_cost_name($costitem)) }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-12">
                                    <b>Files</b>
                                    <br>
                                    <div class="row">
                                    @foreach($files as $file)
                                                <?php
                                                $fx = explode('.', $file->hashname);
                                                $ext = $fx[(count($fx)-1)];
                                                $html5class = '';
                                                if($ext=='jpg'||$ext=='png'||$ext=='jpeg'){
                                                    $html5class = 'html5lightbox';
                                                }
                                                ?>
                                        <div class="col-md-1">
                                            <?php $rfi = \App\Rfi::where('id',$file->rfi_id)->first(); ?>
                                                @if($file->external_link != '')
                                                    <?php $imgurl = $file->external_link;?>
                                                @elseif($storage == 'local')
                                                    <?php $imgurl = asset_url('rfi-files/'.$file->rfi_id.'/'.$file->hashname);?>
                                                @elseif($storage == 's3')
                                                    <?php $imgurl = $url.$companyid.'/rfi-files/'.$file->rfi_id.'/'.$file->hashname;?>
                                                @elseif($storage == 'google')
                                                    <?php $imgurl = $file->google_url;?>
                                                @elseif($storage == 'dropbox')
                                                    <?php $imgurl = $file->dropbox_link;?>
                                                @endif
                                                {!! mimetype_thumbnail($file->filename,$imgurl)  !!}
                                            <a href="javascript:;" data-toggle="tooltip" data-original-title="Delete" onclick="removeFile({{ $file->id }})"
                                               class="btn1 btn-danger btn-circle" ><i class="fa fa-times"></i></a>
                                            <span class="clearfix m-l-10 detail-month">{{ $file->created_at->diffForHumans() }}</span>
                                        </div>

                                    @endforeach
                                    </div>
                                </div>
                                <div class="row m-b-20">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <textarea id="description" name="question" class="form-control summernote">{{ $rfi->question }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-block btn-outline-info btn-sm col-md-2 select-image-button" style="margin-bottom: 10px;display: none "><i class="fa fa-upload"></i> File Select Or Upload</button>
                                        <div id="file-upload-box" >
                                            <div class="row" id="file-dropzone">
                                                <div class="col-md-12">
                                                    <div class="dropzone"
                                                         id="file-upload-dropzone">
                                                        {{ csrf_field() }}
                                                        <div class="fallback">
                                                            <input name="file" type="file" multiple/>
                                                        </div>
                                                        <input name="image_url" id="image_url"type="hidden" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="rfiID" id="rfiID">
                                    </div>
                                </div>
                            </div>
                            <!--/row-->
                        </div>

                        <div class="form-actions">
                            <button type="button" id="update-task" class="btn btn-success btn-width150"><i class="fa fa-check"></i> @lang('app.save')</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->

    {{--Ajax Modal--}}
    <div class="modal fade bs-modal-md in" id="taskCategoryModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Loading...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn blue">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->.
    </div>
    {{--Ajax Modal Ends--}}
@endsection
@push('footer-script')
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>


    <script>
        $(".schimpact").change(function () {
            var schimpact = $(this).val();
            if(schimpact=='yes'){
                $(".schimpactblock").show();
                $(".schimpactblock").attr('required','required');
            }else{
                $(".schimpactblock").hide();
                $(".schimpactblock").removeAttr('required','required');
            }
        });
        $(".costimpact").change(function () {
            var costimpact = $(this).val();
            if(costimpact=='yes'){
                $(".costimpactblock").show();
                $(".costimpactblock").attr('required','required');
            }else{
                $(".costimpactblock").hide();
                $(".costimpactblock").removeAttr('required','required');
            }
        });
        Dropzone.autoDiscover = false;
        //Dropzone class
        myDropzone = new Dropzone("div#file-upload-dropzone", {
            url: "{{ route('admin.rfi.storeImage') }}",
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            paramName: "file",
            maxFilesize: 10,
            maxFiles: 10,
            acceptedFiles: "image/*,application/pdf",
            autoProcessQueue: false,
            uploadMultiple: true,
            addRemoveLinks:true,
            parallelUploads:10,
            init: function () {
                myDropzone = this;
            }
        });

        myDropzone.on('sending', function(file, xhr, formData) {
            console.log(myDropzone.getAddedFiles().length,'sending');
            var ids = '{{ $rfi->id }}';
            formData.append('rfi_id', ids);
            formData.append('rfitype', 'rfi');
        });

        myDropzone.on('completemultiple', function () {
            var msgs = "@lang('RFI Updated successfully')";
            $.showToastr(msgs, 'success');
            window.location.href = '{{ route('admin.rfi.index') }}'

        });

        $(".projectid").change(function () {
            var project = $(this).val();
            if(project){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.rfi.projecttitles') }}",
                    data: {'_token': token,'projectid': project},
                    success: function(data){
                        $("select.titlelist").html("");
                        $("select.titlelist").html(data);
                        $('select.titlelist').select2();
                    }
                });
            }
        });
        $("#titlelist").change(function () {
            var project = $("#projectid").select2().val();
            var titlelist = $(this).val();
            if(project){
                var token = "{{ csrf_token() }}";
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.rfi.costitembytitle') }}",
                    data: {'_token': token,'projectid': project,'title': titlelist},
                    success: function(data){
                        $("select.costitemlist").html("");
                        $("select.costitemlist").html(data);
                        $('select.costitemlist').select2();
                    }
                });
            }
        });
        //    update task
        $('#update-task').click(function () {

            $.easyAjax({
                url: '{{route('admin.rfi.updateRfi', [$rfi->id])}}',
                container: '#updateTask',
                type: "POST",
                data: $('#updateTask').serialize(),
                success: function(response){
                    if(myDropzone.getQueuedFiles().length > 0){
                        rfiID = response.rfiID;
                        $('#rfiID').val(response.rfiID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "@lang('RFI Updated successfully')";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('admin.rfi.index') }}'
                    }
                }
            })

        });

        //    update task
        function removeFile(id) {
            var url = "{{ route('admin.rfi.removeFile',':id') }}";
            url = url.replace(':id', id);
            var token = "{{ csrf_token() }}";
            $.easyAjax({
                url: url,
                container: '#updateTask',
                type: "POST",
                data: {'_token': token, '_method': 'DELETE'},
                success: function(response){
                    if (response.status == "success") {
                        window.location.reload();
                    }
                }
            })

        };

        function updateTask(){
            $.easyAjax({
                url: '{{route('admin.rfi.updateRfi', [$rfi->id])}}',
                container: '#updateTask',
                type: "POST",
                data: $('#updateTask').serialize(),
                success: function(response){
                    if(myDropzone.getQueuedFiles().length > 0){
                        rfiID = response.rfiID;
                        $('#rfiID').val(response.rfiID);
                        myDropzone.processQueue();
                    }
                    else{
                        var msgs = "@lang('RFI Updated successfully')";
                        $.showToastr(msgs, 'success');
                        window.location.href = '{{ route('admin.rfi.index') }}'
                    }
                }
            })
        }

        jQuery('#due_date2, #start_date2').datepicker({
            autoclose: true,
            todayHighlight: true,
            weekStart:'{{ $global->week_start }}',
            format: '{{ $global->date_picker_format }}',
        });

        $(".select2").select2({
            formatNoMatches: function () {
                return "{{ __('messages.noRecordFound') }}";
            }
        });

        $('.summernote').summernote({
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ["view", ["fullscreen"]]
            ]
        });

        $('body').on('click', '.sa-params', function () {
            var id = $(this).data('file-id');
            var deleteView = $(this).data('pk');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the deleted file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {

                    var url = "{{ route('admin.rfi.destroy',':id') }}";
                    url = url.replace(':id', id);

                    var token = "{{ csrf_token() }}";

                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {'_token': token, '_method': 'DELETE', 'view': deleteView},
                        success: function (response) {
                            console.log(response);
                            if (response.status == "success") {
                                $.unblockUI();
                                $('#list ul.list-group').html(response.html);

                            }
                        }
                    });
                }
            });
        });
    </script>

@endpush
