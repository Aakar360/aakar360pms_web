@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ __($pageTitle) }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ __($pageTitle) }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">


@endpush
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <a href="{{ route('admin.tenders.create') }}" class="btn btn-outline btn-success btn-sm">@lang('app.add') @lang('app.menu.tenders') <i class="fa fa-plus" aria-hidden="true"></i></a>
                        </div>
                    </div>

                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-hover toggle-circle default footable-loaded footable" id="users-table">
                        <thead>
                        <tr>
                            <th>@lang('app.id')</th>
                            <th>@lang('app.title')</th>
                            <th>@lang('app.number')</th>
                            <th>@lang('app.status')</th>
                            <th>@lang('app.duedate')</th>
                            <th>Bidding List</th>
                            <th>Contractors List</th>
                            <th>@lang('app.action')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($tendersarray as $tenders)
                            <tr>
                                <td>{{ $tenders->id }}</td>
                                <td>{{ $tenders->name }}</td>
                                <td>{{ $tenders->number }}</td>
                                <td>{{ $tenders->status }}</td>
                                <td>{{ $tenders->deadline ? date('d-m-Y',strtotime($tenders->deadline)) : '' }}</td>
                                <td><a href="{{ route('admin.tenders.bidding-list', [$tenders->id]) }}" class="btn1 btn-info  btn-circle"><i class="fa fa-eye"></i> View</a></td>
                                <td><a href="javascript:void(0);" data-tender="{{ $tenders->id }}" class="btn1 btn-info  btn-circle showContractors"><i class="fa fa-list-alt"></i></a></td>
                                <td>
                                    <a href="{{ route('admin.tenders.edit', [$tenders->id]) }}" class="btn1 edit-btn btn-circle"><i class="fa fa-edit"></i></a>
                                    <a href="javascript:;" data-tenders-id="{{ $tenders->id }}" class="btn1 btn-danger  btn-circle sa-params"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3">@lang('messages.noRecordFound')</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->
    <div class="modal fade bs-modal-md in"  id="ContractorsModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <form method="post" autocomplete="off" action="" id="setbaselineupdate">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <span class="caption-subject font-red-sunglo bold uppercase" id="subTaskModelHeading">Contractors</span>
                    </div>
                    <div class="modal-body">
                        <div id="showerror"></div>
                        <div class="form-group">
                            <table id="contractors-table" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Sl no</th>
                                    <th></th>
                                    <th>@lang('app.name')</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{ csrf_field() }}
                        <input class="tenderid" type="hidden" />
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        <button type="button"  class="btn btn-success" id="send-mail">Send Mail</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@push('footer-script')

    <script src="{{ asset('plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
    <script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        "use strict";
        $(function() {
            $('body').on('click', '.sa-params', function(){
                var id = $(this).data('tenders-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover the deleted team!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {

                        var url = "{{ route('admin.tenders.destroy',':id') }}";
                        url = url.replace(':id', id);

                        var token = "{{ csrf_token() }}";

                        $.easyAjax({
                            type: 'DELETE',
                            url: url,
                            data: {'_token': token},
                            success: function (response) {
                                if (response.status == "success") {
                                    $.unblockUI();
                                    window.location.reload();
                                }
                            }
                        });
                    }
                });
            });
        });
        $(".showContractors").click(function () {
            var id = $(this).attr('data-tender');
            $("#ContractorsModal").modal('show');
            $("#showerror").html("");
            $(".tenderid").val(id);
            var url = "{{ route('admin.tenders.contractorslist',':id') }}";
            url = url.replace(':id', id);
            table  = $('#contractors-table').dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                stateSave: true,
                ajax: url,
                language: {
                    "url": "<?php echo __("app.datatable") ?>"
                },
                "fnDrawCallback": function (oSettings) {
                    $("body").tooltip({
                        selector: '[data-toggle="tooltip"]'
                    });
                },
                "initComplete": function(settings, json) {
                    if(json.status=='fail'){
                        $("#showerror").html("<div  class='alert alert-danger' >"+json.message+"</div>");
                         $("#contractors-table_paginate").hide();
                         $("#contractors-table_info").hide();
                    }
                },
                columns: [
                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                    {data: 'checkbox', name: 'checkbox'},
                    {data: 'name', name: 'name'}
                ]
            });
        });
            $("#send-mail").click(function () {
                var arr = $.map($('.contractors:checked'), function(e, i) {
                    return e.value; // +e.value will return NaN for String and non number values
                });
                $("#showerror").html("");
                var contr = arr.join(',');
                if(contr){
                    var url = '{{ route('admin.tenders.sendcontractorsmail') }}';
                    var  tenderid = $(".tenderid").val();
                    var  token = '{{ csrf_token() }}';
                    $.easyAjax({
                        type: 'POST',
                        url: url,
                        data: {
                            '_token': token,
                            'tender': tenderid,
                            'contractors': contr
                        },
                        success: function (response) {
                            if (response.success) {
                                $("#showerror").html("<div  class='alert alert-success' >Mail sent Successfully</div>");
                            }
                        }
                    });
                }else{
                    $("#showerror").html("<div  class='alert alert-danger' >Please select atleast one contractor</div>");
                }
            });
    </script>
@endpush