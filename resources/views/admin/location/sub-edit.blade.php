<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Sub Location</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'editSubLocation','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Sub Location Name</label>
                        <input type="text" name="name" id="title" class="form-control" value="{{$sublocation_selected->name}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Description</label>
                        <input type="text" name="description" id="title" class="form-control" value="{{$sublocation_selected->description}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Project Location </label>
                        <select class="form-control"  name="location_id" id="location" >



                                <?php  ?>
                                @foreach($location as $locations)
                                    <?php
                                    $selected = '';
                                    if($locations->id !== NULL){
                                        if($locations->id == $sublocation_selected->location_id){
                                            $selected = 'selected';
                                        } }?>
                                        <option  value="{{$locations->id}}" <?=$selected; ?>>{{$locations->location_name}}</option>

                                @endforeach

                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="save-sub-location" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<script>

    $('#save-sub-location').click(function () {

        $.easyAjax({
            url: '{{route('admin.location.sub-update', [$sublocation_selected->id])}}',
            container: '#editSubLocation',
            type: "POST",
            data: $('#editSubLocation').serialize(),
            success: function (response) {
                console.log(response);
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });


</script>

