<?php

namespace App\Observers;


use App\BoqCategory;
use App\ProjectCategory;
use App\TaskCategory;

class BoqCategoryObserver
{
    public function saving(BoqCategory $taskCategory)
    {
        // Cannot put in creating, because saving is fired before creating. And we need company id for check bellow
        if (company()) {
            $taskCategory->company_id = company()->id;
        }
    }

}
