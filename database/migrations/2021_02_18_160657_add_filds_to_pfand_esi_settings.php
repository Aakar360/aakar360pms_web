<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFildsToPfandEsiSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pfand_esi_settings', function (Blueprint $table) {
            //
            $table->datetime('esi_effective_date');
            $table->string('esi_max_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pfand_esi_settings', function (Blueprint $table) {
            //
        });
    }
}
