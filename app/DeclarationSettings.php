<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeclarationSettings extends Model
{
    protected $fillable = ['id', 'monthly_dec_to', 'monthly_dec_from', 'cutoff_dec_month',
        'cutoff_dec_month_date', 'yearly_dec_from', 'yearly_dec_to','apply_to_emp_id'];
}
