
    <div class="combo-sheet">
        <div class="table-wrapper">
            <table class="table table-bordered" style="text-align: center; width:100%; table-layout: fixed;border-spacing: 0px;" >
                        <thead>
                        <tr style="background-color: #003481;color:#fff">
                            <th>Contractor / Activity</th>
                            <th>Task</th>
                            <?php  if($exportype=='mobile'){ ?>
                            <th>% Progress</th>
                            <th>% Complete</th>
                           <?php }else{ ?>
                            <th>Segments</th>
                            <th>Unit</th>
                            <th>Total qty</th>
                            <th>Cumilative Achived Till</th>
                            <th>Planned for the day</th>
                            <th>Progress</th>
                            <th>Cumilative Achived</th>
                            <th>Balance</th>
                            <th>% Progress</th>
                            <th>% Complete</th>
                            <th>Planned for next</th>
                            <th>Remarks</th>
                            <?php }?>
                        </tr>
                        </thead>
                        <tbody>
                        @if($projectid)
                            <?php
                            function reporthtml($reportarray){

                            $projectid = $reportarray['projectid'];
                            $segmentid = $reportarray['segmentid'];
                            $subprojectid = $reportarray['subprojectid'];
                            $parent = $reportarray['parent'];
                            $level = $reportarray['level'];
                            $contracid = $reportarray['contractor'];
                            $dateformat = $reportarray['dateformat'];
                            $startdate = $reportarray['startdate'];
                            $enddate = $reportarray['enddate'];
                            $exportype = $reportarray['exportype'];
                            if(!empty($segmentid)){
                                $proproget = \App\ProjectSegmentsPosition::where('project_id',$projectid)->where('segment',$segmentid);
                            }else{
                                $proproget = \App\ProjectCostItemsPosition::where('project_id',$projectid);
                            }
                            if($subprojectid){
                                $proproget = $proproget->where('title',$subprojectid);
                            }
                            $proproget = $proproget->where('position','row')->where('level',$level)->where('parent',$parent)->orderBy('inc','asc')->get();

                            foreach ($proproget as $propro){
                            $catvalue = $propro->itemid;
                            if(!empty($propro->catlevel)){
                                $catvalue = $propro->catlevel.','.$propro->itemid;
                            }
                            ?>
                            <tr style="border:1px solid #efefef;">
                                <td  style="border:1px solid #000;border-right:0px;margin: 0px;padding: 5px;font-size: 18px;">{{ ucwords($propro->itemname) }}</td>
                                <td style="border:1px solid #000;border-bottom:1px solid #000;margin: 0px;padding: 5px;font-size: 18px;" colspan="13"></td>
                            </tr>
                            <?php
                            if(!empty($segmentid)){
                                $projectcostitemsarray = \App\ProjectSegmentsProduct::where('category',$catvalue)->where('project_id',$projectid)->where('segment',$segmentid);
                            }else{
                                $projectcostitemsarray = \App\ProjectCostItemsProduct::where('category',$catvalue)->where('project_id',$projectid);
                            }
                            if($subprojectid){
                                $projectcostitemsarray = $projectcostitemsarray->where('title',$subprojectid);
                            }
                            if($contracid){
                                $projectcostitemsarray = $projectcostitemsarray->where('contractor',$contracid);
                            }else{
                                $projectcostitemsarray = $projectcostitemsarray->whereNull('contractor');
                            }
                            $projectcostitemsarray = $projectcostitemsarray->where('position_id',$propro->id)->get();
                            foreach ($projectcostitemsarray as $projectcostitems){
                                if($projectcostitems->id){
                                $tasks = \App\Task::where('cost_item_id',$projectcostitems->id)->first();
                                $totalquantity = $projectcostitems->qty;
                                $percentage = $cumiliquantity = $cumiliquantitytill = $achivied=  $balance=  0;
                                    if(!empty($tasks->id)){
                                        $comarchive = 0;
                                        $percentage =  $tasks->percentage;
                                        $start_date = $startdate.' 00:00:00';
                                        $end_date = $enddate.' 23:59:59';
                                        $prevarchived = \App\TaskPercentage::where('task_id',$tasks->id)->where('created_at','<',$start_date)->orderBy('id','desc')->max('percentage');
                                        $prevarchived = !empty($prevarchived) ? (int) $prevarchived : 0;
                                        $perachivied = \App\TaskPercentage::where('task_id',$tasks->id)->where('created_at','<=',$end_date)->orderBy('id','desc')->max('percentage');
                                        $perachivied = !empty($perachivied) ? (int) $perachivied : 0;
                                        $comarchive = $perachivied-$prevarchived;
                                        if(empty($exportype)){
                                            $cumiliquantitytill = \App\TaskPercentage::where('task_id',$tasks->id)->where('created_at','<',$start_date)->sum('taskqty');
                                            $achivied = \App\TaskPercentage::where('task_id',$tasks->id)->where('created_at','>',$start_date)->where('created_at','<',$end_date)->sum('taskqty');
                                            $cumiliquantity = $cumiliquantitytill+$achivied;
                                            $balance = $totalquantity-$cumiliquantity;
                                        }
                                    ?>
                                    <tr class="tableborder">
                                        <td  style="border:1px solid #000;padding: 5px;font-size: 18px;"></td>
                                        <td  style="border:1px solid #000;padding: 5px;font-size: 18px;">{{ ucwords($tasks->heading) }}</td>
                                        <?php
                                        if($exportype=='mobile'){?>
                                            <td  style="border:1px solid #000;padding: 5px;font-size: 18px;">{{ $comarchive }}</td>
                                            <td  style="border:1px solid #000;padding: 5px;font-size: 18px;">{{ $percentage }}</td>
                                        <?php }else{ ?>
                                            <td  style="border:1px solid #000;padding: 5px;font-size: 18px;"></td>
                                            <td  style="border:1px solid #000;padding: 5px;font-size: 18px;">{{ get_unit_name($projectcostitems->unit) }}</td>
                                            <td  style="border:1px solid #000;padding: 5px;font-size: 18px;">{{  $totalquantity }}</td>
                                            <td  style="border:1px solid #000;padding: 5px;font-size: 18px;">{{ $cumiliquantitytill }}</td>
                                            <td  style="border:1px solid #000;padding: 5px;font-size: 18px;"></td>
                                            <td  style="border:1px solid #000;padding: 5px;font-size: 18px;">{{ $achivied }}</td>
                                            <td  style="border:1px solid #000;padding: 5px;font-size: 18px;">{{ $cumiliquantity }}</td>
                                            <td  style="border:1px solid #000;padding: 5px;font-size: 18px;">{{ $balance }}</td>
                                            <td  style="border:1px solid #000;padding: 5px;font-size: 18px;">{{ $comarchive }}</td>
                                            <td  style="border:1px solid #000;padding: 5px;font-size: 18px;">{{ $percentage }}</td>
                                            <td  style="border:1px solid #000;padding: 5px;font-size: 18px;"></td>
                                            <td  style="border:1px solid #000;padding: 5px;font-size: 18px;">{{  $projectcostitems->description }}</td>
                                        <?php }?>
                                    </tr>
                                    <?php  }
                                    }
                                }

                                    $newlvel = (int)$propro->level+1;
                                    $reportarray = array();
                                    $reportarray['projectid'] = $projectid;
                                    $reportarray['segmentid'] = $segmentid;
                                    $reportarray['subprojectid'] = $subprojectid;
                                    $reportarray['parent'] = $propro->itemid;
                                    $reportarray['level'] = $newlvel;
                                    $reportarray['contractor'] = $contracid;
                                    $reportarray['dateformat'] = $dateformat;
                                    $reportarray['startdate'] = $startdate;
                                    $reportarray['enddate'] = $enddate;
                                    $reportarray['exportype'] = $exportype;
                                    echo reporthtml($reportarray);
                                    }
                            }
                            $contractorarray = \App\Contractors::where('company_id',$user->company_id)->pluck('name','user_id')->toArray();
                            $contr = array('0'=>'Company');
                            $contractorarray = $contractorarray+$contr;
                            if(!empty($contractorarray)){
                            foreach ($contractorarray as $contracid => $contracname){
                            ?>
                            <tr style="background-color: #e6f0fb">
                                <td class="cell-inp" style="font-size: 18px;"><b>{{ ucwords($contracname) }}</b></td>
                                <td class="cell-inp" style="font-size: 18px;" colspan="13"></td>
                            </tr>
                            <?php  $reportarray = array();
                            $reportarray['projectid'] = $projectid;
                            $reportarray['segmentid'] = $segmentid;
                            $reportarray['subprojectid'] = $subprojectid;
                            $reportarray['parent'] = 0;
                            $reportarray['level'] = 0;
                            $reportarray['contractor'] = $contracid;
                            $reportarray['dateformat'] = $dateformat;
                            $reportarray['startdate'] = $startdate;
                            $reportarray['enddate'] = $enddate;
                            $reportarray['exportype'] = $exportype;
                                echo reporthtml($reportarray);

                            }  } ?>

                        @endif
                        </tbody>
                    </table>
        </div>
    </div>
