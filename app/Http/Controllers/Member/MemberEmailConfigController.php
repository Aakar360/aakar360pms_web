<?php

namespace App\Http\Controllers\Member;

use App\EmployeeDetails;
use App\Helper\Reply;
use App\Http\Requests\User\UpdateProfile;
use App\SmtpSettingsUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class MemberEmailConfigController extends MemberBaseController
{
    public function __construct() {
        parent::__construct();
        $this->pageTitle = 'app.menu.mailconfig';
        $this->pageIcon = 'icon-basic-mail';
        $this->middleware(function ($request, $next) {
            if (!in_array('mailconfig', $this->user->modules)) {
                abort(403);
            }
            return $next($request);
        });

    }
    public function index() {
        $user = $this->user;
        $this->userDetail = $user;
        $this->smtpSetting = SmtpSettingsUser::where('user_id',$user->id)->where('company_id',$user->company_id)->first();
        if(empty($this->smtpSetting)){
            $smtpuser = new SmtpSettingsUser();
            $smtpuser->mail_username = $user->email;
            $smtpuser->mail_from_email = $user->email;
            $smtpuser->mail_from_name = ucwords($user->name);
            $smtpuser->user_id = $user->id;
            $smtpuser->company_id = $user->company_id;
            $smtpuser->status = 0;
            $smtpuser->save();
            $this->smtpSetting =$smtpuser;
        }
        return view('member.profile.mail-config', $this->data);
    }
    public function emailUpdate(Request $request){
        $user = auth()->user();
        $smtp = SmtpSettingsUser::where('user_id',$user->id)->where('company_id',$user->company_id)->first();
        $smtp->mail_sender = $request->mail_sender;
        $smtp->mail_driver = $request->mail_driver;
        $smtp->mail_host = $request->mail_host;
        $smtp->mail_port = $request->mail_port;
        if($smtp->mail_driver=='mail'){
            $smtp->mail_username = $request->mail_username;
            $smtp->mail_password = $request->mail_password;
            $smtp->mail_from_email = $request->mail_username;
        }else{
            $smtp->mail_username = $request->smtpmail_username;
            $smtp->mail_password = $request->smtpmail_password;
            $smtp->mail_from_email = $request->mail_from_email;
        }
        $smtp->mail_from_name = $request->mail_from_name;
        $smtp->mail_encryption = $request->mail_encryption;
        $smtp->status = $request->status ?: 0;
        $smtp->save();

        return Reply::success(__('messages.settingsUpdated'));
    }
    public function sendTestEmail(Request $request){
        $user = auth()->user();
        $email = $request->test_email;
        $mailarray = array();
        $mailarray['email'] = $email;
        $mailarray['subject'] = "Testing mail";
        $mailarray['message'] = "Testing mail on aakar360";
        $response = $user->sendEmail($mailarray);
        if($response['success']){
            return Reply::success($response['message']);
        }else{
            return Reply::error($response['message']);
        }
    }
}
