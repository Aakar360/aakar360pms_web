<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use App\AssetCategory;
use App\Asset;
use App\ManpowerCategory;
use App\SubAsset;


use App\Location;
use App\Trade;
use Illuminate\Http\Request;

class ManageManpowerCategoryController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Man Power Category';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'pms';
    }


    public function index()
    {
        $this->categories = ManpowerCategory::all();
        return view('admin.manpower-categories.index', $this->data);
    }


    public function store(Request $request)
    {
        $trade = new ManpowerCategory();
        $trade->title = $request->title;
        $trade->save();
        return Reply::success(__('messages.manpowerCategoryAdded'));
    }

    public function edit($id)
    {
        $this->trade = ManpowerCategory::find($id);
        $this->status='success';
        return view('admin.manpower-categories.edit', $this->data);
    }

    public function update(Request $request,$id)
    {
        $trade = ManpowerCategory::find($id);
        $trade->title = $request->title;
        $trade->save();

        return Reply::success(__('messages.manpowerCategoryUpdate'));
    }
    public function destroy($id)
    {
        ManpowerCategory::destroy($id);
        $categoryData = ManpowerCategory::all();
        return Reply::successWithData(__('messages.manpowerCategoryDelete'),['data' => $categoryData]);
    }
}
