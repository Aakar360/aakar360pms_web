<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCostitemproductInProjectSegmentsProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_segments_product', function (Blueprint $table) {
            $table->string('cost_items_product_id')->nullable()->after('cost_items_id');
            $table->string('totalqty')->nullable()->after('qty');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_segments_product', function (Blueprint $table) {
            $table->dropColumn('cost_items_product_id');
            $table->dropColumn('totalqty');
        });
    }
}
