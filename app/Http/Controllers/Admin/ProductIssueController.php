<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use App\Http\Requests\CostItems\StoreCostItems;
use App\CostItemsLavel;
use App\BoqCategory;
use App\CostItems;
use App\CostItemsProduct;
use App\Indent;
use App\Product;
use App\ProductBrand;
use App\ProductIssue;
use App\Project;
use App\Stock;
use App\Store;
use App\Units;
use App\ProductCategory;
use Illuminate\Http\Request;

class ProductIssueController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Product Issue';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'store';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->issuedProducts = ProductIssue::groupBy('unique_id')->get();
        return view('admin.product-issue.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->stores = Store::get();
        $this->projects = Project::get();
        $this->indents = Indent::get();
        $this->units = Units::get();
        $this->products = Product::get();
        return view('admin.product-issue.create', $this->data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $project_id = $request->project_id;
        $product_id = $request->product_id;
        $sku_id = $request->sku_id;
        $unique_id = 'PI-'.date("d-m-Y").'-'.count($product_id);
        if(isset($request->unit_id)) {
            $unit_id = $request->unit_id;
        }
        $quantity = $request->quantity;
        $issued_by = $this->user->id;

        if($request->indent_no !== ''){
            foreach($quantity as $key=>$value ){
                $scheck = Stock::where('sku',$sku_id)->where('cid',$product_id[$key])->where('store_id', $request->store_id)->where('project_id',$project_id)->first();
                if($scheck !== null){
                    if($quantity[$key] > $scheck->stock){
                        return Reply::success(__('Issued Quantity greater then stock'));
                    }else{
                        $bal = $scheck->stock - $value;
                        $scheck->stock = $bal;
                        $scheck->save();
                    }
                }
                $pro = new ProductIssue();
                $pro->project_id = $project_id;
                $pro->product_id = $product_id[$key];
                $pro->unique_id = $unique_id;
                $pro->store_id = $request->store_id;
                $pro->quantity = $quantity[$key];
                $pro->issued_by = $issued_by;
                $pro->save();
            }
        }else {
            foreach ($product_id as $key => $value) {
                if($quantity[$key] !== '') {
                    $scheck = Stock::where('sku',$sku_id)->where('cid',$value)->where('store_id', $request->store_id)->where('project_id',$project_id)->first();
                    if($scheck !== null){
                        if($quantity[$key] > $scheck->stock){
                            return Reply::success(__('Issued Quantity greater then stock'));
                        }else{
                            $bal = $scheck->stock - $quantity[$key];
                            $scheck->stock = $bal;
                            $scheck->save();
                        }
                    }
                    $pro = new ProductIssue();
                    $pro->project_id = $project_id;
                    $pro->product_id = $value;
                    $pro->unique_id = $unique_id;
                    $pro->store_id = $request->store_id;
                    $pro->unit_id = $unit_id[$key];
                    $pro->quantity = $quantity[$key];
                    $pro->issued_by = $issued_by;
                    $pro->save();
                }
            }
        }

        return Reply::success(__('Product issued successfully'));
    }

    public function returns($id)
    {
        $this->stores = Store::get();
        $this->products = ProductIssue::groupBy('unique_id')->get();
        $this->issuedProducts = ProductIssue::where('unique_id',$id)->get();
        return view('admin.product-issue.return', $this->data);
    }

    public function storeReturn(Request $request)
    {
        $id = $request->id;
        $quantity = $request->quantity;
        $returned_quantity = $request->returned_quantity;
        foreach($id as $key=>$value ){
            $qty = $quantity[$key] - $returned_quantity[$key];
            $pro = ProductIssue::find($value);
            $pro->quantity = $qty;
            $pro->save();
        }

        return Reply::success(__('Product returned successfully'));
    }

    public function issuedProductDetail(Request $request, $id){
        $this->issuedProducts = ProductIssue::where('unique_id',$id)->get();
        return view('admin.product-issue.product-issued-detail', $this->data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->categories = BoqCategory::get();
        $this->cats = BoqCategory::all();
        $this->costlavel = CostItemsLavel::where('cost_items_id', $id)->get();
        $this->category = CostItems::where('id', $id)->first();
        $this->units = Units::all();
        return view('admin.cost-items.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = CostItems::find($id);
        $category->company_id = $this->user->company_id;
        $category->cost_item_name = $request->cost_item_name;
        $category->cost_item_description = $request->cost_item_description;
        $category->unit = $request->unit;
        $category->type = $request->type;
        $category->save();
        //dd();
        $cost_item_lavel = $request->cost_item_lavel;
        $costlavel = CostItemsLavel::where('cost_items_id', $id)->get();

        if($costlavel !== NULL){
            CostItemsLavel::where('cost_items_id', $id)->delete();
            for($i = 0; $i < count($cost_item_lavel); $i++ ){
                $costlavel = new CostItemsLavel();
                $costlavel->cost_items_id = $id;
                $costlavel->boq_category_id = $cost_item_lavel[$i];
                $costlavel->save();
            }
        }else {
            for ($i = 0; $i < count($cost_item_lavel); $i++) {
                $costlavel = new CostItemsLavel();
                $costlavel->cost_items_id = $id;
                $costlavel->boq_category_id = $cost_item_lavel[$i];
                $costlavel->save();
            }
        }

        return Reply::success(__('@lang(app.task) Updated Successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CostItems::destroy($id);
        $categoryData = CostItems::all();
        return Reply::successWithData(__('messages.categoryDeleted'),['data' => $categoryData]);
    }

    public function data()
    {
        $products = CostItems::select('id', 'cost_item_name', 'cost_item_description')
            ->get();

        dd($products);
        return DataTables::of($products)
            ->addColumn('action', function($row){
                return '<a href="'.route('admin.cost-items.edit', [$row->id]).'" class="btn btn-info btn-circle"
                      data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                      <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-user-id="'.$row->id.'" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->editColumn('cost_item_name', function ($row) {
                return ucfirst($row->cost_item_name);
            })
            ->editColumn('cost_item_description', function ($row) {
                return ucfirst($row->cost_item_description);
            })

            ->rawColumns(['action'])
            ->make(true);
    }

    public function productAdd($id){
        $this->categories = BoqCategory::get();
        $this->pcat = ProductCategory::get();
        $this->pbrand = ProductBrand::get();
        $this->pcats = ProductCategory::all();
        $this->pbrands = ProductBrand::get();
        $this->units = Units::get();
        $this->costlavel = CostItemsLavel::where('cost_items_id', $id)->get();
        $this->costproducts = CostItemsProduct::where('cost_items_id', $id)->get();
        $this->category = CostItems::where('id', $id)->first();
        $this->costid = $id;
        return view('admin.cost-items.product-add', $this->data);
    }

    public function productEdit($id){
        $this->categories = BoqCategory::get();
        $this->pcat = ProductCategory::get();
        $this->pbrand = ProductBrand::get();
        $this->pcats = ProductCategory::all();
        $this->pbrands = ProductBrand::get();
        $this->units = Units::get();
        $this->costlavel = CostItemsLavel::where('cost_items_id', $id)->get();
        $this->costproduct = CostItemsProduct::where('id', $id)->first();
        $this->category = CostItems::where('id', $id)->first();
        $this->costid = $id;
        return view('admin.cost-items.product-edit', $this->data);
    }

    public function getCost(Request $request){
        $qty = $request->qty;
        $rate = $request->rate;

        $final = $qty*$rate;

        return $final;
    }

    public function storeProduct(Request $request, $id)
    {
        $cost_item_lavel = $request->product_category_id;
        $costlavel = CostItemsProduct::where('cost_items_id', $id)->get();

        if($costlavel !== NULL){
            CostItemsProduct::where('cost_items_id', $id)->delete();
            for($i = 0; $i < count($cost_item_lavel); $i++ ){
                $costlavel = new CostItemsProduct();
                $costlavel->cost_items_id = $id;
                $costlavel->product_category_id = $cost_item_lavel[$i];
                $costlavel->product_brand_id = $request->product_brand_id[$i];
                $costlavel->unit = $request->unit[$i];
                $costlavel->qty = $request->qty[$i];
                $costlavel->wastage = $request->wastage[$i];
                $costlavel->rate = $request->rate[$i];
                $costlavel->cost = $request->cost[$i];
                $costlavel->save();
            }
        }else {
            for ($i = 0; $i < count($cost_item_lavel); $i++) {
                $costlavel = new CostItemsProduct();
                $costlavel->cost_items_id = $id;
                $costlavel->product_category_id = $cost_item_lavel[$i];
                $costlavel->product_brand_id = $request->product_brand_id[$i];
                $costlavel->unit = $request->unit[$i];
                $costlavel->qty = $request->qty[$i];
                $costlavel->wastage = $request->wastage[$i];
                $costlavel->rate = $request->rate[$i];
                $costlavel->cost = $request->cost[$i];
                $costlavel->save();
            }
        }

        return Reply::success(__('@lang(app.task) Updated Successfully'));
    }

    public function productUpdate(Request $request, $id)
    {

        $costlavel = CostItemsProduct::find($id);
        $costlavel->product_category_id = $request->product_category_id;
        $costlavel->product_brand_id = $request->product_brand_id;
        $costlavel->unit = $request->unit;
        $costlavel->qty = $request->qty;
        $costlavel->wastage = $request->wastage;
        $costlavel->rate = $request->rate;
        $costlavel->cost = $request->cost;
        $costlavel->save();

        return Reply::success(__('@lang(app.task) Updated Successfully'));
    }

    public function destroyProduct($id)
    {
        CostItemsProduct::destroy($id);
        $categoryData = CostItemsProduct::all();
        return Reply::successWithData(__('Deleted Successfully'),['data' => $categoryData]);
    }
}
