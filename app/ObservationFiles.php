<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ObservationFiles extends Model
{
    protected $table = 'observations_files';

    protected static function boot()
    {
        parent::boot();
    }
}
