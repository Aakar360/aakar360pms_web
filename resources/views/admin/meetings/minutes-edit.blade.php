<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Edit Minutes</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'editProduct','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="form-body">
            <div class="row">
                <div class="item-row margin-top-5" style="padding-bottom: 10px;">
                    <div class="col-md-12" style="float: left; padding-right: 20px;">
                        <div class="row">
                            <label class="control-label">Description</label>
                            <textarea id="description" name="description" class="form-control summernote">{{ $category->minutes }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="float: left; padding-right: 20px;">&nbsp;</div>
        <div class="form-actions">
            <button type="button" id="update-product" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>



<script>
    function getCo() {
        var qt = $('#qt').val();
        var rat = $('#rat').val();
        var total = (parseFloat(qt)*parseFloat(rat)).toFixed(2);
        $('#co').val(total);
    }

    $('.summernote').summernote({
        height: 200,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ["view", ["fullscreen"]]
        ]
    });

    $('#editProduct').submit(function () {
        $.easyAjax({
            url: '{{route('admin.meetings.updateMinutes', [$category->id])}}',
            container: '#editProduct',
            type: "POST",
            data: $('#editProduct').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
        return false;
    })

    $('#update-product').click(function () {

        $.easyAjax({

            url : '{{route('admin.meetings.updateMinutes', [$category->id])}}',
            container: '#editProduct',
            type: "POST",
            data: $('#editProduct').serialize(),

            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });


</script>