<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Edit Input Fields</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'editBoqCategory','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" name="title" id="title" class="form-control" value="{{ $field->title }}">
                    </div>
                </div>
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Type</label>
                        <select name="type" class="form-control" onchange="getOptionData(this.value);">
                            <option value="">Please select type</option>
                            <option value="button" <?php if($field->type == 'button'){ echo 'selected'; } ?>>Button</option>
                            <option value="checkbox" <?php if($field->type == 'checkbox'){ echo 'selected'; } ?>>Checkbox</option>
                            <option value="date" <?php if($field->type == 'date'){ echo 'selected'; } ?>>Date</option>
                            <option value="email" <?php if($field->type == 'email'){ echo 'selected'; } ?>>Email</option>
                            <option value="file" <?php if($field->type == 'file'){ echo 'selected'; } ?>>File</option>
                            <option value="number" <?php if($field->type == 'number'){ echo 'selected'; } ?>>Number</option>
                            <option value="radio" <?php if($field->type == 'radio'){ echo 'selected'; } ?>>Radio</option>
                            <option value="text" <?php if($field->type == 'text'){ echo 'selected'; } ?>>Text</option>
                            <option value="select" <?php if($field->type == 'select'){ echo 'selected'; } ?>>Select</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Option</label>
                        <?php
                        if($field->options){
                            $xo = json_decode($field->options);
                            foreach ($xo as $x){
                        ?>
                        <div class="col-xs-12 option-row">
                            <div class="form-group">
                                <div class="col-md-10">
                                    <input type="text" name="options[]" id="title" class="form-control" value="{{ $x }}">
                                </div>
                                <div class="col-xs-1 m-t-5">
                                    <button type="button" class="btn btn-danger remove-option"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                        </div>
                        <?php } } ?>
                        <div id="checkbox" style="display:none;">
                            <div id="sortable">
                                <div class="col-xs-10 item-row margin-top-5" style="padding-bottom: 10px;">
                                    <div class="col-md-12" style="float: left; padding-right: 20px;">
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="input-group col-md-12" style="float: left; padding-right: 5px;">
                                                    <input type="text" class="form-control" name="options[]" id="qty" placeholder="Option">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1 m-t-5">
                                <button type="button" class="btn btn-info" id="add-item"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <div id="radio" style="display:none;">
                            <input type="text" name="options[]" class="form-control" placeholder="Option 1">
                            <input type="text" name="options[]" class="form-control" placeholder="Option 2">
                        </div>
                        <div id="select" style="display:none;">
                            <div id="sortableselect">
                                <div class="col-xs-10 item-row margin-top-5" style="padding-bottom: 10px;">
                                    <div class="col-md-12" style="float: left; padding-right: 20px;">
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="input-group col-md-12" style="float: left; padding-right: 5px;">
                                                    <input type="text" class="form-control" name="options[]" id="qty" placeholder="Option">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1 m-t-5">
                                <button type="button" class="btn btn-info" id="add-select-item"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-12">&nbsp;</div>
        <div class="form-actions">
            <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>

<script>
    $('#createBoqCategory').submit(function () {
        $.easyAjax({
            url: '{{route('member.inputFields.updateFields', [$field->id])}}',
            container: '#editBoqCategory',
            type: "POST",
            data: $('#editBoqCategory').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
        return false;
    })



    $('#save-category').click(function () {
        $.easyAjax({
            url: '{{route('member.inputFields.updateFields', [$field->id])}}',
            container: '#editBoqCategory',
            type: "POST",
            data: $('#editBoqCategory').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });

    function getOptionData(val) {
        var type = val;

        if(type == 'checkbox'){
            $('#checkbox').show();
        }
        if(type == 'radio'){
            $('#radio').show();
        }
        if(type == 'select'){
            $('#select').show();
        }
    }

    $('#add-item').click(function () {
        var item = '<div class="col-xs-12 item-row margin-top-5" style="padding-top: 5px;">'
            +'<div class="input-group col-md-10" style="float: left; padding-right: 5px;">'
            +'<input type="text" class="form-control" name="options[]" placeholder="Option">'
            +'</div>'


            +'<div class="col-md-1 text-right visible-md visible-lg">'
            +'<button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>'
            +'</div>'
            +'<div class="col-md-1 hidden-md hidden-lg" style="float: left;">'
            +'<div class="row">'
            +'<button type="button" class="btn btn-circle remove-item btn-danger"><i class="fa fa-remove"></i></button>'
            +'</div>'
            +'</div>'
            +'</div>';
        $(item).hide().appendTo("#sortable").fadeIn(500);
    });
    $('#add-select-item').click(function () {
        var item = '<div class="col-xs-12 item-row margin-top-5" style="padding-top: 5px;">'
            +'<div class="input-group col-md-10" style="float: left; padding-right: 5px;">'
            +'<input type="text" class="form-control" name="options[]" placeholder="Option">'
            +'</div>'


            +'<div class="col-md-1 text-right visible-md visible-lg">'
            +'<button type="button" class="btn remove-item btn-circle btn-danger"><i class="fa fa-remove"></i></button>'
            +'</div>'
            +'<div class="col-md-1 hidden-md hidden-lg" style="float: left;">'
            +'<div class="row">'
            +'<button type="button" class="btn btn-circle remove-item btn-danger"><i class="fa fa-remove"></i></button>'
            +'</div>'
            +'</div>'
            +'</div>';
        $(item).hide().appendTo("#sortableselect").fadeIn(500);
    });
    $(document).on('click','.remove-item', function () {
        $(this).closest('.item-row').fadeOut(300, function() {
            $(this).remove();
            calculateTotal();
        });
    });

    $(document).on('click','.remove-option', function () {
        $(this).closest('.option-row').fadeOut(300, function() {
            $(this).remove();
            calculateTotal();
        });
    });
</script>