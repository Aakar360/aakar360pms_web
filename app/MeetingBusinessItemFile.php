<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class MeetingBusinessItemFile extends Model
{
    protected $table = 'meeting_business_item_files';

    protected static function boot()
    {
        parent::boot();
    }
}
