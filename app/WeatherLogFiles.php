<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class WeatherLogFiles extends Model
{
    protected $table = 'weather_log_files';

    protected static function boot()
    {
        parent::boot();
    }
}
