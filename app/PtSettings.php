<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PtSettings extends Model
{
    // Don't forget to fill this array
    protected $fillable = ['effective_date','pt_state','state_override'];
}
