<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescriptionsToProjectCostItemsProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_cost_items_product', function (Blueprint $table) {
            $table->text('description')->nullable();
            $table->string('worktype')->nullable();
            $table->string('markuptype')->nullable();
            $table->string('markupvalue')->nullable();
            $table->string('contractor')->nullable();
            $table->string('adjustment')->nullable();
            $table->string('finalrate')->nullable();
            $table->string('finalamount')->nullable();
            $table->integer('collock')->default(0);
            $table->integer('inc')->default(0);
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_cost_items_product', function (Blueprint $table) {
            $table->dropColumn(['description']);
            $table->dropColumn(['worktype']);
            $table->dropColumn(['markuptype']);
            $table->dropColumn(['markupvalue']);
            $table->dropColumn(['contractor']);
            $table->dropColumn(['adjustment']);
            $table->dropColumn(['finalrate']);
            $table->dropColumn(['finalamount']);
            $table->dropColumn(['collock']);
            $table->dropColumn(['inc']);
        });
    }
}
