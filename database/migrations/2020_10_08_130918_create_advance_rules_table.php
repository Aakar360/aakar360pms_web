<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvanceRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advance_rules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('enable_overtime')->unsigned();
            $table->integer('rules_id');
            $table->string('in_time_leave_allow');
            $table->string('out_time_leave_allow');
            $table->string('work_time_leave_allow');
            $table->string('in_time_penalty');
            $table->string('out_time_penalty');
            $table->string('work_time_penalty');
            $table->string('in_time_deduction');
            $table->string('out_time_deduction');
            $table->string('work_time_deduction');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advance_rules');
    }
}
