<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Edit Category</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'editProduct','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="form-body">
            <div class="row">
                <div class="item-row margin-top-5" style="padding-bottom: 10px;">
                    <div class="col-md-12" style="float: left; padding-right: 20px;">
                        <div class="row">
                            <div class="form-group">
                                <div class="input-group" style="float: left; padding-right: 5px;">
                                    <label class="control-label">Category Name</label>
                                    <input type="text" class="form-control" name="title" value="{{ $category->title }}" id="qt" placeholder="Category Name">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="float: left; padding-right: 20px;">&nbsp;</div>
        <div class="form-actions">
            <button type="button" id="update-product" data-cat-id="{{ $category->id }}" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>



<script>
    function getCo() {
        var qt = $('#qt').val();
        var rat = $('#rat').val();
        var total = (parseFloat(qt)*parseFloat(rat)).toFixed(2);
        $('#co').val(total);
    }

    $('#editProduct').submit(function () {
        $.easyAjax({
            url: '{{route('admin.meetings.updateCategory', [$category->id])}}',
            container: '#editProduct',
            type: "POST",
            data: $('#editProduct').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
        return false;
    })

    $('#update-product').click(function () {

        $.easyAjax({

            url : '{{route('admin.meetings.updateCategory', [$category->id])}}',
            container: '#editProduct',
            type: "POST",
            data: $('#editProduct').serialize(),

            success: function (response) {
                if(response.meetingID){
                    window.location.reload();
                }
            }
        })
    });


</script>