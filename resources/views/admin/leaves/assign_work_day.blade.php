<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">@lang('modules.attendance.assignedrules')</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">
        {!! Form::open(['id'=>'assignWorkRules','class'=>'ajax-form','method'=>'POST']) !!}
        <div class="row">

            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >@lang('modules.employees.workrules')</label>

                    <select id="rules_id" name="rules_id" class="form-control">
                        @foreach($workrules as $rules)
                            <option value="{{$rules->id}}">{{$rules->name}}</option>
                        @endforeach
                    </select>

                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" >@lang('modules.payrollsettings.effectivedate')</label>
                    <input type="date" name="effective_date" class="form-control date-picker" id="effective_date" autocomplete="off">
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" id="assignwork-form" class="btn btn-success"><i class="fa fa-check"></i>
                    @lang('app.save')
                </button>
                <button type="reset" class="btn btn-default">@lang('app.cancel')</button>
            </div>
        </div>

        {!! Form::hidden('ids',$ids) !!}
        {!! Form::close() !!}

    </div>
</div>
<script>

    $('#assignwork-form').click(function () {

        $.easyAjax({
            url: '{{route('admin.leave.postAssignRule')}}',
            container: '#assignWorkRules',
            type: "POST",
            data: $('#assignWorkRules').serialize(),
            success: function (response) {
                $('#WorkAssignModal').modal('hide');
                window.location.reload();
            }
        });

        return false;
    });
</script>