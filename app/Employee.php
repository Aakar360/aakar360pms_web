<?php

namespace App;

use App\Observers\EmployeeDetailObserver;
use App\Traits\CustomFieldsTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{

    protected $table = 'employee';


    public function attendance()
    {
        return $this->hasMany(Attendance::class, 'emp_id');
    }

}
