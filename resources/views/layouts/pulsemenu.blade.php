<div class="panel panel-default">
    <div class="panel-heading {{ \Illuminate\Support\Facades\Request::is('pulse') ? 'active' : '' }}">
        <h4 class="panel-title">
            <a class="active"  href="{{ route('pulse.index') }}">
                Pulse
            </a>
        </h4>
    </div>
</div>
<?php
if(request()->path()=='admin/user-chat'){?>
<div class="panel panel-default">
    <div class="panel-heading {{ \Illuminate\Support\Facades\Request::is('pulse') ? 'active' : '' }}">
        <h4 class="panel-title">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                Persons
            </a>
        </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse in">
        <div class="panel-body">
            <ul class="chatonline style-none userList p-0">
                @forelse($userList as $users)
                <li id="dp_{{$users->id}}">
                    <a href="javascript:void(0)" id="dpa_{{$users->id}}" onclick="getChatData('{{$users->id}}', '{{$users->name}}')">
                        <img src="{{ get_users_image_link($users->id) }}" alt="user-img"   class="img-circle">
                        <span @if($users->message_seen == 'no' && $users->user_one != $user->id) class="font-bold" @endif> {{$users->name}}
                            <small class="text-simple"> @if($users->last_message){{  \Carbon\Carbon::parse($users->last_message)->diffForHumans()}} @endif
                                @if(\App\User::isAdmin($users->id))
                                    <label class="btn btn-danger btn-xs btn-outline">Admin</label>
                                @elseif(\App\User::isClient($users->id))
                                    <label class="btn btn-success btn-xs btn-outline">Client</label>
                                @else
                                    <label class="btn btn-warning btn-xs btn-outline">Employee</label>
                                @endif
                                </small>
                            </span>
                    </a>
                </li>
                @empty
                <li>
                    @lang("messages.noUser")
                </li>
                @endforelse
                <li class="p-20"></li>
            </ul>
        </div>
    </div>

</div>
<?php }else{ ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a class=""  href="{{ route('admin.user-chat.index') }}">
                Persons
            </a>
        </h4>
    </div>
</div>
<?php }?>
<?php
if(request()->path()=='channels'){?>
<div class="panel panel-default">
    <div class="panel-heading {{ \Illuminate\Support\Facades\Request::is('pulse') ? 'active' : '' }}">
        <h4 class="panel-title">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                Channels
            </a>
        </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse in">
        <div class="panel-body">
            <ul class="chatonline style-none userList p-0">
                <li  >
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#createChannelModal" >  <span class="font-bold" ><i class="fa fa-plus"></i> Create Channel</span>
                    </a>
                </li>
                @forelse($channelsList as $channels)
                    <li id="dp_{{$channels->id}}">
                        <a href="javascript:void(0)" id="dpa_{{$channels->id}}" onclick="getChannelData('{{$channels->id}}')">
                            <img src="{{ $channels->channelimage }}" alt="user-img"   class="img-circle">
                            <span class="font-bold" >{{ ucwords($channels->name) }}</span>
                        </a>
                    </li>
                @empty
                    <li>
                        @lang("messages.noUser")
                    </li>
                @endforelse
            </ul>
        </div>
    </div>

</div>
<?php }else{ ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a class=""  href="{{ route('channels.index') }}">
                Channels
            </a>
        </h4>
    </div>
</div>
<?php }?>

<div class="panel panel-default">
    <div class="panel-heading {{ \Illuminate\Support\Facades\Request::is('mentions') ? 'active' : '' }}">
        <h4 class="panel-title">
            <a class=""  href="{{ route('mentions.index') }}">
                Mentions
            </a>
        </h4>
    </div>
</div>