<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryBaseComponentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_base_components', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('basic');
            $table->string('hra');
            $table->string('pf_employer')->nullable();
            $table->string('esi_employer')->nullable();
            $table->string('special_allowance_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_base_components');
    }
}
