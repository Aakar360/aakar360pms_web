<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class TenderBidding extends Model
{
    protected $table = 'tender_biddings';

    protected static function boot()
    {
        parent::boot();
    }
}
