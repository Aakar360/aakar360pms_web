<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ProjectCostItemsResourceRate extends Model
{
    protected $table = 'project_cost_item_resource_rate';

    protected static function boot()
    {
        parent::boot();
    }
}
