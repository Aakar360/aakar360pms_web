<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class WorkOrderFiles extends Model
{
    protected $table = 'work_order_files';

    protected static function boot()
    {
        parent::boot();
    }
}
