<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use App\InputFields;
use Illuminate\Http\Request;

class ManageInputFieldsController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Input Fields';
        $this->pageIcon = 'icon-user';
        $this->activeMenu = 'pms';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->fields = InputFields::all();
        return view('admin.input-fields.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.input-fields.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createFields()
    {
        return view('admin.input-fields.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
//    public function store(Request $request)
//    {
//        $category = new InspectionType();
//        $category->title = $request->title;
//        $category->save();
//
//        return Reply::success(__('messages.categoryAdded'));
//    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeFields(Request $request)
    {
        $category = new InputFields();
        $category->title = $request->title;
        $category->type = $request->type ?: '';
        $o = $c = array();
        if($request->type == 'button'||$request->type == 'dropdown') {
            foreach ($request->options as $op) {
                if($op !== null) {
                    $o[] = $op;
                }
            }
            foreach ($request->colours as $cl) {
                if($cl !== null) {
                    $c[] = $cl;
                }
            }
        }
        $category->options = !empty($o) ? implode(',',$o) : '';
        $category->colors = !empty($c) ? implode(',',$c) : '';
        $category->save();
        $categoryData = InputFields::all();
        return Reply::successWithData(__('Input field added successfully'),['data' => $categoryData]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editFields($id)
    {
        $this->fields = InputFields::all();
        $this->field = InputFields::where('id',$id)->first();
        return view('admin.input-fields.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateFields(Request $request, $id)
    {
        $category = InputFields::find($id);
        $category->title = $request->title;
        $category->type = $request->type;
        $o = $c = array();
        if($request->type == 'button'||$request->type == 'dropdown') {
            foreach ($request->options as $op) {
                if($op !== null) {
                    $o[] = $op;
                }
            }
            foreach ($request->colours as $cl) {
                if($cl !== null) {
                    $c[] = $cl;
                }
            }
        }
        $category->options = !empty($o) ? implode(',',$o) : '';
        $category->colors = !empty($c) ? implode(',',$c) : '';
        $category->save();

        return Reply::success(__('Input field updated successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyFields($id)
    {
        InputFields::destroy($id);
        $categoryData = InputFields::all();
        return Reply::successWithData(__('Input field deleted successfully'),['data' => $categoryData]);
    }
}
