<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class AwardContractProducts extends Model
{
    protected $table = 'awarded_contracts_products';

    protected static function boot()
    {
        parent::boot();
    }
}
