<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <!-- .User Profile -->
        <ul class="nav" id="side-menu">
            {{--<li class="sidebar-search hidden-sm hidden-md hidden-lg">--}}
                {{--<!-- / Search input-group this is only view in mobile-->--}}
                {{--<div class="input-group custom-search-form">--}}
                    {{--<input type="text" class="form-control" placeholder="Search...">--}}
                        {{--<span class="input-group-btn">--}}
                        {{--<button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>--}}
                        {{--</span>--}}
                {{--</div>--}}
                {{--<!-- /input-group -->--}}
            {{--</li>--}}

            <li class="user-pro">
                @if(is_null($user->image))
                    <a href="#" class="waves-effect"><img src="{{ asset('default-profile-3.png') }}" alt="user-img" class="img-circle"> <span class="hide-menu">{{ (strlen($user->name) > 24) ? substr(ucwords($user->name), 0, 20).'..' : ucwords($user->name) }}
                            <span class="fa arrow"></span></span>
                    </a>
                @else
                    <a href="#" class="waves-effect"><img src="{{ asset('user-uploads/avatar/'.$user->image) }}" alt="user-img" class="img-circle"> <span class="hide-menu">{{ ucwords($user->name) }}
                            <span class="fa arrow"></span></span>
                    </a>
                @endif
                <ul class="nav nav-second-level">
                    <li><a href="{{ route('client.profile.index') }}"><i class="ti-user"></i> @lang("app.menu.profileSettings")</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                        ><i class="fa fa-power-off"></i> @lang('app.logout')</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>


            <li><a href="{{ route('client.dashboard.index') }}" class="waves-effect"><i class="icon-speedometer"></i> <span class="hide-menu">@lang('app.menu.dashboard') </span></a> </li>

            @if(in_array('projects',$modules))
                <li>
                    <a href="{{ route('client.projects.index') }}" class="waves-effect">
                        <i class="icon-layers"></i>
                        <span class="hide-menu">@lang("app.menu.projects") </span>
                        @if($unreadProjectCount > 0)
                            <div class="notify notification-color">
                                <span class="heartbit"></span>
                                <span class="point"></span>
                            </div>
                        @endif
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{ route('client.projects.index') }}">@lang('app.menu.projects') </a></li>
                        <li><a href="{{ route('client.projects.tenderBiddings') }}">Tender Bidding</a></li>
                    </ul>
                </li>
            @endif
            @if(in_array("punch_items", $modules))
                <li>
                    <a href="{{ route('client.punch-items.index') }}" class="waves-effect">
                        <i class="icon-layers"></i>
                        <span class="hide-menu">@lang('app.menu.punchItems') </span>
                    </a>
                </li>
            @endif

            @if(in_array("boq", $modules))
                <li>
                    <a href="{{ route('client.product-category.index') }}" class="waves-effect">
                        <i class="icon-layers"></i>
                        <span class="hide-menu">Product Category</span>
                    </a>
                </li>
            @endif

            @if(in_array("boq", $modules))
                <li>
                    <a href="{{ route('client.condition.index') }}" class="waves-effect">
                        <i class="icon-layers"></i>
                        <span class="hide-menu">Conditions</span>
                    </a>
                </li>
            @endif

            @if(in_array("boq", $modules))
                <li>
                    <a href="#" class="waves-effect">
                        <i class="ti-layout-list-thumb"></i>
                        <span class="hide-menu"> Type <span class="fa arrow"></span> </span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{ route('client.product-type-workforce.workforce-index') }}">Workforce</a></li>
                        <li><a href="{{ route('client.product-type-equipment.equipment-index') }}">Equipment</a></li>
                        <li><a href="{{ route('client.product-type-material.material-index') }}">Material</a></li>
                        <li><a href="{{ route('client.product-type-commitment.commitment-index') }}">Commitment</a></li>
                        <li><a href="{{ route('client.product-type-owner-cost.owner-cost-index') }}">Owner Cost</a></li>
                        <li><a href="{{ route('client.product-type-professional.professional-index') }}">Professional Services</a></li>
                        <li><a href="{{ route('client.product-type-other.other-index') }}">Other</a></li>
                    </ul>
                </li>
            @endif

            @if(in_array("boq", $modules))
                <li>
                    <a href="{{ route('client.product-brand.index') }}" class="waves-effect">
                        <i class="icon-layers"></i>
                        <span class="hide-menu">Product Brands</span>
                    </a>
                </li>
            @endif

            @if(in_array("rfi", $modules))
                <li>
                    <a href="{{ route('client.rfi.index') }}" class="waves-effect">
                        <i class="icon-layers"></i>
                        <span class="hide-menu">RFI</span>
                    </a>
                </li>
            @endif

            @if(in_array("boq", $modules))
                <li>
                    <a href="#" class="waves-effect">
                        <i class="ti-layout-list-thumb"></i>
                        <span class="hide-menu"> BOQ <span class="fa arrow"></span> </span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{ route('client.activity.index') }}">Boq Category</a></li>
                        <li><a href="{{ route('client.cost-items.index') }}">Cost Items</a></li>
                    </ul>
                </li>
            @endif

            @if(in_array("inspection", $modules))
                <li>
                    <a href="#" class="waves-effect">
                        <i class="ti-layout-list-thumb"></i>
                        <span class="hide-menu"> Inspection <span class="fa arrow"></span> </span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{ route('client.inspection-type.index') }}">Inspection Type</a></li>
                        <li><a href="{{ route('client.input-fields.index') }}">Input Fields</a></li>
                        <li><a href="{{ route('client.inspection-name.index') }}">Inspection Form</a></li>
                        <li><a href="{{ route('client.inspectionName.inspectionReplies') }}">Replies</a></li>
                    </ul>
                </li>
            @endif
            @if(in_array('products',$modules))
                <li><a href="{{ route('client.products.index') }}" class="waves-effect"><i class="icon-layers"></i> <span class="hide-menu">@lang('app.menu.products') </span> @if($unreadProjectCount > 0) <div class="notify notification-color"><span class="heartbit"></span><span class="point"></span></div>@endif</a> </li>
            @endif
            @if(in_array('tickets',$modules))
                <li><a href="{{ route('client.tickets.index') }}" class="waves-effect"><i class="ti-ticket"></i> <span class="hide-menu">@lang("app.menu.tickets") </span></a> </li>
            @endif

            @if(in_array('invoices',$modules))
                <li><a href="{{ route('client.invoices.index') }}" class="waves-effect"><i class="ti-receipt"></i> <span class="hide-menu">@lang('app.menu.invoices') </span> @if($unreadInvoiceCount > 0) <div class="notify notification-color"><span class="heartbit"></span><span class="point"></span></div>@endif</a> </li>
            @endif

            @if(in_array('estimates',$modules))
                <li><a href="{{ route('client.estimates.index') }}" class="waves-effect"><i class="icon-doc"></i> <span class="hide-menu">@lang('app.menu.estimates') </span> @if($unreadEstimateCount > 0) <div class="notify notification-color"><span class="heartbit"></span><span class="point"></span></div>@endif</a> </li>
            @endif

            @if(in_array('payments',$modules))
                <li><a href="{{ route('client.payments.index') }}" class="waves-effect"><i class="fa fa-money"></i> <span class="hide-menu">@lang('app.menu.payments') </span> @if($unreadEstimateCount > 0) <div class="notify notification-color"><span class="heartbit"></span><span class="point"></span></div>@endif</a> </li>
            @endif

            @if(in_array('events',$modules))
                <li><a href="{{ route('client.events.index') }}" class="waves-effect"><i class="icon-calender"></i> <span class="hide-menu">@lang('app.menu.Events')</span></a> </li>
            @endif

            @if(in_array('notices',$modules))
                <li><a href="{{ route('client.notices.index') }}" class="waves-effect"><i class="ti-layout-media-overlay"></i> <span class="hide-menu">@lang("app.menu.noticeBoard") </span></a> </li>
            @endif

            @if(in_array('contracts',$modules))
                <li><a href="{{ route('client.contracts.index') }}" class="waves-effect"><i class="fa fa-file"></i> <span class="hide-menu">@lang('app.menu.contracts')</span></a> </li>
            @endif

            @if($gdpr->enable_gdpr)
                <li><a href="{{ route('client.gdpr.index') }}" class="waves-effect"><i class="icon-lock"></i> <span class="hide-menu">@lang('app.menu.gdpr')</span></a> </li>
            @endif

            @if(in_array('messages',$modules))
            @if($messageSetting->allow_client_admin == 'yes' || $messageSetting->allow_client_employee == 'yes')
            <li><a href="{{ route('client.user-chat.index') }}" class="waves-effect"><i class="icon-envelope"></i> <span class="hide-menu">@lang('app.menu.messages') @if($unreadMessageCount > 0)<span class="label label-rouded label-custom pull-right">{{ $unreadMessageCount }}</span> @endif</span></a> </li>
            @endif
            @endif

        </ul>
    </div>
</div>
