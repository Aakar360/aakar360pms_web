<?php
$exportarray = array();
                if($contractor){
                     $datesarray[] = '';
                    $exportarray[] = array(ucwords($contractor->name));
                    $curmonth = '10-'.$curmonth.'-'.$curyear;
                    $day = date('t',strtotime($curmonth));
                    $month = date('m',strtotime($curmonth));
                    $year = date('Y',strtotime($curmonth));
                    for($x=1;$x<=$day;$x++){
                    $datesarray[] = date('d M Y',mktime( 0, 0, 0, $month, $x, $year));
                    }
                $exportarray[] = $datesarray;
                $manpowercategoryarray = \App\ManpowerCategory::all();
                    if(!empty($manpowercategoryarray)){
                        foreach ($manpowercategoryarray as $manpowercategory){
                            $datesarray = array();
                            $datesarray[] = ucwords($manpowercategory->title);
                            for($x=1;$x<=$day;$x++){
                                $date = date('Y-m-d',mktime( 0, 0, 0, $month, $x, $year));
                                $mancount = \App\ManpowerLog::where('manpower_category',$manpowercategory->id)->where('work_date',$date);
                                if(!empty($contractor->id)){
                                    $mancount = $mancount->where('contractor',$contractor->id);
                                }
                                if(!empty($projectid)){
                                    $mancount = $mancount->where('project_id',$projectid);
                                }
                                if(!empty($subprojectid)){
                                    $mancount = $mancount->where('title_id',$subprojectid);
                                }
                                if(!empty($segmentid)){
                                    $mancount = $mancount->where('segment_id',$segmentid);
                                }
                                $datesarray[] = $mancount->count();
                            }
                            $exportarray[] = $datesarray;
                        }
                    }
                }
echo json_encode($exportarray);