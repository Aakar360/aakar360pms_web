<?php

namespace App\Http\Controllers\Api;

use App\AppProject;
use App\BoqCategory;
use App\Company;
use App\Employee;
use App\Helper\Reply;
use App\Http\Controllers\Controller;
use App\Indent;
use App\InvitedUser;
use App\ManpowerLog;
use App\Module;
use App\Package;
use App\Permission;
use App\PermissionRole;
use App\ProjectActivity;
use App\ProjectAttachmentDesigns;
use App\ProjectAttachmentFiles;
use App\ProjectCategory;
use App\ProjectCostItemsFinalQty;
use App\ProjectCostItemsPosition;
use App\ProjectCostItemsProduct;
use App\ProjectMember;
use App\ProjectMilestone;
use App\ProjectPermission;
use App\ProjectSegmentsPosition;
use App\ProjectSegmentsProduct;
use App\ProjectsLogs;
use App\ProjectTemplate;
use App\ProjectTimeLog;
use App\PunchItem;
use App\PunchItemFiles;
use App\Rfi;
use App\Role;
use App\Segment;
use App\Store;
use App\Task;
use App\Title;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\FileManager;
use App\Project;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use DB;
use Intervention\Image\Facades\Image;

class AppProjectController extends Controller
{
    use AuthenticatesUsers;

    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }

    protected function validatetToken($apikey, $token)
    {
        if (isset($apikey)) {
            if (!$this->validateAPI($apikey)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            $response = array();
            if (isset($token)) {
                if (!empty($token)) {
                    $user = User::where('appid', $token)->first();
                    if ($user === null) {
                        $response['message'] = 'Invalid token';
                        $response['status'] = 401;
                        return $response;
                    }
                    $response['user'] = $user;
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    return $response;
                }
                $response['message'] = 'Api Token cannot be empty';
                $response['status'] = 401;
                return $response;
            }
            $response['message'] = 'Api Token not sent';
            $response['status'] = 401;
            return $response;

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function __construct(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];

            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', $user->company_id)->first();

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function appProjectCategory(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projects = ProjectCategory::where('company_id', $user->company_id)->get();
                $response['status'] = 200;
                $response['message'] = 'Project List Fetched';
                $response['response'] = $projects;
                return $response;
            } catch (\Exception $e) {
                $logs = new AppLogs();
                $logs->status = 500;
                $logs->user_id = !empty($user->id) ?: 0;
                $logs->message = $e->getMessage();
                $logs->line = $e->getLine();
                $logs->file = $e->getFile();
                $logs->api_name = 'app-projects-category';
                $logs->medium = 'api';
                $logs->save();
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function projectDetailsCategory(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projects = ProjectCategory::where('company_id', $user->company_id)->where('id', $request->categoryid)->first();
                $response['status'] = 200;
                $response['message'] = 'Project Category Fetched';
                $response['response'] = $projects;
                return $response;
            } catch (\Exception $e) {
                $logs = new AppLogs();
                $logs->status = 500;
                $logs->user_id = !empty($user->id) ?: 0;
                $logs->message = $e->getMessage();
                $logs->line = $e->getLine();
                $logs->file = $e->getFile();
                $logs->api_name = 'projects-category-details';
                $logs->medium = 'api';
                $logs->save();
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function createProjectCategory(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $name = $request->name;
                $categoryid = $request->categoryid;
                if ($categoryid) {
                    $projectcategory = ProjectCategory::where('company_id', $user->company_id)->where('id', $categoryid)->first();
                } else {
                    $projectcategory = ProjectCategory::where('company_id', $user->company_id)->where('category_name', $name)->first();
                    if (empty($projectcategory->id)) {
                        $projectcategory = new ProjectCategory();
                        $projectcategory->company_id = $user->company_id;
                    }
                }
                $medium = !empty($request->medium) ? $request->medium : 'android';
                $projectcategory->category_name = $name;
                $projectcategory->save();
                $createlog = new ProjectsLogs();
                $createlog->company_id = $user->company_id;
                $createlog->added_id = $user->id;
                $createlog->module_id = $projectcategory->id;
                $createlog->module = 'project_category';
                $createlog->project_id = 0;
                $createlog->medium = $medium;
                $createlog->heading =  'Project Category Created';
                $createlog->description = 'Project Category log created by ' . $user->name;
                $createlog->save();

                $response['status'] = 200;
                $response['message'] = 'Project Category Fetched';
                $response['response'] = $projectcategory;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-projects-category',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteProjectCategory(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $manpowerarray = array_filter(explode(',', $request->categoryid));
                foreach ($manpowerarray as $manpower) {
                    $task = ProjectCategory::findOrFail($manpower);
                    $task->delete();
                    ProjectsLogs::where('module_id',$task->id)->where('module_id','project_category')->delete();
                }
                $response['status'] = 200;
                $response['message'] = 'Project Deleted Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-projects-category',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function appProjects(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectarray = explode(',',$request->project_id);
                /* $pm = ProjectMember::where('user_id',$user->id)->whereIn('project_id',$projectarray)->where('share_project','1')->get()->pluck('project_id')->toArray();
                $project = Project::where('company_id',$user->company_id)->get()->pluck('id')->toArray();
                 $prarray = array_filter(array_merge($project,$pm));*/
                $page = $request->page;
                if($page=='all'){
                    $data = AppProject::whereIn('id', $projectarray)->get();
                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    $data = AppProject::whereIn('id', $projectarray)->offset($skip)->take($count)->get();
                }
                $response['status'] = 200;
                $response['message'] = 'Project List Fetched';
                $response['response'] = $data;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'app-projects',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function appProjectsCompanies(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $proData = array();
                $companies = InvitedUser::where('user_id', $user->id)->get();
                $proData['company_count'] = count($companies);
                if (count($companies) > 1) {
                    foreach ($companies as $company) {
                        $proData['companies'] = Company::find($company->company_id);
                    }
                } else {
                    $proData['projects'] = AppProject::where('company_id', $user->company_id)->get();
                }
                $response['status'] = 200;
                $response['message'] = 'Project List Fetched';
                $response['response'] = $proData;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'app-projects-companies',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function projectDetails(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->projectid;
                if (!empty($projectid)) {
                    $projects = AppProject::where('company_id', $user->company_id)->where('id', $projectid)->first();
                    if (!empty($projects['id'])) {
                        $response['status'] = 200;
                        $response['message'] = 'Project List Fetched';
                        $response['response'] = $projects;
                        return $response;
                    }
                }
                $response['status'] = 300;
                $response['message'] = 'Project Not Found';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'projects-details',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function createProject(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->projectid;
                $memberExistsInTemplate = false;
                if (!empty($projectid)) {
                    $project = AppProject::find($projectid);
                } else {
                    $project = new AppProject();
                    $project->added_by = $user->id;
                    $project->project_admin = $user->id;
                    $project->company_id = $user->company_id;
                }
                $project->project_name = $request->project_name;
                if ($request->project_summary != '') {
                    $project->project_summary = $request->project_summary;
                }
                if ($request->start_date != '') {
                    $project->start_date = date('Y-m-d', strtotime($request->start_date));
                    /*$project->start_date = Carbon::createFromFormat($this->global->date_format, $request->start_date)->format('Y-m-d');*/
                }
                if (!$request->has('without_deadline')) {
                    if ($request->deadline != '') {
                        $project->deadline = date('Y-m-d', strtotime($request->deadline));
                        /*$project->deadline = Carbon::createFromFormat($this->global->date_format, $request->deadline)->format('Y-m-d');*/
                    }
                }
                if ($request->notes != '') {
                    $project->notes = $request->notes;
                }
                if ($request->category_id != '') {
                    $project->category_id = $request->category_id;
                }
                $project->client_id = $request->client_id;
                if ($request->client_view_task) {
                    $project->client_view_task = 'enable';
                } else {
                    $project->client_view_task = "disable";
                }
                if (($request->client_view_task) && ($request->client_task_notification)) {
                    $project->allow_client_notification = 'enable';
                } else {
                    $project->allow_client_notification = "disable";
                }
                if ($request->manual_timelog) {
                    $project->manual_timelog = 'enable';
                } else {
                    $project->manual_timelog = "disable";
                }
                $project->project_budget = $request->project_budget;
                $project->currency_id = $request->currency_id;
                $project->hours_allocated = $request->hours_allocated;
                $project->status = $request->status ?: 'not started';
                $project->save();
                if ($request->hasFile('file')) {
                    $fileData = $request->file;
                    $storage = storage();
                    $company = $user->company_id;
                    $filename = '';
                    switch ($storage) {
                        case 'local':
                            $destinationPath = 'uploads/project-files/'.$project->id;
                            if (!file_exists('public/'.$destinationPath)) {
                                mkdir('public/'.$destinationPath, 0777, true);
                            }
                            $fileData->storeAs($destinationPath, $fileData->hashName());
                            $filename = $fileData->hashName();
                            break;
                        case 's3':
                            Storage::disk('s3')->putFileAs('/project-files/' . $project->id, $fileData, $fileData->hashName(), 'public');
                            $filename = $fileData->hashName();
                            break;
                        case 'google':
                            $dir = '/';
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                            $dir = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', 'project-files')
                                ->first();
                            if (!$dir) {
                                Storage::cloud()->makeDirectory('project-files');
                            }
                            $directory = $dir['path'];
                            $recursive = false;
                            $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                            $directory = $contents->where('type', '=', 'dir')
                                ->where('filename', '=', $project->id)
                                ->first();

                            if (!$directory) {
                                Storage::cloud()->makeDirectory($dir['path'] . '/' . $project->id);
                                $contents = collect(Storage::cloud()->listContents($directory, $recursive));
                                $directory = $contents->where('type', '=', 'dir')
                                    ->where('filename', '=', $project->id)
                                    ->first();
                            }
                            Storage::cloud()->putFileAs($directory['basename'], $fileData, $fileData->hashName());
                            $filename = Storage::cloud()->url($directory['path'] . '/' . $fileData->hashName());
                            break;
                        case 'dropbox':
                            Storage::disk('dropbox')->putFileAs($company . '/project-files/' . $project->id, $fileData, $fileData->hashName());
                            $dropbox = new Client(['headers' => ['Authorization' => "Bearer " . config('filesystems.disks.dropbox.token'), "Content-Type" => "application/json"]]);
                            $res = $dropbox->request('POST', 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings',
                                [\GuzzleHttp\RequestOptions::JSON => ["path" => '/project-files/' . $project->id . '/' . $fileData->hashName()]]
                            );
                            $dropboxResult = $res->getBody();
                            $dropboxResult = json_decode($dropboxResult, true);
                            $filename = $dropboxResult['url'];
                            break;
                    }
                    $project->image = $filename;
                }
                $project->save();

                if (empty($projectid)) {
                    $store = new Store();
                    $store->company_name = $project->project_name;
                    $store->project_id = $project->id;
                    $store->added_by = $user->id;
                    $store->save();
                }

                $medium = !empty($request->medium) ? $request->medium : 'android';
                $createlog = new ProjectsLogs();
                $createlog->company_id = $user->company_id;
                $createlog->added_id = $user->id;
                $createlog->module_id = $project->id;
                $createlog->module = 'projects';
                $createlog->modulename = 'projects_created';
                $createlog->project_id = $project->id;
                $createlog->medium = $medium;
                if (!empty($projectid)) {
                    $createlog->heading =  'Project updated';
                    $createlog->description = 'Project updated by ' . $user->name;
                } else {
                    $createlog->heading =  'Project created';
                    $createlog->description = 'Project created by ' . $user->name;
                }
                $createlog->save();

                /*$company = Company::find($user->company_id);
                $package = Package::find($company->package_id);
                $mods = $package->module_in_package;
                $packageModules = Module::whereIn('module_name', (array)json_decode($mods))->get()->pluck('id');
                $permissions = Permission::whereIn('module_id',$packageModules)->get()->pluck('id');

                foreach ($permissions as $perm) {
                    $pprm = new ProjectPermission();
                    $pprm->project_id = $project->id;
                    $pprm->user_id = $user->id;
                    $pprm->permission_id = $perm;
                    $pprm->save();
                }*/

                $response['status'] = 200;
                $response['project_id'] = $project->id;
                if (!empty($projectid)) {
                    $response['message'] = 'Project updated successfully';
                } else {
                    $response['message'] = 'Project added successfully';
                }
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-projects',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteProject(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $errormsg = '';
                $projectid = $request->project_id;
                if (!empty($projectid)) {
                    $project = AppProject::find($projectid);
                    $id = $project->id;
                    if($project->added_by==$user->id){
                        $projectde = Title::where('project_id', $id)->first();
                        if (!empty($projectde->id)) {
                            $errormsg = 'Please delete Sub project first.';
                        }
                        $projectde = Segment::where('projectid', $id)->first();
                        if (!empty($projectde->id)) {
                            $errormsg = 'Please remove Segments first.';
                        }
                        $projectde = ProjectSegmentsPosition::where('project_id', $id)->first();
                        if (!empty($projectde->id)) {
                            $errormsg = 'Please remove Segments BOQ first.';
                        }
                        $projectde = ProjectSegmentsProduct::where('project_id', $id)->first();
                        if (!empty($projectde->id)) {
                            $errormsg = 'Please remove Segments BOQ first.';
                        }
                        $projectde = ProjectCostItemsProduct::where('project_id', $id)->first();
                        if (!empty($projectde->id)) {
                            $errormsg = 'Please remove BOQ first.';
                        }
                        $projectde = ProjectCostItemsPosition::where('project_id', $id)->first();
                        if (!empty($projectde->id)) {
                            $errormsg = 'Please remove BOQ first.';
                        }
                        $projectde = ManpowerLog::where('project_id', $id)->first();
                        if (!empty($projectde->id)) {
                            $errormsg = 'Please remove Manpower log first.';
                        }
                        $projectde = PunchItem::where('projectid', $id)->first();
                        if (!empty($projectde->id)) {
                            $errormsg = 'Please remove Issues  first.';
                        }
                        $projectde = Indent::where('project_id', $id)->first();
                        if (!empty($projectde->id)) {
                            $errormsg = 'Please remove Indents  first.';
                        }
                        $projectde = ProjectMember::where('project_id', $id)->first();
                        if (!empty($projectde->id)) {
                            $errormsg = 'Please remove assigned members first.';
                        }
                        $projectde = FileManager::where('project_id', $id)->where('parent', '<>','0')->first();
                        if (!empty($projectde->id)) {
                            $errormsg = 'Please remove attached files.';
                        }
                        if(!empty($errormsg)){
                            $response['status'] = 300;
                            $response['message'] = $errormsg;
                            return $response;
                        }else{
                            $project = AppProject::where('id',$id)->where('added_by',$user->id)->first();
                            if($project->added_by==$user->id){
                                FileManager::where('project_id',$id)->delete();
                                $project->delete();

                            }
                            ProjectsLogs::where('module_id',$project->id)->where('module_id','projects')->delete();
                        }
                    }else{
                        ProjectPermission::where('project_id',$id)->where('user_id',$user->id)->delete();
                        ProjectMember::where('project_id',$id)->where('user_id',$user->id)->delete();
                    }
                    $response['status'] = 200;
                    $response['message'] = 'Project Deleted Successfully';
                    return $response;
                }
                $response['status'] = 300;
                $response['message'] = 'Please try again';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-projects',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function leaveProjects(Request $request){
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $projectarray = explode(',',$request->projectids);
                foreach ($projectarray as $project){
                    ProjectPermission::where('project_id',$project)->where('user_id',$user->id)->delete();
                    ProjectMember::where('project_id',$project)->where('user_id',$user->id)->delete();
                }
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'leave-projects',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function subProjectsList(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $projectid = $request->project_id;
                $response = array();
                $projects = Title::where('project_id', $projectid)->get()->toArray();
                $response['status'] = 200;
                $response['message'] = 'Sub project List Fetched';
                $response['response'] = $projects;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'sub-projects-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function createSubProjects(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] == 301) {
                return response()->json(['error' => $validate['message']], 301);
            }
            $user = $validate['user'];
            try{
                $projectid = $request->project_id;
                $title = $request->title;
                $subprojectid = $request->subproject_id;
                $response = array();
                if (!empty($subprojectid)) {
                    $projects = Title::find($subprojectid);
                } else {
                    $projects = new Title();
                }
                $projects->project_id = $projectid;
                $projects->title = $title;
                $projects->save();

                $medium = !empty($request->medium) ? $request->medium : 'android';
                $createlog = new ProjectsLogs();
                $createlog->company_id = $user->company_id;
                $createlog->added_id = $user->id;
                $createlog->module_id = $projects->id;
                $createlog->module = 'title';
                $createlog->project_id = $projects->project_id;
                $createlog->subproject_id = $projects->id;
                $createlog->medium = $medium;
                $createlog->heading =  'Sub project created';
                $createlog->description = 'Sub Project created by ' . $user->name;
                $createlog->save();

                $response['status'] = 200;
                $response['message'] = 'Sub project Updated Successfully';
                $response['response'] = $projects;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-sub-projects',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteSubProjects(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectarray = array_filter(explode(',', $request->subprojectid));
                foreach ($projectarray as $projectid) {
                    if (!empty($projectid)) {
                        $projectde = Title::where('id', $projectid)->first();
                        $projectde->delete();
                        ProjectsLogs::where('module_id',$projectde->id)->where('module_id','title')->delete();
                        $response['status'] = 200;
                        $response['message'] = 'Sub Project Deleted Successfully';
                        return $response;
                    }
                }
                $response['status'] = 300;
                $response['message'] = 'Please try again';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-sub-projects',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function segmentsList(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $projectid = $request->project_id;
                $subprojectid = $request->subproject_id;
                $response = array();
                $projects = Segment::where('projectid', $projectid);
                if ($subprojectid) {
                    $projects = $projects->where('titleid', $subprojectid);
                }
                $projects = $projects->get()->toArray();
                $response['status'] = 200;
                $response['message'] = 'Segments List Fetched';
                $response['response'] = $projects;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'segments-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function createSegments(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $projectid = $request->project_id;
                $title = $request->title;
                $subprojectid = $request->subproject_id;
                $segmentid = $request->segment_id;
                $response = array();
                if (!empty($segmentid)) {
                    $projects = Segment::find($segmentid);
                } else {
                    $projects = new Segment();
                }
                $projects->company_id = $user->company_id;
                $projects->projectid = $projectid;
                $projects->titleid = $subprojectid;
                $projects->name = $title;
                $projects->save();
                $medium = !empty($request->medium) ? $request->medium : 'android';
                $createlog = new ProjectsLogs();
                $createlog->company_id = $user->company_id;
                $createlog->added_id = $user->id;
                $createlog->module_id = $projects->id;
                $createlog->module = 'segment';
                $createlog->project_id = $projects->projectid;
                $createlog->subproject_id = $projects->titleid;
                $createlog->segment_id = $projects->id;
                $createlog->heading =  'Segment created';
                $createlog->medium =  $medium;
                $createlog->description = 'Segment created by ' . $user->name;
                $createlog->save();

                $response['status'] = 200;
                $response['message'] = 'Segments Updated Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-segments',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteSegments(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectarray = array_filter(explode(',', $request->segment_id));
                foreach ($projectarray as $projectid) {
                    if (!empty($projectid)) {
                        $projectde = Segment::where('id', $projectid)->first();
                        $projectde->delete();

                        ProjectsLogs::where('module_id',$projectde->id)->where('module_id','segment')->delete();
                        $response['status'] = 200;
                        $response['message'] = 'Segments Deleted Successfully';
                        return $response;
                    }
                }
                $response['status'] = 300;
                $response['message'] = 'Please try again';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-segments',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function getProject(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $projectid = $request->project_id;
                $subprojectid = $request->subproject_id;
                $parent = $request->parent ?: 0;
                $workid = $request->workid;
                $response = array();
                $projectsarray = array();
                $subprojectsarray = Title::where('project_id', $projectid);
                $segmentsarray = Segment::where('projectid', $projectid);
                $workssarray = ProjectCostItemsPosition::select('id', 'itemid', 'itemname', 'parent', 'catlevel')->where('position', 'row')->where('project_id', $projectid)->where('parent', $parent);
                $tasksarray = Task::where('project_id', $projectid);
                if (!empty($subprojectid)) {
                    $segmentsarray = $segmentsarray->where('titleid', $subprojectid);
                    $tasksarray = $tasksarray->where('title', $subprojectid);
                    $workssarray = $workssarray->where('title', $subprojectid);
                }
                if ($workid) {
                    $tasksarray = $tasksarray->where('task_category_id', $workid);
                }
                $projectsarray['subprojectsarray'] = $subprojectsarray->get()->toArray();
                $projectsarray['segmentsarray'] = $segmentsarray->get()->toArray();
                $projectsarray['tasksarray'] = $tasksarray->get()->toArray();
                $projectsarray['worksarray'] = $workssarray->get()->toArray();
                $response['status'] = 200;
                $response['message'] = 'Sub project List Fetched';
                $response['response'] = $projectsarray;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'get-project',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function getEmployeeProject(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $employeeid = $request->employee_id;
                $userid = $request->user_id;
                $employee = Employee::where('id',$employeeid)->where('user_id',$userid)->first();
                if(empty($employee->id)){
                    $response['message'] = 'Employee Not found';
                    $response['status'] = 300;
                    return $response;
                }
                $susers = User::where('id',$employee->user_id)->first();
                if(empty($susers->id)){
                    $response['message'] = 'User Not found';
                    $response['status'] = 300;
                    return $response;
                }
                $project = AppProject::where('company_id',$user->company_id)->get()->pluck('id')->toArray();
               $pm = ProjectMember::where('user_id',$user->id)->where('share_project',1)->groupBy('project_id')->get()->pluck('project_id')->toArray();
               $proarray = array_filter(array_unique(array_merge($project,$pm)));
                $page = $request->page;
                $response = array();
                if($page=='all'){
                    $projectsarray = AppProject::whereIn('id',$proarray)->get();
                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    $projectsarray = AppProject::whereIn('id',$proarray)->offset($skip)->take($count)->get();
                }
                $prarray = array();
                foreach ($projectsarray as $projects){
                    $addedtoproject = 0;
                    $sharetoproject = 0;
                    $checkpermission = 0;
                    $level = 0;
                    $prmembers = ProjectMember::where('project_id',$projects->id)->where('employee_id',$employee->id)->where('user_id',$employee->user_id)->first();
                    if(!empty($prmembers->id)){
                        $addedtoproject = 1;
                        if($prmembers->share_project=='1'){
                            $sharetoproject = 1;
                        }
                        if($prmembers->check_permission=='1'){
                            $checkpermission = 1;
                        }
                        $level = $prmembers->level;
                    }
                    $pro = array();
                    $pro['id'] = $projects->id;
                    $pro['name'] = $projects->project_name;
                    $pro['image'] = $projects->imageurl;
                    $pro['addedtoproject'] = $addedtoproject;
                    $pro['sharetoproject'] = $sharetoproject;
                    $pro['checkpermission'] = $checkpermission;
                    $pro['level'] = $level;
                    $pro['project_added_by'] = $projects->added_by;
                    $prarray[] = $pro;
                }
                $response['status'] = 200;
                $response['message'] = 'Project List Fetched';
                $response['response'] = $prarray;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'get-employee-projects',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function getProjectsEmployee(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $employeeid = $request->employee_id;
                $userid = $request->user_id;
                $employee = Employee::where('id',$employeeid)->where('user_id',$userid)->first();
                if(empty($employee->id)){
                    $response['message'] = 'Employee Not found';
                    $response['status'] = 300;
                    return $response;
                }
                $susers = User::where('id',$employee->user_id)->first();
                if(empty($susers->id)){
                    $response['message'] = 'User Not found';
                    $response['status'] = 300;
                    return $response;
                }

                $prmemarray = ProjectMember::where('user_id',$employee->user_id)->where('employee_id',$employee->id)->groupBy('project_id')->get()->pluck('project_id')->toArray();
                if(empty($prmemarray)){
                    $response['response'] = array();
                    $response['message'] = 'Projects Not found';
                    $response['status'] = 201;
                    return $response;
                }
                $pername = $request->permission_name;
                $permissions = Permission::where('name',$pername)->first();
                if(empty($permissions->id)){
                    $response['message'] = 'Invalid Permission name';
                    $response['status'] = 300;
                    return $response;
                }
                $prarray = array();
                foreach ($prmemarray as $prm){
                    $projectdetails = AppProject::find($prm);
                    if(!empty($projectdetails->id)){
                        if($projectdetails->added_by==$user->id){
                            $prarray[] = $projectdetails->id;
                        }else{
                          $prmisin =   ProjectPermission::where('user_id',$user->id)->where('permission_id',$permissions->id)->where('project_id',$projectdetails->id)->first();
                            if(!empty($prmisin->id)){
                                $prarray[] = $projectdetails->id;
                            }
                        }
                    }
                }
                $page = $request->page;
                $response = array();
                if($page=='all'){
                    $projectsarray = AppProject::whereIn('id', $prarray)->get();
                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    $projectsarray = AppProject::whereIn('id', $prarray)->offset($skip)->take($count)->get();
                }
                $prarray = array();
                foreach ($projectsarray as $projects){
                    $addedtoproject = 0;
                    $sharetoproject = 0;
                    $checkpermission = 0;
                    $level = 0;
                    $prmembers = ProjectMember::where('project_id',$projects->id)->where('employee_id',$employee->id)->where('user_id',$employee->user_id)->first();
                    if(!empty($prmembers->id)){
                        $addedtoproject = 1;
                        if($prmembers->share_project=='1'){
                            $sharetoproject = 1;
                        }
                        if($prmembers->check_permission=='1'){
                            $checkpermission = 1;
                        }
                        $level = $prmembers->level;
                    }
                    $pro = array();
                    $pro['id'] = $projects->id;
                    $pro['name'] = $projects->project_name;
                    $pro['image'] = $projects->imageurl;
                    $pro['addedtoproject'] = $addedtoproject;
                    $pro['sharetoproject'] = $sharetoproject;
                    $pro['checkpermission'] = $checkpermission;
                    $pro['level'] = $level;
                    $pro['project_added_by'] = $projects->added_by;
                    $prarray[] = $pro;
                }
                $response['status'] = 200;
                $response['message'] = 'Project List Fetched';
                $response['response'] = $prarray;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'get-projects-by-employee',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function timeLineLogs(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectselectarray = explode(',',$request->project_id);
                $subprojectid = $request->subproject_id;
                $segmentid = $request->segment_id;
                $page = $request->page;
                $count = pagecount();
                $skip = 0;
                if($page){
                    $skip = $page*$count;
                }
                if(!empty($projectselectarray)){
                    $prarray = $projectselectarray;
                }else{
                    $project = AppProject::where('company_id',$user->company_id)->get()->pluck('id')->toArray();
                    $pm = ProjectMember::where('user_id',$user->id)->where('share_project','1')->get()->pluck('project_id')->toArray();
                    $prarray = array_filter(array_merge($project,$pm));
                }

                $projectsarray = ProjectsLogs::whereIn('project_id',$prarray);
                if (!empty($subprojectid)) {
                    $projectsarray = $projectsarray->where('subproject_id', $subprojectid);
                }
                if (!empty($segmentid)) {
                    $projectsarray = $projectsarray->where('segment_id', $segmentid);
                }
                $dontsohw = array('projects','project_category');
                $projectsarray = $projectsarray->whereNotIn('module',$dontsohw);
                $projectsarray = $projectsarray->offset($skip)->take($count)->orderBy('id','desc')->get();
                $response['status'] = 200;
                $response['message'] = 'Timeline logs List Fetched';
                $response['response'] = $projectsarray;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'timeline-logs',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
    public function mentionLogs(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projectid = $request->project_id;
                $subprojectid = $request->subproject_id;
                $segmentid = $request->segment_id;
                $page = $request->page;
                $count = pagecount();
                $skip = 0;
                if($page){
                    $skip = $page*$count;
                }
                $projectsarray = ProjectsLogs::whereNotNull('mentionusers')->whereRaw('FIND_IN_SET(?,mentionusers)', [$user->ID]);
                if (!empty($subprojectid)) {
                    $projectsarray = $projectsarray->where('subproject_id', $subprojectid);
                }
                if (!empty($segmentid)) {
                    $projectsarray = $projectsarray->where('segment_id', $segmentid);
                }
                $dontsohw = array('projects','project_category');
                $projectsarray = $projectsarray->whereNotIn('module',$dontsohw);
                $projectsarray = $projectsarray->offset($skip)->take($count)->orderBy('id','desc')->get();
                $response['status'] = 200;
                $response['message'] = 'Timeline logs List Fetched';
                $response['response'] = $projectsarray;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'timeline-logs',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function projectStatusList(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $statusarray = array();
                $statusarray[] = array('id'=>1,'slug'=>'not started','name'=>'Not Started');
                $statusarray[] = array('id'=>2,'slug'=>'in progress','name'=>'In Progress');
                $statusarray[] = array('id'=>3,'slug'=>'in problem','name'=>'In Problem');
                $statusarray[] = array('id'=>4,'slug'=>'on hold','name'=>'On Hold');
                $statusarray[] = array('id'=>5,'slug'=>'canceled','name'=>'Canceled');
                $statusarray[] = array('id'=>6,'slug'=>'completed','name'=>'Completed');
                $response = array();
                $response['status'] = 200;
                $response['responselist'] = $statusarray;
                $response['message'] = 'Status list Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'project-status-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function getProjects(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $pm = ProjectMember::where('user_id',$request->user_id)->where('share_project','1')->get()->pluck('project_id')->toArray();
                $data = AppProject::WhereIn('id', $pm)->get();
                $response['status'] = 200;
                $response['message'] = 'Project List Fetched';
                $response['response'] = $data;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'get-projects',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function launchProjectLists(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{

                $response = array();
                $project = AppProject::where('company_id',$user->company_id)->get()->pluck('id')->toArray();
                $pm = ProjectMember::where('user_id',$user->id)->where('share_project',1)->get()->pluck('project_id')->toArray();
                $prarray = array_filter(array_merge($project,$pm));
                $page = $request->page;
                $response = array();
                if($page=='all'){

                    $data = AppProject::whereIn('id', $prarray)->get();
                }else{
                    $count = pagecount();
                    $skip = 0;
                    if($page){
                        $skip = $page*$count;
                    }
                    $data = AppProject::whereIn('id', $prarray)->offset($skip)->take($count)->get();
                }

                $response['status'] = 200;
                $response['pagecount'] = pagecount();
                $response['message'] = 'Project List Fetched';
                $response['response'] = $data;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'launch-projects-list',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
}