<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectCostItemBaselineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_cost_item_baseline', function (Blueprint $table) {
            $table->increments('id');
            $table->string('project_id')->default(0);
            $table->string('title')->default(0);
            $table->string('label')->default('');
            $table->string('color')->default('');
            $table->longText('cost_item_product')->default('');
            $table->longText('dates')->default('');
            $table->string('status')->default('');
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_cost_item_baseline');

    }
}
