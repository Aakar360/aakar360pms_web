<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('company_id');
            $table->integer('added_id');
            $table->string('module_id');
            $table->string('module');
            $table->string('category_id')->default(0)->nullable();
            $table->string('category_module')->default(0)->nullable();
            $table->integer('project_id');
            $table->integer('subproject_id')->default(0)->nullable();
            $table->integer('segment_id')->default(0)->nullable();
            $table->text('description');
            $table->string('medium')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects_logs');
    }
}
