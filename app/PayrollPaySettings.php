<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollPaySettings extends Model
{
    protected $fillable = [
        'effective_date',
        'pay_frequncy',
        'pay_structure',
        'pay_cycle',
        'input_cycle',
        'payout_date',
        'doj_date',
        'include_weekly_leave',
        'include_hollyday_leave'
    ];
}
