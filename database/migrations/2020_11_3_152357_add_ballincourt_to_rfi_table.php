<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBallincourtToRfiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rfi', function (Blueprint $table) {
            $table->string('ballincourt')->nullable()->after('rfi_manager');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rfi', function (Blueprint $table) {
            $table->dropIfExists('ballincourt');
        });
    }
}
