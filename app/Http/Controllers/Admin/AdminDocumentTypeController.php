<?php

namespace App\Http\Controllers\Admin;

use App\ClientContact;
use App\ContractType;
use App\DocumentType;
use App\Helper\Reply;
use App\Http\Requests\Admin\DocumentType\StoreRequest;
use App\Http\Requests\Admin\DocumentType\UpdateRequest;
use App\User;
use Yajra\DataTables\Facades\DataTables;

class AdminDocumentTypeController extends AdminBaseController
{
    public function __construct() {
        parent::__construct();
        $this->pageIcon = 'user-follow';
        $this->pageTitle = 'Document Type';
        $this->activeMenu = 'hr';
//        $this->middleware(function ($request, $next) {
//            if(!in_array('contracts',$this->user->modules)){
//                abort(403);
//            }
//            return $next($request);
//        });

    }

    public function index() {
        $this->pageIcon = '';
        $this->pageTitle = __('Documents Type');
        return view('admin.document-type.index', $this->data);
    }

    public function data() {
        $contractType = DocumentType::all();

        return DataTables::of($contractType)
            ->addColumn('action', function($row){
                return '<a href="javascript:;" class="btn btn-info btn-circle edit-contact"
                      data-toggle="tooltip" onclick="editDocumentType('.$row->id.')"  data-original-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                    <a href="javascript:;" class="btn btn-danger btn-circle sa-params"
                      data-toggle="tooltip" data-document-id="'.$row->id.'" data-original-title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
            })
            ->editColumn('name', function($row){
                return ucwords($row->name);
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function create() {

        return view('admin.document-type.create-modal');
    }
    public function store(StoreRequest $request) {
        $contract = new DocumentType();
        $contract->name = $request->name;
        $contract->company_id = $this->user->company_id;
        $contract->save();

        return Reply::success(__('Document type added'));
    }

    public function edit($id) {
        $this->contract = DocumentType::findOrFail($id);
        return view('admin.document-type.edit', $this->data);
    }

    public function update(UpdateRequest $request, $id) {
        $contract = DocumentType::findOrFail($id);
        $contract->name = $request->name;
        $contract->save();

        return Reply::success(__('Document type updated'));
    }

    public function destroy($id) {
        DocumentType::destroy($id);
        $contractTypeData = DocumentType::all();
        return Reply::successWithData(__('Document type deleted'),['data' => $contractTypeData]);
    }

    public function download($id)
    {

    }
}
