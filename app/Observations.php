<?php

namespace App;

//use App\Observers\ProjectCategoryObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Observations extends Model
{
    protected $table = 'observations';

    protected static function boot()
    {
        parent::boot();

    }
}
