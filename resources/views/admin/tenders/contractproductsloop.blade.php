<?php
if(!empty($awardedcontract)){
    function costitemsloop($boqarray){
        $awardedcontract = $boqarray['awardedcontract'];
        $level = $boqarray['level'];
        $catitem = $boqarray['catitem'];
        $unitsarray = $boqarray['unitsarray'];
        $costitemsarray = $boqarray['costitemsarray'];
        $categoryarray = $boqarray['categoryarray'];
        $projectid = $boqarray['projectid'];
        $subprojectid = $boqarray['subprojectid'];

$level1categoryarray =  \App\AwardContractCategory::where('awarded_contract_id',$awardedcontract->id)->where('level',$level)->orderBy('inc','asc')->get();
if($level1categoryarray){
foreach ($level1categoryarray as $level1category){
    if(empty($catitem)){
        $catitem = $level1category->category;
        }else{
        $catitem = ','.$level1category->category;
        }
?>
<tr class="item-row">
    <td><a href="javascript:void(0);"  class="red remove-item-category" data-catitem="{{ $catitem }}" data-catrow="{{ $catitem }}" data-toggle="tooltip"  data-original-title="Delete Category" ><i class="fa fa-trash"></i></a></td>
    <td><i class="fa fa-plus-circle"></i></td>
    <td></td>
    <td>{{ get_category($level1category->category) }}</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<?php  $tenderproductsarray  = \App\AwardContractProducts::where('awarded_contract_id',$awardedcontract->id)->where('category',$catitem)->orderBy('inc','asc')->get();
foreach ($tenderproductsarray as $tenderproducts){
?>
<tr class="item-row" id="rowitem{{ $tenderproducts->id }}">
    <td><a href="javascript:void(0);"  class="red remove-item" data-rowid="{{ $tenderproducts->id }}" data-toggle="tooltip"  data-original-title="Delete" ><i class="fa fa-trash"></i> </a></td>
    <td><i class="fa fa-plus-circle"></i></td>
    <td></td>
    <td>
        <input  class="cell-inp" list="costitemlist{{ $tenderproducts->id }}"  value="{{ get_cost_name($tenderproducts->cost_items) }}">
        <datalist  id="costitemlist{{ $tenderproducts->id }}">
            <?php    foreach($costitemsarray as $costitem =>$value){
            $costitemname = get_cost_name($value); ?>
            <option value="{{ $costitem }}"  >{{ $costitemname }}</option>
            <?php   } ?>
        </datalist>
    </td>
    <td>
        <input  class="cell-inp unitvalue{{ $tenderproducts->id }}" list="unitdatacat{{ $tenderproducts->id }}"  onchange="calmarkup('{{ $tenderproducts->id }}')" value="{{ get_unit_name($tenderproducts->unit) }}">
        <datalist id="unitdatacat{{ $tenderproducts->id }}">
           <?php foreach($unitsarray as $units){ ?>
            <option  value="{{ $units->id }}" >{{ $units->name }}</option>
            <?php   } ?>
        </datalist>
    </td>
    <td><input type="text" class="cell-inp qty{{ $tenderproducts->id }}" onchange="calmarkup('{{ $tenderproducts->id }}')" value="{{ $tenderproducts->qty }}"></td>
    <td><input type="text" class="cell-inp ratevalue{{ $tenderproducts->id }}"  onchange="calmarkup('{{ $tenderproducts->id }}')" value="{{ $tenderproducts->rate }}"></td>
    <td><input type="text" class="cell-inp finalrate{{ $tenderproducts->id }}" name="finalrate[{{ $tenderproducts->id }}]"  onchange="calmarkup('{{ $tenderproducts->id }}')" value="{{ $tenderproducts->finalrate }}"></td>

</tr>
<?php }?>
<tr>
    <td></td>
    <td></td>
    <td></td>
    <td>
        <input  class="cell-inp costitemrow" data-cat="{{ $catitem }}" list="costitemlist" placeholder="@lang('app.task')">
        <datalist class="costitemslist"  id="costitemlist">
            <?php  if(!empty($costitemsarray)){
            foreach ($costitemsarray as $key => $value) {
            $costitemname = get_cost_name($value); ?>
            <option value="{{ $key }}" data-name="{{ $costitemname }}" >{{ $costitemname }}</option>
            <?php     } }?>
        </datalist>
    </td>
    <td><input type="text" class="cell-inp"></td>
    <td>
        <input  class="cell-inp " list="unitdatacat" >
        <datalist id="unitdatacat">
            <?php foreach($unitsarray as $units){ ?>
            <option data-value="{{ $units->id }}" >{{ $units->name }}</option>
            <?php }?>
        </datalist>
    </td>
    <td><input type="text" class="cell-inp"></td>
    <td><input type="text" class="cell-inp"></td>
</tr>
<?php
$boqarray = array();
$boqarray['awardedcontract'] = $awardedcontract;
$boqarray['unitsarray'] = $unitsarray;
$boqarray['categoryarray'] = $categoryarray;
$boqarray['costitemsarray'] = $costitemsarray;
$boqarray['level'] = $level+1;
$boqarray['catitem'] = $catitem;
$boqarray['projectid'] = $projectid;
$boqarray['subprojectid'] = $subprojectid;
echo costitemsloop($boqarray);
?>
<?php }?>
<?php }?>

<tr>
    <td></td>
    <td></td>
    <td><i class="fa fa-plus-circle"></i></td>
    <td>
        <input  class="cell-inp costitemcatrow" data-parent="{{ $catitem }}" data-level="{{ $level }}"  list="costitemcatlist" placeholder="@lang('app.task') Category">
        <datalist class="costitemscatlist"  id="costitemcatlist">
            <?php  if(!empty($categoryarray)){
            foreach ($categoryarray as $colums) { ?>
            <option value="{{ $colums->id }} " data-name="{{ $colums->title }}" >{{ $colums->title }}</option>
            <?php     } }?>

        </datalist>
    </td>
    <td><input type="text" class="cell-inp"></td>
    <td><input type="text" class="cell-inp"></td>
    <td><input type="text" class="cell-inp"></td>
    <td><input type="text" class="cell-inp"></td>
</tr>
<?php }    ?>
    <?php
    $boqarray = array();
    $boqarray['awardedcontract'] = $awardedcontract;
    $boqarray['unitsarray'] = $unitsarray;
    $boqarray['categoryarray'] = $categoryarray;
    $boqarray['costitemsarray'] = $costitemsarray;
    $boqarray['level'] = 1;
    $boqarray['catitem'] = '';
    $boqarray['projectid'] = $projectid;
    $boqarray['subprojectid'] = $subprojectid;
    echo costitemsloop($boqarray);
    ?>
<?php }?>