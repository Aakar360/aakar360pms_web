<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChannelsMembers extends Model
{
    protected $table = 'channels_members';
}