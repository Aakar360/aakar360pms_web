<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeEducationDetails extends Model
{
    protected $fillable = ['company_id','employee_id','user_id','qualification_type','course_name','course_type','stream','course_start_date','course_end_date','college_name','univercity_name'];
}
