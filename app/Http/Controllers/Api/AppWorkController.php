<?php

namespace App\Http\Controllers\Api;

use App\AppProject;
use App\BoqCategory;
use App\Company;
use App\CostItems;
use App\Helper\Files;
use App\Helper\Reply;
use App\Http\Controllers\Controller;
use App\Notifications\NewClientTask;
use App\Notifications\NewTask;
use App\ProjectActivity;
use App\ProjectAttachmentDesigns;
use App\ProjectAttachmentFiles;
use App\ProjectCostItemsFinalQty;
use App\ProjectCostItemsPosition;
use App\ProjectCostItemsProduct;
use App\ProjectMember;
use App\ProjectMilestone;
use App\ProjectSegmentsPosition;
use App\ProjectsLogs;
use App\ProjectTemplate;
use App\ProjectTimeLog;
use App\Rfi;
use App\Task;
use App\TaskboardColumn;
use App\TaskFile;
use App\Title;
use App\Traits\ProjectProgress;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\FileManager;
use App\Project;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use DB;

class AppWorkController extends Controller
{
    protected function validateAPI($key)
    {
        $result = DB::table('oauth_clients')->where(DB::raw('BINARY `secret`'), '=', $key)->where('personal_access_client', 1)->count();
        if ($result) {
            return true;
        }
        return false;
    }
    protected function validatetToken($apikey, $token)
    {
        if (isset($apikey)) {
            if (!$this->validateAPI($apikey)) {
                $response['message'] = 'Unauthorised';
                $response['status'] = 401;
                return $response;
            }
            $response = array();
            if (isset($token)) {
                if (!empty($token)) {
                    $user = User::where('appid', $token)->first();
                    if ($user === null) {
                        $response['message'] = 'Invalid token';
                        $response['status'] = 401;
                        return $response;
                    }
                    $response['user'] = $user;
                    $response['message'] = 'success';
                    $response['status'] = 200;
                    return $response;
                }
                $response['message'] = 'Api Token cannot be empty';
                $response['status'] = 401;
                return $response;
            }
            $response['message'] = 'Api Token not sent';
            $response['status'] = 401;
            return $response;

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function __construct(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];

            $this->global = $this->company = Company::with('currency', 'package')->withoutGlobalScope('active')->where('id', $user->company_id)->first();

        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function appWorks(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $companyid = $user->company_id;
                $projects = BoqCategory::where('company_id', $companyid)->get()->toArray();
                $response['status'] = 200;
                $response['message'] = 'Activity List Fetched';
                $response['response'] = $projects;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'app-works',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function createWork(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $id = $request->id;
                $workid = $request->workid;
                $projectid = $request->project_id;
                $subprojectid = $request->subproject_id ?: 0;
                $segmentid = $request->segment_id ?: 0;
                if(!empty($workid)){
                    $work = BoqCategory::find($workid);
                    if(!empty($segmentid)){
                        $columsarray = ProjectSegmentsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('segment',$segmentid)->where('id',$id)->first();
                     }else{
                        $columsarray = ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('id',$id)->first();
                     }
                }else{
                    $work = new BoqCategory();
                    if(!empty($segmentid)){
                        $maxpositionarray = \App\ProjectSegmentsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('segment',$segmentid)->where('position','row')->where('level',$request->level)->orderBy('inc','desc')->first();
                        $newid = !empty($maxpositionarray->inc) ? $maxpositionarray->inc+1 : 1;

                        $columsarray = new ProjectSegmentsPosition();
                        $columsarray->segment = $segmentid ?: 0;
                        $columsarray->inc = $newid;
                        $columsarray->level = $request->level ?: 0;
                    }else{
                        $maxpositionarray = \App\ProjectCostItemsPosition::where('project_id',$projectid)->where('title',$subprojectid)->where('position','row')->where('level',$request->level)->orderBy('inc','desc')->first();
                        $newid = !empty($maxpositionarray->inc) ? $maxpositionarray->inc+1 : 1;

                        $columsarray = new ProjectCostItemsPosition();
                        $columsarray->inc = $newid;
                        $columsarray->level = $request->level ?: 0;
                    }
                    $columsarray->project_id = $projectid;
                    $columsarray->title = $subprojectid;
                }
                $work->company_id = $user->company_id;
                $work->title = $request->title;
                $work->parent = $request->parent ?: 0;
                $work->save();

                $columsarray->position = 'row';
                $columsarray->itemid = $work->id;
                $columsarray->itemname = $work->title;
                $columsarray->itemslug = filter_string($work->title);
                $columsarray->collock = 0;
                $columsarray->parent = $request->parent ?: 0;
                $columsarray->catlevel = $request->catlevel;
                $columsarray->save();

                $medium = $request->medium ?: 'android';
                $createlog = new ProjectsLogs();
                $createlog->company_id = $user->company_id;
                $createlog->added_id = $user->id;
                $createlog->module_id = $columsarray->id;
                $createlog->module = 'boq_categories';
                $createlog->project_id = $projectid;
                $createlog->subproject_id = $subprojectid;
                $createlog->segment_id = $segmentid;
                $createlog->heading =  $work->title;
                if(empty($workid)) {
                    $createlog->modulename = 'activity_created';
                    $createlog->description = $work->title . ' activity created by ' . $user->name . ' for the project ' . get_project_name($projectid);
                }else{
                    $createlog->modulename = 'activity_updated';
                    $createlog->description = $work->title . ' activity updated by ' . $user->name . ' for the project ' . get_project_name($projectid);
                }
                $createlog->medium = $medium;
                $createlog->save();

                $response['status'] = 200;
                $response['message'] = 'Activity Added Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'create-works',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function deleteWork(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] == 401) {
                return response()->json(['error' => $validate['message']], 401);
            }
            $user = $validate['user'];
            try{
                $response = array();
                $id = $request->id;
                $workid = $request->workid;
                 $positiondetails  =  ProjectCostItemsPosition::where('id', $id)->first();
                $manpower = ProjectCostItemsPosition::where('project_id',$positiondetails->project_id)->where('title',$positiondetails->title)->where('parent',$workid)->first();
                if(!empty($manpower->id)){
                    $response['message'] = 'Please remove sub activity';
                    $response['status'] = 300;
                    return $response;
                }
                $costproductsarray = ProjectCostItemsProduct::where('project_id',$positiondetails->project_id)->where('title',$positiondetails->title)->where('position_id', $id)->pluck('id')->toArray();
                if(!empty($costproductsarray)){
                    $tasksarray = Task::whereIn('cost_item_id',$costproductsarray)->first();
                    if(!empty($tasksarray->id)){
                        $response['message'] = 'Please remove task';
                        $response['status'] = 300;
                        return $response;
                    }
                }
                $task = BoqCategory::findOrFail($workid);
                ProjectCostItemsPosition::where('id', $id)->delete();
                ProjectCostItemsProduct::where('position_id', $id)->delete();
                ProjectsLogs::where('module_id',$id)->where('module','boq_categories')->delete();
                $task->delete();

                $response['status'] = 200;
                $response['message'] = 'Activity removed Successfully';
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'delete-works',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }

    public function appCostitems(Request $request)
    {
        if (isset($request->API_KEY) && isset($request->token)) {
            $validate = $this->validatetToken($request->API_KEY, $request->token);
            if ($validate['status'] != 200) {
                $response['message'] = $validate['message'];
                $response['status'] = $validate['status'];
                return $response;
            }
            $user = $validate['user'];
            try{
                $response = array();
                $projects = CostItems::get()->toArray();
                $response['status'] = 200;
                $response['message'] = 'Works List Fetched';
                $response['response'] = $projects;
                return $response;
            } catch (\Exception $e) {
                $userid = !empty($user->id) ? $user->id : 0;
                app_log($e,'app-costitems',$userid);
                $response['status'] = 500;
                $response['message'] = $e->getMessage();
                $response['line'] = $e->getLine();
                $response['file'] = $e->getFile();
                return $response;
            }
        }
        $response['message'] = 'Unauthorised';
        $response['status'] = 401;
        return $response;
    }
}