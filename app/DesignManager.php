<?php

namespace App;

use App\Observers\DesignManagerObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class DesignManager extends Model
{
    protected $table = 'design_manager';
    protected static function boot()
    {
        parent::boot();

        static::observe(DesignManagerObserver::class);

        $company = company();

        static::addGlobalScope('company', function (Builder $builder) use($company) {
            if ($company) {
                $builder->where('design_manager.company_id', '=', $company->id);
            }
        });
    }
}
