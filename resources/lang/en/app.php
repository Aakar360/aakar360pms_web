<?php

return array (
    'loginAsEmployee' => 'Login As Employee',
    'loginAsAdmin' => 'Login As Admin',
    'title' => 'Title',
    'task' => 'Task',
    'units' => 'Units',
    'boqOption' => 'Boq Option',
    'note' => 'Note',
    'save' => 'Save',
    'update' => 'Update',
    'back' => 'Back',
    'reply'=>'Reply',
  'reset' => 'Reset',
  'addNew' => 'Add New',
  'edit' => 'Edit',
  'id' => 'Id',
  'invite' => 'Invite',
  'invite_again' => 'Invite Again',
  'username' => 'Username',
  'name' => 'Name',
  'email' => 'Email',
  'phone' => 'Phone',
  'mobile' => 'Mobile',
  'gender' => 'Gender',
  'password' => 'Password',
  'landline' => 'Landline',
  'createdAt' => 'Created At',
  'action' => 'Action',
  'search' => 'Search...',
  'markRead' => 'Mark as Read',
  'deadline' => 'Deadline',
  'project' => 'Project',
  'subproject' => 'Sub Project',
  'segment' => 'Segment',
  'completion' => 'Completion',
  'address' => 'Address',
  'completed' => 'Completed',
  'hideCompletedTasks' => 'Hide Completed Tasks',
  'dueDate' => 'Due Date',
  'status' => 'Status',
  'client' => 'Client',
  'supplier' => 'Supplier',
  'contractor' => 'Contractor',
  'contractors' => 'Contractors',
  'details' => 'Details',
  'stop' => 'Stop',
  'remove' => 'Remove',
  'description' => 'Description',
  'incomplete' => 'Incomplete',
  'invoice' => 'Invoice',
  'date' => 'Date',
  'selectDateRange' => 'Select Date Range',
  'selectProject' => 'Select Project',
  'apply' => 'Apply',
  'filterResults' => 'Filter Results',
  'selectImage' => 'Select Image',
  'change' => 'Change',
  'adminPanel' => 'Admin Panel',
  'employeePanel' => 'Employee Panel',
  'clientPanel' => 'Client Panel',
  'logout' => 'Logout',
  'active' => 'Active',
  'clientName' => 'Client Name',
  'lead' => 'Lead',
  'paymentOn' => 'Payment On',
  'amount' => 'Amount',
  'gateway' => 'Gateway',
  'transactionId' => 'Transaction Id',
  'timeLog' => 'Daily Log ',
  'selectTask' => 'Select Task',
  'category' => 'Category',
  'panel' => 'Panel',
  'projectTemplate' => 'Project Template',
  'selectStatus' => 'Select Status',
  'store' => 'Store',
  'projects' => 'Project',
  'subprojects' => 'Sub Project',
  'remarks' => 'Remarks',
  'approve' => 'Approve',
  'approval' => 'Approval',
  'projectName' => 'Project Name',
    'manager'=>'Manager',
    'type'=>'Types',
    'projectboq'=>'Project BOQ',
    'percentage'=>'Percentage',
    'quantity'=>'Quantity',
    'totalquantity'=>'Total Quantity',
    'exportpdf'=>'Export PDF',
    'activity'=>'Activity',
    'notify'=>'Notify',
    'invited_by'=>'Invited By',
    'invitation'=>'Invitation',
    'users'=>'Users',
    'inspection'=>'Inspection',
    'inspectiontype'=>'Inspection Type',
    'addedby'=>'Added By',
    'rfi'=>'RFI',
    'files'=>'Files',
    'location'=>'Location',
    'duedate'=>'Due date',
    'inspectionname'=>'Inspection name',
    'tenders'=>'Tenders',
    'distribution'=>'Distribution',
    'menu' =>
        array (
            'pms' => 'Project Management',
            'hr' => 'Members',
            'store' => 'Store',
            'assets' => 'Assets',
            'maintenance' => 'Maintenance',
            'more' => 'More...',
            'home' => 'Home',
            'dashboard' => 'Dashboard',
            'masters' => 'Masters',
            'suppliers' => 'Suppliers',
            'contractors' => 'Contractors',
            'combo' => 'Combo Sheet',
            'clients' => 'Clients',
            'stores' => 'Stores',
            'indent' => 'Indent',
            'rfq' => 'RFQ',
            'quotes' => 'Quotations',
            'po' => 'Purchase Orders',
            'inventory' => 'Inventory',
            'resources' => 'Resources',
            'makePayment' => 'Make Payment',
            'inward' => 'Product Inward',
            'stock' => 'Stock',
            'pinvoices' => 'Purchase Invoices',
            'return' => 'Product Return',
            'product-issue' => 'Product Issue',
            'purchaseHistory' => 'Purchase History',
            'employees' => 'Employees',
            'projects' => 'Projects',
            'taskCalendar' => 'Task Calendar',
            'invoices' => 'Invoices',
            'issues' => 'Issues',
            'timeLogs' => 'Daily Logs',
            'tasks' => 'Tasks',
            'noticeBoard' => 'Notice Board',
            'stickyNotes' => 'Sticky Notes',
            'reports' => 'Reports',
            'taskReport' => 'Task Report',
            'timeLogReport' => 'Daily Log Report',
            'financeReport' => 'Finance Report',
            'settings' => 'Settings',
            'accountSettings' => 'Company Settings',
            'profileSettings' => 'Profile Settings',
            'emailSettings' => 'Email Settings',
            'mailConfig' => 'Mail Config',
            'moduleSettings' => 'Module Settings',
            'currencySettings' => 'Currency Settings',
            'contacts' => 'Contacts',
            'messages' => 'Messages',
            'themeSettings' => 'Theme Settings',
            'estimates' => 'Estimates',
            'paymentGatewayCredential' => 'Payment Credentials',
            'payments' => 'Payments',
            'expenses' => 'Expenses',
            'incomeVsExpenseReport' => 'Income Vs Expense Report',
            'invoiceSettings' => 'Invoice Settings',
            'slackSettings' => 'Slack Settings',
            'updates' => 'Update Log',
            'ticketSettings' => 'Ticket Settings',
            'ticketAgents' => 'Ticket Agents',
            'ticketTypes' => 'Ticket Types',
            'ticketChannel' => 'Ticket Channel',
            'replyTemplates' => 'Reply Templates',
            'tickets' => 'Tickets',
            'attendanceSettings' => 'Attendance Settings',
            'attendance' => 'Attendance',
            'finance' => 'Finance',
            'customFields' => 'Custom Fields',
            'Events' => 'Events',
            'rolesPermission' => 'Roles & Permissions',
            'leaves' => 'Leaves',
            'messageSettings' => 'Message Settings',
            'storageSettings' => 'Storage Settings',
            'leaveSettings' => 'Leaves Settings',
            'employeeList' => 'Employee List',
            'teams' => 'Teams',
            'leaveReport' => 'Leave Report',
            'lead' => 'Leads',
            'leadSource' => 'Lead Source',
            'leadStatus' => 'Lead Status',
            'products' => 'Products',
            'holiday' => 'Holiday',
            'offlinePaymentMethod' => 'Offline Payment Method',
            'onlinePayment' => 'Online Payment Credential',
            'method' => 'Method',
            'timeLogSettings' => 'Daily Log Settings',
            'projectTemplate' => 'Project Template',
            'projectTemplateTask' => 'Project Template Task',
            'projectTemplateMember' => 'Project Template Member',
            'addProjectTemplate' => 'Project Templates',
            'template' => 'Template',
            'leadFiles' => 'Lead Files',
            'pushNotifications' => 'Push Notifications',
            'notificationSettings' => 'Notification Settings',
            'viewArchive' => 'View Archive',
            'clientModule' => 'Client Module Setting',
            'employeeModule' => 'Employee Module Setting',
            'adminModule' => 'Admin Module Setting',
            'employeeDocs' => 'Employee Documents',
            'pushNotificationSetting' => 'Push Notification Setting',
            'companies' => 'Companies',
            'packages' => 'Packages',
            'billing' => 'Billing',
            'features' => 'FEATURES',
            'pricing' => 'PRICING',
            'contact' => 'CONTACT',
            'credit-note' => 'Credit Note',
            'financeSettings' => 'Finance Settings',
            'accountSetup' => 'Account Setup',
            'attendanceReport' => 'Attendance Report',
            'projectSettings' => 'Project Settings',
            'faq' => 'FAQ',
            'faqCategory' => 'FAQ Category',
            'designation' => 'Designation',
            'contractType' => 'Contract Type',
            'contract' => 'Contract',
            'taskFiles' => 'Task Files',
            'contracts' => 'Contracts',
            'gdpr' => 'GDPR',
            'files' => 'Manage Files',
            'documents' => 'Documents',
            'punchItems' => 'Punch Items',
            'issue' => 'Issue',
            'timeline' => 'Timeline',
            'manpowerlogs' => 'Man power logs',
            'markAttendance'=>'Mark Attendance',
            'singleAttendance'=>'Single Attendance',
            'posAttendance'=>'POS Attendance',
            'groupAttendace'=>'Group Attendance',
            'salarySlip'=>'Salary Slip',
            'myleaves'=>'My Leaves',
            'allowance'=>'Allowance',
            'overtime'=>'Over Time',
            'allowancelessdiduction'=>'Allowance Less Diduction',
            'project-members'=>'Project Members',
            'milestones'=>'Milestones',
            'projects-milestone'=>'Project Milestones',
            'projects-task'=>'Project Tasks',
            'outline'=>'Outline',
            'drawings'=>'Drawings',
            'dailylogs'=>'Daily logs',
            'burndown'=>'Burn Down',
            'boqtemplate'=>'BOQ Template',
            'boq'=>'BOQ',
            'schedule'=>'Schedule',
            'sourcing-packages'=>'Sourcing package',
            'payroll'=>'Payroll',
            'rulesAttendace'=>'Attendance Rules',
            'salarystucture'=>'Salary Structure',
            'taxscheme'=>'Tax Scheme',
            'hra'=>'HRA',
            'deduction'=>'Deduction',
            'incomeandloss'=>'Income/Loss From House Property',
            'lta'=>'LTA',
            'incomeprevious'=>'Income From Previous Year',
            'forms'=>'Forms',
            'grn'=>'GRN',
            'direct-grn'=>'Direct GRN',
            'grn-against-po'=>'GRN Against PO',
            'tenders'=>'Tenders',
            'selecttenders' => 'Select Tenders',
            'stores-projects'=>'Project Stores',
            'stores-stores'=>'Store Create',
            'overview'=>'Overview',
            'leave'=>'Leave',
            'salaryrevision'=>'Salary Revision',
            'variableandadhoc'=>'Variable & Adhoc',
            'salaryonhold'=>'Salary On Hold',
            'workweak'=>'Work Weak',
            'assignworkweak'=>'Assign Work Weak',
            'createnewworkweak'=>'Create New Work Weak',
            'taxoverride'=>'Tax Override',
            'meetings'=>'Meetings',
            'ratesheet'=>'Rate Sheet',
            'mailconfig'=>'Mail Config',
            'manpowerLogs'=>'Man Power logs',
            'weatherLogs'=>'Weather logs',
            'material'=>'Material',
            'segments'=>'Segments',
            'manpowerReport'=>'Man Power Report',
            'progressReport'=>'Progress Report',
            'role'=>'Roles & Permissions',
            'runpayroll'=>'Preview and Run Payroll',
            'variable'=>'Variable',
            'adhoc'=>'Adhoc',
            'leavesandholiday'=>'Leaves & Holiday',
            'salarycomponent'=>'Salary Component',
            'salary'=>'Salary',
            'sub_projects'=>'Sub Projects',
            'segment'=>'Segment',
            'members'=>'Members',
            'employee'=>'Employee',
            'client'=>'Client',
            'photos'=>'Photos',
            'pulse'=>'Pulse',
            'awardedcontracts'=>'Awarded contracts',
            'awardtenders'=>'Award Tenders',
            'channels'=>'Channels',
            'users'=>'Users',
            ),
    'exportExcel' => 'Export To Excel',
    'importExcel' => 'Import To Excel',
    'datatable' => '//cdn.datatables.net/plug-ins/1.10.15/i18n/English.json',
    'role' => 'User Role',
    'projectAdminPanel' => 'Project Admin Panel',
    'loginAsProjectAdmin' => 'Login As Project Admin',
    'pending' => 'Pending',
    'price' => 'Price',
    'duration' => 'Duration',
    'last' => 'Last',
    'month' => 'Month',
    'select' => 'Select',
    'income' => 'Income',
    'expense' => 'Expense',
    'total' => 'Total',
    'days' => 'Days',
    'year' => 'Year',
    'yes' => 'Yes',
    'no' => 'No',
    'day' => 'Day',
    'week' => 'Week',
    'submit' => 'Submit',
    'required' => 'Required',
    'filterBy' => 'Filter by',
    'others' => 'Others',
    'delete' => 'Delete',
    'close' => 'Close',
    'value' => 'Value',
    'add' => 'Add',
    'view' => 'View',
    'accept' => 'Accept',
    'reject' => 'Reject',
    'to' => 'To',
    'monday' => 'Monday',
    'tuesday' => 'Tuesday',
    'wednesday' => 'Wednesday',
    'thursday' => 'Thursday',
    'friday' => 'Friday',
    'saturday' => 'Saturday',
    'sunday' => 'Sunday',
    'newNotifications' => 'New notifications',
    'from' => 'From',
    'cancel' => 'Cancel',
    'january' => 'January',
    'february' => 'February',
    'march' => 'March',
    'april' => 'April',
    'may' => 'May',
    'june' => 'June',
    'july' => 'July',
    'august' => 'August',
    'september' => 'September',
    'october' => 'October',
    'november' => 'November',
    'december' => 'December',
    'complete' => 'Complete',
    'low' => 'Low',
    'Manage role' => 'Manage role',
    'Search:' => 'Search:',
    'mo' => 'Mo',
    'tu' => 'Tu',
    'we' => 'We',
    'th' => 'Th',
    'fr' => 'Fr',
    'sa' => 'Sa',
    'su' => 'Su',
    'approved' => 'Approved',
    'selectFile' => 'Select File',
    'login' => 'Log In',
    'rememberMe' => 'Remember Me',
    'forgotPassword' => 'Forgot Password',
    'welcome' => 'Welcome',
    'admin' => 'Admin',
    'employee' => 'Employee',
    'forbiddenError' => 'Forbidden Error',
    'noPermission' => 'You do not have permission to access this.',
    'recoverPassword' => 'Recover Password',
    'sendPasswordLink' => 'Send Reset Link',
    'enterEmailInstruction' => 'Enter your Email and instructions will be sent to you!',
    'medium' => 'Medium',
    'high' => 'High',
    'urgent' => 'Urgent',
    'open' => 'Open',
    'resolved' => 'Resolved',
    'optional' => 'Optional',
    'sampleFile' => 'Sample File',
    'male' => 'male',
    'female' => 'female',
    'language' => 'Language',
    'team' => 'Team',
    'manage' => 'Manage',
    'deactive' => 'Deactive',
    'upcoming' => 'Upcoming',
    'source' => 'Source',
    'next_follow_up' => 'Next Follow Up',
    'followUp' => 'Follow Up',
    'createdOn' => 'Created On',
    'remark' => 'Remark',
    'notice' => 'Note',
    'minutes' => 'Minutes',
    'onLeave' => 'On Leave',
    'inclusiveAllTaxes' => 'Inclusive All Taxes',
    'reason' => 'Reason',
    'enable' => 'Enable',
    'disable' => 'Disable',
    'progress' => 'Progress',
    'all' => 'All',
    'paid' => 'Paid',
    'unpaid' => 'Unpaid',
    'partial' => 'Partial',
    'startDate' => 'Start Date',
    'inactive' => 'Inactive',
    'global' => 'Global',
    'superAdminPanel' => 'Super Admin Panel',
    'endDate' => 'End Date',
    'earnings' => 'Earnings',
    'watchTutorial' => 'Watch Tutorial',
    'skills' => 'Skills',
    'latitude' => 'Latitude',
    'longitude' => 'Longitude',
    'front' => 'Front',
    'image' => 'Image',
    'icon' => 'Icon',
    'featureWithImage' => 'Feature With Image',
    'featureWithIcon' => 'Feature With Icon',
    'gstNumber' => 'GST Number',
    'showGst' => 'Show GST Number',
    'gstIn' => 'GSTIN',
    'module' => 'Module',
    'package' => 'Package',
    'mobileNumber' => 'Mobile Number',
    'superAdmin' => 'Super Admin',
    'new' => 'New',
    'password' => 'Password',
    'freeTrial' => 'Free Trial',
    'max' => 'Max',
    'annual' => 'Annual',
    'monthly' => 'Monthly',
    'webhook' => 'Webhook URL',
    'searchResults' => 'Search Results',
    'estimate' => 'Estimate',
    'leave_type' => 'Leave Type',
    'tableView' => 'Table View',
    'calendarView' => 'Calendar View',
    'disableCache' => 'Disable Cache',
    'refreshCache' => 'Refresh Cache',
    'enableCache' => 'Enable Cache',
    'credit-note' => 'Credit Note',
    'company_name' => 'Company Name',
    'invoiceNumber' => 'Invoice Number',
    'purchasePackage' => 'Purchase Package',
    'list' => 'List',
    'thumbnail' => 'Thumbnail',
    'zero' => 'Zero',
    'company' => 'Company',
    'loading' => 'Loading...',
    'changes' => 'changes',
    'viewInvoice' => 'View Invoice',
    'viewPayments' => 'View Payments',
    'billedTo' => 'Billed To',
    'thanks' => 'Thanks',
    'terms' => 'Terms',
    'congratulations' => 'Congratulations',
    'paperlessOffice' => 'You have taken the first step to create a paperless office',
    'info' => 'Info',
    'copied' => 'copied',
    'signup' => 'Sign Up',
    'summary' => 'Summary',
    'language_code' => 'Language Code',
    'enabled' => 'Enabled',
    'disabled' => 'Disabled',
    'faqCategory' => 'FAQ Category',
    'department' => 'Department',
    'designation' => 'Designation',
    'slug' => 'Slug',
    'yourEmailAddress' => 'Your Email Address',
    'confirmPassword' => 'Confirm Password',
    'timelogs' => 'Daily Logs',
    'clients' => 'Clients',
    'client' => 'Client',
    'employees' => 'Employees',
    'lastActivity' => 'Last Activity',
    'inProgress' => 'In Progress',
    'onHold' => 'On Hold',
    'notStarted' => 'Not Started',
    'canceled' => 'Canceled',
    'finished' => 'Finished',
    'decline' => 'Decline',
    'sign' => 'Sign',
    'signed' => 'Signed',
    'contracts' =>
        array (
            'description' => 'Description',
        ),
    'content' => 'Content',
    'download' => 'Download',
    'subject' => 'Subject',
    'renew' => 'Renew',
    'applyToInvoice' => 'Apply To Invoice',
    'credit-notes' =>
        array (
            'invoiceDate' => 'Invoice Date',
            'invoiceAmount' => 'Invoice Amount',
            'invoiceBalanceDue' => 'Invoice Balance Due',
            'amountToCredit' => 'Amount To Credit',
            'remainingAmount' => 'Remaining Amount',
            'amountCredited' => 'Amount Credited',
        ),
    'creditedInvoices' => 'Invoices Credited',
    'appliedCredits' => 'Applied Credits',
    'upload' => 'Upload',
    'paymentReminder' => 'Payment Reminder',
    'leadAgent' => 'Lead Agent',
    'customers' => 'Customers',
    'rejected' => 'Rejected',
    'purchaseAllow' => 'Purchase Allow',
    'allowed' => 'Allowed',
    'notAllowed' => 'Not Allowed',
    'purchase' => 'Purchase',
    'purchaseDate' => 'Purchase Date',
    'product' => 'Product',
    'offlineRequest' => 'Offline Request',
    'file' => 'File',
    'updateContactDetails' => 'Update Contact Details',
    'verify' => 'Verify',
    'mark' => 'Mark',
    'copy' => 'Copy',
    'client_name' => 'Client Name',
    'snapshotImage'=>'Take Snapshot',
        'updateCostitem' => 'Update Tender Task',
        'number' => 'Number',
    'manpowercategory' => 'Man Power Category'
);
