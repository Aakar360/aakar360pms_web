<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title">Upload Document</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">
        <form method="post" id="createBoqCategory" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-body">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Select Document Type</label>
                        <select name="document_type" id="parent" class="form-control">
                            <option value="">Select Document Type</option>
                            @foreach($types as $type)
                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="form-group">
                        <label>Document</label>
                        <input type="file" name="file" id="image" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="save-category" class="btn btn-success"> <i class="fa fa-check"></i> @lang('app.save')</button>
        </div>
        </form>
    </div>
</div>

<script>
    $('#save-category').click(function () {
        $.easyAjax({
            url: '{{route('admin.employees.uploadId',[$id])}}',
            container: '#createBoqCategory',
            type: "POST",
            file: (document.getElementById("image").files.length == 0) ? false : true,
            data: $('#createBoqCategory').serialize(),
            success: function (response) {
                if(response.status == 'success'){
                    window.location.reload();
                }
            }
        })
    });
</script>