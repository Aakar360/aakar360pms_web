<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFcmIntoUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('users', 'gender')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('gender');
            });
        }
        if (Schema::hasColumn('users', 'status')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('status');
            });
        }
        if (Schema::hasColumn('users', 'login')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('login');
            });
        }
        if (Schema::hasColumn('users', 'super_admin')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('super_admin');
            });
        }
        Schema::table('users', function (Blueprint $table) {
            $table->string('gender')->default('male');
            $table->string('status')->default('active');
            $table->string('login')->default('enable');
            $table->string('super_admin')->default('0');
            $table->string('fcm')->default(0)->after('otp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('gender');
            $table->dropColumn('status');
            $table->dropColumn('login');
            $table->dropColumn('super_admin');
            $table->dropColumn('fcm');
        });
    }
}
